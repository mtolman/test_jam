#include <test-jam-cxx/gens.h>
#include <emscripten/bind.h>

using namespace emscripten;
namespace tg = test_jam::gens;

namespace world {
    struct country {
        std::string name;
        std::string genc;
        std::string iso2;
        std::string iso3;
        std::string isoNum;
        std::string stanag;
        std::string tld;
    };

    country countries(TEST_JAM_SEED_TYPE seed) {
        printf("C++ Seed: %d\t", (int)seed);
        const auto v = test_jam::gens::world::countries().create(seed);
        return {
            v.name,
            v.genc.value_or(""),
            v.iso2.value_or(""),
            v.iso3.value_or(""),
            v.isoNum.value_or(""),
            v.stanag.value_or(""),
            v.tld.value_or("")
        };
    }
}

namespace ascii {
    namespace a = tg::ascii;
    auto string(TEST_JAM_SEED_TYPE seed, const std::string &charSet, size_t minLen, size_t maxLen) -> std::string {
        if (charSet.empty()) {
            return string(seed, a::any.chars, minLen, maxLen);
        }
        return a::str({
            .charSet={charSet},
            .minLen=minLen,
            .maxLen=maxLen
        }).create(seed);
    }

    auto any() { return a::any.chars; }
    auto alpha() { return a::alpha.chars; }
    auto alpha_lower() { return a::alpha_lower.chars; }
    auto alpha_upper() { return a::alpha_upper.chars; }
    auto alpha_numeric() { return a::alpha_numeric.chars; }
    auto alpha_numeric_lower() { return a::alpha_numeric_lower.chars; }
    auto alpha_numeric_upper() { return a::alpha_numeric_upper.chars; }
    auto binary() { return a::binary.chars; }
    auto bracket() { return a::bracket.chars; }
    auto control() { return a::control.chars; }
    auto domain_piece() { return a::domain_piece.chars; }
    auto hex() { return a::hex.chars; }
    auto hex_lower() { return a::hex_lower.chars; }
    auto hex_upper() { return a::hex_upper.chars; }
    auto html_named() { return a::html_named.chars; }
    auto math() { return a::math.chars; }
    auto numeric() { return a::numeric.chars; }
    auto octal() { return a::octal.chars; }
    auto printable() { return a::printable.chars; }
    auto quote() { return a::quote.chars; }
    auto safe_punctuation() { return a::safe_punctuation.chars; }
    auto sentence_punctuation() { return a::sentence_punctuation.chars; }
    auto symbol() { return a::symbol.chars; }
    auto text() { return a::text.chars; }
    auto url_unencoded_chars() { return a::url_unencoded_chars.chars; }
    auto url_unencoded_hash_chars() { return a::url_unencoded_hash_chars.chars; }
    auto whitespace() { return a::whitespace.chars; }
}

namespace numbers {
    namespace n = tg::numbers;

    auto uint32(TEST_JAM_SEED_TYPE seed, uint32_t min, uint32_t max) { return n::uint32({min, max}).create(seed); }
    auto uint16(TEST_JAM_SEED_TYPE seed, uint16_t min, uint16_t max) { return n::uint16({min, max}).create(seed); }
    auto uint8(TEST_JAM_SEED_TYPE seed, uint8_t min, uint8_t max) { return n::uint8({min, max}).create(seed); }

    auto int32(TEST_JAM_SEED_TYPE seed, int32_t min, int32_t max) { return n::int32({min, max}).create(seed); }
    auto int16(TEST_JAM_SEED_TYPE seed, int16_t min, int16_t max) { return n::int16({min, max}).create(seed); }
    auto int8(TEST_JAM_SEED_TYPE seed, int8_t min, int8_t max) { return n::int8({min, max}).create(seed); }

    auto f64(TEST_JAM_SEED_TYPE seed, double min, double max, bool nan, bool inf) {
        return n::f64({
            .min=min,
            .max=max,
            .allowNan=nan,
            .allowInf=inf
        }).create(seed);
    }
    auto f32(TEST_JAM_SEED_TYPE seed, float min, float max, bool nan, bool inf) { return n::f32({min, max, nan, inf}).create(seed); }
}

namespace aviation {
    namespace av = tg::aviation;
    auto aircraft_type(TEST_JAM_SEED_TYPE seed) { return av::aircraft_types().create(seed); }
    auto airlines(TEST_JAM_SEED_TYPE seed) { return av::airlines().create(seed); }
    auto airplanes(TEST_JAM_SEED_TYPE seed) { return av::airplanes().create(seed); }
    auto airports(TEST_JAM_SEED_TYPE seed) { return av::airports().create(seed); }
}

namespace color {
    namespace c = tg::color;
    auto named(TEST_JAM_SEED_TYPE seed) { return c::named_colors().create(seed); }
    auto rgb(TEST_JAM_SEED_TYPE seed) { return c::rgb_colors().create(seed); }
    auto rgba(TEST_JAM_SEED_TYPE seed) { return c::rgba_colors().create(seed); }
    auto hsl(TEST_JAM_SEED_TYPE seed) { return c::hsl_colors().create(seed); }
    auto hsla(TEST_JAM_SEED_TYPE seed) { return c::hsla_colors().create(seed); }
}

namespace computer {
    namespace c = tg::computer;
    auto cmake_system(TEST_JAM_SEED_TYPE seed) { return c::cmake_system_names().create(seed); }
    auto cpu_arch(TEST_JAM_SEED_TYPE seed) { return c::cpu_architectures().create(seed); }
    auto file_ext(TEST_JAM_SEED_TYPE seed) { return c::file_extensions().create(seed); }
    auto mime_map(TEST_JAM_SEED_TYPE seed) { return c::file_mime_mappings().create(seed); }
    auto file_type(TEST_JAM_SEED_TYPE seed) { return c::file_types().create(seed); }
    auto mime_type(TEST_JAM_SEED_TYPE seed) { return c::mime_types().create(seed); }
    auto os(TEST_JAM_SEED_TYPE seed) { return c::operating_systems().create(seed); }
}

namespace financial {
    namespace f = tg::financial;
    auto accounts(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) { return f::account_names(r).create(seed); }

    struct cc_net {
        std::string id;
        std::string name;
    };

    auto cc_network(TEST_JAM_SEED_TYPE seed) {
        const auto n = f::credit_card_networks().create(seed);
        return cc_net {
            .id = n.id,
            .name = n.name
        };
    }

    struct cc {
        std::string network_id;
        std::string network_name;

        std::string number;
        std::string cvv;
        uint32_t exp_year;
        uint16_t exp_month;
    };
    auto cc_card(TEST_JAM_SEED_TYPE seed, uint32_t minExpYear, uint16_t minExpMonth, uint32_t maxExpYear, uint16_t maxExpMonth) {
        const auto c = f::credit_cards({
            .minExpDate={.year=minExpYear, .month=minExpMonth},
            .maxExpDate={.year=maxExpYear, .month=maxExpMonth},
        }).create(seed);
        return cc {
            .network_id=c.network.id,
            .network_name=c.network.name,
            .number=c.number,
            .cvv=c.cvv,
            .exp_year=c.expirationDate.year,
            .exp_month=c.expirationDate.month
        };
    }

    auto account_number(TEST_JAM_SEED_TYPE seed, uint32_t length) { return f::account_numbers(length).create(seed); }

    auto amount(TEST_JAM_SEED_TYPE seed, double max, double min, unsigned decimalPlaces, const std::string& separator, const std::string& symbol, bool prefix) {
        return f::amounts(f::amount_options{
            .max=max,
            .min=min,
            .decimalPlaces=decimalPlaces,
            .decimalSeparator=separator,
            .symbol=symbol,
            .prefixSymbol=prefix,
        }).create(seed);
    }

    auto bic(TEST_JAM_SEED_TYPE seed, bool includeBranch) { return f::bics(includeBranch).create(seed); }

    auto masked_num(TEST_JAM_SEED_TYPE seed, uint32_t unMaskedLen, uint32_t maskedLen, char maskChar) {
        return f::masked_numbers({
            .unMaskedLen = unMaskedLen,
            .maskedLen = maskedLen,
            .maskedChar = maskChar
        }).create(seed);
    }
}

namespace internet {
    namespace web = tg::internet;
    auto tld(TEST_JAM_SEED_TYPE seed) { return web::top_level_domains().create(seed); }

    auto ipv4(TEST_JAM_SEED_TYPE seed) { return web::ipv4_addresses().create(seed); }

    auto ipv6(TEST_JAM_SEED_TYPE seed, unsigned flags) {
        return web::ipv6_addresses(flags).create(seed);
    }

    auto domain(TEST_JAM_SEED_TYPE seed, uint32_t minLen, uint32_t maxLen) {
        return web::domains({.minLen = minLen, .maxLen = maxLen}).create(seed);
    }

    auto url_default_schema(
            TEST_JAM_SEED_TYPE seed,
            uint32_t minLen,
            uint32_t maxLen,
            unsigned flags,
            uint32_t minDomainLen,
            uint32_t maxDomainLen
    ) {
        return web::urls({
                                 .flags = flags,
                                 .minLen = minLen,
                                 .maxLen = maxLen,
                                 .domainOptions = {
                                         .minLen = minDomainLen,
                                         .maxLen = maxDomainLen
                                 }
                         }).create(seed);
    }

    auto url(
            TEST_JAM_SEED_TYPE seed,
            uint32_t minLen,
            uint32_t maxLen,
            unsigned flags,
            const std::vector<std::string>& possibleSchemas,
            uint32_t minDomainLen,
            uint32_t maxDomainLen
    ) {
        return web::urls({
            .flags = flags,
            .minLen = minLen,
            .maxLen = maxLen,
            .possibleSchemas = possibleSchemas,
            .domainOptions = {
                    .minLen = minDomainLen,
                    .maxLen = maxDomainLen
            }
        }).create(seed);
    }

    auto email(
            TEST_JAM_SEED_TYPE seed,
            uint32_t minLen,
            uint32_t maxLen,
            unsigned flags,
            uint32_t minDomainLen,
            uint32_t maxDomainLen
    ) {
        return web::emails({
            .flags = flags,
            .minLen = minLen,
            .maxLen = maxLen,
            .domainOptions = {
                    .minLen = minDomainLen,
                    .maxLen = maxDomainLen
            }
        }).create(seed);
    }
}

namespace malicious {
    namespace m = tg::malicious;

    auto sql_injection(TEST_JAM_SEED_TYPE seed) { return m::sql_injection().create(seed); }

    auto xss(TEST_JAM_SEED_TYPE seed) { return m::xss().create(seed); }

    auto format_injection(TEST_JAM_SEED_TYPE seed) { return m::format_injection().create(seed); }

    auto edge_cases(TEST_JAM_SEED_TYPE seed) { return m::edge_cases().create(seed); }
}

namespace person {
    namespace p = tg::person;

    struct person_t {
        tg::person::Sex sex;
        std::string full;
        std::string given;
        std::string sur;
        std::string prefix;
        std::string suffix;
    };

    auto person(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) {
        const auto v = p::person(r).create(seed);
        return person_t{
            .sex=v.sex,
            .full=v.fullName,
            .given=v.givenName,
            .sur=v.surname,
            .prefix=v.prefix.value_or(""),
            .suffix=v.suffix.value_or("")
        };
    }

    auto full(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) {
        return p::full_name(r).create(seed);
    }

    auto given(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) {
        return p::given_name(r).create(seed);
    }

    auto sur(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) {
        return p::surname(r).create(seed);
    }

    auto prefix(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) {
        return p::prefix(r).create(seed).value_or("");
    }

    auto suffix(TEST_JAM_SEED_TYPE seed, tg::common::complexity_range r) {
        return p::suffix(r).create(seed).value_or("");
    }

    auto sex(TEST_JAM_SEED_TYPE seed) {
        return p::sex().create(seed);
    }
}

EMSCRIPTEN_BINDINGS(TestJam) {
    register_vector<std::string>("StdStringVector");

    function("person", person::person);
    function("person_name_full", person::full);
    function("person_name_given", person::given);
    function("person_name_sur", person::sur);
    function("person_name_prefix", person::prefix);
    function("person_name_suffix", person::suffix);
    function("person_sex", person::sex);

    class_<person::person_t>("Person")
            .property("sex", &person::person_t::sex)
            .property("full", &person::person_t::full)
            .property("given", &person::person_t::given)
            .property("sur", &person::person_t::sur)
            .property("prefix", &person::person_t::prefix)
            .property("suffix", &person::person_t::suffix);

    enum_<tg::person::Sex>("PersonSex")
            .value("MALE", tg::person::Sex::MALE)
            .value("FEMALE", tg::person::Sex::FEMALE);

    function("sql_inject", &malicious::sql_injection);
    function("xss", &malicious::xss);
    function("format_inject", &malicious::format_injection);
    function("edge", &malicious::edge_cases);

    function("tld", &internet::tld);
    function("ipv4", &internet::ipv4);
    function("ipv6", &internet::ipv6);
    function("domain", &internet::domain);
    function("url", &internet::url);
    function("urlDefaultSchemas", &internet::url_default_schema);
    function("email", &internet::email);

    enum_<tg::internet::EmailFlags>("EmailFlags")
            .value("IPV6_ALLOW_IPV4", tg::internet::EmailFlags::EMAIL_ALLOW_IPV6_IPV4)
            .value("IPV6_ALLOW_FOLDING", tg::internet::EmailFlags::EMAIL_ALLOW_IPV6_FOLDING)
            .value("IPV6_SHRINK_ZEROS", tg::internet::EmailFlags::EMAIL_IPV6_SHRINK_ZEROS)
            .value("ALLOW_IPV6_HOST", tg::internet::EmailFlags::EMAIL_ALLOW_IPV6_HOST)
            .value("ALLOW_IPV4_HOST", tg::internet::EmailFlags::EMAIL_ALLOW_IPV4_HOST)
            .value("ALLOW_PLUS", tg::internet::EmailFlags::EMAIL_ALLOW_PLUS)
            .value("ALLOW_QUOTES", tg::internet::EmailFlags::EMAIL_ALLOW_QUOTES)
            .value("ALLOW_SPECIALS", tg::internet::EmailFlags::EMAIL_ALLOW_SPECIALS);

    enum_<tg::internet::UrlFlags>("UrlFlags")
            .value("IPV6_ALLOW_IPV4", tg::internet::UrlFlags::URL_ALLOW_IPV6_IPV4)
            .value("IPV6_ALLOW_FOLDING", tg::internet::UrlFlags::URL_ALLOW_IPV6_FOLDING)
            .value("IPV6_SHRINK_ZEROS", tg::internet::UrlFlags::URL_IPV6_SHRINK_ZEROS)
            .value("ALLOW_IPV6_HOST", tg::internet::UrlFlags::URL_ALLOW_IPV6_HOST)
            .value("ALLOW_IPV4_HOST", tg::internet::UrlFlags::URL_ALLOW_IPV4_HOST)
            .value("ALLOW_ENCODED_CHARS", tg::internet::UrlFlags::URL_ALLOW_ENCODED_CHARS)
            .value("ALLOW_HASH", tg::internet::UrlFlags::URL_ALLOW_HASH)
            .value("ALLOW_RANDOM_PATH", tg::internet::UrlFlags::URL_ALLOW_RANDOM_PATH)
            .value("ALLOW_USERNAME", tg::internet::UrlFlags::URL_ALLOW_USERNAME)
            .value("ALLOW_PASSWORD", tg::internet::UrlFlags::URL_ALLOW_PASSWORD)
            .value("ALLOW_QUERY", tg::internet::UrlFlags::URL_ALLOW_QUERY)
            .value("ALLOW_PORT", tg::internet::UrlFlags::URL_ALLOW_PORT);

    enum_<tg::internet::Ipv6Flags>("Ipv6Flags")
            .value("ALLOW_IPV4", tg::internet::Ipv6Flags::IPV6_ALLOW_IPV4)
            .value("ALLOW_FOLDING", tg::internet::Ipv6Flags::IPV6_ALLOW_FOLDING)
            .value("SHRINK_ZEROS", tg::internet::Ipv6Flags::IPV6_SHRINK_ZEROS);

    enum_<tg::common::Complexity>("Complexity")
            .value("RUDIMENTARY", tg::common::Complexity::RUDIMENTARY)
            .value("BASIC", tg::common::Complexity::BASIC)
            .value("INTERMEDIATE", tg::common::Complexity::INTERMEDIATE)
            .value("ADVANCED", tg::common::Complexity::ADVANCED)
            .value("COMPLEX", tg::common::Complexity::COMPLEX)
            .value("EDGE_CASE", tg::common::Complexity::EDGE_CASE);

    class_<tg::common::complexity_range>("ComplexityRange")
            .constructor()
            .property("minComplexity", &tg::common::complexity_range::minComplexity)
            .property("maxComplexity", &tg::common::complexity_range::maxComplexity);

    class_<financial::cc>("CreditCard")
            .property("networkId", &financial::cc::network_id)
            .property("networkName", &financial::cc::network_name)
            .property("number", &financial::cc::number)
            .property("cvv", &financial::cc::cvv)
            .property("expirationYear", &financial::cc::exp_year)
            .property("expirationMonth", &financial::cc::exp_month);

    function("cc", &financial::cc_card);

    function("accounts", &financial::accounts);
    function("accountNumber", &financial::account_number);
    function("amount", &financial::amount);
    function("bic", &financial::bic);
    function("maskedNum", &financial::masked_num);

    class_<financial::cc_net>("CreditCardNetwork")
            .property("id", &financial::cc_net::id)
            .property("name", &financial::cc_net::name);

    function("creditCardNetwork", &financial::cc_network);

    class_<tg::computer::file_mime_map>("FileMimeMap")
            .property("mime", &tg::computer::file_mime_map::mimeType)
            .property("ext", &tg::computer::file_mime_map::fileExtension);

    function("cmakeSys", &computer::cmake_system);
    function("cpuArch", &computer::cpu_arch);
    function("fileExt", &computer::file_ext);
    function("mimeMap", &computer::mime_map);
    function("mimeType", &computer::mime_type);
    function("fileType", &computer::file_type);
    function("os", &computer::os);

    class_<tg::color::rgb_color>("RGB")
            .property("red", &tg::color::rgb_color::red)
            .property("green", &tg::color::rgb_color::green)
            .property("blue", &tg::color::rgb_color::blue);

    class_<tg::color::rgba_color>("RGBA")
            .property("red", &tg::color::rgba_color::red)
            .property("green", &tg::color::rgba_color::green)
            .property("blue", &tg::color::rgba_color::blue)
            .property("alpha", &tg::color::rgba_color::alpha);

    class_<tg::color::hsl_color>("HSL")
            .property("hue", &tg::color::hsl_color::hue)
            .property("saturation", &tg::color::hsl_color::saturation)
            .property("luminosity", &tg::color::hsl_color::luminosity);


    class_<tg::color::hsla_color>("HSLA")
            .property("hue", &tg::color::hsla_color::hue)
            .property("saturation", &tg::color::hsla_color::saturation)
            .property("luminosity", &tg::color::hsla_color::luminosity)
            .property("alpha", &tg::color::hsla_color::alpha);

    class_<tg::color::named_color>("NamedColor")
            .property("name", &tg::color::named_color::name)
            .property("hex", &tg::color::named_color::hex)
            .property("rgb", &tg::color::named_color::rgb);

    function("rgb", &color::rgb);
    function("rgba", &color::rgba);
    function("hsl", &color::hsl);
    function("hsla", &color::hsla);
    function("namedColor", &color::named);

    function("u32", &numbers::uint32);
    function("u16", &numbers::uint16);
    function("u8", &numbers::uint8);
    function("i32", &numbers::int32);
    function("i16", &numbers::int16);
    function("i8", &numbers::int8);
    function("f64", &numbers::f64);
    function("f32", &numbers::f32);

    class_<tg::aviation::record>("AviationRecord")
            .property("name", &tg::aviation::record::name)
            .property("iata", &tg::aviation::record::iata)
            .property("icao", &tg::aviation::record::icao);

    function("aircraftType", &aviation::aircraft_type);
    function("airlines", &aviation::airlines);
    function("airplanes", &aviation::airplanes);
    function("airports", &aviation::airports);

    class_<world::country>("Country")
            .property("name", &world::country::name)
            .property("genc", &world::country::genc)
            .property("iso2", &world::country::iso2)
            .property("iso3", &world::country::iso3)
            .property("isoNum", &world::country::isoNum)
            .property("stanag", &world::country::stanag)
            .property("tld", &world::country::tld);
    function("country", &world::countries);

    class_<test_jam::gens::common::sequence_next<std::string>>("SeqNextStdString")
        .property("present", &test_jam::gens::common::sequence_next<std::string>::present)
        .property("value", &test_jam::gens::common::sequence_next<std::string>::value);

    class_<test_jam::gens::common::sequence<std::string>>("SeqStdString")
            .function("next", &test_jam::gens::common::sequence<std::string>::next_bind);

    function("str", &ascii::string);
    function("charsetAny", &ascii::any);
    function("charsetAlpha", &ascii::alpha);
    function("charsetAlphaLower", &ascii::alpha_lower);
    function("charsetAlphaUpper", &ascii::alpha_upper);
    function("charsetAlphaNumeric", &ascii::alpha_numeric);
    function("charsetAlphaNumericLower", &ascii::alpha_numeric_lower);
    function("charsetAlphaNumericUpper", &ascii::alpha_numeric_upper);
    function("charsetBinary", &ascii::binary);
    function("charsetBracket", &ascii::bracket);
    function("charsetControl", &ascii::control);
    function("charsetDomainPiece", &ascii::domain_piece);
    function("charsetHex", &ascii::hex);
    function("charsetHexLower", &ascii::hex_lower);
    function("charsetHexUpper", &ascii::hex_upper);
    function("charsetHtmlNamed", &ascii::html_named);
    function("charsetMath", &ascii::math);
    function("charsetNumeric", &ascii::numeric);
    function("charsetOctal", &ascii::octal);
    function("charsetPrintable", &ascii::printable);
    function("charsetQuote", &ascii::quote);
    function("charsetSafePunctuation", &ascii::safe_punctuation);
    function("charsetSentencePunctuation", &ascii::sentence_punctuation);
    function("charsetSymbol", &ascii::symbol);
    function("charsetText", &ascii::text);
    function("charsetUrlUnencodedChars", &ascii::url_unencoded_chars);
    function("charsetUrlUnencodedHashChars", &ascii::url_unencoded_hash_chars);
    function("charsetWhitespace", &ascii::whitespace);
}
