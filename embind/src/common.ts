
/**
 * Returns a range of number
 * @param fromOrTo If `to` is not provided, the (exclusive) end. If `to` is provided, the (inclusive) start
 * @param to If provided, the (exclusive) end. If not provided, the range will start at 0
 */
export function range(fromOrTo: number, to?: number) : number[] {
    if (typeof to === "undefined") {
        return range(0, fromOrTo)
    }
    else if (fromOrTo < to) {
        return [...(new Array(to - fromOrTo)).keys()].map(i => i + fromOrTo)
    }
    else {
        return []
    }
}


export function final<T>(iter: Iterable<T>) : T|undefined {
    let last = undefined
    for (const e of iter) {
        last = e
    }
    return last
}


export interface DataGenerator<T> {
    create(seed: number) : T;
    simplify(seed: number) : Iterable<T>;
}

export function defaultOnNil<T>(value: T|undefined|null, defaultVal: T) : T {
    if (typeof value === "undefined" || value === null) {
        return defaultVal
    }
    return value
}


export function* zip(...arrs: Iterable<any>[]) {
    const iters: (Iterator<any>|null)[] = arrs.map(iter => iter[Symbol.iterator]())
    const values = (new Array(arrs.length)).fill(null)
    while(true) {
        let oneHadValue = false
        for (let a = 0; a < iters.length; ++a) {
            const iter = iters[a]
            if (iter === null) {
                continue
            }
            const res = iter.next()
            if (res.done) {
                iters[a] = null
                values[a] = null
            }
            else {
                values[a] = res.value
                oneHadValue = true
            }
        }
        if (oneHadValue) {
            yield [...values]
        }
        else {
            return
        }
    }
}

