import {DataGenerator} from "../common";
const wasm = require('../wasm')

export interface Rgb {
    red: number;
    green: number;
    blue: number;
}

export function rgb(): DataGenerator<Rgb> {
    return {
        create(seed: number): Rgb {
            const c = wasm.rgb(seed)
            try {
                return {
                    red: c.red,
                    green: c.green,
                    blue: c.blue
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number): Iterable<Rgb> {}
    }
}

export interface Rgba extends Rgb {
    alpha: number;
}

export function rgba(): DataGenerator<Rgba> {
    return {
        create(seed: number): Rgba {
            const c = wasm.rgba(seed)
            try {
                return {
                    red: c.red,
                    green: c.green,
                    blue: c.blue,
                    alpha: c.alpha
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number): Iterable<Rgba> {}
    }
}

export interface Hsl {
    hue: number;
    saturation: number;
    luminosity: number;
}

export function hsl(): DataGenerator<Hsl> {
    return {
        create(seed: number): Hsl {
            const c = wasm.hsl(seed)
            try {
                return {
                    hue: c.hue,
                    saturation: c.saturation,
                    luminosity: c.luminosity,
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number): Iterable<Hsl> {}
    }
}

export interface Hsla extends Hsl {
    alpha: number;
}

export function hsla(): DataGenerator<Hsla> {
    return {
        create(seed: number): Hsla {
            const c = wasm.hsla(seed)
            try {
                return {
                    hue: c.hue,
                    saturation: c.saturation,
                    luminosity: c.luminosity,
                    alpha: c.alpha
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number): Iterable<Hsla> {}
    }
}

export interface Named {
    name: string
    hex: string
    rgb: Rgb
}

export function named(): DataGenerator<Named> {
    return {
        create(seed: number): Named {
            const c = wasm.namedColor(seed)
            try {
                return {
                    name: c.name,
                    hex: c.hex,
                    rgb: {
                        red: c.rgb.red,
                        green: c.rgb.green,
                        blue: c.rgb.blue,
                    }
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number): Iterable<Named> {}
    }
}
