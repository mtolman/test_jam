import {Complexity, ComplexityRange, defaultRange, jsComplexityToCppComplexity} from "./common";
import {DataGenerator, defaultOnNil} from "../common";

const wasm = require('../wasm')

export function accounts(range: ComplexityRange = {}): DataGenerator<string> {
    return {
        create(seed: number): string {
            const rangeJs = defaultRange(range)
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                return wasm.accounts(seed, rangeCpp)
            }
            finally {
                rangeCpp.delete()
            }
        },
        *simplify(seed: number): Iterable<string> {}
    }
}

export interface CreditCardNetwork {
    id: string
    name: string
}

export function creditCardNetwork() : DataGenerator<CreditCardNetwork> {
    return {
        create(seed: number): CreditCardNetwork {
            const c = wasm.creditCardNetwork(seed);
            try {
                return {
                    id: c.id,
                    name: c.name
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed) {}
    }
}

export interface ExpirationDate {
    year: number;
    month: number
}

export interface CreditCard {
    network: {id: string, name: string},
    number: string,
    cvv: string,
    expiration: ExpirationDate
}

export interface CreditCardOptions {
    minExpirationDate?: ExpirationDate
    maxExpirationDate?: ExpirationDate
}

export function creditCard(options: CreditCardOptions = {}) : DataGenerator<CreditCard> {
    const minExpiration = defaultOnNil(options.minExpirationDate, {year: 2000, month: 1})
    const maxExpiration = defaultOnNil(options.maxExpirationDate, {year: 2100, month: 12})
    return {
        create(seed: number): CreditCard {
            const cc = wasm.cc(seed, minExpiration.year, minExpiration.month, maxExpiration.year, maxExpiration.month)
            try {
                return {
                    network: {
                        id: cc.network_id,
                        name: cc.network_name
                    },
                    number: cc.number,
                    cvv: cc.cvv,
                    expiration: {
                        year: cc.exp_year,
                        month: cc.exp_month
                    }
                }
            }
            finally {
                cc.delete()
            }
        },
        *simplify(seed: number): Iterable<CreditCard> {}
    }
}

export function accountNumber(length: number = 15) : DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.accountNumber(seed, length)
        },
        *simplify(seed) {}
    }
}

export function pin(length: number = 4) : DataGenerator<string> {
    return accountNumber(length)
}

export interface AmountOptions {
    max?: number
    min?: number
    decimalPlaces?: number
    decimalSeparator?: string
    symbol?: string
    prefixSymbol?: boolean
}

export function amount(opts: AmountOptions = {}) : DataGenerator<string> {
    const max = defaultOnNil(opts.max, 10000.0)
    const min = defaultOnNil(opts.min, 0)
    const decimalPlaces = defaultOnNil(opts.decimalPlaces, 2)
    const decimalSeparator = defaultOnNil(opts.decimalSeparator, ".")
    const symbol = defaultOnNil(opts.symbol, "")
    const prefix = defaultOnNil(opts.prefixSymbol, true)

    return {
        create(seed: number): string {
            return wasm.amount(seed, max, min, decimalPlaces, decimalSeparator, symbol, prefix);
        },
        *simplify(){}
    }
}

export function bic(includeBranchCode: boolean = false) : DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.bic(seed, includeBranchCode)
        },
        *simplify(){}
    }
}

export interface MaskedNumberOptions {
    unMaskedLen?: number
    maskedLen?: number
    maskedChar?: string
}

export function maskedNumber(options: MaskedNumberOptions = {}) : DataGenerator<string> {
    const char = (defaultOnNil(options.maskedChar, '*') || '*')
        .charCodeAt(0)
    const maskedLen = defaultOnNil(options.maskedLen, 4)
    const unMaskedLen = defaultOnNil(options.unMaskedLen, 4)
    return {
        create(seed: number): string {
            return wasm.maskedNum(seed, unMaskedLen, maskedLen, char)
        },
        *simplify(){}
    }
}
