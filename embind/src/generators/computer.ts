import {DataGenerator} from "../common";
const wasm = require('../wasm')

export function cmakeSystemName() : DataGenerator<string> {
    return {
        create(seed) { return wasm.cmakeSys(seed) },
        *simplify(seed) {}
    }
}

export function cpuArchitecture() : DataGenerator<string> {
    return {
        create(seed) { return wasm.cpuArch(seed) },
        *simplify(seed) {}
    }
}

export function fileExtension() : DataGenerator<string> {
    return {
        create(seed) { return wasm.fileExt(seed) },
        *simplify(seed) {}
    }
}

export function mimeType() : DataGenerator<string> {
    return {
        create(seed) { return wasm.mimeType(seed) },
        *simplify(seed) {}
    }
}

export function fileType() : DataGenerator<string> {
    return {
        create(seed) { return wasm.fileType(seed) },
        *simplify(seed) {}
    }
}

export function operatingSystem() : DataGenerator<string> {
    return {
        create(seed) { return wasm.os(seed) },
        *simplify(seed) {}
    }
}

export interface mimeMapping {
    mime: string,
    fileExt: string
}

export function mimeMap() : DataGenerator<mimeMapping> {
    return {
        create(seed) {
            const c = wasm.mimeMap(seed)
            try {
                return {
                    mime: c.mime,
                    fileExt: c.ext
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed) {}
    }
}
