import {defaultOnNil} from "../common";
const wasm = require('../wasm')

export enum Complexity {
    RUDIMENTARY,
    BASIC,
    INTERMEDIATE,
    ADVANCED,
    COMPLEX,
    EDGE_CASE,
}

export interface ComplexityRange {
    min?: Complexity
    max?: Complexity
}

export function defaultRange(range: ComplexityRange = {}) : {min: Complexity, max: Complexity} {
    return {
        min: defaultOnNil(range.min, Complexity.RUDIMENTARY),
        max: defaultOnNil(range.max, Complexity.EDGE_CASE)
    }
}

export function jsComplexityToCppComplexity(complexity: Complexity) : any {
    switch (complexity) {
        case Complexity.RUDIMENTARY:
            return wasm.Complexity.RUDIMENTARY;
        case Complexity.BASIC:
            return wasm.Complexity.BASIC;
        case Complexity.INTERMEDIATE:
            return wasm.Complexity.INTERMEDIATE;
        case Complexity.ADVANCED:
            return wasm.Complexity.ADVANCED;
        case Complexity.COMPLEX:
            return wasm.Complexity.COMPLEX;
        case Complexity.EDGE_CASE:
            return wasm.Complexity.EDGE_CASE;
    }
    throw "Invalid complexity provided!"
}

