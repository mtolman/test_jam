import {DataGenerator} from "../common";
const wasm = require('../wasm')

export interface Country {
    name: string;
    genc: string|null;
    iso2: string|null;
    iso3: string|null;
    isoNum: string|null;
    stanag: string|null;
    tld: string|null;
}

export function country(): DataGenerator<Country> {
    return {
        create(seed: number): Country {
            const c = wasm.country(seed)
            try {
                return {
                    name: c.name,
                    genc: c.genc || null,
                    iso2: c.iso2 || null,
                    iso3: c.iso3 || null,
                    isoNum: c.isoNum || null,
                    stanag: c.stanag || null,
                    tld: c.tld || null
                }
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number): Iterable<Country> {}
    }
}
