import {DataGenerator} from "../common";
const wasm = require('../wasm')

export function aircraftTypes(): DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.aircraftType(seed)
        },
        *simplify(seed: number): Iterable<string> {}
    }
}

export interface Record {
    name: string;
    iata: string;
    icao: string;
}

function toRecord(c: any): Record {
    return {
        name: c.name,
        iata: c.iata,
        icao: c.icao
    }
}

export function airlines(): DataGenerator<Record> {
    return {
        create(seed:number): Record {
            const c = wasm.airlines(seed)
            try {
                return toRecord(c)
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number) {}
    }
}

export function airplanes(): DataGenerator<Record> {
    return {
        create(seed:number): Record {
            const c = wasm.airplanes(seed)
            try {
                return toRecord(c)
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number) {}
    }
}

export function airports(): DataGenerator<Record> {
    return {
        create(seed:number): Record {
            const c = wasm.airports(seed)
            try {
                return toRecord(c)
            }
            finally {
                c.delete()
            }
        },
        *simplify(seed: number) {}
    }
}
