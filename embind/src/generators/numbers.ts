import {DataGenerator, defaultOnNil} from "../common";

const wasm = require('../wasm.js')

export interface IntOptions {
    min?: number
    max?: number
}

function* simplifyInt(value: number, min: number, max: number) {
    yield value
    while (value !== 0 && value >= min && value <= max) {
        if (Math.abs(value) > 60) {
            value = (value / 2) | 0
            if (value < min) {
                value = min
                yield value
                return
            }
            else if (value > max) {
                value = max
                yield value
                return
            }
        }
        else if (value > 0) {
            --value
        }
        else if (value < 0) {
            ++value
        }
        if (value >= min && value <= max) {
            yield value
        }
    }
}

export function uint32(options: IntOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, 0)
    const max = defaultOnNil(options.max, 0xffffffff)
    return {
        create(seed) {
            return wasm.u32(seed, min, max)
        },
        *simplify(seed: number): Iterable<number> {
            yield* simplifyInt(wasm.u32(seed, min, max), min, max)
        }
    }
}

export function uint16(options: IntOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, 0)
    const max = defaultOnNil(options.max, 0xffff)
    return {
        create(seed) {
            return wasm.u16(seed, min, max)
        },
        *simplify(seed: number): Iterable<number> {
            yield* simplifyInt(wasm.u16(seed, min, max), min, max)
        }
    }
}

export function uint8(options: IntOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, 0)
    const max = defaultOnNil(options.max, 0xff)
    return {
        create(seed) {
            return wasm.u8(seed, min, max)
        },
        *simplify(seed: number): Iterable<number> {
            yield* simplifyInt(wasm.u8(seed, min, max), min, max)
        }
    }
}

export function int32(options: IntOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, -2147483648)
    const max = defaultOnNil(options.max, 0x7fffffff)
    return {
        create(seed) {
            return wasm.i32(seed, min, max)
        },
        *simplify(seed: number): Iterable<number> {
            yield* simplifyInt(wasm.i32(seed, min, max), min, max)
        }
    }
}

export function int16(options: IntOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, -32768)
    const max = defaultOnNil(options.max, 0x7fff)
    return {
        create(seed) {
            return wasm.i16(seed, min, max)
        },
        *simplify(seed: number): Iterable<number> {
            yield* simplifyInt(wasm.i16(seed, min, max), min, max)
        }
    }
}

export function int8(options: IntOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, -128)
    const max = defaultOnNil(options.max, 0x7f)
    return {
        create(seed) {
            return wasm.i8(seed, min, max)
        },
        *simplify(seed: number): Iterable<number> {
            yield* simplifyInt(wasm.i8(seed, min, max), min, max)
        }
    }
}

export interface FloatOptions {
    min?: number
    max?: number
    allowNan?: boolean
    allowInfinity?: boolean
}

function* simplifyFloat(value: number, min: number, max: number) : Iterable<number> {
    yield value
    if (!isFinite(value)) {
        return
    }
    while (value >= min + Number.EPSILON && value <= max - Number.EPSILON) {
        const next = Math.abs(max) > Math.abs(min) ? (value / 2 + min / 2) : (value / 2 + max / 2)
        if (next <= value + Number.EPSILON && next >= value - Number.EPSILON) {
            return
        }
        if (Math.abs(max) > Math.abs(min)) {
            max = value
        }
        else {
            min = value
        }
        yield next
        value = next
    }
}

export function f64(options: FloatOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, 0)
    const max = defaultOnNil(options.max, Number.MAX_VALUE)
    const nan = defaultOnNil(options.allowNan, false)
    const inf = defaultOnNil(options.allowInfinity, false)
    return {
        create(seed: number) : number {
            return wasm.f64(seed, min, max, nan, inf)
        },
        *simplify(seed: number): Iterable<number> {
            yield * simplifyFloat(wasm.f64(seed, min, max, nan, inf), min, max)
        }
    }
}

export function f32(options: FloatOptions = {}) : DataGenerator<number> {
    const min = defaultOnNil(options.min, 0)
    const max = defaultOnNil(options.max, 4194304)
    const nan = defaultOnNil(options.allowNan, false)
    const inf = defaultOnNil(options.allowInfinity, false)
    return {
        create(seed: number) : number {
            return wasm.f32(seed, min, max, nan, inf)
        },
        *simplify(seed: number): Iterable<number> {
            yield * simplifyFloat(wasm.f32(seed, min, max, nan, inf), min, max)
        }
    }
}

export const number = f64

export const int = int32;
export const uint = uint32;
