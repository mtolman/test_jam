import {DataGenerator, defaultOnNil, zip} from "../common";
import Random from "../tinymt";

export function filter<T>(gen: DataGenerator<T>, pred: (v: T) => boolean): DataGenerator<T> {
    return {
        create(seed: number): T {
            let index = seed
            let res = gen.create(index)
            for (;!pred(res); ++index, res = gen.create(index)){}
            return res
        },
        *simplify(seed: number){
            for (const simplification of gen.simplify(seed)) {
                if (pred(simplification)) {
                    yield simplification
                }
            }
        }
    }
}

export function map<T, R>(gen: DataGenerator<T>, transform: (v: T, seed: number) => R): DataGenerator<R> {
    return {
        create(seed: number): R {
            return transform(gen.create(seed), seed)
        },
        *simplify(seed: number) {
            for (const v of gen.simplify(seed)) {
                yield transform(v, seed)
            }
        }
    }
}

export function recover<T>(gen: DataGenerator<T>, onErr: (err: any) => T) : DataGenerator<T> {
    return {
        create(seed: number) {
            try {
                return gen.create(seed)
            }
            catch (e) {
                return onErr(e)
            }
        },
        *simplify(seed: number) {
            try {
                yield* gen.simplify(seed)
            }
            catch (e) {
                yield onErr(e)
            }
        }
    }
}

function* simplifyGenerators(rootSeed: number, generators: DataGenerator<any>[]) {
    const rnd = new Random(rootSeed)
    const seeds = [...generators.map(_ => rnd.uint())]
    let curValues = [...generators.map((g, i) => g.create(seeds[i]))]
    const iters: (Iterator<any>|null)[] = [
        ...generators.map(
            (g, i) => (
                (g.simplify(seeds[i])) as Iterable<any>
            )[Symbol.iterator]()
        )
    ]
    let foundValue = false
    do {
        foundValue = false
        for (let i = 0; i < iters.length; ++i) {
            const iter: Iterator<any>|null = iters[i]
            if (iter === null) {
                continue
            }
            const res = iter.next()
            if (!res.done) {
                curValues[i] = res.value
                yield [...curValues]
                foundValue = true
            }
        }
    } while(foundValue)
}

export function combine(gens: {[key: string]: DataGenerator<any>}): DataGenerator<{[key: string]: any}> {
    const mappings = Object.entries(gens)
    return {
        create(seed: number): { [p: string]: any } {
            const rnd = new Random(seed)
            return Object.fromEntries(mappings.map(([k, g]) => {
                return [k, g.create(rnd.uint())]
            }));
        },
        *simplify(seed: number) {
            const keys = mappings.map(([key, _]) => key)
            const gens = mappings.map(([_, v]) => v)
            const iter = simplifyGenerators(seed, gens)
            for (const valSet of iter) {
                const entries = [...zip(keys, valSet)]
                yield Object.fromEntries(entries)
            }
        }
    }
}

export function tuple(...gens: DataGenerator<any>[]): DataGenerator<any[]> {
    return {
        create(seed: number): any[] {
            const rnd = new Random(seed)
            return gens.map(g => g.create(rnd.uint()))
        },
        *simplify(seed: number) {
            yield* simplifyGenerators(seed, gens)
        }
    }
}

export function just<T>(v: T): DataGenerator<T> {
    return {
        create(seed: number): T {
            return v
        },
        *simplify(){}
    }
}

export function oneOf<T>(...gens: DataGenerator<T>[]): DataGenerator<T> {
    return {
        create(seed: number) {
            return gens[seed % gens.length].create(seed / gens.length)
        },
        *simplify(seed: number) {
            yield* gens[seed % gens.length].simplify(seed / gens.length)
        }
    }
}

export interface ArrayOptions {
    minLen?: number,
    maxLen?: number
}

export function array<T>(gen: DataGenerator<T>, options: ArrayOptions = {}) : DataGenerator<T[]> {
    const minLen = defaultOnNil(options.minLen, 0)
    const maxLen = defaultOnNil(options.maxLen, 1024)
    return {
        create(seed: number): T[] {
            const rnd = new Random(seed)
            return (new Array((rnd.uint() % (maxLen - minLen)) + minLen))
                .fill(null)
                .map(() => gen.create(rnd.uint()))
        },
        *simplify(seed: number) {
            let arr = this.create(seed)
            const rnd = new Random(seed)
            while(arr.length > 0) {
                const sliceAmt = (rnd.uint() % 3)
                arr = arr.slice(0, -sliceAmt)
                yield arr
            }
        }
    }
}
