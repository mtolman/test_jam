import {DataGenerator, defaultOnNil} from "../common";
const wasm = require('../wasm')

export function topLevelDomain() : DataGenerator<string> {
    return {
        *simplify(){},
        create(seed: number): string { return wasm.tld(seed) }
    }
}

export function ipv4() : DataGenerator<string> {
    return {
        *simplify(){},
        create(seed: number) : string { return wasm.ipv4(seed); }
    }
}

export interface Ipv6Options {
    allowIpv4?: boolean
    allowFolding?: boolean
    shrinkZeroes?: boolean
}

export function ipv6(options: Ipv6Options = {}) : DataGenerator<string> {
    const allowIpv4 = defaultOnNil(options.allowIpv4, true)
    const allowFolding = defaultOnNil(options.allowFolding, true)
    const shrink = defaultOnNil(options.shrinkZeroes, true)
    const flags = (allowIpv4 && wasm.Ipv6Flags.ALLOW_IPV4) |
        (allowFolding && wasm.Ipv6Flags.ALLOW_FOLDING) |
        (allowIpv4 && wasm.Ipv6Flags.SHRINK_ZEROS);

    return {
        *simplify(seed: number) {
            yield this.create(seed)
            if (allowIpv4) {
                yield* ipv6({...options, allowIpv4: false}).simplify(seed)
            }
            else if (allowFolding) {
                yield* ipv6({...options, allowFolding: false}).simplify(seed)
            }
            else if (shrink) {
                yield* ipv6({...options, shrinkZeroes: false}).simplify(seed)
            }
        },
        create(seed: number): string {
            return wasm.ipv6(seed, flags)
        }
    }
}

export interface DomainOptions {
    minLen?: number
    maxLen?: number
}

export function domain(options: DomainOptions = {}) : DataGenerator<string> {
    const minLen = defaultOnNil(options.minLen, 3)
    const maxLen = defaultOnNil(options.maxLen, 63)
    return {
        *simplify(seed: number){
            let v = wasm.domain(minLen, maxLen)
            while (v.length > minLen) {
                v = wasm.domain(seed, minLen, v.length - 1)
                yield v
            }
        },
        create(seed: number): string {
            return wasm.domain(seed, minLen, maxLen)
        }
    }
}

export enum UrlCredentials {
    NONE,
    USERNAME_ONLY,
    FULL
}

export interface UrlOptions {
    ipv6Options?: Ipv6Options,
    allowIpv6?: boolean
    allowIpv4?: boolean
    allowPort?: boolean
    allowEncodedChars?: boolean
    allowRandomPath?: boolean
    allowCredentials?: UrlCredentials
    allowQuery?: boolean
    allowHash?: boolean,
    minLen?: number
    maxLen?: number
    possibleSchemas?: string[]
    domainOptions?: DomainOptions
}

export function url(options: UrlOptions = {}) : DataGenerator<string> {
    const ipv6AllowIpv4 = defaultOnNil(options.ipv6Options?.allowIpv4, true)
    const ipv6AllowFolding = defaultOnNil(options.ipv6Options?.allowFolding, true)
    const ipv6Shrink = defaultOnNil(options.ipv6Options?.shrinkZeroes, true)
    const allowIpv6 = defaultOnNil(options.allowIpv6, true)
    const allowIpv4 = defaultOnNil(options.allowIpv4, true)
    const allowPort = defaultOnNil(options.allowPort, true)
    const allowEncoded = defaultOnNil(options.allowEncodedChars, true)
    const allowPath = defaultOnNil(options.allowRandomPath, true)
    const allowCreds = defaultOnNil(options.allowCredentials, UrlCredentials.FULL)
    const allowQuery = defaultOnNil(options.allowQuery, true)
    const allowHash = defaultOnNil(options.allowHash, true)

    const flags = 0x0
        | (allowIpv6 && wasm.UrlFlags.ALLOW_IPV6_HOST)
        | (ipv6AllowIpv4 && wasm.UrlFlags.IPV6_ALLOW_IPV4)
        | (ipv6AllowFolding && wasm.UrlFlags.IPV6_ALLOW_FOLDING)
        | (ipv6Shrink && wasm.UrlFlags.IPV6_SHRINK_ZEROS)
        | (allowIpv4 && wasm.UrlFlags.ALLOW_IPV4_HOST)
        | (allowPort && wasm.UrlFlags.ALLOW_PORT)
        | (allowEncoded && wasm.UrlFlags.ALLOW_ENCODED_CHARS)
        | (allowPath && wasm.UrlFlags.ALLOW_RANDOM_PATH)
        | (allowCreds === UrlCredentials.FULL && ((wasm.UrlFlags.ALLOW_USERNAME | wasm.UrlFlags.ALLOW_PASSWORD) as any))
        | (allowCreds === UrlCredentials.USERNAME_ONLY && wasm.UrlFlags.ALLOW_USERNAME)
        | (allowQuery && wasm.UrlFlags.ALLOW_QUERY)
        | (allowHash && wasm.UrlFlags.ALLOW_HASH)

    const minLen = defaultOnNil(options.minLen, 20)
    const maxLen = defaultOnNil(options.maxLen, 256)
    const schemas = defaultOnNil(options.possibleSchemas, [])
    const minDomLen = defaultOnNil(options.domainOptions?.minLen, 3)
    const maxDomLen = defaultOnNil(options.domainOptions?.minLen, 63)

    return {
        create(seed: number): string {
            let res = ""
            const cSchema = new wasm.StdStringVector()
            try {
                for(const v of schemas) {
                    cSchema.push_back(v)
                }

                res = wasm.url(
                    seed,
                    minLen,
                    maxLen,
                    flags,
                    cSchema,
                    minDomLen,
                    maxDomLen
                )
            }
            finally {
                cSchema.delete()
            }
            return res
        },
        *simplify(seed: number): Iterable<string> {
            let newFlags = flags
            while (newFlags) {
                newFlags &= newFlags >> 1
                let res = ""
                const cSchema = new wasm.StdStringVector()
                try {
                    for(const v of schemas) {
                        cSchema.push_back(v)
                    }

                    res = wasm.url(
                        seed,
                        minLen,
                        maxLen,
                        flags,
                        cSchema,
                        minDomLen,
                        maxDomLen
                    )
                }
                finally {
                    cSchema.delete()
                }
                yield res
            }
        }
    }
}

export interface EmailOptions {
    ipv6Options?: Ipv6Options
    allowIpv6?: boolean
    allowIpv4?: boolean
    allowPlus?: boolean
    allowQuotes?: boolean
    allowSpecials?: boolean
    domainOptions?: DomainOptions
    minLen?: number
    maxLen?: number
}

export function email(options: EmailOptions = {}) : DataGenerator<string> {
    const ipv6AllowIpv4 = defaultOnNil(options.ipv6Options?.allowIpv4, true)
    const ipv6AllowFolding = defaultOnNil(options.ipv6Options?.allowFolding, true)
    const ipv6Shrink = defaultOnNil(options.ipv6Options?.shrinkZeroes, true)
    const allowIpv6 = defaultOnNil(options.allowIpv6, true)
    const allowIpv4 = defaultOnNil(options.allowIpv4, true)
    const allowPlus = defaultOnNil(options.allowPlus, true)
    const allowQuotes = defaultOnNil(options.allowQuotes, true)
    const allowSpecials = defaultOnNil(options.allowSpecials, true)
    const minDomLen = defaultOnNil(options.domainOptions?.minLen, 3)
    const maxDomLen = defaultOnNil(options.domainOptions?.minLen, 63)
    const minLen = defaultOnNil(options.minLen, 10)
    const maxLen = defaultOnNil(options.minLen, 90)

    const flags = 0x0
        | (allowIpv6 && wasm.EmailFlags.ALLOW_IPV6_HOST)
        | (ipv6AllowIpv4 && wasm.EmailFlags.IPV6_ALLOW_IPV4)
        | (ipv6AllowFolding && wasm.EmailFlags.IPV6_ALLOW_FOLDING)
        | (ipv6Shrink && wasm.EmailFlags.IPV6_SHRINK_ZEROS)
        | (allowIpv4 && wasm.EmailFlags.ALLOW_IPV4_HOST)
        | (allowPlus && wasm.EmailFlags.ALLOW_PLUS)
        | (allowQuotes && wasm.EmailFlags.ALLOW_QUOTES)
        | (allowSpecials && wasm.EmailFlags.ALLOW_SPECIALS)

    return {
        *simplify(seed: number): Iterable<string> {
            let newFlags = flags
            while (newFlags) {
                newFlags &= newFlags >> 1
                let res = ""
                res = wasm.email(
                    seed,
                    minLen,
                    maxLen,
                    flags,
                    minDomLen,
                    maxDomLen
                )
                yield res
            }
        },
        create(seed: number): string {
            return wasm.email(
                seed,
                minLen,
                maxLen,
                flags,
                minDomLen,
                maxDomLen
            )
        }
    }
}
