import {DataGenerator} from "../common";
const wasm = require('../wasm')

export function sqlInjection() : DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.sql_inject(seed)
        },
        *simplify(seed: number): Iterable<string> {}
    }
}

export function xss() : DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.xss(seed)
        },
        *simplify(seed: number): Iterable<string> {}
    }
}

export function formatInjection() : DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.format_inject(seed)
        },
        *simplify(seed: number): Iterable<string> {}
    }
}

export function edgeCases() : DataGenerator<string> {
    return {
        create(seed: number): string {
            return wasm.edge(seed)
        },
        *simplify(seed: number): Iterable<string> {}
    }
}
