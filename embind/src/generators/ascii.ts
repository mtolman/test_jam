import {DataGenerator, defaultOnNil} from "../common";

const wasm = require('../wasm.js')

export interface StrOpts {
    charset?: string,
    minLen?: number
    maxLen?: number
}

export const charsets = {
    any: wasm.charsetAny(),
    alpha: wasm.charsetAlpha(),
    alphaLower: wasm.charsetAlphaLower(),
    alphaUpper: wasm.charsetAlphaUpper(),
    alphaNumeric: wasm.charsetAlphaNumeric(),
    alphaNumericLower: wasm.charsetAlphaNumericLower(),
    alphaNumericUpper: wasm.charsetAlphaNumericUpper(),
    binary: wasm.charsetBinary(),
    bracket: wasm.charsetBracket(),
    control: wasm.charsetControl(),
    domainPiece: wasm.charsetDomainPiece(),
    hex: wasm.charsetHex(),
    hexLower: wasm.charsetHexLower(),
    hexUpper: wasm.charsetHexUpper(),
    htmlNamed: wasm.charsetHtmlNamed(),
    math: wasm.charsetMath(),
    numeric: wasm.charsetNumeric(),
    octal: wasm.charsetOctal(),
    printable: wasm.charsetPrintable(),
    quote: wasm.charsetQuote(),
    safePunctuation: wasm.charsetSafePunctuation(),
    sentencePunctuation: wasm.charsetSentencePunctuation(),
    symbol: wasm.charsetSymbol(),
    text: wasm.charsetText(),
    urlUnencodedChars: wasm.charsetUrlUnencodedChars(),
    urlUnencodedHashChars: wasm.charsetUrlUnencodedHashChars(),
    whitespace: wasm.charsetWhitespace()
}

Object.freeze(charsets)

export function str(opts: StrOpts = {}) : DataGenerator<string> {
    const options = {
        charset: defaultOnNil(opts.charset, charsets.any),
        minLen: defaultOnNil(opts.minLen, 0),
        maxLen: defaultOnNil(opts.maxLen, 1024)
    }
    return {
        create(seed) {
            return wasm.str(seed, options.charset, options.minLen, options.maxLen)
        },
        *simplify(seed: number): Iterable<string> {
            const sOpts = {...options}
            let v = str(sOpts).create(seed)
            while (v.length > sOpts.minLen) {
                v = v.slice(0, -1)
                yield v
            }
        }
    }
}