import {DataGenerator, defaultOnNil} from "../common";
import {ComplexityRange, defaultRange, jsComplexityToCppComplexity} from "./common";

const wasm = require('../wasm.js')

export enum Sex {
    MALE,
    FEMALE
}

export interface Person {
    sex: Sex
    name: {
        full: string
        given: string
        sur: string
        prefix: string|null
        suffix: string|null
    }
}

export function person(range: ComplexityRange = {}) : DataGenerator<Person> {
    const rangeJs = defaultRange(range)
    return {
        *simplify(){},
        create(seed: number): Person {
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                const v = wasm.person(seed, rangeCpp)
                try {
                    const s = v.sex === wasm.PersonSex.MALE ? Sex.MALE : Sex.FEMALE
                    return {
                        sex: s,
                        name: {
                            full: v.full,
                            given: v.given,
                            sur: v.sur,
                            prefix: v.prefix || null,
                            suffix: v.suffix || null
                        }
                    }
                }
                finally {
                    v.delete()
                }
            }
            finally {
                rangeCpp.delete()
            }
        }
    }
}

export function fullName(range: ComplexityRange = {}) : DataGenerator<string> {
    const rangeJs = defaultRange(range)
    return {
        *simplify(){},
        create(seed: number): string {
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                return wasm.person_name_full(seed, rangeCpp)
            }
            finally {
                rangeCpp.delete()
            }
        }
    }
}

export function givenName(range: ComplexityRange = {}) : DataGenerator<string> {
    const rangeJs = defaultRange(range)
    return {
        *simplify(){},
        create(seed: number): string {
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                return wasm.person_name_given(seed, rangeCpp)
            }
            finally {
                rangeCpp.delete()
            }
        }
    }
}

export function surname(range: ComplexityRange = {}) : DataGenerator<string> {
    const rangeJs = defaultRange(range)
    return {
        *simplify(){},
        create(seed: number): string {
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                return wasm.person_name_sur(seed, rangeCpp)
            }
            finally {
                rangeCpp.delete()
            }
        }
    }
}

export function prefix(range: ComplexityRange = {}) : DataGenerator<string|null> {
    const rangeJs = defaultRange(range)
    return {
        *simplify(){},
        create(seed: number): string {
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                return wasm.person_name_prefix(seed, rangeCpp) || null
            }
            finally {
                rangeCpp.delete()
            }
        }
    }
}

export function suffix(range: ComplexityRange = {}) : DataGenerator<string|null> {
    const rangeJs = defaultRange(range)
    return {
        *simplify(){},
        create(seed: number): string {
            const rangeCpp = new wasm.ComplexityRange()
            try {
                rangeCpp.minComplexity = jsComplexityToCppComplexity(rangeJs.min)
                rangeCpp.maxComplexity = jsComplexityToCppComplexity(rangeJs.max)
                return wasm.person_name_suffix(seed, rangeCpp) || null
            }
            finally {
                rangeCpp.delete()
            }
        }
    }
}

export function sex() : DataGenerator<Sex> {
    return {
        *simplify(){},
        create(seed: number): Sex {
            return wasm.person_sex(seed) === wasm.PersonSex.MALE ? Sex.MALE : Sex.FEMALE
        }
    }
}
