import {shrink} from "./shrink";
import {DataGenerator} from "./common";
import Random from "./tinymt";
import {tuple} from "./generators/utils";

export interface Property {
    withSeed(seed: number): Property
    withGens(...gens: DataGenerator<any>[]): Property
    test(code: (...params: any[]) => any): void
    withIterations(iters: number): Property
}

const baseSeed = (Math.random() * 3899430923593257328) >>> 0

export function property(name: string, ...gens: DataGenerator<any>[]) {
    let seed = baseSeed
    if (process && process.env && process.env.PROP_TEST_SEED) {
        seed = (+process.env.PROP_TEST_SEED) >>> 0
    }
    let ret: any = {}
    let maxIters: number = +(process.env.PROP_TEST_ITERS || 500) || 500
    ret.withSeed = function (newSeed: number) {
        seed = newSeed
        return ret
    }
    ret.withGens = function (...generators: DataGenerator<any>[]) {
        gens = generators
        return ret
    }
    ret.withIterations = function (iters: number) {
        maxIters = iters
        return ret
    }

    const iteration = (runSeed: number, code: (...params: any[]) => any) => {
        // making a copy so other code won't interfere
        const paramGen = tuple(...gens)
        const params = paramGen.create(runSeed)
        try {
            code(...params)
        }
        catch (e: any) {
            const simplified = shrink(runSeed, paramGen, (params: any) => {
                try {
                    code(...params)
                    return true
                }
                catch (e) {
                    return false
                }
            })

            const msg = `

Property Failed: ${name}
------------------------------------------------
Seed: ${seed}
Original Input: ${JSON.stringify(params)}
Simplified Input: ${JSON.stringify(simplified)}
------------------------------------------------

`;

            if (typeof e === 'object' && e !== null) {
                if ('matcherResult' in e) {
                    e.matcherResult.input = params;
                    e.matcherResult.simplifiedInput = simplified;
                }
                if ('message' in e) {
                    e.message = msg + e.message
                }
                else if ('stack' in e) {
                    e.stack = msg + e.stack
                }
            }
            else {
                console.error(msg)
            }
            throw e
        }
    }

    ret.test = function(code: (...params: any[]) => any) {
        const rnd = new Random(seed)
        for (let i = 0; i < maxIters; ++i) {
            iteration(rnd.uint(), code)
        }
    }
    return ret as Property
}