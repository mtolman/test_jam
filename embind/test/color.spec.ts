import {range} from "../src/common";
import {rgb, rgba, hsl, hsla, named} from "../src/generators/color";


describe('color', function () {
    it('can create rgb', () => {
        const gen = rgb()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c).toBeTruthy()
        })
    })

    it('can create rgba', () => {
        const gen = rgba()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c).toBeTruthy()
        })
    })

    it('can create hsl', () => {
        const gen = hsl()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c).toBeTruthy()
        })
    })

    it('can create hsla', () => {
        const gen = hsla()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c).toBeTruthy()
        })
    })

    it('can create named', () => {
        const gen = named()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c).toBeTruthy()
            expect(c.name).toBeTruthy()
            expect(c.hex).toBeTruthy()
        })
    })
});
