import {ipv4, ipv6, topLevelDomain, domain, url, email} from "../src/generators/internet";
import {range} from "../src/common";

describe('internet', function () {
    it('can make TLDs', () => {
        range(300).forEach(s => expect(topLevelDomain().create(s)).toMatch(/\w+/))
    })

    it('can make IPv4', () => {
        range(300).forEach(s => expect(ipv4().create(s)).toMatch(/(\d+\.){3}\d+/))
    })

    it('can make IPv6', () => {
        range(300).forEach(s => expect(ipv6().create(s)).toMatch(/((::)?([a-fA-F0-9]+:)+[a-fA-F0-9]+)|(::ffff:(\d+\.)+\d+)/))
    })

    it('can make IPv6', () => {
        range(300).forEach(s => expect(ipv6().create(s)).toMatch(/((::)?([a-fA-F0-9]+:)+[a-fA-F0-9]+)|(::ffff:(\d+\.)+\d+)/))
    })

    it('can make domains', () => {
        range(300).forEach(s => expect(domain().create(s)).toMatch(/([a-z0-9\-]+\.)+[a-z0-9\-]+/))
    })

    it('can make urls', () => {
        range(300).forEach(s => expect(url().create(s)).toMatch(/(https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})*)?/))
    })

    it('can make emails', () => {
        range(300).forEach(s => expect(email().create(s)).toMatch(/(([A-Za-z0-9!#$%&'*+-\/=?^_`{|}~]+(\.[A-Za-z0-9!#$%&'*+\-/=?^_`{|}~]+)*)|("([^\\"]|\\.)+"))@((([a-z0-9\-]+\.)+[a-z0-9\-]+)|(\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(\[IPv6:[a-fA-F0-9:.]+\]))/))
    })
});