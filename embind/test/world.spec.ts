import {country} from "../src/generators/world";
import {range} from "../src/common";

function propCheck(prop: string, obj: any) {
    if (obj[prop]) { expect(typeof obj[prop]).toEqual("string") }
    else { expect(obj[prop]).toBeNull() }
}

describe('country', function () {
    it('can create', () => {
        const gen = country()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c.name).toBeTruthy()
            expect(typeof c.name).toEqual("string")

            propCheck('genc', c)
            propCheck('iso2', c)
            propCheck('iso3', c)
            propCheck('isoNum', c)
            propCheck('stanag', c)
            propCheck('tld', c)
        })
    })
});
