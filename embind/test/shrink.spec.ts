import {shrink} from "../src/shrink";
import {int} from "../src/generators/numbers";

describe('shrink', () => {
    it('can shrink', () => {
        const gen = int({min: 1, max: 200})
        const pred = (n: number) => n <= 8
        expect(shrink(123, gen, pred)).toEqual(9)
    })
})