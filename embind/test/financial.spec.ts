import {
    accountNumber,
    accounts,
    amount,
    bic,
    creditCard,
    creditCardNetwork,
    maskedNumber
} from "../src/generators/financial";
import {range} from "../src/common";
import {Complexity} from "../src/generators/common";

describe('financial', () => {
    it('accounts', () => {
        range(300).forEach(s => expect(accounts().create(s)).toBeTruthy())
        range(300).forEach(s => expect(accounts({max: Complexity.RUDIMENTARY}).create(s)).toMatch(/[\s\w\-]+/))
    })

    it('cc network', () => {
        range(300).forEach(s => {
            expect(creditCardNetwork().create(s)).toBeTruthy()
        })
    })

    it('cc', () => {
        range(300).forEach(s => {
            expect(creditCard().create(s)).toBeTruthy()
        })
    })

    it('account number', () => {
        range(300).forEach(s => {
            expect(accountNumber().create(s)).toMatch(/\d+/)
        })
    })

    it('amount', () => {
        range(300).forEach(s => {
            expect(amount().create(s)).toMatch(/\d+\.\d+/)
        })
    })

    it('bic', () => {
        range(300).forEach(s => {
            expect(bic().create(s)).toMatch(/[\w\d]+/)
        })
    })

    it('masked number', () => {
        range(300).forEach(s => {
            expect(maskedNumber().create(s)).toMatch(/\*{4}\d{4}/)
        })
    })
})
