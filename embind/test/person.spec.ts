import {fullName, givenName, person, prefix, sex, Sex, suffix, surname} from "../src/generators/person";
import {range} from "../src/common";

function truthyOrNull(val: any) {
    if (val === null) {
        expect(val).toBeNull()
    }
    else {
        expect(val).toBeTruthy()
    }
}

describe('person', function () {
    it('can generate a person', () => {
        range(300).forEach(s => {
            const v = person().create(s)
            expect(v.sex).toBeGreaterThanOrEqual(Sex.MALE)
            expect(v.sex).toBeLessThanOrEqual(Sex.FEMALE)

            expect(v.name.full).toBeTruthy()
            expect(v.name.given).toBeTruthy()
            expect(v.name.sur).toBeTruthy()
            truthyOrNull(v.name.prefix)
            truthyOrNull(v.name.suffix)
        })
    })

    it('can generate a full name', () => {
        range(300).forEach(s => {
            expect(fullName().create(s)).toBeTruthy()
        })
    })

    it('can generate a given name', () => {
        range(300).forEach(s => {
            expect(givenName().create(s)).toBeTruthy()
        })
    })

    it('can generate a surname', () => {
        range(300).forEach(s => {
            expect(surname().create(s)).toBeTruthy()
        })
    })

    it('can generate a prefix', () => {
        range(300).forEach(s => {
            truthyOrNull(prefix().create(s))
        })
    })

    it('can generate a suffix', () => {
        range(300).forEach(s => {
            truthyOrNull(suffix().create(s))
        })
    })

    it('can generate a sex', () => {
        range(300).forEach(s => {
            const v = sex().create(s)
            expect(v).toBeGreaterThanOrEqual(Sex.MALE)
            expect(v).toBeLessThanOrEqual(Sex.FEMALE)
        })
    })
});