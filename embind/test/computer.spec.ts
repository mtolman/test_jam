import {
    cmakeSystemName,
    cpuArchitecture,
    fileExtension,
    mimeMap,
    mimeType,
    operatingSystem
} from "../src/generators/computer";
import {range} from "../src/common";

describe('computer', function () {
    it('cmake', () => {
        const gen = cmakeSystemName()
        range(100).forEach(r => expect(gen.create(r)).toMatch(/Windows|Darwin|Linux/))
    })

    it('cpuArch', () => {
        const gen = cpuArchitecture()
        range(100).forEach(r => expect(gen.create(r)).toBeTruthy())
    })

    it('fileExt', () => {
        const gen = fileExtension()
        range(100).forEach(r => expect(gen.create(r)).toBeTruthy())
    })

    it('mimeMap', () => {
        const gen = mimeMap()
        range(100).forEach(r => {
            expect(gen.create(r).mime).toBeTruthy()
            expect(gen.create(r).fileExt).toBeTruthy()
        })
    })

    it('mimeType', () => {
        const gen = mimeType()
        range(100).forEach(r => expect(gen.create(r)).toBeTruthy())
    })

    it('os', () => {
        const gen = operatingSystem()
        range(100).forEach(r => expect(gen.create(r)).toBeTruthy())
    })
})