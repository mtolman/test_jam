import {uint, uint16, uint32, uint8, int, int16, int32, int8, number, f64, f32} from "../src/generators/numbers";
import {final, range} from "../src/common";

function expectInRange(val: number, min: number, max: number) {
    expect(val).toBeGreaterThanOrEqual(min)
    expect(val).toBeLessThanOrEqual(max)
}

describe('ints', function () {
    describe.each([int, int32, int16, int8])
    ('int %p', (f) => {
        const max = (f.name === int8.name) ? 127 : 1000
        const min = (f.name === int8.name) ? -128 : -1000

        const gen1 = f()
        const gen2 = f({min: 0, max})
        const gen3 = f({min, max: 0})
        const gen4 = f({min, max: -5})
        const gen5 = f({min: 5, max})
        it('can generate', () => {
            range(1000).forEach(s => {
                expect(gen1.create(s)).toBe(gen1.create(s) | 0)
                expectInRange(gen2.create(s), 0, max)
                expectInRange(gen3.create(s), min, 0)
                expectInRange(gen4.create(s), min, -5)
                expectInRange(gen5.create(s), 5, max)
            })
        })

        it('can simplify', () => {
            range(100).forEach(s => {
                expect(final(gen1.simplify(s))).toBe(0)
                expect(final(gen2.simplify(s))).toBe(0)
                expect(final(gen3.simplify(s))).toBe(0)
                expect(final(gen4.simplify(s))).toBe(-5)
                expect(final(gen5.simplify(s))).toBe(5)
            })
        })
    })

    describe.each([uint, uint32, uint16, uint8])
    ('unsigned int %p', (f) => {
        const max = (f.name === uint8.name) ? 255 : 1000
        const min = 0

        const gen1 = f()
        const gen2 = f({min, max: 0})
        const gen3 = f({min: 5, max})
        it('can generate', () => {
            range(1000).forEach(s => {
                expect(gen1.create(s)).toBe(gen1.create(s) | 0)
                expectInRange(gen2.create(s), min, 0)
                expectInRange(gen3.create(s), 5, max)
            })
        })

        it('can simplify', () => {
            range(100).forEach(s => {
                expect(final(gen1.simplify(s))).toBe(0)
                expect(final(gen2.simplify(s))).toBe(0)
                expect(final(gen3.simplify(s))).toBe(5)
            })
        })
    })
});


describe('floats', function () {
    describe.each([number, f64, f32])
    ('floating point %p', (f) => {
        const gen1 = f({allowInfinity: false, allowNan: false})
        const gen2 = f({allowInfinity: false, allowNan: false, min: 0, max: 1000})
        const gen3 = f({allowInfinity: false, allowNan: false, min: -1000, max: 0})
        const gen4 = f({allowInfinity: false, allowNan: false, min: -1000, max: -5})
        const gen5 = f({allowInfinity: false, allowNan: false, min: 5, max: 1000})

        it('can generate', () => {
            range(1000).forEach(s => {
                expect(typeof gen1.create(s)).toEqual('number')
                expectInRange(gen2.create(s), 0, 1000)
                expectInRange(gen3.create(s), -1000, 0)
                expectInRange(gen4.create(s), -1000, -5)
                expectInRange(gen5.create(s), 5, 1000)
            })
        })

        it('can simplify', () => {
            range(100).forEach(s => {
                expect(final(gen1.simplify(s))).toBeCloseTo(0, 5)
                expect(final(gen2.simplify(s))).toBeCloseTo(0, 5)
                expect(final(gen3.simplify(s))).toBeCloseTo(0, 5)
                expect(final(gen4.simplify(s))).toBeCloseTo(-5, 5)
                expect(final(gen5.simplify(s))).toBeCloseTo(5, 5)
            })
        })
    })
});
