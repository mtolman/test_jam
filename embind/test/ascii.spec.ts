import {charsets, str} from "../src/generators/ascii";
import {range} from "../src/common";

const numIters = 100

const regexes: {[key: string]: RegExp} = {
    any: /[\x00-\x7f]+/,
    alpha: /[a-z]+/i,
    alphaUpper: /[A-Z]+/,
    alphaLower: /[a-z]+/,
    alphaNumeric: /[a-z0-9]+/i,
    alphaNumericUpper: /[A-Z0-9]+/,
    alphaNumericLower: /[a-z0-9]+/,
    binary: /[01]+/,
    bracket: /[(){}\[\]<>]+/,
    control: /[\x00-\x1f\x7f]+/,
    domainPiece: /[a-z0-9\-]+/,
    hex: /[a-f0-9]+/i,
    hexLower: /[a-f0-9]+/,
    hexUpper: /[A-F0-9]+/,
    htmlNamed: /[!-,.-/:-@\[-`{-~]+/,
    math: /[\s!%(->^A-Za-z]+/,
    numeric: /\d+/,
    octal: /[0-7]+/,
    printable: /[\s!-~]+/,
    quote: /["'`]+/,
    safePunctuation: /[,.]+/,
    sentencePunctuation: /[,-."?:;'-)]+/,
    symbol: /[!-/:-@\[-`{-~]+/,
    text: /[\s,-."?:;'-)a-zA-Z0-9]+/,
    urlUnencodedChars: /[a-zA-Z0-9+\-]+/,
    urlUnencodedHashChars: /[a-zA-Z0-9+\-.?=/&]+/,
    whitespace: /\s+/,
}

describe('generate', () => {
    it.each(Object.entries(charsets).map(([n, v]) => ({name: n, charset: v})))
    ('$name can generate', ({name, charset}) => {
        const opts = {minLen: 1, charset: charset }
        const gen = str(opts)
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(regexes[name]))
    })
})

describe('simplify', () => {
    it.each(Object.entries(charsets).map(([n, v]) => ({name: n, charset: v})))
    ('$name can simplify', ({name, charset}) => {
        const opts = {minLen: 0, charset }
        const gen = str(opts)
        range(numIters / 10).forEach(s => {
            let final = ""
            for (const val of gen.simplify(s)) {
                if (val.length > 0) {
                    expect(val).toMatch(regexes[name])
                }
                final = val
            }
            expect(final).toEqual("")
        })
    })
})