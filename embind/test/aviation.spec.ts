import {range} from "../src/common";
import {aircraftTypes, airlines, airplanes, airports} from "../src/generators/aviation";


describe('aviation', function () {
    it('can create types', () => {
        const gen = aircraftTypes()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c).toBeTruthy()
        })
    })

    it('can create airlines', () => {
        const gen = airlines()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c.name).toBeTruthy()
            expect(c.iata).toBeTruthy()
            expect(c.icao).toBeTruthy()
        })
    })

    it('can create airplanes', () => {
        const gen = airplanes()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c.name).toBeTruthy()
            expect(c.iata).toBeTruthy()
            expect(c.icao).toBeTruthy()
        })
    })

    it('can create airports', () => {
        const gen = airports()
        range(300).forEach(s => {
            const c = gen.create(s)
            expect(c.name).toBeTruthy()
            expect(c.iata).toBeTruthy()
            expect(c.icao).toBeTruthy()
        })
    })
});
