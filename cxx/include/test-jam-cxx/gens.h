#pragma once

#include "gens/ascii.h"
#include "gens/aviation.h"
#include "gens/color.h"
#include "gens/computer.h"
#include "gens/financial.h"
#include "gens/internet.h"
#include "gens/malicious.h"
#include "gens/numbers.h"
#include "gens/person.h"
#include "gens/utils.h"
#include "gens/world.h"
