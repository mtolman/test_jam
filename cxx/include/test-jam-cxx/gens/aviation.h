#pragma once

#include "common.h"
#include <string>

namespace test_jam::gens::aviation {
    common::no_shrink_generator<std::string> aircraft_types();

    struct record {
        std::string name;
        std::string iata;
        std::string icao;
    };

    common::no_shrink_generator<record> airlines();
    common::no_shrink_generator<record> airplanes();
    common::no_shrink_generator<record> airports();
}
