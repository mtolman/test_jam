#pragma once

#include "common.h"
#include <string>

namespace test_jam::gens::computer {
    struct file_mime_map {
        std::string mimeType;
        std::string fileExtension;
    };

    common::no_shrink_generator<std::string> cmake_system_names();
    common::no_shrink_generator<std::string> cpu_architectures();
    common::no_shrink_generator<std::string> file_extensions();
    common::no_shrink_generator<file_mime_map> file_mime_mappings();
    common::no_shrink_generator<std::string> file_types();
    common::no_shrink_generator<std::string> mime_types();
    common::no_shrink_generator<std::string> operating_systems();
}
