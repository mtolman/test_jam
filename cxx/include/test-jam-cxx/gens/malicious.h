#pragma once

#include "common.h"
#include <string>

namespace test_jam::gens::malicious {
    auto sql_injection() -> common::no_shrink_generator<std::string>;
    auto xss() -> common::no_shrink_generator<std::string>;
    auto format_injection() -> common::no_shrink_generator<std::string>;
    auto edge_cases() -> common::no_shrink_generator<std::string>;
}
