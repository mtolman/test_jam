#pragma once

#include "common.h"
#include <string>

namespace test_jam::gens::ascii {
    struct char_set {
        std::string chars;
    };

    extern const char_set any;

    struct str_options {
        char_set charSet = any;
        size_t minLen = 0;
        size_t maxLen = 500;
    };

    auto str(const str_options& opts = {}) -> common::shrink_state_generator<std::string, str_options>;

    extern const char_set alpha;
    extern const char_set alpha_lower;
    extern const char_set alpha_upper;
    extern const char_set alpha_numeric;
    extern const char_set alpha_numeric_lower;
    extern const char_set alpha_numeric_upper;
    extern const char_set binary;
    extern const char_set bracket;
    extern const char_set control;
    extern const char_set domain_piece;
    extern const char_set hex;
    extern const char_set hex_lower;
    extern const char_set hex_upper;
    extern const char_set html_named;
    extern const char_set math;
    extern const char_set numeric;
    extern const char_set octal;
    extern const char_set printable;
    extern const char_set quote;
    extern const char_set safe_punctuation;
    extern const char_set sentence_punctuation;
    extern const char_set symbol;
    extern const char_set text;
    extern const char_set url_unencoded_chars;
    extern const char_set url_unencoded_hash_chars;
    extern const char_set whitespace;
}
