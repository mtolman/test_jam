#pragma once

#include "common.h"
#include <limits>

namespace test_jam::gens::numbers {
    template<typename T>
    struct int_opts {
        T min = std::numeric_limits<T>::min();
        T max = std::numeric_limits<T>::max();
    };

    auto uint64(int_opts<uint64_t> opts = {}) -> common::shrink_state_generator<uint64_t, int_opts<uint64_t>>;
    auto uint32(int_opts<uint32_t> opts = {}) -> common::shrink_state_generator<uint32_t, int_opts<uint32_t>>;
    auto uint16(int_opts<uint16_t> opts = {}) -> common::shrink_state_generator<uint16_t, int_opts<uint16_t>>;
    auto uint8(int_opts<uint8_t> opts = {}) -> common::shrink_state_generator<uint8_t, int_opts<uint8_t>>;

    auto int64(int_opts<int64_t> opts = {}) -> common::shrink_state_generator<int64_t, int_opts<int64_t>>;
    auto int32(int_opts<int32_t> opts = {}) -> common::shrink_state_generator<int32_t, int_opts<int32_t>>;
    auto int16(int_opts<int16_t> opts = {}) -> common::shrink_state_generator<int16_t, int_opts<int16_t>>;
    auto int8(int_opts<int8_t> opts = {}) -> common::shrink_state_generator<int8_t, int_opts<int8_t>>;

    template<typename T>
    struct float_opts {
        T min = std::numeric_limits<T>::min();
        T max = std::numeric_limits<T>::max();
        bool allowNan = true;
        bool allowInf = true;
    };

    auto f64(float_opts<double> opts = {}) -> common::shrink_state_generator<double, float_opts<double>>;

    auto f32(float_opts<float> opts = {}) -> common::shrink_state_generator<float, float_opts<float>>;
}
