#pragma once

#include <array>
#include "common.h"

namespace test_jam::gens::collections {
    template<size_t N, typename Gen>
    auto array(const Gen& generator) -> common::shrink_generator<common::impl::GenCreationType<Gen>::type>;
}
