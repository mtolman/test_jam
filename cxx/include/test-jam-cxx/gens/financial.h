#pragma once

#include <string>
#include <vector>
#include "common.h"

namespace test_jam::gens::financial {
    auto account_names(const common::complexity_range& complexity = {}) -> common::no_shrink_complexity_generator<std::string>;

    struct credit_card_network {
        std::string id;
        std::string name;
        std::vector<std::string> starts;
        std::vector<int32_t> lengths;
        unsigned cvvLength;
    };
    auto credit_card_networks() -> common::no_shrink_generator<credit_card_network>;

    struct currency {
        std::string name;
        std::string isoCode;
        std::string symbol;
    };
    auto currencies() -> common::no_shrink_generator<currency>;

    auto transaction_types() -> common::no_shrink_generator<std::string>;

    struct credit_card {
        credit_card_network network;
        std::string number;
        std::string cvv;
        struct exp_date {
            uint32_t year;
            uint16_t month;
        } expirationDate;
    };

    struct credit_card_options {
        credit_card::exp_date minExpDate = {.year=2000, .month=1};
        credit_card::exp_date maxExpDate = {.year=2200, .month=12};
    };

    auto credit_cards(const credit_card_options& opts = {}) -> common::no_shrink_state_generator<credit_card, credit_card_options>;

    auto account_numbers(size_t length = 15) -> common::no_shrink_state_generator<std::string, size_t>;

    auto pins(size_t length = 4) -> common::no_shrink_state_generator<std::string, size_t>;

    struct amount_options {
        double max = 10000.0;
        double min = 0.0;
        unsigned decimalPlaces = 2;
        std::string decimalSeparator = ".";
        std::string symbol = {};
        bool prefixSymbol = true;
    };

    auto amounts(const amount_options& options = {}) -> common::no_shrink_state_generator<std::string, amount_options>;

    auto bics(bool includeBranchCode = false) -> common::no_shrink_state_generator<std::string, bool>;

    struct masked_number_options {
        size_t unMaskedLen = 4;
        size_t maskedLen = 4;
        char maskedChar = '*';
    };

    auto masked_numbers(const masked_number_options& opts = {}) -> common::no_shrink_state_generator<std::string, masked_number_options>;
}
