#pragma once

#include "../defs.h"

#include <functional>
#include <vector>
#include <optional>
#include <ostream>
#include <tuple>
#include <string>

namespace test_jam::gens::common {
    enum class Complexity {
        RUDIMENTARY,
        BASIC,
        INTERMEDIATE,
        ADVANCED,
        COMPLEX,
        EDGE_CASE,
    };

    struct complexity_range {
        Complexity minComplexity = Complexity::RUDIMENTARY;
        Complexity maxComplexity = Complexity::EDGE_CASE;
    };

    template<typename T>
    struct sequence_iterator {
        using iterator_category = std::input_iterator_tag;
        using difference_type   = std::ptrdiff_t;
        using value_type        = T;
        using pointer           = const value_type*;
        using reference         = const value_type&;

        using ResType = T;

        sequence_iterator(std::function<std::optional<ResType> ()> next) : nextFunc(next), lastValue(nextFunc()) {}
        sequence_iterator() : sequence_iterator([](){return std::nullopt;}) {}

        reference operator*() const { return *lastValue; }
        pointer operator->() { return &*lastValue; }

        // Prefix increment
        sequence_iterator& operator++() { lastValue = nextFunc(); return *this; }

        // Postfix increment
        sequence_iterator operator++(int) { auto tmp = *this; ++(*this); return tmp; }

        friend bool operator== (const sequence_iterator& a, const sequence_iterator& b) {
            if (!a.lastValue && !b.lastValue) {
                return true;
            }
            return false;
        };
        friend bool operator!= (const sequence_iterator& a, const sequence_iterator& b) { return !(a == b); };
    private:
        std::function<std::optional<ResType> ()> nextFunc;
        std::optional<T> lastValue;
    };

    template<typename T>
    struct sequence_next {
        bool present;
        T value;
    };

    template<typename T>
    struct sequence {
        using ResType = T;

        using iterator = sequence_iterator<T>;
        using const_iterator = sequence_iterator<T>;
        auto begin() const -> iterator { return iterator{next}; }
        auto end() const -> iterator { return iterator{}; }

        auto next_value() const { return next(); }
        auto next_bind() const {
            const auto v = next();
            if (v) {
                return sequence_next<T>{true, *v};
            }
            return sequence_next<T>{false};
        }

        sequence(std::function<std::optional<ResType> ()> next) : next(next) {}
        sequence() : next(+[](){return std::nullopt;}){}
    private:
        std::function<std::optional<ResType> ()> next;
    };

    /**
     * @tparam T
     */
    template<typename T>
    struct no_shrink_generator {
        using Gen = T (*)(TEST_JAM_SEED_TYPE);
        Gen genFunc;

        [[nodiscard]] auto create(TEST_JAM_SEED_TYPE seed) const -> T {
            return genFunc(seed);
        }
        [[nodiscard]] auto simplify(TEST_JAM_SEED_TYPE seed) const -> sequence<T> { return {[]() -> std::optional<T> {return std::nullopt;}}; }
    };

    /**
     * @tparam T
     */
    template<typename T, typename S>
    struct no_shrink_state_generator {
        using Gen = T (*)(const S&, TEST_JAM_SEED_TYPE);
        S state;
        Gen genFunc;

        [[nodiscard]] auto create(TEST_JAM_SEED_TYPE seed) const -> T {
            return genFunc(state, seed);
        }
        [[nodiscard]] auto simplify(TEST_JAM_SEED_TYPE seed) const -> sequence<T> { return {[]() -> std::optional<T> {return std::nullopt;}}; }
    };

    /**
     * @tparam T
     */
    template<typename T>
    using no_shrink_complexity_generator = no_shrink_state_generator<T, complexity_range>;

    /**
     * @tparam T
     */
    template<typename T>
    struct shrink_generator {
        using Gen = T (*)(TEST_JAM_SEED_TYPE);
        Gen genFunc;

        using Simp = sequence<T> (*)(TEST_JAM_SEED_TYPE);
        Simp simplifyFunc;

        [[nodiscard]] auto create(TEST_JAM_SEED_TYPE seed) const -> T { return genFunc(seed); }
        [[nodiscard]] auto simplify(TEST_JAM_SEED_TYPE seed) const -> sequence<T> { return simplifyFunc(seed); }
    };

    /**
     * @tparam T
     */
    template<typename T, typename S>
    struct shrink_state_generator {
        S state;

        using Gen = T (*)(const S&, TEST_JAM_SEED_TYPE);
        Gen genFunc;

        using Simp = sequence<T> (*)(const S&, TEST_JAM_SEED_TYPE);
        Simp simplifyFunc;

        [[nodiscard]] auto create(TEST_JAM_SEED_TYPE seed) const -> T { return genFunc(state, seed); }
        [[nodiscard]] auto simplify(TEST_JAM_SEED_TYPE seed) const -> sequence<T> { return simplifyFunc(state, seed); }
    };

    /**
     * @tparam T
     */
    template<typename T>
    using shrink_complexity_generator = shrink_state_generator<T, complexity_range>;


    namespace impl {
        template<typename Type, typename = void>
        struct HasCreate : std::false_type {
        };

        template<typename Type>
        struct HasCreate<Type, typename std::enable_if_t<std::is_member_function_pointer_v<decltype(&Type::create)>>>
                : std::true_type {
        };

        template<typename Type, typename = void>
        struct HasSimplify : std::false_type {
        };

        template<typename Type>
        struct HasSimplify<Type, typename std::enable_if_t<std::is_member_function_pointer_v<decltype(&Type::simplify)>>>
                : std::true_type {
        };

        template<typename Type, typename = void>
        struct HasGeneratorSimplify : std::false_type {
        };

        template<typename Type>
        struct HasGeneratorSimplify<Type, typename std::enable_if_t<std::is_same_v<sequence<decltype(Type{}.create(
                TEST_JAM_SEED_TYPE{}))>, decltype(Type{}.simplify(TEST_JAM_SEED_TYPE{}))>>> : HasSimplify<Type> {
        };

        template<typename Type, typename = void>
        struct HasTupleSize : std::false_type {
        };

        template<typename Type>
        struct HasTupleSize<Type, typename std::enable_if_t<std::tuple_size<Type>::value>> : std::true_type {
        };

        template<typename Type, typename = void>
        struct HasTupleGet : std::false_type {
        };

        template<typename Type>
        struct HasTupleGet<Type, typename std::enable_if_t<std::get<0>(Type{})>> : std::true_type {
        };

        template<typename Gen>
        struct GenCreationType {
            Gen g;
            using type = decltype(g.create({}));
        };

        template<class Class>
        struct HasOstreamOp {
            template<class V>
            static auto test(V*) -> decltype(std::declval<std::ostream&>() << std::declval<V>());
            template<typename>
            static auto test(...) -> std::false_type;

            using type = typename std::is_same<std::ostream&, decltype(test<Class>(nullptr))>::type;
        };
    }

    template<typename T>
    constexpr bool is_generator = impl::HasCreate<T>::value && impl::HasSimplify<T>::value && impl::HasGeneratorSimplify<T>::value;

    template<typename T>
    constexpr bool is_tuple = impl::HasTupleSize<T>::value && impl::HasTupleGet<T>::value;

    template<typename T>
    constexpr bool is_ostream_print = impl::HasOstreamOp<T>::type::value;

    static_assert(is_generator<no_shrink_generator<int>>, "'no_shrink_generator' must be a generator");
    static_assert(is_generator<no_shrink_complexity_generator<int>>, "'no_shrink_complexity_generator' must be a generator");
    static_assert(is_generator<shrink_generator<int>>, "'shrink_generator' m");

    static_assert(is_ostream_print<std::string>);
    static_assert(is_ostream_print<int32_t>);
}
