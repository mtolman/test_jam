#pragma once

#include "./common.h"

namespace test_jam::gens::person {
    enum class Sex {
        MALE,
        FEMALE
    };

    struct Person {
        Sex sex;
        std::string fullName;
        std::string givenName;
        std::string surname;
        std::optional<std::string> prefix;
        std::optional<std::string> suffix;
    };

    auto person(common::complexity_range complexity = {}) -> common::no_shrink_complexity_generator<Person>;
    auto given_name(common::complexity_range complexity = {}) -> common::no_shrink_complexity_generator<std::string>;
    auto surname(common::complexity_range complexity = {}) -> common::no_shrink_complexity_generator<std::string>;
    auto full_name(common::complexity_range complexity = {}) -> common::no_shrink_complexity_generator<std::string>;
    auto prefix(common::complexity_range complexity = {}) -> common::no_shrink_complexity_generator<std::optional<std::string>>;
    auto suffix(common::complexity_range complexity = {}) -> common::no_shrink_complexity_generator<std::optional<std::string>>;
    auto sex() -> common::no_shrink_generator<Sex>;
}
