#pragma once

#include "common.h"
#include <random>
#include <tuple>
#include <variant>

namespace test_jam::gens::utils {
// Forward declarations confuse MSVC
#ifndef WIN32
    template<typename Result, typename... Generators>
    auto combine(const Generators &...generators) -> common::shrink_state_generator<Result, std::tuple<Generators...>>;

    template<typename Generator>
    auto filter(
            const Generator& generator,
            const std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& filter
    ) -> common::shrink_state_generator<decltype(std::declval<Generator>().create(size_t{})), std::tuple<
            Generator,
            std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>
    >>;

    template<typename T, typename Generator>
    auto map(
            const Generator& generator,
            const std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& mapper
    ) -> common::shrink_state_generator<T, std::tuple<
            Generator,
            std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>
    >>;

    template<typename T>
    auto just(const T& value) -> common::no_shrink_state_generator<T, T>;

    template<typename... Generator>
    auto one_of(const Generator & ...generator);
#endif

    namespace impl {
        template<typename Tuple, size_t... Index>
        auto get_values(std::default_random_engine& rnd, const Tuple& tuple, std::index_sequence<Index...>) {
            return std::make_tuple(std::get<Index>(tuple).create(rnd())...);
        }

        template<typename Result, typename Tuple, size_t... Index>
        auto combine(std::default_random_engine& rnd, const Tuple& tuple, std::index_sequence<Index...>) {
            return Result{(std::get<Index>(tuple).create(rnd()))...};
        }

        template<typename Tuple, size_t... Index>
        auto simplifiers(std::default_random_engine& rnd, const Tuple& tuple, std::index_sequence<Index...>) {
            return std::make_tuple(std::get<Index>(tuple).simplify(rnd())...);
        }

        template<typename Tuple, typename SimplifyTuple, size_t... Index>
        auto simplifyValue(const Tuple& lastValues, const SimplifyTuple& simplifiers, std::index_sequence<Index...>) {
            return std::make_tuple(std::get<Index>(simplifiers).next_value().value_or(std::get<Index>(lastValues))...);
        }

        template<typename Result, typename Tuple, size_t... Index>
        auto make_result(const Tuple& tuple, std::index_sequence<Index...>) {
            return Result{(std::get<Index>(tuple))...};
        }
    }

    template<typename Result, typename... Generators>
    auto combine(const Generators &...generators) -> common::shrink_state_generator<Result, std::tuple<Generators...>> {
        using Tuple = std::tuple<Generators...>;
        const auto tuple = std::make_tuple(generators...);

        return {
            .state=tuple,
            .genFunc=+[](const Tuple& tuple, TEST_JAM_SEED_TYPE seed) {
                std::default_random_engine rnd(static_cast<uint64_t>(seed));
                return impl::combine<Result>(rnd, tuple, std::make_index_sequence<std::tuple_size<Tuple>::value>{});
            },
            .simplifyFunc=+[](const Tuple& tuple, TEST_JAM_SEED_TYPE seed) -> common::sequence<Result> {
                std::default_random_engine rnd(static_cast<uint64_t>(seed));
                auto values = impl::get_values(rnd, tuple, std::make_index_sequence<std::tuple_size<Tuple>::value>{});

                rnd = std::default_random_engine(static_cast<uint64_t>(seed));
                auto sequences = impl::simplifiers(rnd, tuple, std::make_index_sequence<std::tuple_size<Tuple>::value>{});
                return {
                    [sequences, lastValue=values]() mutable {
                        lastValue = impl::simplifyValue(lastValue, sequences, std::make_index_sequence<std::tuple_size<Tuple>::value>{});
                        return impl::make_result<Result>(lastValue, std::make_index_sequence<std::tuple_size<Tuple>::value>{});
                    }
                };
            }
        };
    }

    template<typename Generator>
    auto filter(
            const Generator& generator,
            const std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& filter
    ) -> common::shrink_state_generator<decltype(std::declval<Generator>().create(size_t{})), std::tuple<
            Generator,
            std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>
        >> {
        static_assert(common::is_generator<Generator>, "Can only filter generators");
        using State = std::tuple<
                Generator,
                std::function<bool (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>
        >;
        using Res = decltype(std::declval<Generator>().create(size_t{}));
        return {
            .state=std::make_tuple(generator, filter),
            .genFunc=+[](const State& state, TEST_JAM_SEED_TYPE seed) -> Res {
                for (auto i = static_cast<size_t>(seed); ; ++i) {
                    auto val = std::get<0>(state).create(i);
                    if (std::get<1>(state)(val)) {
                        return val;
                    }
                }
            },
            .simplifyFunc=+[](const State& state, TEST_JAM_SEED_TYPE seed) -> common::sequence<Res> {
                for (; !std::get<1>(state)(std::get<0>(state).create(seed)); ++seed) {}
                return {
                    [filter=std::get<1>(state), seq=std::get<0>(state).simplify(seed)]() mutable -> std::optional<Res> {
                        auto v = seq.next_value();
                        while (v && !filter(*v)) {
                            v = seq.next_value();
                        }
                        return v;
                    }
                };
            }
        };
    }

    template<typename Generator>
    auto map(
            const Generator& generator,
            const std::function<decltype(std::declval<Generator>().create(size_t{})) (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& mapper
    ) -> common::shrink_state_generator<
            decltype(std::declval<Generator>().create(size_t{})),
            std::tuple<Generator, std::function<decltype(std::declval<Generator>().create(size_t{})) (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>>
    > {
        using Func = std::function<decltype(std::declval<Generator>().create(size_t{})) (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>;
        static_assert(common::is_generator<Generator>, "Can only map generators");
        using Res = decltype(std::declval<Generator>().create(size_t{}));
        return {
                .state=std::make_tuple(generator, mapper),
                .genFunc=+[](const std::tuple<Generator, Func>& state, TEST_JAM_SEED_TYPE seed) -> Res {
                    return std::get<1>(state)(std::get<0>(state).create(seed));
                },
                .simplifyFunc=+[](const std::tuple<Generator, Func>& state, TEST_JAM_SEED_TYPE seed) -> common::sequence<Res> {
                    const auto generator = std::get<0>(state);
                    const auto mapper = std::get<1>(state);
                    return {
                            [mapper, seq=generator.simplify(seed)]() mutable -> std::optional<Res> {
                                auto v = seq.next_value();
                                if (v.has_value()) {
                                    return mapper(*v);
                                }
                                return std::nullopt;
                            }
                    };
                }
        };
    }

    template<typename T, typename Generator>
    auto map(
            const Generator& generator,
            const std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>& mapper
    ) -> common::shrink_state_generator<T, std::tuple<
            Generator,
            std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>
        >> {
        using State = std::tuple<
                Generator,
                std::function<T (std::add_lvalue_reference_t<std::add_const_t<decltype(std::declval<Generator>().create(size_t{}))>>)>
        >;
        static_assert(common::is_generator<Generator>, "Can only map generators");
        using Res = T;
        return {
                .state=std::make_tuple(generator, mapper),
                .genFunc=+[](const State& state, TEST_JAM_SEED_TYPE seed) -> Res {
                    return std::get<1>(state)(std::get<0>(state).create(seed));
                },
                .simplifyFunc=+[](const State& state, TEST_JAM_SEED_TYPE seed) -> common::sequence<Res> {
                    return {
                            [mapper=std::get<1>(state), seq=std::get<0>(state).simplify(seed)]() mutable -> std::optional<Res> {
                                auto v = seq.next_value();
                                if (v.has_value()) {
                                    return mapper(*v);
                                }
                                return std::nullopt;
                            }
                    };
                }
        };
    }

    template<typename T>
    auto just(const T& value) -> common::no_shrink_state_generator<T, T> {
        return {
            .state=value,
            .genFunc=+[](const T& val, TEST_JAM_SEED_TYPE seed) {
                return val;
            }
        };
    }

    namespace impl::one_of {
        template <typename T, typename... List>
        struct IsContained;

        template <typename T, typename Head, typename... Tail>
        struct IsContained<T, Head, Tail...>
        {
            static constexpr bool value = std::is_same_v<T, Head> || IsContained<T, Tail...>::value;
        };

        template <typename T>
        struct IsContained<T>
        {
            static constexpr bool value = false;
        };

        template<bool V, typename T, typename ...R>
        struct VariantOf;

        template<typename T, typename ...R>
        struct VariantOf<true, T, R...> {
            using type = std::variant<R...>;
        };

        template<typename T, typename ...R>
        struct VariantOf<false, T, R...> {
            using type = std::variant<T, R...>;
        };

        template<typename... T>
        struct VariantOr;

        template<typename T>
        struct VariantOr<T>{
            using type = T;
        };

        template<typename T>
        struct VariantOr<T, T>{
            using type = T;
        };

        template<typename T, typename... R>
        struct VariantOr<T, std::variant<R...>> {
            using type = typename VariantOf<IsContained<T, R...>::value, T, R...>::type;
        };

        template<typename T, typename R>
        struct VariantOr<T, R>{
            using type = std::variant<T, R>;
        };

        template<typename... Gen>
        struct OneOfCreateType;

        template<>
        struct OneOfCreateType<> {
            using type = void;
        };

        template<typename Gen>
        struct OneOfCreateType<Gen> {
            Gen g;
            using type = decltype(g.create({}));
        };

        template<typename Gen, typename... Gens>
        struct OneOfCreateType<Gen, Gens...> {
            Gen g;
            using my_type = decltype(g.create({}));
            using next_type = typename OneOfCreateType<Gens...>::type;
            using type = typename VariantOr<my_type, next_type>::type;
        };

        template<size_t Index, typename Result, typename ...Generator>
        auto create_insert(std::optional<Result>& res, size_t index, size_t seed, const std::tuple<Generator...>& generators) -> bool {
            if (index == Index) {
                res = Result{std::get<Index>(generators).create(seed)};
            }
            return true;
        }

        template<typename... Generator, size_t... Index>
        auto create(size_t genIndex, size_t seed, const std::tuple<Generator...>& generators, std::index_sequence<Index...>)
            -> typename impl::one_of::OneOfCreateType<Generator...>::type
        {
            using Tuple = std::tuple<Generator...>;
            using Result = typename impl::one_of::OneOfCreateType<Generator...>::type;
            auto res = std::optional<Result>{};
            const auto _ = (create_insert<Index>(res, genIndex, seed, generators) && ...);
            return res.value();
        }

        template<size_t Index, typename Result, typename ...Generator>
        auto simplify_insert(std::optional<common::sequence<Result>>& res, size_t index, size_t seed, const std::tuple<Generator...>& generators) -> bool {
            if (index == Index) {
                const auto& gen = std::get<Index>(generators);
                const auto seq = gen.simplify(seed);
                res = common::sequence<Result>{
                    [seq=seq]() -> std::optional<Result> {
                        auto v = seq.next_value();
                        if (v) {
                            return Result{*v};
                        }
                        return std::nullopt;
                    }
                };
            }
            return true;
        }

        template<typename... Generator, size_t... Index>
        auto simplify(size_t genIndex, size_t seed, const std::tuple<Generator...>& generators, std::index_sequence<Index...>) {
            using Result = typename impl::one_of::OneOfCreateType<Generator...>::type;
            auto res = std::optional<common::sequence<Result>>{};
            const auto _ = (simplify_insert<Index, Result, Generator...>(res, genIndex, seed, generators) && ...);
            return *res;
        }
    }

    namespace impl {
      template<typename... Generator>
      auto gen_index(size_t seed) {
        static_assert(sizeof...(Generator) > 0, "Make sure you pass your generator types!");
        using Generators = std::tuple<Generator...>;
        return seed % (std::tuple_size_v<Generators>);
      };

      template<typename... Generator>
      auto adjusted_seed(size_t seed) {
        static_assert(sizeof...(Generator) > 0, "Make sure you pass your generator types!");
        using Generators = std::tuple<Generator...>;
        return seed % (std::tuple_size_v<Generators>);
      };
    }

    template<typename... Generator>
    auto one_of(const Generator & ...generator) {
        using Result = typename impl::one_of::OneOfCreateType<Generator...>::type;
        using Generators = std::tuple<Generator...>;
        auto generators = std::make_tuple(generator...);
        return common::shrink_state_generator<Result, Generators> {
            .state=generators,
            .genFunc=+[](const Generators& generators, TEST_JAM_SEED_TYPE seed) -> Result {
                return impl::one_of::create(impl::gen_index<Generator...>(seed), impl::adjusted_seed<Generator...>(seed), generators, std::make_index_sequence<std::tuple_size<Generators>::value>{});
            },
            .simplifyFunc=+[](const Generators& generators, TEST_JAM_SEED_TYPE seed) -> common::sequence<Result> {
                return impl::one_of::simplify(impl::gen_index<Generator...>(seed), impl::adjusted_seed<Generator...>(seed), generators, std::make_index_sequence<std::tuple_size<Generators>::value>{});
            }
        };
    }
}
