#pragma once

#include "common.h"
#include <string>
#include <optional>

namespace test_jam::gens::world {
    struct country {
        std::string name;
        std::optional<std::string> genc;
        std::optional<std::string> iso2;
        std::optional<std::string> iso3;
        std::optional<std::string> isoNum;
        std::optional<std::string> stanag;
        std::optional<std::string> tld;
    };

    auto countries() -> common::no_shrink_generator<country>;
    auto timezones() -> common::no_shrink_generator<std::string>;
}
