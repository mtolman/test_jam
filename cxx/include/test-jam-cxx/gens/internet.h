#pragma once

#include "common.h"
#include <string>

namespace test_jam::gens::internet {
    auto top_level_domains() -> common::no_shrink_generator<std::string>;
    auto ipv4_addresses() -> common::no_shrink_generator<std::string>;

    enum Ipv6Flags : unsigned {
        IPV6_NONE = 0x0,
        IPV6_ALLOW_IPV4 = 0x1 << 0,
        IPV6_ALLOW_FOLDING = 0x1 << 1,
        IPV6_SHRINK_ZEROS = 0x1 << 2,
    };

    auto ipv6_addresses(unsigned flags = ~0u) -> common::shrink_state_generator<std::string, unsigned>;

    struct domain_options {
        size_t minLen = 3;
        size_t maxLen = 63;
    };

    auto domains(const domain_options& options = {}) -> common::shrink_state_generator<std::string, domain_options>;

    enum UrlFlags : unsigned {
        URL_NONE = 0x0,
        // IPv6 options come first so that we can just mask these values and send them to our ip6 generator
        URL_ALLOW_IPV6_IPV4 = 0x1 << 0,
        URL_ALLOW_IPV6_FOLDING = 0x1 << 1,
        URL_IPV6_SHRINK_ZEROS = 0x1 << 2,

        // Options specific to URL generation
        URL_ALLOW_IPV6_HOST = 0x1 << 3,
        URL_ALLOW_IPV4_HOST = 0x1 << 4,
        URL_ALLOW_ENCODED_CHARS = 0x1 << 5,
        URL_ALLOW_HASH = 0x1 << 6,
        URL_ALLOW_RANDOM_PATH = 0x1 << 7,
        URL_ALLOW_USERNAME = 0x1 << 8,
        URL_ALLOW_PASSWORD = 0x1 << 9,
        URL_ALLOW_QUERY = 0x1 << 10,
        URL_ALLOW_PORT = 0x1 << 11,
    };

    struct url_options {
        unsigned flags = ~0u;
        size_t minLen = 20;
        size_t maxLen = 255;
        std::vector<std::string> possibleSchemas = {};
        domain_options domainOptions = {};
    };

    auto urls(const url_options& options = {}) -> common::shrink_state_generator<std::string, url_options>;

    enum EmailFlags : unsigned {
        EMAIL_NONE = 0x0,
        // IPv6 options come first so that we can just mask these values and send them to our ip6 generator
        EMAIL_ALLOW_IPV6_IPV4 = 0x1 << 0,
        EMAIL_ALLOW_IPV6_FOLDING = 0x1 << 1,
        EMAIL_IPV6_SHRINK_ZEROS = 0x1 << 2,

        // Options specific to URL generation
        EMAIL_ALLOW_IPV6_HOST = 0x1 << 3,
        EMAIL_ALLOW_IPV4_HOST = 0x1 << 4,
        EMAIL_ALLOW_PLUS = 0x1 << 5,
        EMAIL_ALLOW_QUOTES = 0x1 << 6,
        EMAIL_ALLOW_SPECIALS = 0x1 << 7,
    };

    struct email_options {
        unsigned flags = ~0u;
        size_t minLen = 10;
        size_t maxLen = 90;
        domain_options domainOptions = {};
    };

    auto emails(const email_options& options = {}) -> common::shrink_state_generator<std::string, email_options>;
}
