#pragma once

#include "common.h"
#include <string>

namespace test_jam::gens::color {
    struct rgb_color {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    };

    struct rgba_color {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
        uint8_t alpha;
    };

    struct hsl_color {
        double hue;
        double saturation;
        double luminosity;
    };

    struct hsla_color {
        double hue;
        double saturation;
        double luminosity;
        double alpha;
    };

    struct named_color {
        std::string name;
        std::string hex;
        rgb_color rgb;
    };

    common::no_shrink_generator<named_color> named_colors();
    common::no_shrink_generator<rgb_color> rgb_colors();
    common::no_shrink_generator<rgba_color> rgba_colors();
    common::no_shrink_generator<hsl_color> hsl_colors();
    common::no_shrink_generator<hsla_color> hsla_colors();
}

