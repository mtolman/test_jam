#pragma once

// Note: You must include Doctest before including this file!
#ifndef TEST_CASE
#error "You must include Doctest prior to including the TestJame Doctest integration!"
#endif

#include <test-jam-cxx/test_jam.h>
#include <functional>
#include <map>
#include <sstream>
#include <iostream>

#define TEST_JAM_CAT_IMPL(s1, s2) s1##s2
#define TEST_JAM_CAT(s1, s2)      TEST_JAM_CAT_IMPL(s1, s2)
#ifdef __COUNTER__  // not standard and may be missing for some compilers
#define TEST_JAM_ANONYMOUS(x) TEST_JAM_CAT(x, __COUNTER__)
#else  // __COUNTER__
#define TEST_JAM_ANONYMOUS(x) TEST_JAM_CAT(x, __LINE__)
#endif

#define TEST_JAM_DOCTEST_ANONYMOUS(x) TEST_JAM_CAT(x, __LINE__)

// Don't use this namespace directly! Use the TEST_JAM_CASE macro instead!
namespace test_jam::impl::doctest_integration {
    template<typename ...Gens>
    bool check_suite(const test_jam::impl::checker<Gens...> &check,
                     const std::function<void(typename gens::common::impl::GenCreationType<Gens...>::type ...)> &test) {
        return check.run(test);
    }

    template<typename ...Gens>
    struct Case {
        using Test = test_jam::impl::RunMethod<Gens...>;
        std::string name;
        std::tuple<Gens...> gens;

        auto run(const test_jam::impl::RunMethod<Gens...> &test) const {
            return test_jam::property(name).with_generators(gens).run(test);
        }
    };

    template<typename ...Gens>
    struct Dummy {
        std::string name;
        std::tuple<Gens...> gens;
    };

    template<typename ...Gens>
    auto test_case(const std::string &name, Gens... gens) {
        return Case<Gens...>{name, std::make_tuple(gens...)};
    }
}// namespace prop_test::impl::doctest_integration

/**
 * Usage:
 *
 * ```cpp
 * // You don't have to use a test suite, but I prefer having test suites group my properties
 * TEST_SUITE("my test suite") {
 *      // This will create a TEST_CASE for a single TestJam property check
 *      TEST_JAM_CASE("my test case",
 *          // Generators to use here
 *          n::f32(), n::f32(), n::f32(), n::f32()
 *      )
 *      // test case definition
 *      // Note: You'll need a parameter for every generator listed above
 *      (float x1, float y1, float x2, float y2) {
 *          float resX, resY;
 *          vec2d::add(resX, resY, x1, y1, x2, y2);
 *          // Use the TEST_JAM_CHECK_* macros, not the CHECK_* macros
 *          TEST_JAM_CHECK_APPROX_EQ(resX, x1 + x2, 0.00001f);
 *          TEST_JAM_CHECK_APPROX_EQ(resY, y1 + y2, 0.00001f);
 *      };
 *  }
 * ```
 */
#define TEST_JAM_CASE(NAME, ...) \
const auto TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE) = test_jam::impl::doctest_integration::test_case(NAME, __VA_ARGS__); \
extern decltype(TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE))::Test TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE_FUNC);            \
TEST_CASE(NAME) {                \
    const auto res = TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE).run(TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE_FUNC));         \
    CHECK_EQ(res.state, test_jam::RunState::PASSED);                                                        \
    if (res.state != test_jam::RunState::PASSED) {                                                          \
        std::cerr << "TestJam Failure!\n\t"                                                                 \
                  << res.failureMessage                                                                     \
                  << "\n\t\tInput: "                                                                        \
                  << res.inputString                                                                        \
                  << "\n\t\tSimplified: "                                                                   \
                  << res.simplifiedInputString                                                              \
                  << "\n\t\tSimplified: "                                                                   \
                  << std::endl;  \
    }                            \
}                                \
decltype(TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE))::Test TEST_JAM_DOCTEST_ANONYMOUS(_TEST_JAM_CASE_FUNC) = []

