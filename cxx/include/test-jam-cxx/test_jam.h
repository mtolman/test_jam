#pragma once

#include "defs.h"
#include "gens.h"
#include <map>
#include <tuple>
#include <sstream>
#include <string>
#include "print.h"

namespace test_jam {
    class property;

    struct failed_case {
        bool error;
        std::string testName;
        std::string failureMessage;
        TEST_JAM_SEED_TYPE seed;
        std::string inputStr = {};
        std::string simplifiedInputStr = {};
    };

    struct passed_case {
        std::string testName;
    };

    class assert_failed_error: public std::runtime_error{
    public:
        assert_failed_error(const std::string& msg):runtime_error(msg){}
    };

    enum class RunState {
        PASSED,
        FAILED,
        ERRORED
    };

    namespace impl {
        template<typename ...Generators>
        using RunMethod = std::function<void (typename gens::common::impl::GenCreationType<Generators>::type ...)>;

        template<typename Generator, typename Predicate, size_t MaxShrinks = 8000>
        auto shrink(size_t index, const Generator& generator, const Predicate& predicate) {
            static_assert(gens::common::is_generator<Generator>, "Must provide a generator. A generator must have a create and shrink");
            auto lastFailure = generator.create(index);
            if (predicate(lastFailure)) {
                return lastFailure;
            }
            size_t trial = 0;
            for (const auto& v : generator.simplify(index)) {
                if (predicate(v) || trial++ > MaxShrinks) {
                    return lastFailure;
                }
                lastFailure = v;
            }
            return lastFailure;
        }

        template<typename ...Generators>
        struct checker {
            using Gen = gens::common::shrink_state_generator<std::tuple<typename gens::common::impl::GenCreationType<Generators>::type...>, std::tuple<Generators...>>;
            using InputType = std::tuple<typename gens::common::impl::GenCreationType<Generators>::type ...>;

            struct run_summary {
                std::string name = {};
                RunState state;
                std::optional<InputType> input = std::nullopt;
                std::optional<InputType> simplified = std::nullopt;
                std::string inputString = "";
                std::string simplifiedInputString = "";
                std::string failureMessage = "";
            };

            Gen generator;
            TEST_JAM_SEED_TYPE seed;
            std::string testName;
            size_t numTrials;

            auto run(RunMethod<Generators...> func) -> run_summary {
                run_summary summary = {.name=testName};
                bool testRun = true;
                std::default_random_engine rnd(seed);

                std::optional<failed_case> failure = std::nullopt;

                auto passed = [&func, &testRun, &failure, this](const auto&... params) {
                    try {
                        func(params...);
                        return true;
                    }
                    catch (const assert_failed_error& e) {
                        if (testRun) {
                            failure = failed_case{
                                    .error = false,
                                    .testName=testName,
                                    .failureMessage=e.what(),
                                    .seed=seed,
                            };
                        }
                        return false;
                    }
                    catch (const std::runtime_error& e) {
                        if (testRun) {
                            failure = failed_case{
                                    .error = true,
                                    .testName=testName,
                                    .failureMessage=std::string("<std::runtime_error: ") + e.what() + ">",
                                    .seed=seed,
                            };
                        }
                        return false;
                    }
                    catch (const std::string& msg) {
                        if (testRun) {
                            failure = failed_case{
                                    .error = true,
                                    .testName=testName,
                                    .failureMessage="<std::string: " + msg + ">",
                                    .seed=seed,
                            };
                        }
                        return false;
                    }
                    catch (int i) {
                        if (testRun) {
                            failure = failed_case{
                                    .error = true,
                                    .testName=testName,
                                    .failureMessage="<int: " + std::to_string(i) + ">",
                                    .seed=seed,
                            };
                        }
                        return false;
                    }
                    catch (...) {
                        if (testRun) {
                            failure = failed_case{
                                    .error = true,
                                    .testName=testName,
                                    .failureMessage="<UNKNOWN>",
                                    .seed=seed,
                            };
                        }
                        return false;
                    }
                };

                auto call = [&func, &passed, this](const auto& params) {
                    return call_func(passed, params, std::make_index_sequence<sizeof...(Generators)>{});
                };

                for (size_t i = 0; i < numTrials; ++i) {
                    const auto index = rnd();
                    auto val = generator.create(index);
                    if (!call(val)) {
                        testRun = false;
                        auto shrunkInputs = shrink(index, generator, call);
                        std::stringstream inputStream;
                        print::print_inputs(inputStream, val);

                        std::stringstream shrunkStream;
                        print::print_inputs(shrunkStream, shrunkInputs);

                        summary.input = val;
                        summary.simplified = shrunkInputs;
                        summary.inputString = inputStream.str();
                        summary.simplifiedInputString = shrunkStream.str();
                        summary.failureMessage = failure->failureMessage;

                        if (failure->error) {
                            summary.state = RunState::ERRORED;
                        }
                        else {
                            summary.state = RunState::FAILED;
                        }
                        return summary;
                    }
                }
                summary.state = RunState::PASSED;
                return summary;
            }

        private:
            template<typename F, typename ...T, size_t ...Index>
            auto call_func(const F& func, const std::tuple<typename gens::common::impl::GenCreationType<Generators>::type...>& params, std::index_sequence<Index...>) {
                return func(std::get<Index>(params)...);
            }
        };
    }

    class property {
        size_t numTrials;
        std::string prop_name;
        TEST_JAM_SEED_TYPE seed;
    public:
        property(const std::string& name, TEST_JAM_SEED_TYPE seed, size_t numTrials) : numTrials(numTrials), prop_name(name), seed(seed) {}
        property(const std::string& name, TEST_JAM_SEED_TYPE seed) : numTrials(500), prop_name(name), seed(seed) {}
        property(const std::string& name) : numTrials(500), prop_name(name) {
            std::random_device device;
            seed = static_cast<TEST_JAM_SEED_TYPE>((static_cast<uint64_t>(device()) << 32) | device());
        }

        auto with_seed(TEST_JAM_SEED_TYPE seed) -> property& {
            this->seed = seed;
            return *this;
        }

        template<typename ...Generators>
        auto with_generators(std::tuple<Generators...> generators) -> impl::checker<Generators...> {
            return impl::checker<Generators...>{
                    .generator=combine_impl(generators, std::make_index_sequence<sizeof...(Generators)>{}),
                    .seed=seed,
                    .testName=prop_name,
                    .numTrials = numTrials
            };
        }

        template<typename ...Generators>
        auto with_generators(Generators... generators) -> impl::checker<Generators...> {
            return with_generators(std::make_tuple(generators...));
        }
    private:
        template<typename ...Gens, size_t ...Index>
        static auto combine_impl(const std::tuple<Gens...>& generators, std::index_sequence<Index...>) {
            return gens::utils::combine<std::tuple<typename gens::common::impl::GenCreationType<Gens>::type...>>(std::get<Index>(generators)...);
        }
    };
}

#define _TEST_JAM_CHECK_MESSAGE_INTERNAL(COND, MSG) \
if (!(COND)) {               \
    auto msg = std::string("Assertion {") + #COND + "} failed at " + \
        std::string(__FILE__) + ":" + std::to_string(__LINE__) + "\n" + MSG;                       \
    throw test_jam::assert_failed_error(msg);                                      \
}

#define _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, OP) _TEST_JAM_CHECK_MESSAGE_INTERNAL(LEFT OP RIGHT, "Received: " + test_jam::print::to_string(LEFT) + (" " #OP " ") + test_jam::print::to_string(RIGHT))


#define TEST_JAM_CHECK(COND) \
if (!(COND)) {               \
    auto msg = std::string("Assertion {") + #COND + "} failed at " + \
        std::string(__FILE__) + ":" + std::to_string(__LINE__);                       \
    throw test_jam::assert_failed_error(msg);                                      \
}

#define TEST_JAM_CHECK_MESSAGE(COND, MSG) _TEST_JAM_CHECK_MESSAGE_INTERNAL(COND, "Message: " + test_jam::print::to_string(MSG))
#define TEST_JAM_CHECK_EQ(LEFT, RIGHT) _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, ==)
#define TEST_JAM_CHECK_LE(LEFT, RIGHT) _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, <=)
#define TEST_JAM_CHECK_LT(LEFT, RIGHT) _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, <)
#define TEST_JAM_CHECK_GE(LEFT, RIGHT) _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, >=)
#define TEST_JAM_CHECK_GT(LEFT, RIGHT) _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, >)
#define TEST_JAM_CHECK_NE(LEFT, RIGHT) _TEST_JAM_CHECK_BINARY_INTERNAL(LEFT, RIGHT, !=)
#define TEST_JAM_CHECK_FALSE(COND) TEST_JAM_CHECK(!(COND))
#define _TEST_JAM_APPROX_MSG_INTERNAL_LE(LEFT, RIGHT, EPSILON) \
    "Received: "                                               \
        + test_jam::print::to_string(LEFT) + " > " + test_jam::print::to_string(RIGHT + EPSILON) \
        + " (aka " + test_jam::print::to_string(LEFT) + " not ~= " + test_jam::print::to_string(RIGHT) + ")\n" \
        + "Expected: " + test_jam::print::to_string(LEFT) + " ~= " + test_jam::print::to_string(RIGHT)
#define _TEST_JAM_APPROX_MSG_INTERNAL_GE(LEFT, RIGHT, EPSILON) \
    "Received: "                                               \
        + test_jam::print::to_string(LEFT) + " < " + test_jam::print::to_string(RIGHT - EPSILON) \
        + " (aka " + test_jam::print::to_string(LEFT) + " not ~= " + test_jam::print::to_string(RIGHT) + ")\n" \
        + "Expected: " + test_jam::print::to_string(LEFT) + " ~= " + test_jam::print::to_string(RIGHT)

#define _TEST_JAM_APPROX_NE_MSG_INTERNAL(LEFT, RIGHT, EPSILON) \
    "Received: " \
    + test_jam::print::to_string(RIGHT - EPSILON) + " <= " + test_jam::print::to_string(LEFT) + " <= " + test_jam::print::to_string(RIGHT + EPSILON) \
    + " (aka " + test_jam::print::to_string(LEFT) + " ~= " + test_jam::print::to_string(RIGHT) + ")\n" \
    + "Expected: " + test_jam::print::to_string(LEFT) + " not ~= " + test_jam::print::to_string(RIGHT)
#define TEST_JAM_CHECK_APPROX_EQ(LEFT, RIGHT, EPSILON) \
_TEST_JAM_CHECK_MESSAGE_INTERNAL(LEFT <= (RIGHT + EPSILON), _TEST_JAM_APPROX_MSG_INTERNAL_LE(LEFT, RIGHT, EPSILON)) \
_TEST_JAM_CHECK_MESSAGE_INTERNAL(LEFT >= (RIGHT - EPSILON), _TEST_JAM_APPROX_MSG_INTERNAL_GE(LEFT, RIGHT, EPSILON))

#define TEST_JAM_CHECK_APPROX_NE(LEFT, RIGHT, EPSILON) \
_TEST_JAM_CHECK_MESSAGE_INTERNAL(LEFT > (RIGHT + EPSILON) || LEFT < (RIGHT - EPSILON), _TEST_JAM_APPROX_NE_MSG_INTERNAL(LEFT, RIGHT, EPSILON))
