#pragma once

#include <sstream>
#include <test-jam-cxx/deps/magic_enum.hpp>
#include "gens/common.h"
#include <set>
#include <unordered_set>
#include <list>
#include <map>
#include <test-jam-cxx/deps/magic_enum.hpp>
#include <unordered_map>
#include <bitset>

namespace test_jam::print {

    template<typename T>
    struct PrintInput;

    template<typename ...T>
    struct PrintInput<std::tuple<T...>> {
      static auto print(std::stringstream& ss, const std::tuple<T...>& t) {
        ss << "(";
        print_elems(ss, t, std::make_index_sequence<sizeof...(T)>{});
        ss << ")";
      }

      template<size_t ...Index>
      static auto print_elems(std::stringstream& ss, const std::tuple<T...>& t, std::index_sequence<Index...>) {
        const auto _ = (print_elem<Index>(ss, std::get<Index>(t)) | ...);
      }

      template<size_t Index, typename E>
      static auto print_elem(std::stringstream& ss, const E& t) {
        if constexpr (Index != 0) {
          ss << ", ";
        }
        PrintInput<E>::print(ss, t);
        return true;
      }
    };

    template<typename T>
    struct PrintInput<std::vector<T>> {
      static auto print(std::stringstream& ss, const std::vector<T>& t) {
        ss << "[";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "]";
      }
    };

    template<typename T, size_t N>
    struct PrintInput<std::array<T, N>> {
      static auto print(std::stringstream& ss, const std::array<T, N>& t) {
        ss << "[" << N << ":";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "]";
      }
    };

    template<typename T>
    struct PrintInput<std::set<T>> {
      static auto print(std::stringstream& ss, const std::set<T>& t) {
        ss << "set{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput<std::multiset<T>> {
      static auto print(std::stringstream& ss, const std::multiset<T>& t) {
        ss << "multiset{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput<std::unordered_set<T>> {
      static auto print(std::stringstream& ss, const std::unordered_set<T>& t) {
        ss << "unordered_set{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput<std::list<T>> {
      static auto print(std::stringstream& ss, const std::list<T>& t) {
        ss << "list{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<T>::print(ss, e);
          first = false;
        }
        ss << "}";
      }
    };

    template<size_t N>
    struct PrintInput<std::bitset<N>> {
      static auto print(std::stringstream& ss, const std::bitset<N>& t) {
        ss << "bitset{" << N << ":" << t.to_string() << "}";
      }
    };

    template<typename K, typename V>
    struct PrintInput<std::map<K, V>> {
      static auto print(std::stringstream& ss, const std::map<K, V>& t) {
        ss << "map{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<K>::print(ss, e.first);
          ss << ": ";
          PrintInput<V>::print(ss, e.second);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename K, typename V>
    struct PrintInput<std::unordered_map<K, V>> {
      static auto print(std::stringstream& ss, const std::unordered_map<K, V>& t) {
        ss << "unordered_map{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<K>::print(ss, e.first);
          ss << ": ";
          PrintInput<V>::print(ss, e.second);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename K, typename V>
    struct PrintInput<std::multimap<K, V>> {
      static auto print(std::stringstream& ss, const std::multimap<K, V>& t) {
        ss << "multimap{";
        bool first = true;
        for (const auto& e : t) {
          if (!first) {
            ss << ", ";
          }
          PrintInput<K>::print(ss, e.first);
          ss << ": ";
          PrintInput<V>::print(ss, e.second);
          first = false;
        }
        ss << "}";
      }
    };

    template<typename T>
    struct PrintInput {
        static auto print(std::stringstream& ss, const T& t) {
            if constexpr (magic_enum::is_unscoped_enum_v<T> || magic_enum::is_scoped_enum_v<T>) {
                if (magic_enum::enum_contains<T>(t)) {
                    ss << magic_enum::enum_type_name<T>() << "::" << magic_enum::enum_name(t);
                }
                else {
                    ss << magic_enum::enum_type_name<T>() << "::<INVALID:" << static_cast<int64_t>(t) << ">";
                }
            }
            else if constexpr (std::is_same_v<T, bool>) {
                if (t) {
                    ss << "true";
                }
                else {
                    ss << "false";
                }
            }
            else if constexpr (gens::common::is_ostream_print<T>) {
                ss << t;
            }
            else {
                ss << "{?}";
            }
        }
    };

    template<typename ...T>
    auto print_inputs(std::stringstream& ss, const std::tuple<T...>& inputs) {
        PrintInput<std::tuple<T...>>::print(ss, inputs);
    }

    template<typename T>
    auto to_string(const T& input) -> std::string {
        std::stringstream ss;
        PrintInput<T>::print(ss, input);
        return ss.str();
    }
}
