#include "doctest.h"
#include <test-jam-cxx/test_jam.h>
#include <regex>

static constexpr auto is_even = [](auto i) {
    return i % 2 == 0;
};

enum class TestEnum {
    A,
    B
};

TEST_SUITE("property") {
    using namespace test_jam;
    TEST_CASE("handles success cases") {
        const auto res = property("is even")
            .with_generators(
                gens::utils::map(
                    gens::numbers::int32(),
                    [](auto i) { return i * 2; }
                )
            )
            .run([](int32_t v) {
                TEST_JAM_CHECK(is_even(v));
            });

        CHECK_EQ(res.state, test_jam::RunState::PASSED);
    }

    TEST_CASE("handles failure cases") {
        const auto re = std::regex(R"(Assertion \{is_even\(v\)\} failed at .*?test_jam.cpp:\d+)");
        const auto res = property("is even")
                .with_generators(gens::numbers::int32())
                .run([](int32_t v) {
                    TEST_JAM_CHECK(is_even(v));
                });

        CHECK_EQ(res.state, test_jam::RunState::FAILED);
        CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
    }

    TEST_CASE("handles error cases") {
        const auto res = property("is even")
                .with_generators(gens::numbers::int32())
                .run([](int32_t v) {
                    throw std::runtime_error("error");
                });

        CHECK_EQ(res.state, test_jam::RunState::ERRORED);
        CHECK_EQ(res.failureMessage, "<std::runtime_error: error>");
    }

    TEST_CASE("handles failure messages") {
        SUBCASE("int") {
            const auto re = std::regex(R"(Assertion \{is_even\(v\)\} failed at .*?test_jam.cpp:\d+\nMessage: 24)");
            const auto res = property("is even")
                    .with_generators(gens::numbers::int32())
                    .run([](int32_t v) {
                        TEST_JAM_CHECK_MESSAGE(is_even(v), 24);
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }

        SUBCASE("string") {
            const auto re = std::regex(R"(Assertion \{is_even\(v\)\} failed at .*?test_jam.cpp:\d+\nMessage: hello)");
            const auto res = property("is even")
                    .with_generators(gens::numbers::int32())
                    .run([](int32_t v) {
                        TEST_JAM_CHECK_MESSAGE(is_even(v), "hello");
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }

        SUBCASE("enum") {
            const auto re = std::regex(R"(Assertion \{is_even\(v\)\} failed at .*?test_jam.cpp:\d+\nMessage: TestEnum::A)");
            const auto res = property("is even")
                    .with_generators(gens::numbers::int32())
                    .run([](int32_t v) {
                        TEST_JAM_CHECK_MESSAGE(is_even(v), TestEnum::A);
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }

        SUBCASE("equality") {
            const auto re = std::regex(R"(Assertion \{is_even\(v\) == true\} failed at .*?test_jam.cpp:\d+\nReceived: false == true)");
            const auto res = property("is even")
                    .with_generators(gens::numbers::int32())
                    .run([](int32_t v) {
                        TEST_JAM_CHECK_EQ(is_even(v), true);
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }

        SUBCASE("less than or equal to") {
            const auto re = std::regex(R"(Assertion \{v <= 5\} failed at .*?test_jam.cpp:\d+\nReceived: \d+ <= 5)");
            const auto res = property("is even")
                    .with_generators(gens::numbers::uint32({6}))
                    .run([](uint32_t v) {
                        TEST_JAM_CHECK_LE(v, 5);
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }

        SUBCASE("approx equal") {
            const auto re = std::regex(R"(Assertion \{v <= \(\d+ \+ \d+\)\} failed at .*?test_jam.cpp:\d+\nReceived: \d+ > \d+ \(aka \d+ not ~= \d+\)\nExpected: \d+ ~= \d+)");
            auto res = property("is close to nine")
                    .with_generators(gens::numbers::uint32({6, 13}))
                    .run([](uint32_t v) {
                        TEST_JAM_CHECK_APPROX_EQ(v, 9, 4);
                    });

            CHECK_EQ(res.state, test_jam::RunState::PASSED);
            CHECK_EQ(res.failureMessage, "");

            res = property("is not close to nine")
                    .with_generators(gens::numbers::uint32({12, 45}))
                    .run([](uint32_t v) {
                        TEST_JAM_CHECK_APPROX_EQ(v, 9, 4);
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }

        SUBCASE("approx not equal") {
            const auto re = std::regex(R"(Assertion \{v > \(\d+ \+ \d+\) \|\| v < \(\d+ \- \d+\)\} failed at .*?test_jam.cpp:\d+\nReceived: \d+ <= \d+ <= \d+ \(aka \d+ ~= \d+\)\nExpected: \d+ not ~= \d+)");
            auto res = property("is above nine")
                    .with_generators(gens::numbers::uint32({12, 45}))
                    .run([](uint32_t v) {
                        TEST_JAM_CHECK_APPROX_NE(v, 9, 2);
                    });

            CHECK_EQ(res.state, test_jam::RunState::PASSED);
            CHECK_EQ(res.failureMessage, "");

            res = property("is below nine")
                    .with_generators(gens::numbers::uint32({0, 6}))
                    .run([](uint32_t v) {
                        TEST_JAM_CHECK_APPROX_NE(v, 9, 2);
                    });

            CHECK_EQ(res.state, test_jam::RunState::PASSED);
            CHECK_EQ(res.failureMessage, "");

            res = property("is close to nine")
                    .with_generators(gens::numbers::uint32({6, 13}))
                    .run([](uint32_t v) {
                        TEST_JAM_CHECK_APPROX_NE(v, 9, 4);
                    });

            CHECK_EQ(res.state, test_jam::RunState::FAILED);
            CHECK_MESSAGE(std::regex_match(res.failureMessage, re), res.failureMessage);
        }
    }
}
