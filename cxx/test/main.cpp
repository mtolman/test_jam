#define DOCTEST_CONFIG_IMPLEMENT
#include "doctest.h"

extern "C" {
    extern size_t test_jam_mem_amt_allocated();
};

int main(int argc, char** argv) {
    doctest::Context context;
    context.applyCommandLine(argc, argv);
    int res = context.run();

    size_t bytesLeft = test_jam_mem_amt_allocated();

    if (bytesLeft > 0) {
        std::cerr << "Memory leak detected! Num bytes not freed: " << bytesLeft << "\n";
        return -1;
    }

    return res;
}