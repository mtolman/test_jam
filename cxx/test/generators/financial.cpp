#include "../doctest.h"
#include <test-jam-cxx/gens/financial.h>
#include <regex>

TEST_SUITE("FinancialGenerators") {
    using namespace test_jam::gens::financial;
    static constexpr size_t numIters = 500;

    TEST_CASE("account_names") {
        const auto gen = account_names();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.empty());
        }
    }

    TEST_CASE("credit_card_networks") {
        const auto gen = credit_card_networks();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.name.empty());
            CHECK_FALSE(v.id.empty());
            CHECK_FALSE(v.lengths.empty());
            CHECK_FALSE(v.starts.empty());
            CHECK_GE(v.cvvLength, 3);
            CHECK_LE(v.cvvLength, 4);
        }
    }

    TEST_CASE("currencies") {
        const auto gen = currencies();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.name.empty());
            CHECK_FALSE(v.isoCode.empty());
        }
    }

    TEST_CASE("transaction_types") {
        const auto gen = transaction_types();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.empty());
        }
    }

    TEST_CASE("credit_cards") {
        const auto gen = credit_cards();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.cvv.empty());
            CHECK_FALSE(v.number.empty());
            CHECK_GE(v.expirationDate.month, 1);
            CHECK_LE(v.expirationDate.month, 12);
            CHECK_GE(v.expirationDate.year, 2000);
            CHECK_FALSE(v.network.name.empty());
            CHECK_FALSE(v.network.id.empty());
            CHECK_FALSE(v.network.lengths.empty());
            CHECK_FALSE(v.network.starts.empty());
            CHECK_GE(v.network.cvvLength, 3);
            CHECK_LE(v.network.cvvLength, 4);
        }
    }

    TEST_CASE("account_numbers") {
        const auto gen = account_numbers();
        const auto re = std::regex(R"(\d{15})");
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_MESSAGE(std::regex_match(v, re), v);
        }
    }

    TEST_CASE("pin") {
        const auto gen = pins();
        const auto re = std::regex(R"(\d{4})");
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_MESSAGE(std::regex_match(v, re), v);
        }
    }

    TEST_CASE("amounts") {
        SUBCASE("default options") {
            const auto gen = amounts();
            const auto re = std::regex(R"(\d+\.\d{2})");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }

        SUBCASE("custom options") {
            const auto gen = amounts(
                    {
                            .decimalPlaces=4,
                            .decimalSeparator=",",
                            .symbol="$"
                    }
            );
            const auto re = std::regex(R"(\$\d+\,\d{4})");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }

        SUBCASE("custom options postfix") {
            const auto gen = amounts(
                    {
                            .decimalPlaces=4,
                            .decimalSeparator=",",
                            .symbol="$",
                            .prefixSymbol=false
                    }
            );
            const auto re = std::regex(R"(\d+\,\d{4}\$)");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }
    }

    TEST_CASE("bics") {
        SUBCASE("Exclude Branch Code") {
            const auto gen = bics(false);
            const auto re = std::regex(R"([A-Z0-9]{8})");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }

        SUBCASE("Include Branch Code") {
            const auto gen = bics(true);
            const auto re = std::regex(R"([A-Z0-9]{11})");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }
    }

    TEST_CASE("masked_numbers") {
        SUBCASE("Default Options") {
            const auto gen = masked_numbers();
            const auto re = std::regex(R"(\*{4}\d{4})");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }

        SUBCASE("Custom Options") {
            const auto gen = masked_numbers({
                                                    .unMaskedLen=3,
                                                    .maskedLen=5,
                                                    .maskedChar='#'
                                            });
            const auto re = std::regex(R"(\#{5}\d{3})");
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_MESSAGE(std::regex_match(v, re), v);
            }
        }
    }
}