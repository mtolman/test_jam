#include "../doctest.h"
#include <test-jam-cxx/gens/person.h>

#define TEST_JAM_PERSON_TEST(GEN)         \
TEST_CASE(#GEN) {                           \
    const auto gen = GEN();                   \
    for (size_t i = 0; i < numIters; ++i) { \
        CHECK_FALSE(gen.create(i).empty()); \
    }                                       \
}

#define TEST_JAM_PERSON_OPT_TEST(GEN)         \
TEST_CASE(#GEN) {                             \
    const auto gen = GEN();                   \
    for (size_t i = 0; i < numIters; ++i) {   \
        const auto v = gen.create(i);         \
        if (v) {                              \
            CHECK_FALSE(v->empty());          \
        }                                     \
    }                                         \
}

TEST_SUITE("Person") {
    using namespace test_jam::gens::person;
    static constexpr size_t numIters = 500;

    TEST_CASE("person") {
        const auto gen = person();
        for(size_t i = 0; i < numIters; ++i) {
            auto p = gen.create(i);
            CHECK((p.fullName == p.givenName + " " + p.surname || p.fullName == p.surname + " " + p.givenName));
            CHECK_FALSE(p.givenName.empty());
            CHECK_FALSE(p.surname.empty());
            if (p.prefix) {
                CHECK_FALSE(p.prefix->empty());
            }
            if (p.suffix) {
                CHECK_FALSE(p.suffix->empty());
            }
        }
    }

    TEST_JAM_PERSON_TEST(given_name)
    TEST_JAM_PERSON_TEST(surname)
    TEST_JAM_PERSON_TEST(full_name)
    TEST_JAM_PERSON_OPT_TEST(suffix)
    TEST_JAM_PERSON_OPT_TEST(prefix)
}
