#include "../doctest.h"
#include <test-jam-cxx/gens/numbers.h>
#include <limits>
#include <cmath>

#define INT_TEST(TYPE) \
TEST_CASE(#TYPE) {     \
    SUBCASE("default") { \
        const auto gen = TYPE(); \
        for (size_t i = 0; i < numIters; ++i) { \
            CHECK_GE(gen.create(i), std::numeric_limits<TYPE ## _t>::min()); \
        }              \
    }                  \
                       \
    SUBCASE("min max") { \
        const auto gen = TYPE({   \
            .min=static_cast<TYPE ## _t>(15),        \
            .max=static_cast<TYPE ## _t>(120)        \
        });             \
        for (size_t i = 0; i < numIters; ++i) { \
            CHECK_GE(gen.create(i), static_cast<TYPE ## _t>(15));        \
            CHECK_LE(gen.create(i), static_cast<TYPE ## _t>(120));       \
        }              \
    }                  \
                       \
    SUBCASE("simplify") { \
        const auto gen = TYPE(); \
        for (size_t i = 0; i < numIters; ++i) { \
            const auto s = gen.simplify(i);     \
            auto l = gen.create(i);             \
            for (const auto v : s) {            \
                l = v;           \
            }          \
            CHECK_EQ(l, 0); \
        }              \
    }                  \
}

TEST_SUITE("NumberGenerator") {
    using namespace test_jam::gens::numbers;
    static constexpr size_t numIters = 500;

    INT_TEST(uint64)
    INT_TEST(uint32)
    INT_TEST(uint16)
    INT_TEST(uint8)

    INT_TEST(int64)
    INT_TEST(int32)
    INT_TEST(int16)
    INT_TEST(int8)

    TEST_CASE("double") {
        SUBCASE("create") {
            const auto gen = f64({.allowNan=false,.allowInf=false});
            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_FALSE(std::isnan(v));
                CHECK_FALSE(std::isinf(v));
            }
        }

        SUBCASE("simplify") {
            const auto gen = f64({.allowNan=false,.allowInf=false});
            for (size_t i = 0; i < numIters; ++i) {
                auto last = gen.create(i);
                const auto s = gen.simplify(i);
                for (const auto& v : s) {
                    last = v;
                }
                CHECK_GE(last, -1.0);
                CHECK_LE(last, 1.0);
            }
        }
    }
}
