#include "../doctest.h"
#include <test-jam-cxx/gens/color.h>
#include <cmath>

TEST_SUITE("ColorGenerators") {
    using namespace test_jam::gens::color;
    static constexpr size_t numIters = 500;

    TEST_CASE("named_colors") {
        const auto gen = named_colors();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).name.empty());
            CHECK_FALSE(gen.create(i).hex.empty());
        }
    }

    TEST_CASE("rgb_colors") {
        const auto gen = rgb_colors();
        // Just checking linking works properly
        const auto _ = gen.create(1);
    }

    TEST_CASE("rgba_colors") {
        const auto gen = rgba_colors();
        // Just checking linking works properly
        const auto _ = gen.create(1);
    }

    TEST_CASE("hsl_colors") {
        const auto gen = hsl_colors();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(std::isnan(v.hue));
            CHECK_FALSE(std::isnan(v.saturation));
            CHECK_FALSE(std::isnan(v.luminosity));
            CHECK_FALSE(std::isinf(v.hue));
            CHECK_FALSE(std::isinf(v.saturation));
            CHECK_FALSE(std::isinf(v.luminosity));
        }
    }

    TEST_CASE("hsla_colors") {
        const auto gen = hsla_colors();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(std::isnan(v.hue));
            CHECK_FALSE(std::isnan(v.saturation));
            CHECK_FALSE(std::isnan(v.luminosity));
            CHECK_FALSE(std::isnan(v.alpha));
            CHECK_FALSE(std::isinf(v.hue));
            CHECK_FALSE(std::isinf(v.saturation));
            CHECK_FALSE(std::isinf(v.luminosity));
            CHECK_FALSE(std::isinf(v.alpha));
        }
    }
}
