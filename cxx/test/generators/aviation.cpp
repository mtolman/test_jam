#include "../doctest.h"
#include <test-jam-cxx/gens/aviation.h>

TEST_SUITE("AviationGenerators") {
    using namespace test_jam::gens::aviation;
    static constexpr size_t numIters = 500;

    TEST_CASE("aircraft_types") {
        const auto gen = aircraft_types();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).empty());
        }
    }

    TEST_CASE("airlines") {
        const auto gen = airlines();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).name.empty());
            CHECK_FALSE(gen.create(i).icao.empty());
            CHECK_FALSE(gen.create(i).iata.empty());
        }
    }

    TEST_CASE("airplanes") {
        const auto gen = airplanes();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).name.empty());
            CHECK_FALSE(gen.create(i).icao.empty());
            CHECK_FALSE(gen.create(i).iata.empty());
        }
    }

    TEST_CASE("airports") {
        const auto gen = airports();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).name.empty());
            CHECK_FALSE(gen.create(i).icao.empty());
            CHECK_FALSE(gen.create(i).iata.empty());
        }
    }
}
