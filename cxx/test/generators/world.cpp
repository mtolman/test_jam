#include "../doctest.h"
#include <test-jam-cxx/gens/world.h>

TEST_SUITE("WorldGenerators") {
    using namespace test_jam::gens::world;
    static constexpr size_t numIters = 500;

    TEST_CASE("countries") {
        const auto gen = countries();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.name.empty());
            if (v.tld.has_value()) {
                CHECK_FALSE(v.tld->empty());
            }
            if (v.stanag.has_value()) {
                CHECK_FALSE(v.stanag->empty());
            }
            if (v.isoNum.has_value()) {
                CHECK_FALSE(v.isoNum->empty());
            }
            if (v.iso3.has_value()) {
                CHECK_FALSE(v.iso3->empty());
            }
            if (v.iso2.has_value()) {
                CHECK_FALSE(v.iso2->empty());
            }
            if (v.genc.has_value()) {
                CHECK_FALSE(v.genc->empty());
            }
        }
    }

    TEST_CASE("timezones") {
        const auto gen = timezones();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).empty());
        }
    }
}