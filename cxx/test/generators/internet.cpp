#include "../doctest.h"
#include <test-jam-cxx/gens/internet.h>
#include <regex>
#include <memory>

TEST_SUITE("InternetGenerators") {
    using namespace test_jam::gens::internet;
    static constexpr size_t numIters = 500;

    TEST_CASE("top_level_domains") {
        const auto gen = top_level_domains();
        const auto re = std::regex(R"(\w+)");
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_MESSAGE(std::regex_match(gen.create(i), re), gen.create(i));
        }
    }

    TEST_CASE("ipv4_addresses") {
        const auto gen = ipv4_addresses();
        const auto re = std::regex(R"(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})");
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_MESSAGE(std::regex_match(gen.create(i), re), gen.create(i));
        }
    }

    TEST_CASE("ipv6_addresses") {
        const auto gen = ipv6_addresses();
        SUBCASE("create") {
            const auto re = std::regex(R"((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))");
            for (size_t i = 0; i < numIters; ++i) {
                CHECK_MESSAGE(std::regex_match(gen.create(i), re), gen.create(i));
            }
        }
        SUBCASE("simplify") {
            const auto re = std::regex(R"(([a-fA-F0-9]{4}:)+[a-fA-F0-9]{4})");

            for (size_t i = 0; i < numIters; ++i) {
                const auto s = gen.simplify(i);
                auto last = gen.create(i);
                for (const auto& ip : s) {
                    last = ip;
                }
                CHECK_MESSAGE(std::regex_match(last, re), i);
            }
        }
    }

    TEST_CASE("domains") {
        const auto gen = domains();
        SUBCASE("create") {
            const auto re = std::regex(R"(([a-z0-9\-]+\.)+[a-z0-9\-]+)");
            for (size_t i = 0; i < numIters; ++i) {
                CHECK_MESSAGE(std::regex_match(gen.create(i), re), gen.create(i));
            }
        }

        SUBCASE("simplify") {
            const auto re = std::regex(R"([a-z0-9\-]\.[a-z0-9\-])");
            for (size_t i = 0; i < numIters; ++i) {
                const auto s = gen.simplify(i);
                auto last = gen.create(i);
                for (const auto& v : s) {
                    last = v;
                }
                CHECK_MESSAGE(std::regex_match(last, re), i);
            }
        }
    }

    TEST_CASE("urls") {
        const auto gen = urls();
        const auto re = std::regex(R"((https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})*)?)");
        SUBCASE("create") {
            for (size_t i = 0; i < numIters; ++i) {
                CHECK_MESSAGE(std::regex_match(gen.create(i), re), gen.create(i));
            }
        }

        SUBCASE("simplify") {
            const auto re = std::regex(R"((https?|s?ftps?):\/\/(([a-z\-0-9A-Z]+\.)+[a-z\-A-Z]+)(\/[a-z\-A-Z]*)?)");
            for (size_t i = 0; i < numIters; ++i) {
                const auto s = gen.simplify(i);
                auto last = gen.create(i);
                for (const auto& v : s) {
                    last = v;
                }
                CHECK_MESSAGE(std::regex_match(last, re), i);
            }
        }
    }

    TEST_CASE("emails") {
        const auto gen = emails();
        SUBCASE("create") {
            const auto re = std::regex(R"RE((([A-Za-z0-9!#$%&'*+-\/=?^_`{|}~]+(\.[A-Za-z0-9!#$%&'*+\-/=?^_`{|}~]+)*)|("([^\\"]|\\.)+"))@((([a-z0-9\-]+\.)+[a-z0-9\-]+)|(\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(\[IPv6:[a-fA-F0-9:.]+\])))RE");
            for (size_t i = 0; i < numIters; ++i) {
                const auto val = gen.create(i);
                if (!std::regex_match(val, re)) {
                    printf("%d\n", (int)val.size());
                }
                CHECK_MESSAGE(std::regex_match(val, re), val);
            }
        }
    }
}