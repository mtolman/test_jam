#include "../doctest.h"
#include <test-jam-cxx/gens/utils.h>
#include <test-jam-cxx/gens/ascii.h>
#include <test-jam-cxx/gens/numbers.h>

struct TestStruct1 {
    int32_t a;
    std::string b;
};

TEST_SUITE("UtilGenerators") {
    using namespace test_jam::gens;
    static constexpr size_t numIters = 500;

    TEST_CASE("combine") {
        SUBCASE("struct") {
            const auto gen = utils::combine<TestStruct1>(
                    numbers::int32({10, 200}),
                    ascii::str({ascii::alpha, 1, 20})
            );

            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_GE(v.a, 10);
                CHECK_LE(v.a, 200);

                CHECK_GE(v.b.size(), 1);
                CHECK_LE(v.b.size(), 20);
            }
        }

        SUBCASE("tuple") {
            const auto gen = utils::combine<std::tuple<int32_t, std::string>>(
                    numbers::int32({10, 200}),
                    ascii::str({ascii::alpha, 1, 20})
            );

            for (size_t i = 0; i < numIters; ++i) {
                const auto v = gen.create(i);
                CHECK_GE(std::get<0>(v), 10);
                CHECK_LE(std::get<0>(v), 200);

                CHECK_GE(std::get<1>(v).size(), 1);
                CHECK_LE(std::get<1>(v).size(), 20);
            }
        }
    }

    TEST_CASE("filter") {
        const auto gen = utils::filter(numbers::int32({1, 200}), [](auto v) -> bool { return v % 2 == 0; });

        SUBCASE("create") {
            for (size_t i = 0; i < numIters; ++i) {
                CHECK_MESSAGE(gen.create(i) % 2 == 0, i);
            }
        }

        SUBCASE("simplify") {
            for (size_t i = 0; i < 100; ++i) {
                auto l = gen.create(i);
                for (const auto& v : gen.simplify(i)) {
                    l = v;
                }
                CHECK_EQ(l, 2);
            }
        }
    }

    TEST_CASE("map") {
        const auto iGen = numbers::int32({1, 200});
        const auto gen = utils::map<bool>(iGen, [](auto v) -> bool { return v % 2 == 0; });

        SUBCASE("create") {
            for (size_t i = 0; i < numIters; ++i) {
                CHECK_EQ(gen.create(i), iGen.create(i) % 2 == 0);
            }
        }

        SUBCASE("simplify") {
            for (size_t i = 0; i < 100; ++i) {
                auto l = gen.create(i);
                for (const auto& v : gen.simplify(i)) {
                    l = v;
                }
                CHECK_EQ(l, false);
            }
        }
    }

    TEST_CASE("just") {
        const auto gen = utils::just<int>(949);

        SUBCASE("create") {
            for (size_t i = 0; i < numIters; ++i) {
                CHECK_EQ(gen.create(i), 949);
            }
        }
    }

    TEST_CASE("one_of") {
        SUBCASE("one") {
            const auto gen = utils::one_of(numbers::int32({0, 400}));

            SUBCASE("create") {
                for (size_t i = 0; i < numIters; ++i) {
                    auto v = gen.create(i);
                    CHECK_LE(v, 400);
                    CHECK_GE(v, 0);
                }
            }

            SUBCASE("simplify") {
                for (size_t i = 0; i < numIters; ++i) {
                    auto l = gen.create(i);
                    for (const auto& v : gen.simplify(i)) {
                        l = v;
                    }
                    CHECK_EQ(l, 0);
                }
            }
        }

        SUBCASE("one type") {
            const auto gen = utils::one_of(numbers::int32({0, 400}), numbers::int32({402, 800}));

            SUBCASE("create") {
                for (size_t i = 0; i < numIters; ++i) {
                    auto v = gen.create(i);
                    CHECK_LE(v, 800);
                    CHECK_GE(v, 0);
                    CHECK_NE(v, 401);
                }
            }

            SUBCASE("simplify") {
                for (size_t i = 0; i < numIters; ++i) {
                    auto l = gen.create(i);
                    for (const auto& v : gen.simplify(i)) {
                        l = v;
                    }
                    CHECK_MESSAGE((l == 0 || l == 402), l);
                }
            }
        }

        SUBCASE("two types") {
            const auto gen = utils::one_of(numbers::int32({0, 400}), ascii::str({ascii::alpha, 1, 20}));

            SUBCASE("create") {
                for (size_t i = 0; i < numIters; ++i) {
                    auto v = gen.create(i);
                    if (std::holds_alternative<int32_t>(v)) {
                        auto i32 = std::get<int32_t>(v);
                        CHECK_LE(i32, 800);
                        CHECK_GE(i32, 0);
                        CHECK_NE(i32, 401);
                    }
                    else {
                        const auto str = std::get<std::string>(v);
                        CHECK_FALSE(str.empty());
                        CHECK_LE(str.size(), 20);
                    }
                }
            }

            SUBCASE("simplify") {
                for (size_t i = 0; i < numIters; ++i) {
                    auto l = gen.create(i);
                    for (const auto& v : gen.simplify(i)) {
                        l = v;
                    }

                    if (std::holds_alternative<int32_t>(l)) {
                        CHECK_EQ(std::get<int32_t>(l), 0);
                    }
                    else {
                        CHECK_EQ(std::get<std::string>(l).size(), 1);
                    }
                }
            }
        }
    }
}
