#include "../doctest.h"
#include <test-jam-cxx/gens/malicious.h>

TEST_SUITE("MaliciousGenerators") {
    using namespace test_jam::gens::malicious;
    static constexpr size_t numIters = 500;

    TEST_CASE("sql_injection") {
        const auto gen = sql_injection();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).empty());
        }
    }

    TEST_CASE("xss") {
        const auto gen = xss();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).empty());
        }
    }

    TEST_CASE("format_injection") {
        const auto gen = format_injection();
        for (size_t i = 0; i < numIters; ++i) {
            CHECK_FALSE(gen.create(i).empty());
        }
    }

    TEST_CASE("edge_cases") {
        const auto gen = edge_cases();
        for (size_t i = 0; i < numIters; ++i) {
            const auto _ = gen.create(i);
        }
    }
}
