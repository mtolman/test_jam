#include "../doctest.h"
#include <test-jam-cxx/gens/computer.h>

#define TEST_JAM_COMPUTER_TEST(GEN)         \
TEST_CASE(#GEN) {                           \
    const auto gen = GEN();                   \
    for (size_t i = 0; i < numIters; ++i) { \
        CHECK_FALSE(gen.create(i).empty()); \
    }                                       \
}

TEST_SUITE("ComputerGenerators") {
    using namespace test_jam::gens::computer;
    static constexpr size_t numIters = 500;

    TEST_JAM_COMPUTER_TEST(cmake_system_names)
    TEST_JAM_COMPUTER_TEST(cpu_architectures)
    TEST_JAM_COMPUTER_TEST(file_extensions)
    TEST_CASE("file_mime_mappings") {
        const auto gen = file_mime_mappings();
        for (size_t i = 0; i < numIters; ++i) {
            const auto v = gen.create(i);
            CHECK_FALSE(v.mimeType.empty());
            CHECK_FALSE(v.fileExtension.empty());
        }
    }

    TEST_JAM_COMPUTER_TEST(file_types)
    TEST_JAM_COMPUTER_TEST(mime_types)
    TEST_JAM_COMPUTER_TEST(operating_systems)
}