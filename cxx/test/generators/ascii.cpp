#include "../doctest.h"
#include <test-jam-cxx/gens/ascii.h>
#include <regex>

#define ASCII_GEN_TEST(GROUP, REGEX) TEST_CASE(#GROUP) { \
    const auto re = std::regex(REGEX);                   \
    const auto gen = str({.charSet=GROUP});              \
    SUBCASE("create") {                                  \
        for (size_t i = 0; i < numIters; ++i) {          \
            CHECK_MESSAGE(                               \
                std::regex_match(gen.create(i), re),     \
                gen.create(i)                            \
            );                                           \
        }                                                \
    }                                                    \
                                                         \
    SUBCASE("simplify") {                                \
        const auto val = gen.create(2944);               \
        const auto s = gen.simplify(2944);               \
        bool compared = false;                           \
        auto last = val;                                 \
        for (const auto& v : s) {                        \
            if (!compared) {                             \
                CHECK_EQ(v, val);                        \
                compared = true;                         \
            }                                            \
            last = v;                                    \
        }                                                \
        CHECK_EQ(last, "");                              \
    }                                                    \
}

TEST_SUITE("AsciiGenerators") {
    using namespace test_jam::gens::ascii;
    static constexpr size_t numIters = 500;

    ASCII_GEN_TEST(any, R"([\x00-\x7f]*)")

    ASCII_GEN_TEST(alpha, R"([a-zA-Z]*)")

    ASCII_GEN_TEST(alpha_lower, R"([a-z]*)")

    ASCII_GEN_TEST(alpha_upper, R"([A-Z]*)")

    ASCII_GEN_TEST(alpha_numeric, R"([a-zA-Z0-9]*)")

    ASCII_GEN_TEST(alpha_numeric_lower, R"([a-z0-9]*)")

    ASCII_GEN_TEST(alpha_numeric_upper, R"([A-Z0-9]*)")

    ASCII_GEN_TEST(binary, R"([01]*)")

    ASCII_GEN_TEST(bracket, R"([(){}\[\]<>]*)")

    ASCII_GEN_TEST(control, R"([\x00-\x1f\x7f]*)")

    ASCII_GEN_TEST(domain_piece, R"([a-z0-9\-]*)")

    ASCII_GEN_TEST(hex, R"([a-fA-F0-9]*)")

    ASCII_GEN_TEST(hex_lower, R"([a-f0-9]*)")

    ASCII_GEN_TEST(hex_upper, R"([A-F0-9]*)")

    ASCII_GEN_TEST(html_named, R"([!-,.-/:-@\[-`{-~]*)") // \x21-\x2C\x2E-\x2F\x3A-\x40\x5B-\x60\x7B-\x7E

    ASCII_GEN_TEST(math, R"([\s!%(->^A-Za-z]*)")

    ASCII_GEN_TEST(numeric, R"([0-9]*)")

    ASCII_GEN_TEST(octal, R"([0-7]*)")

    ASCII_GEN_TEST(printable, R"([\s!-~]*)")

    ASCII_GEN_TEST(quote, R"(["'`]*)")

    ASCII_GEN_TEST(safe_punctuation, R"([,.]*)")

    ASCII_GEN_TEST(sentence_punctuation, R"([,-."?:;'-)]*)")

    ASCII_GEN_TEST(symbol, R"([!-/:-@\[-`{-~]*)")

    ASCII_GEN_TEST(text, R"([\s,-."?:;'-)a-zA-Z0-9]*)")

    ASCII_GEN_TEST(url_unencoded_chars, R"([a-zA-Z0-9+\-]*)")

    ASCII_GEN_TEST(url_unencoded_hash_chars, R"([a-zA-Z0-9+\-.?=/&]*)")

    ASCII_GEN_TEST(whitespace, R"(\s*)")
}
