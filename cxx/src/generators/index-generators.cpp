#include <test-jam-cxx/gens/aviation.h>
#include <test-jam/generators/aviation.h>

#include <test-jam-cxx/gens/color.h>
#include <test-jam/generators/color.h>

#include <test-jam-cxx/gens/computer.h>
#include <test-jam/generators/computer.h>

#include <test-jam-cxx/gens/malicious.h>
#include <test-jam/generators/malicious.h>

#include "utils.h"

namespace utils = test_jam::utils;
namespace common = test_jam::gens::common;
namespace aviation = test_jam::gens::aviation;
namespace color = test_jam::gens::color;
namespace computer = test_jam::gens::computer;
namespace malicious = test_jam::gens::malicious;

static auto to_record(const TestJamAviationRecord& r) -> aviation::record {
    return {
        .name=utils::to_str(r.name),
        .iata=utils::to_str(r.iata),
        .icao=utils::to_str(r.icao),
    };
}

common::no_shrink_generator<std::string> aviation::aircraft_types() {
        return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_aviation_aircraft_type(static_cast<uint64_t>(seed)));
            }
        };
}

common::no_shrink_generator<aviation::record> aviation::airlines() {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> aviation::record {
                return to_record(test_jam_aviation_airline(static_cast<uint64_t>(seed)));
            }
    };
}

common::no_shrink_generator<aviation::record> aviation::airplanes() {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> aviation::record {
                return to_record(test_jam_aviation_airplane(static_cast<uint64_t>(seed)));
            }
    };
}

common::no_shrink_generator<aviation::record> aviation::airports() {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> aviation::record {
                return to_record(test_jam_aviation_airport(static_cast<uint64_t>(seed)));
            }
    };
}

static auto to_rgb(const TestJamColorRgb& c) -> color::rgb_color {
    return {
        .red=c.red,
        .green=c.green,
        .blue=c.blue
    };
}

common::no_shrink_generator<color::named_color> color::named_colors() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> named_color {
            const auto r = test_jam_color_named(static_cast<uint64_t>(seed));
            return {
                    .name=utils::to_str(r.name),
                    .hex=utils::to_str(r.hex),
                    .rgb=to_rgb(r.rgb),
            };
        }
    };
}

common::no_shrink_generator<color::rgb_color> color::rgb_colors() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> rgb_color {
            return to_rgb(test_jam_color_rgb(static_cast<uint64_t>(seed)));
        }
    };
}

common::no_shrink_generator<color::rgba_color> color::rgba_colors() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> rgba_color {
            const auto r = test_jam_color_rgba(static_cast<uint64_t>(seed));
            return {
                    .red=r.red,
                    .green=r.green,
                    .blue=r.blue,
                    .alpha=r.alpha
            };
        }
    };
}

common::no_shrink_generator<color::hsl_color> color::hsl_colors() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> hsl_color {
            const auto r = test_jam_color_hsl(static_cast<uint64_t>(seed));
            return {
                    .hue=r.hue,
                    .saturation=r.saturation,
                    .luminosity=r.luminosity
            };
        }
    };
}

common::no_shrink_generator<color::hsla_color> color::hsla_colors() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> hsla_color {
            const auto r = test_jam_color_hsla(static_cast<uint64_t>(seed));
            return {
                    .hue=r.hue,
                    .saturation=r.saturation,
                    .luminosity=r.luminosity,
                    .alpha=r.alpha
            };
        }
    };
}

common::no_shrink_generator<std::string> computer::cmake_system_names() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
            return utils::to_str(test_jam_computer_cmake_system_name(static_cast<uint64_t>(seed)));
        }
    };
}

common::no_shrink_generator<std::string> computer::cpu_architectures() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
            return utils::to_str(test_jam_computer_cpu_architecture(static_cast<uint64_t>(seed)));
        }
    };
}

common::no_shrink_generator<std::string> computer::file_extensions() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
            return utils::to_str(test_jam_computer_file_extension(static_cast<uint64_t>(seed)));
        }
    };
}

common::no_shrink_generator<computer::file_mime_map> computer::file_mime_mappings() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> file_mime_map {
            const auto r = test_jam_computer_file_mime_mapping(static_cast<uint64_t>(seed));
            return {
                    .mimeType = utils::to_str(r.mime),
                    .fileExtension = utils::to_str(r.extension)
            };
        }
    };
}

common::no_shrink_generator<std::string> computer::file_types() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
            return utils::to_str(test_jam_computer_file_type(static_cast<uint64_t>(seed)));
        }
    };
}

common::no_shrink_generator<std::string> computer::mime_types() {
    return {
        .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
            return utils::to_str(test_jam_computer_mime_type(static_cast<uint64_t>(seed)));
        }
    };
}

common::no_shrink_generator<std::string> computer::operating_systems() {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_computer_operating_system(static_cast<uint64_t>(seed)));
            }
    };
}

auto malicious::sql_injection() -> common::no_shrink_generator<std::string> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_malicious_sql_injection(static_cast<uint64_t>(seed)));
            }
    };
}


auto malicious::xss() -> common::no_shrink_generator<std::string> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_malicious_xss(static_cast<uint64_t>(seed)));
            }
    };
}


auto malicious::format_injection() -> common::no_shrink_generator<std::string> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_malicious_format_injection(static_cast<uint64_t>(seed)));
            }
    };
}


auto malicious::edge_cases() -> common::no_shrink_generator<std::string> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_malicious_edge_cases(static_cast<uint64_t>(seed)));
            }
    };
}
