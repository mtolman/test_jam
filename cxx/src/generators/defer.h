#pragma once

namespace test_jam {
    template<typename F>
    struct PrivDefer {
        F f;
        PrivDefer(F f) :
        f(f) {}

        ~PrivDefer() { f(); }
    };

    struct Dummy{
        template<typename F>
        PrivDefer<F> operator+(F func) const {
            return PrivDefer{func};
        }
    };
}

#define DEFER_1(x, y) x##y
#define DEFER_2(x, y) DEFER_1(x, y)
#define DEFER_3(x)    DEFER_2(x, __COUNTER__)
#define defer   auto DEFER_3(_defer_) = test_jam::Dummy{} + [&]()
