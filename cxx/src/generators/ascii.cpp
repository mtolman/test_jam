#include <test-jam-cxx/gens/ascii.h>
#include <test-jam/generators/ascii.h>
#include <test-jam/simplifiers/ascii.h>
#include <test-jam/data/ascii.h>
#include "defer.h"
#include "utils.h"
#include <memory>

namespace utils = test_jam::utils;

struct SimplifierCallable {
    std::shared_ptr<TestJamStringSimplifier> simplifier;

    SimplifierCallable(TEST_JAM_SEED_TYPE seed, const test_jam::gens::ascii::char_set& charSet, size_t minLen = 0, size_t maxLen = 500)
            : simplifier(new TestJamStringSimplifier(), +[](TestJamStringSimplifier* s)-> void {
        test_jam_string_simplifier_free(s);
        delete s;
    })
    {
        TestJamAsciiCharSet chSet = {
                .length=charSet.chars.size(),
                .chars=charSet.chars.data()
        };
        test_jam_simplifier_ascii(simplifier.get(), seed, &chSet, minLen, maxLen, nullptr);
    }

    std::optional<std::string> operator()() const {
        std::optional<std::string> res = std::nullopt;
        if (simplifier->hasValue) {
            res = std::string(simplifier->value.data, simplifier->value.length);
            test_jam_string_simplifier_next(simplifier.get());
        }
        return res;
    }
};

auto test_jam::gens::ascii::str(const str_options& opts) -> common::shrink_state_generator<std::string, str_options> {
    return {
        .state=opts,
        .genFunc=+[](const str_options& opts, TEST_JAM_SEED_TYPE seed) -> std::string {
            TestJamString str;
            TestJamAsciiCharSet chSet = {
                    .length=opts.charSet.chars.size(),
                    .chars=opts.charSet.chars.data()
            };
            test_jam_ascii_from_charset(&str, seed, &chSet, opts.minLen, opts.maxLen, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        },
        .simplifyFunc=+[](const str_options& opts, TEST_JAM_SEED_TYPE seed) -> test_jam::gens::common::sequence<std::string> {
            return { SimplifierCallable(seed, opts.charSet, opts.minLen, opts.maxLen) };
        }
    };
}

static test_jam::gens::ascii::char_set from_c_set(const TestJamAsciiCharSet* ch) {
    return {
        .chars=std::string(ch->chars, ch->length)
    };
}

const test_jam::gens::ascii::char_set test_jam::gens::ascii::any = from_c_set(test_jam_data_ascii_any());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::alpha = from_c_set(test_jam_data_ascii_alpha());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::alpha_lower = from_c_set(test_jam_data_ascii_alpha_lower());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::alpha_upper = from_c_set(test_jam_data_ascii_alpha_upper());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::alpha_numeric = from_c_set(test_jam_data_ascii_alpha_numeric());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::alpha_numeric_lower = from_c_set(test_jam_data_ascii_alpha_numeric_lower());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::alpha_numeric_upper = from_c_set(test_jam_data_ascii_alpha_numeric_upper());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::binary = from_c_set(test_jam_data_ascii_binary());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::bracket = from_c_set(test_jam_data_ascii_bracket());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::control = from_c_set(test_jam_data_ascii_control());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::domain_piece = from_c_set(test_jam_data_ascii_domain_piece());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::hex = from_c_set(test_jam_data_ascii_hex());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::hex_lower = from_c_set(test_jam_data_ascii_hex_lower());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::hex_upper = from_c_set(test_jam_data_ascii_hex_upper());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::html_named = from_c_set(test_jam_data_ascii_html_named());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::math = from_c_set(test_jam_data_ascii_math());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::numeric = from_c_set(test_jam_data_ascii_numeric());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::octal = from_c_set(test_jam_data_ascii_octal());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::printable = from_c_set(test_jam_data_ascii_printable());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::quote = from_c_set(test_jam_data_ascii_quote());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::safe_punctuation = from_c_set(test_jam_data_ascii_safe_punctuation());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::sentence_punctuation = from_c_set(test_jam_data_ascii_sentence_punctuation());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::symbol = from_c_set(test_jam_data_ascii_symbol());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::text = from_c_set(test_jam_data_ascii_text());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::url_unencoded_chars = from_c_set(test_jam_data_ascii_url_unencoded_chars());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::url_unencoded_hash_chars = from_c_set(test_jam_data_ascii_url_unencoded_hash_chars());
const test_jam::gens::ascii::char_set test_jam::gens::ascii::whitespace = from_c_set(test_jam_data_ascii_whitespace());
