#include <test-jam-cxx/gens/internet.h>
#include <test-jam/generators/internet.h>
#include <test-jam/simplifiers/internet.h>
#include <memory>

#include "defer.h"
#include "utils.h"

auto test_jam::gens::internet::top_level_domains() -> common::no_shrink_generator<std::string> {
    return {
        .genFunc=+[](TEST_JAM_SEED_TYPE seed) -> std::string {
            return utils::to_str(test_jam_internet_top_level_domain(static_cast<uint64_t>(seed)));
        }
    };
}

auto test_jam::gens::internet::ipv4_addresses() -> common::no_shrink_generator <std::string> {
    return {
            .genFunc=+[](TEST_JAM_SEED_TYPE seed) -> std::string {
                TestJamString str;
                test_jam_internet_ipv4(&str, static_cast<uint64_t>(seed), nullptr);
                defer { test_jam_free_string_contents(&str, nullptr); };
                return utils::to_str(str);
            }
    };
}


struct Ipv6SimplifierCallable {
    std::shared_ptr<TestJamStringSimplifier> simplifier;
    TestJamIpv6Options opts;

    Ipv6SimplifierCallable(size_t seed, unsigned flags)
            : simplifier(
                    new TestJamStringSimplifier(),
                    +[](TestJamStringSimplifier* s)-> void {
                        test_jam_string_simplifier_free(s);
                        delete s;
                    }
              )
            , opts({flags})
    {
        test_jam_internet_simplifier_ipv6(simplifier.get(), seed, &opts, nullptr);
    }

    std::optional<std::string> operator()() const {
        std::optional<std::string> res = std::nullopt;
        if (simplifier->hasValue) {
            res = std::string(simplifier->value.data, simplifier->value.length);
            test_jam_string_simplifier_next(simplifier.get());
        }
        return res;
    }
};

auto test_jam::gens::internet::ipv6_addresses(unsigned flags) -> common::shrink_state_generator<std::string, unsigned> {
    return {
        .state=flags,
        .genFunc=+[](const unsigned& flags, TEST_JAM_SEED_TYPE seed) -> std::string {
            TestJamString str;
            TestJamIpv6Options opts = {.flags=flags};
            test_jam_internet_ipv6(&str, static_cast<uint64_t>(seed), &opts, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        },
        .simplifyFunc=+[](const unsigned& flags, TEST_JAM_SEED_TYPE seed) -> common::sequence<std::string> {
            return { Ipv6SimplifierCallable(static_cast<uint64_t>(seed), flags) };
        }
    };
}


struct DomainSimplifierCallable {
    std::shared_ptr<TestJamStringSimplifier> simplifier;
    TestJamDomainOptions opts;

    DomainSimplifierCallable(size_t seed, TestJamDomainOptions opts)
            : simplifier(
                    new TestJamStringSimplifier(),
                    +[](TestJamStringSimplifier* s)-> void {
                        test_jam_string_simplifier_free(s);
                        delete s;
                    }
            )
            , opts(opts)
    {
        test_jam_internet_simplifier_domain(simplifier.get(), seed, &opts, nullptr);
    }

    std::optional<std::string> operator()() const {
        std::optional<std::string> res = std::nullopt;
        if (simplifier->hasValue) {
            res = std::string(simplifier->value.data, simplifier->value.length);
            test_jam_string_simplifier_next(simplifier.get());
        }
        return res;
    }
};

auto test_jam::gens::internet::domains(const domain_options& options) -> common::shrink_state_generator<std::string, domain_options> {
    return {
            .genFunc=+[](const domain_options& options, TEST_JAM_SEED_TYPE seed) -> std::string {
                TestJamDomainOptions opts = {
                        .minLen=options.minLen,
                        .maxLen=options.maxLen
                };
                TestJamString str;
                test_jam_internet_domain(&str, static_cast<uint64_t>(seed), &opts, nullptr);
                defer { test_jam_free_string_contents(&str, nullptr); };
                return utils::to_str(str);
            },
            .simplifyFunc=+[](const domain_options& options, TEST_JAM_SEED_TYPE seed) -> common::sequence<std::string> {
                TestJamDomainOptions opts = {
                        .minLen=options.minLen,
                        .maxLen=options.maxLen
                };
                return { DomainSimplifierCallable(static_cast<uint64_t>(seed), opts) };
            }
    };
}


struct UrlSimplifierCallable {
    std::shared_ptr<TestJamStringSimplifier> simplifier;
    std::vector<std::string> possibleSchemas;
    std::vector<TestJamConstString> cStrSchemas = {};
    std::shared_ptr<TestJamConstStringArray> cPossibleSchemas = std::make_shared<TestJamConstStringArray>();
    std::shared_ptr<TestJamDomainOptions> domainOpts = std::make_shared<TestJamDomainOptions>();
    std::shared_ptr<TestJamUrlOptions> urlOpts = std::make_shared<TestJamUrlOptions>();

    UrlSimplifierCallable(
            uint64_t seed,
            unsigned flags,
            const std::vector<std::string>& schemas,
            size_t minLen,
            size_t maxLen,
            TestJamDomainOptions domainOptions
    )
        : simplifier(
            new TestJamStringSimplifier(),
            +[](TestJamStringSimplifier* s)-> void {
                test_jam_string_simplifier_free(s);
                delete s;
            }
        )
        , possibleSchemas(schemas)
    {
        *domainOpts = domainOptions;
        cStrSchemas.reserve(possibleSchemas.size());
        for (const auto& str : possibleSchemas) {
            cStrSchemas.push_back(
                {
                    .length=str.size(),
                    .data=str.c_str()
                }
            );
        }

        cPossibleSchemas->values = cStrSchemas.data();
        cPossibleSchemas->size = cStrSchemas.size();
        *urlOpts = {
                .flags=flags,
                .possibleSchemas=cPossibleSchemas.get(),
                .minLen=minLen,
                .maxLen=maxLen,
                .domainOptions=domainOpts.get()
        };
        test_jam_internet_simplifier_url(simplifier.get(), seed, urlOpts.get(), nullptr);
    }

    std::optional<std::string> operator()() const {
        std::optional<std::string> res = std::nullopt;
        if (simplifier->hasValue) {
            res = std::string(simplifier->value.data, simplifier->value.length);
            test_jam_string_simplifier_next(simplifier.get());
        }
        return res;
    }
};


auto test_jam::gens::internet::urls(const url_options& options) -> common::shrink_state_generator <std::string, url_options> {
    return {
        .state=options,
        .genFunc=+[](const url_options& options, TEST_JAM_SEED_TYPE seed) -> std::string {
            TestJamDomainOptions domainOptions = {
                    .minLen=options.minLen,
                    .maxLen=options.maxLen
            };

            std::vector<TestJamConstString> schemas;
            schemas.reserve(options.possibleSchemas.size());
            for (const auto& str : options.possibleSchemas) {
                schemas.push_back({
                    .length=str.size(),
                    .data=str.c_str()
                });
            }

            TestJamConstStringArray arr;
            arr.values = schemas.data();
            arr.size = schemas.size();

            TestJamUrlOptions opts = {
                    .flags=options.flags,
                    .possibleSchemas=&arr,
                    .minLen=options.minLen,
                    .maxLen=options.maxLen,
                    .domainOptions=&domainOptions,
            };

            TestJamString str;
            test_jam_internet_url(&str, static_cast<uint64_t>(seed), &opts, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        },
        .simplifyFunc=+[](const url_options& options, TEST_JAM_SEED_TYPE seed) -> common::sequence<std::string> {
            TestJamDomainOptions domainOptions = {
                    .minLen=options.minLen,
                    .maxLen=options.maxLen
            };

            return {
                UrlSimplifierCallable(
                    static_cast<uint64_t>(seed),
                    options.flags,
                    options.possibleSchemas,
                    options.minLen,
                    options.maxLen,
                    domainOptions
                )
            };
        }
    };
}


struct EmailSimplifierCallable {
    std::shared_ptr<TestJamStringSimplifier> simplifier;
    TestJamDomainOptions domainOpts;
    TestJamEmailOptions opts;

    EmailSimplifierCallable(size_t seed, unsigned flags, size_t minLen, size_t maxLen, TestJamDomainOptions domainOptions)
            : simplifier(
                    new TestJamStringSimplifier(),
                    +[](TestJamStringSimplifier* s)-> void {
                        test_jam_string_simplifier_free(s);
                        delete s;
                    }
            )
            , domainOpts(domainOptions)
            , opts(
                    TestJamEmailOptions{
                           .flags=flags,
                           .minLen=minLen,
                           .maxLen=maxLen,
                           .domainOptions=&domainOpts
                   }
              )
    {
        test_jam_internet_simplifier_email(simplifier.get(), static_cast<uint64_t>(seed), &opts, nullptr);
    }

    std::optional<std::string> operator()() const {
        std::optional<std::string> res = std::nullopt;
        if (simplifier->hasValue) {
            res = std::string(simplifier->value.data, simplifier->value.length);
            test_jam_string_simplifier_next(simplifier.get());
        }
        return res;
    }
};

auto test_jam::gens::internet::emails(const email_options& options) -> common::shrink_state_generator<std::string, email_options> {
    return {
        .genFunc = +[](const email_options& options, TEST_JAM_SEED_TYPE seed) -> std::string {
            TestJamDomainOptions domainOptions = {
                    .minLen=options.minLen,
                    .maxLen=options.maxLen
            };
            TestJamEmailOptions opts = {
                    .flags=options.flags,
                    .minLen=options.minLen,
                    .maxLen=options.maxLen,
                    .domainOptions=&domainOptions
            };
            TestJamString str;
            test_jam_internet_email(&str, static_cast<uint64_t>(seed), &opts, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        },
        .simplifyFunc = +[](const email_options& options, TEST_JAM_SEED_TYPE seed) -> common::sequence<std::string> {
            TestJamDomainOptions domainOptions = {
                    .minLen=options.minLen,
                    .maxLen=options.maxLen
            };
            return {
                EmailSimplifierCallable(static_cast<uint64_t>(seed), options.flags, options.minLen, options.maxLen, domainOptions)
            };
        }
    };
};
