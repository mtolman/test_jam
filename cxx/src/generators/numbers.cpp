#include <test-jam-cxx/gens/numbers.h>
#include <test-jam/generators/numbers.h>
#include <test-jam/simplifiers/numbers.h>

#include "utils.h"

namespace utils = test_jam::utils;
namespace numbers = test_jam::gens::numbers;

#define TEST_JAM_NUMBER_INT_DECL(TYPE, CAP_TYPE) \
auto test_jam::gens::numbers::TYPE(numbers::int_opts<TYPE ## _t> opts) -> common::shrink_state_generator<TYPE ## _t, numbers::int_opts<TYPE ## _t>> { \
    return {                                     \
            .state=opts, \
            .genFunc = +[](const numbers::int_opts<TYPE ## _t>& opts, TEST_JAM_SEED_TYPE seed) { return test_jam_numbers_ ## TYPE(seed, opts.min, opts.max); },        \
            .simplifyFunc = +[](const numbers::int_opts<TYPE ## _t>& opts, TEST_JAM_SEED_TYPE seed) -> common::sequence<TYPE ## _t> {                        \
                auto simplifier = TestJam ## CAP_TYPE ## Simplifier{};                                       \
                test_jam_numbers_simplifier_ ## TYPE(&simplifier, static_cast<uint64_t>(seed), opts.min, opts.max);                           \
                return common::sequence<TYPE ## _t> {                                                        \
                        [simplifier]() mutable -> std::optional<TYPE ## _t> {                                \
                            auto* ptr = test_jam_ ## TYPE ##_simplifier_next(&simplifier);                   \
                            if (ptr) {          \
                                return *ptr;    \
                            }                   \
                            return std::nullopt;\
                        }                       \
                };                              \
            }                                   \
    };                                          \
}


TEST_JAM_NUMBER_INT_DECL(uint64, Uint64)
TEST_JAM_NUMBER_INT_DECL(uint32, Uint32)
TEST_JAM_NUMBER_INT_DECL(uint16, Uint16)
TEST_JAM_NUMBER_INT_DECL(uint8, Uint8)

TEST_JAM_NUMBER_INT_DECL(int64, Int64)
TEST_JAM_NUMBER_INT_DECL(int32, Int32)
TEST_JAM_NUMBER_INT_DECL(int16, Int16)
TEST_JAM_NUMBER_INT_DECL(int8, Int8)

auto test_jam::gens::numbers::f32(numbers::float_opts<float> opts) -> common::shrink_state_generator<float, float_opts<float>> {
    return {
        .state=opts,
        .genFunc=[](const numbers::float_opts<float>& opts, TEST_JAM_SEED_TYPE seed) {
            return test_jam_numbers_float(static_cast<uint64_t>(seed), opts.min, opts.max, opts.allowNan, opts.allowInf);
        },
        .simplifyFunc=+[](const numbers::float_opts<float>& opts, TEST_JAM_SEED_TYPE seed) -> common::sequence<float> {
            auto simplifier = TestJamFloatSimplifier{};
            test_jam_numbers_simplifier_float(&simplifier,  static_cast<uint64_t>(seed), opts.min, opts.max, opts.allowNan, opts.allowInf);
            return common::sequence<float> {
                    [simplifier]()mutable -> std::optional<float> {
                        auto *ptr = test_jam_float_simplifier_next(&simplifier);
                        if (ptr) { return *ptr; }
                        return std::nullopt;
                    }
            };
        }
    };
}

auto test_jam::gens::numbers::f64(numbers::float_opts<double> opts) -> common::shrink_state_generator<double, numbers::float_opts<double>> {
    return {
        .state=opts,
        .genFunc=+[](const numbers::float_opts<double>& opts, TEST_JAM_SEED_TYPE seed) {
            return test_jam_numbers_double(static_cast<uint64_t>(seed), opts.min, opts.max, opts.allowNan, opts.allowInf);
        },
        .simplifyFunc=+[](const numbers::float_opts<double>& opts, TEST_JAM_SEED_TYPE seed) -> common::sequence<double> {
            auto simplifier = TestJamDoubleSimplifier{};
            test_jam_numbers_simplifier_double(&simplifier, static_cast<uint64_t>(seed), opts.min, opts.max, opts.allowNan, opts.allowInf);
            return common::sequence<double> {
                    [simplifier]()mutable -> std::optional<double> {
                        auto *ptr = test_jam_double_simplifier_next(&simplifier);
                        if (ptr) { return *ptr; }
                        return std::nullopt;
                    }
            };
        }
    };
}
