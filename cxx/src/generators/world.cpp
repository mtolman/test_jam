#include <test-jam-cxx/gens/world.h>
#include <test-jam/generators/world.h>
#include "utils.h"

namespace world = test_jam::gens::world;
namespace utils = test_jam::utils;

static constexpr auto to_country = +[](const TestJamCountry& c) -> world::country {
    return {
            .name=utils::to_str(c.name),
            .genc=utils::to_str_opt(c.genc),
            .iso2=utils::to_str_opt(c.iso2),
            .iso3=utils::to_str_opt(c.iso3),
            .isoNum=utils::to_str_opt(c.isoNum),
            .stanag=utils::to_str_opt(c.stanag),
            .tld=utils::to_str_opt(c.tld),
    };
};

auto world::countries() -> common::no_shrink_generator<country> {
    return {
        .genFunc=+[](TEST_JAM_SEED_TYPE seed) {
            return to_country(test_jam_world_country(static_cast<uint64_t>(seed)));;
        }
    };
}


auto world::timezones() -> common::no_shrink_generator<std::string> {
    return {
        .genFunc=+[](TEST_JAM_SEED_TYPE seed) {
            return utils::to_str(test_jam_world_timezone(static_cast<uint64_t>(seed)));
        }
    };
}
