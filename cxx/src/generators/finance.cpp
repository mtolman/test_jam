#include <test-jam-cxx/gens/financial.h>
#include <test-jam/generators/financial.h>

#include "utils.h"
#include "defer.h"

namespace utils = test_jam::utils;
namespace financial = test_jam::gens::financial;

auto financial::account_names(const common::complexity_range& ranges) -> common::no_shrink_complexity_generator<std::string> {
    return {
            .state=ranges,
            .genFunc = [](const common::complexity_range& ranges, TEST_JAM_SEED_TYPE seed) -> std::string {
                const auto r = test_jam_financial_account_name_complexity(
                        static_cast<uint64_t>(seed),
                        static_cast<TestJamComplexity>(ranges.minComplexity),
                        static_cast<TestJamComplexity>(ranges.maxComplexity)
                );
                return utils::to_str(r);
            }
    };
}

static auto convert_cc_network(const TestJamCcNetwork& r) -> financial::credit_card_network {
    auto starts = std::vector<std::string>{};
    starts.resize(r.starts.size);
    for (size_t i = 0; i < r.starts.size; ++i) {
        starts[i] = utils::to_str(r.starts.values[i]);
    }

    auto lengths = std::vector<int32_t>{};
    lengths.resize(r.lengths.size);
    for (size_t i = 0; i < r.lengths.size; ++i) {
        lengths[i] = r.lengths.values[i];
    }

    return {
            .id=utils::to_str(r.id),
            .name=utils::to_str(r.name),
            .starts=starts,
            .lengths=lengths,
            .cvvLength=r.cvvLen
    };
}

auto financial::credit_card_networks() -> common::no_shrink_generator<credit_card_network> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> credit_card_network {
                return convert_cc_network(test_jam_financial_cc_network(static_cast<uint64_t>(seed)));
            }
    };
}

auto financial::currencies() -> common::no_shrink_generator<currency> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> currency {
                const auto r = test_jam_financial_currency(static_cast<uint64_t>(seed));
                return {
                        .name=utils::to_str(r.name),
                        .isoCode=utils::to_str(r.isoCode),
                        .symbol=utils::to_str(r.symbol),
                };
            }
    };
}

auto financial::transaction_types() -> common::no_shrink_generator<std::string> {
    return {
            .genFunc = +[](TEST_JAM_SEED_TYPE seed) -> std::string {
                return utils::to_str(test_jam_financial_transaction_type(static_cast<uint64_t>(seed)));
            }
    };
}

auto financial::credit_cards(const credit_card_options& opts) -> common::no_shrink_state_generator<credit_card, credit_card_options> {
    return {
        .state = opts,
        .genFunc = +[](const credit_card_options& opts, TEST_JAM_SEED_TYPE seed) -> credit_card {
            constexpr auto to_c_date = +[](const credit_card::exp_date& d) -> TestJamFinancialCcDate {
                return {
                        .year=d.year,
                        .month=d.month
                };
            };

            const auto minDate = to_c_date(opts.minExpDate),
                       maxDate = to_c_date(opts.maxExpDate);

            TestJamFinancialCreditCard cc;
            test_jam_financial_credit_card(&cc, static_cast<uint64_t>(seed), &minDate, &maxDate, nullptr);
            defer { test_jam_free_financial_credit_card_contents(&cc, nullptr); };
            return {
                    .network = convert_cc_network(cc.network),
                    .number=utils::to_str(cc.number),
                    .cvv=utils::to_str(cc.cvv),
                    .expirationDate={
                            .year=cc.expirationDate.year,
                            .month=cc.expirationDate.month
                    },
            };
        }
    };
}


auto financial::account_numbers(size_t length) -> common::no_shrink_state_generator<std::string, size_t> {
    return {
        .state = length,
        .genFunc = +[](const size_t& length, TEST_JAM_SEED_TYPE seed) {
            TestJamString str;
            test_jam_financial_account_number(&str, static_cast<uint64_t>(seed), length, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        }
    };
}

auto financial::pins(size_t length) -> common::no_shrink_state_generator<std::string, size_t> {
    return account_numbers(length);
}

auto financial::amounts(const amount_options& opts) -> common::no_shrink_state_generator<std::string, amount_options> {
    return {
            .state=opts,
            .genFunc=+[](const amount_options& opts, TEST_JAM_SEED_TYPE seed) {
                TestJamFinancialAmountOptions options = {
                        .max=opts.max,
                        .min=opts.min,
                        .decimalPlaces=opts.decimalPlaces,
                        .decimalSeparator=opts.decimalSeparator.c_str(),
                        .symbol=opts.symbol.c_str(),
                        .prefixSymbol=opts.prefixSymbol
                };

                TestJamString str;
                test_jam_financial_amount(&str, static_cast<uint64_t>(seed), &options, nullptr);
                defer { test_jam_free_string_contents(&str, nullptr); };
                return utils::to_str(str);
            }
    };
}

auto financial::bics(bool includeBranchCode) -> common::no_shrink_state_generator<std::string, bool> {
    return {
        .state=includeBranchCode,
        .genFunc=+[](const bool& includeBranchCode, TEST_JAM_SEED_TYPE seed) {TestJamString str;
            test_jam_financial_bic(&str, static_cast<uint64_t>(seed), includeBranchCode, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        }
    };
}

auto financial::masked_numbers(const masked_number_options& opts) -> common::no_shrink_state_generator<std::string, masked_number_options> {
    return {
        .state=opts,
        .genFunc=+[](const masked_number_options& opts, TEST_JAM_SEED_TYPE seed) {    TestJamFinancialMaskedNumOptions options = {
                .unMaskedLen=opts.unMaskedLen,
                .maskedLen=opts.maskedLen,
                .maskedChar=opts.maskedChar
        };
            TestJamString str;
            test_jam_financial_masked_num(&str, static_cast<uint64_t>(seed), &options, nullptr);
            defer { test_jam_free_string_contents(&str, nullptr); };
            return utils::to_str(str);
        }
    };
}
