#pragma once

#include <test-jam/objects.h>
#include <string>
#include <optional>

namespace test_jam::utils {
    inline auto to_str(const TestJamString& cstr) -> std::string { return {cstr.data, cstr.length}; }
    inline auto to_str(const TestJamConstString& cstr) -> std::string { return {cstr.data, cstr.length}; }
    inline auto to_str_opt(const TestJamConstStringOptional& cstr) -> std::optional<std::string> {
        if (cstr.present) {
            return to_str(cstr.str);
        }
        return std::nullopt;
    }
}
