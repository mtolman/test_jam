#include <test-jam-cxx/gens/person.h>
#include <test-jam/generators/person.h>
#include "utils.h"

auto test_jam::gens::person::person(common::complexity_range complexity) -> common::no_shrink_complexity_generator<Person> {
    return {
        .state=complexity,
        .genFunc=+[](const common::complexity_range& complexityRange, TEST_JAM_SEED_TYPE seed) {
            const auto cPerson = test_jam_person_complexity(static_cast<uint64_t>(seed),
                                                            static_cast<TestJamComplexity>(complexityRange.minComplexity),
                                                            static_cast<TestJamComplexity>(complexityRange.maxComplexity));
            std::string fullName;

            if (cPerson.westernNameOrder) {
                fullName = utils::to_str(cPerson.givenName) + " " + utils::to_str(cPerson.surname);
            }
            else {
                fullName = utils::to_str(cPerson.surname) + " " + utils::to_str(cPerson.givenName);
            }

            return Person{
                    .sex=static_cast<Sex>(cPerson.sex),
                    .fullName=fullName,
                    .givenName=utils::to_str(cPerson.givenName),
                    .surname=utils::to_str(cPerson.surname),
                    .prefix=utils::to_str_opt(cPerson.prefix),
                    .suffix=utils::to_str_opt(cPerson.suffix)
            };
        }
    };
}

auto test_jam::gens::person::given_name(common::complexity_range complexity) -> common::no_shrink_complexity_generator<std::string> {
    return {
        .state=complexity,
        .genFunc=+[](const common::complexity_range& complexityRange, TEST_JAM_SEED_TYPE seed) {
            return utils::to_str(
                    test_jam_person_given_name_complexity(
                            static_cast<uint64_t>(seed),
                            static_cast<TestJamComplexity>(complexityRange.minComplexity),
                            static_cast<TestJamComplexity>(complexityRange.maxComplexity)
                    )
            );
        }
    };
}

auto test_jam::gens::person::surname(test_jam::gens::common::complexity_range complexity)
    -> test_jam::gens::common::no_shrink_complexity_generator<std::string>
{
    return {
            .state=complexity,
            .genFunc=[](const common::complexity_range& complexity, TEST_JAM_SEED_TYPE seed) {
                return utils::to_str(test_jam_person_surname_complexity(
                        static_cast<uint64_t>(seed),
                        static_cast<TestJamComplexity>(complexity.minComplexity),
                        static_cast<TestJamComplexity>(complexity.maxComplexity)));
            }
    };
}

auto test_jam::gens::person::prefix(test_jam::gens::common::complexity_range complexity)
    -> test_jam::gens::common::no_shrink_complexity_generator<std::optional<std::string>>
{
    return {
            .state=complexity,
            .genFunc=[](const common::complexity_range& complexity, TEST_JAM_SEED_TYPE seed) {
                return utils::to_str_opt(test_jam_person_prefix_complexity(
                       static_cast<uint64_t>(seed),
                       static_cast<TestJamComplexity>(complexity.minComplexity),
                       static_cast<TestJamComplexity>(complexity.maxComplexity))
               );
            }
    };
}

auto test_jam::gens::person::suffix(test_jam::gens::common::complexity_range complexity)
    -> test_jam::gens::common::no_shrink_complexity_generator<std::optional<std::string>>
{
    return {
            .state=complexity,
            .genFunc=[](const common::complexity_range& complexity, TEST_JAM_SEED_TYPE seed) {
                return utils::to_str_opt(test_jam_person_suffix_complexity(
                       static_cast<uint64_t>(seed),
                       static_cast<TestJamComplexity>(complexity.minComplexity),
                       static_cast<TestJamComplexity>(complexity.maxComplexity)));
            }
    };
}

auto test_jam::gens::person::full_name(test_jam::gens::common::complexity_range complexity)
    -> test_jam::gens::common::no_shrink_complexity_generator<std::string>
{
    return {
            .state=complexity,
            .genFunc=+[](const common::complexity_range& complexity, TEST_JAM_SEED_TYPE seed) -> std::string {
                const auto cPerson = test_jam_person_complexity(
                        static_cast<uint64_t>(seed),
                        static_cast<TestJamComplexity>(complexity.minComplexity),
                        static_cast<TestJamComplexity>(complexity.maxComplexity)
                );
                std::string fullName;

                if (cPerson.westernNameOrder) {
                    fullName = utils::to_str(cPerson.givenName) + " " + utils::to_str(cPerson.surname);
                }
                else {
                    fullName = utils::to_str(cPerson.surname) + " " + utils::to_str(cPerson.givenName);
                }
                return fullName;
            }
    };
}

auto test_jam::gens::person::sex() -> common::no_shrink_generator<Sex> {
    return {
        .genFunc=+[](TEST_JAM_SEED_TYPE seed) {
            return static_cast<uint64_t>(seed) % 2 == 1 ? Sex::MALE : Sex::FEMALE;
        }
    };
}
