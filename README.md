# Test Jam

Test Jam is a property testing and test data generation library. It supports C, C++, TypeScript/JavaScript, and WASM.
Test Jam provides both data generation and automatic shrinking, and it has zero allocation APIs for C code.

## Building

Currently, this is only available from source. I have yet to publish the NPM package.

### C/C++

Simply use CMake's FetchContent package to include the Test Jam repository. You'll want to set the following options to `OFF`:

- `test_jam_BUILD_JS`
- `test_jam_BUILD_WASM`

### JavaScript/TypeScript

For the JavaScript/TypeScript build, you'll need to install NPM and CMake. You'll need to initialize the CMake configuration with the flag `test_jam_BUILD_JS` set to `ON`. Finally, run the `build` CMake preset.

### WASM

You'll need to install the Emscripten toolchain and then configure the project with `emcmake` (or use Emscripten's CMake toolchain file). You'll also need to set the option `test_jame_BUILD_WASM` to `ON`.

## Property Testing Usage

The property testing API is available for C++ and TypeScript/JavaScript. C doesn't have property testing APIs available at the moment.

### C++

The recommended way to do property testing in C++ is to use the Doctest integration. Test cases would be written as follows:

```cpp
// Important! You need to include doctest yourself, it is not bundled with test jam
#include <doctest.h>
#include <test-jam-cxx/integrations/doctest.h>

// Include some generators
#include <test-jam-cxx/gens/numbers.h>
namespace n = test_jam::gens::numbers;

// Can use test suites as normal
TEST_SUITE("My Doctest Suite") {
    // This declares a property test case
    // You'll provide a name followed by the generators to use
    TEST_JAM_CASE("My test case",
        n::f32(), n::f32(), n::f32(), n::f32()
    )
    // name the generated values
    (float x, float y, float a, float b) {
        // Define the test case here
        auto v1 = Vec2d{x, y};
        auto v2 = Vec2d{a, b};
        auto v3 = v1 + v2;

        // Use the TEST_JAM check macros for shrinking to work
        TEST_JAM_CHECK_APPROX_EQ(v3.x, v1.x + v2.x, 0.00001f);
        TEST_JAM_CHECK_APPROX_EQ(v3.y, v1.y + v2.y, 0.00001f);
    }
}
```

### TypeScript/JavaScript

> Tested with Jest only, though should work with other testing libraries

For TypeScript/JavaScript, the `property` method is provided to define a property. Currently it does need to be inside an `it` block (at least for Jest).

```JavaScript
const {property} = require('path/to/test_jam/property')
const {Ints} = require('path/to/test_jam/generators/numbers')

describe('example', () => {
    it('handles property tests', () => {
        // define the property
        property('commutative')
            .withGens(
                new Ints({min: 1, max: 500}),
                new Ints({min: 1, max: 500})
            )
            // Optional, though usually not needed
            .withSeed(1940392834)
            .test((a, b) => {
                expect(a + b).toEqual(b + a)
            })
    })
})
```

## Data Generation Usage

C, C++, and TypeScript/JavaScript all support data generators. This includes simple random primitives (e.g. numbers) to more complicated data formats (e.g. email addresses and URLs). C does have zero allocation APIs available, however they usually require that enough space be given for a reasonable data element size.

Many of these modules are inspired by FakerJS. However, there are differences in what is supported and the data generation libraries require a seed being passed in, rather than using a global random number generator.

### Aviation Data

The aviation data module generates aviation-related data. The data used includes lists of real airports and planes. The data provided is prebuilt into the distributed code/binary.

#### C

```c
#include <test-jam/generators/aviation.h>

int main(int argc, char** argv) {
    printf("%s", test_jam_aviation_aircraft_type(343).data);
    return 0;
}
```

#### C++

```cpp
#include <test-jam-cxx/gens/aviation.h>
#include <iostream>

int main() {
    std::cout << test_jam::gens::aviation::aircraft_types().create(232));
}
```

#### JavaScript/TypeScript

```javascript
const {AircraftTypes} = require('path/to/test_jam/generators/aviation')

console.log((new AircraftTypes()).create(432))
```
