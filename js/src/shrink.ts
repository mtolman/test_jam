import {Generator} from "./common";

export function shrink<T>(seed: number, generator: Generator<T>, predicate: (_: T) => boolean, maxShrinks: number = 8000): T {
    let smallest = generator.create(seed)
    let trials = 0
    for (const simplification of generator.simplify(seed)) {
        if (predicate(simplification) || trials++ > maxShrinks) {
            break
        }
        smallest = simplification
    }
    return smallest
}