// TypeScript port of TinyMT.
// Original C code: https://github.com/MersenneTwister-Lab/TinyMT

const minLoop = 8
const preLoop = 8
const mask = 0x7fffffff >>> 0
const sh0 = 1
const sh1 = 10
const sh8 = 8

class RandomState {
    public status: number[] = (new Array(4)).fill(0);
    public mat1: number = 0x8f7011ee >>> 0;
    public mat2: number = 0xfc78ff1f >>> 0;
    public tmat: number = 0x3793fdff >>> 0;
}

function multiplyUnsigned(leftHand: number, rightHand: number) : number {
    return Math.imul(leftHand, rightHand) >>> 0;
}

function defaultSeed() {
    return Math.round(Math.random() * 569023) + (new Date()).getMilliseconds()
}

export default class Random {
    private state = new RandomState()

    /**
     * Generates a new tiny random number generator
     * @param seed Seed to use (defaults to a random seed)
     */
    public constructor(seed: number = defaultSeed()) {
        this.state.status[0] = seed >>> 0
        this.state.status[1] = this.state.mat1
        this.state.status[2] = this.state.mat2
        this.state.status[3] = this.state.tmat
        for (let i = 1; i < minLoop; ++i) {
            const xorL = (this.state.status[(i - 1) & 3] >>> 0);
            const xorR = this.state.status[(i - 1) & 3] >>> 30;
            const mult = multiplyUnsigned(1812433253 >>> 0, xorL ^ xorR);
            const sum = i + mult;
            this.state.status[i & 3] = (this.state.status[i & 3] ^ sum) >>> 0;
        }
        this.periodCertification()
        for (let i = 0; i < preLoop; ++i) {
            this.nextState()
        }
    }

    public uint() : number {
        this.nextState()
        return this.temper()
    }

    public float() : number {
        this.nextState()
        return this.temperConv() - 1
    }

    private periodCertification() {
        if ((this.state.status[0] & mask) === 0
            && this.state.status[1] == 0
            && this.state.status[2] == 0
            && this.state.status[3] === 0) {
            this.state.status[0] = 'M'.charCodeAt(0)
            this.state.status[1] = 'A'.charCodeAt(0)
            this.state.status[2] = 'T'.charCodeAt(0)
            this.state.status[3] = 'T'.charCodeAt(0)
        }
    }

    private nextState() {
        let y = this.state.status[3]
        let x = ((((this.state.status[0] & mask) >>> 0
            ^ this.state.status[1]) >>> 0)
            ^ this.state.status[2]) >>> 0
        x = (x ^ ((x << sh0) >>> 0)) >>> 0
        y = (y ^ (((y >>> sh0) ^ x) >>> 0)) >>> 0
        this.state.status[0] = this.state.status[1]
        this.state.status[1] = this.state.status[2]
        this.state.status[2] = (x ^ ((y << sh1) >>> 0)) >>> 0
        this.state.status[3] = y
        const a = -((y & 1) | 0) & (this.state.mat1 | 0)
        const b = -((y & 1) | 0) & (this.state.mat2 | 0)
        this.state.status[1] = (this.state.status[1] ^ (a >>> 0)) >>> 0
        this.state.status[2] = (this.state.status[2] ^ (b >>> 0)) >>> 0
    }

    private temper() {
        let t0 = this.state.status[3] >>> 0
        let t1 = (this.state.status[0] + (this.state.status[2] >>> sh8)) >>> 0
        t0 = (t0 ^ t1) >>> 0
        if ((t1 & 1) !== 0) {
            t0 = (t0 ^ this.state.tmat) >>> 0
        }
        return t0 >>> 0
    }

    private temperConv() {
        // 1.0 <= result < 2.0
        let t0 = this.state.status[3] >>> 0
        let t1 = (this.state.status[0] + (this.state.status[2] >>> sh8)) >>> 0
        t0 ^= t1
        t0 >>>= 0
        let u = 0
        if ((t1 & 1) !== 0) {
            u = (((t0 ^ this.state.tmat) >>> 9) |0x3f800000) >>> 0
        }
        else {
            u = ((t0 >>> 9) | 0x3f800000) >>> 0
        }
        const buffer = new ArrayBuffer(4)
        const bytes = new Uint8Array(buffer)
        bytes[0] = (u >>> 24) & 0xff
        bytes[1] = (u >>> 16) & 0xff
        bytes[2] = (u >>> 8) & 0xff
        bytes[3] = u & 0xff
        const dataView = new DataView(buffer)
        return dataView.getFloat32(0, false)
    }
}
