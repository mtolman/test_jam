import Random from "./tinymt";

export interface Generator<T> {
    create(seed?: number): T;
    simplify(seed: number): Iterable<T>;
}

export abstract class NoShrinkGenerator<T> implements Generator<T>{
    public abstract create(seed?: number): T;

    public simplify(seed: number): Iterable<T> { return []; }
}

export abstract class ShrinkGenerator<T> implements Generator<T>{
    public abstract create(seed?: number): T;

    public abstract simplify(seed: number): Iterable<T>;
}

export abstract class IndexGenerator<T = string, R = any> extends NoShrinkGenerator<T>{
    public create(seed?: number): T {
        const rnd = new Random(seed)
        const elems = this.elements()
        return this.convert(elems[rnd.uint() % elems.length])
    }

    protected abstract elements() : R[];

    protected convert(elem: R) : T {
        if (typeof elem === 'object' && Object.values(elem as any).length === 1) {
            return Object.values(elem as any)[0] as any
        }
        return elem as any
    }
}

export enum Complexity {
    RUDIMENTARY = 'RUDIMENTARY',
    BASIC = 'BASIC',
    INTERMEDIATE = 'INTERMEDIATE',
    ADVANCED = 'ADVANCED',
    COMPLEX = 'COMPLEX',
    EDGE_CASE = 'EDGE_CASE'
}

export const complexities = [
    Complexity.RUDIMENTARY,
    Complexity.BASIC,
    Complexity.INTERMEDIATE,
    Complexity.ADVANCED,
    Complexity.COMPLEX,
    Complexity.EDGE_CASE
]

export abstract class ComplexityGenerator<T = string, R = any> extends NoShrinkGenerator<T>{
    public create(seed?: number): T {
        const rnd = new Random(seed)
        const elems = this.elements(complexities[rnd.uint() % complexities.length])
        return this.convert(elems[rnd.uint() % elems.length])
    }

    protected abstract elements(complexity: Complexity) : R[];

    protected convert(elem: R) : T {
        if (typeof elem === 'object' && Object.values(elem as any).length === 2) {
            return (elem as any)[Object.keys(elem as any).filter(k => k.toLowerCase() !== 'complexity')[0]] as any
        }
        return elem as any
    }
}


/**
 * Returns a range of number
 * @param fromOrTo If `to` is not provided, the (exclusive) end. If `to` is provided, the (inclusive) start
 * @param to If provided, the (exclusive) end. If not provided, the range will start at 0
 */
export function range(fromOrTo: number, to?: number) : number[] {
    if (typeof to === "undefined") {
        return range(0, fromOrTo)
    }
    else if (fromOrTo < to) {
        return [...(new Array(to - fromOrTo)).keys()].map(i => i + fromOrTo)
    }
    else {
        return []
    }
}


export function final<T>(iter: Iterable<T>) : T|undefined {
    let last = undefined
    for (const e of iter) {
        last = e
    }
    return last
}


export function hexStr(byte: number) {
    byte |= 0
    const strs = "0123456789ABCDEF"
    const lower = byte & 0b1111
    const upper = (byte >> 4) & 0b1111
    return `${strs[upper]}${strs[lower]}`
}

export function* zip(...arrs: Iterable<any>[]) {
    const iters: (Iterator<any>|null)[] = arrs.map(iter => iter[Symbol.iterator]())
    const values = (new Array(arrs.length)).fill(null)
    while(true) {
        let oneHadValue = false
        for (let a = 0; a < iters.length; ++a) {
            const iter = iters[a]
            if (iter === null) {
                continue
            }
            const res = iter.next()
            if (res.done) {
                iters[a] = null
                values[a] = null
            }
            else {
                values[a] = res.value
                oneHadValue = true
            }
        }
        if (oneHadValue) {
            yield [...values]
        }
        else {
            return
        }
    }
}


export function* limit<T>(limit: number, iter: Iterable<T>) {
    for (const v of iter) {
        yield v
        if (--limit == 0) {
            return
        }
    }
}

