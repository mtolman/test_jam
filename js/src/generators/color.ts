import {IndexGenerator, NoShrinkGenerator} from "../common";
import named from '../data/color/named'
import Random from "../tinymt";

export interface RgbColor {
    red: number,
    green: number,
    blue: number
}

export interface RgbaColor extends RgbColor {
    alpha: number
}

export interface NamedColor extends RgbColor {
    name:string
    hex:string
}

export interface HslColor {
    hue: number
    saturation: number
    luminosity: number
}

export interface HslaColor extends HslColor {
    alpha: number
}

export class Named extends IndexGenerator<NamedColor, {name:string,hex:string,red:string,green:string,blue:string}>{
    protected elements(): { name: string; hex: string; red: string; green: string; blue: string }[] {
        return named
    }

    protected convert(elem: { name:string,hex:string,red:string,green:string,blue:string }): NamedColor {
        return {
            name: elem.name,
            hex: elem.hex,
            red: +elem.red,
            green: +elem.green,
            blue: +elem.blue
        }
    }
}

export class Rgb extends NoShrinkGenerator<RgbColor> {
    create(seed?: number): RgbColor {
        const rnd = new Random(seed)
        return {
            red: rnd.uint() % 256,
            green: rnd.uint() % 256,
            blue: rnd.uint() % 256
        }
    }
}

export class Rgba extends NoShrinkGenerator<RgbaColor> {
    create(seed?: number): RgbaColor {
        const rnd = new Random(seed)
        return {
            red: rnd.uint() % 256,
            green: rnd.uint() % 256,
            blue: rnd.uint() % 256,
            alpha: rnd.uint() % 256
        }
    }
}

export class Hsl extends NoShrinkGenerator<HslColor> {
    create(seed?: number): HslColor {
        const rnd = new Random(seed)
        return {
            hue: rnd.float() * 360,
            saturation: rnd.float(),
            luminosity: rnd.float()
        }
    }
}

export class Hsla extends NoShrinkGenerator<HslaColor> {
    create(seed?: number): HslaColor {
        const rnd = new Random(seed)
        return {
            hue: rnd.float() * 360,
            saturation: rnd.float(),
            luminosity: rnd.float(),
            alpha: rnd.float()
        }
    }
}
