import {Complexity, ComplexityGenerator, IndexGenerator, NoShrinkGenerator} from "../common";
import account_names from "../data/financial/account_names";
import credit_card_networks from "../data/financial/credit_card_networks";
import currencies from "../data/financial/currencies";
import transaction_types from "../data/financial/transaction_types";
import Random from "../tinymt";
import {AsciiGenerator, charsets} from "./ascii";
import {Countries} from "./world";

export interface CreditCardNetwork {
    id: string,
    name: string,
    starts: string[],
    lengths: number[],
    cvvLen: number
}

export interface Currency {
    isoCode:string
    name:string
    symbol:string
}

export class AccountNames extends ComplexityGenerator {
    protected elements(complexity: Complexity): any[] {
        return account_names[complexity];
    }
}

export class CreditCardNetworks extends IndexGenerator<CreditCardNetwork, {id: string,name: string,starts: string[],lengths: string[],cvvlen: string}> {
    protected elements(): {id: string,name: string,starts: string[],lengths: string[],cvvlen: string}[] {
        return credit_card_networks;
    }

    protected convert(elem: {id: string,name: string,starts: string[],lengths: string[],cvvlen: string}): CreditCardNetwork {
        return {
            id:elem.id,
            name:elem.name,
            starts:elem.starts,
            lengths:elem.lengths.map(e => +e),
            cvvLen:+elem.cvvlen
        }
    }
}

export class Currencies extends IndexGenerator<Currency> {
    protected elements(): any[] {
        return currencies;
    }
}

export class TransactionTypes extends IndexGenerator {
    protected elements(): any[] {
        return transaction_types
    }
}

export interface CreditCard {
    network: CreditCardNetwork
    number: string
    cvv: string
    expirationDate: {
        year: number
        month: number
    }
}

export interface CreditCardOptions {
    minExpiration?: {year: number, month: number},
    maxExpiration?: {year: number, month: number}
}

const defaultCardOptions = {
    minExpiration: {year: 2000, month: 1},
    maxExpiration: {year: 2200, month: 12}
}

export class CreditCards extends NoShrinkGenerator<CreditCard> {
    private networkGenerator = new CreditCardNetworks()
    private minExpiration: { month: number; year: number };
    private maxExpiration: { month: number; year: number };

    public constructor(options: CreditCardOptions = {}) {
        super();
        this.minExpiration = {...defaultCardOptions.minExpiration, ...(options.minExpiration || {})}
        this.maxExpiration = {...defaultCardOptions.maxExpiration, ...(options.maxExpiration || {})}
    }

    create(seed?: number): CreditCard {
        const rnd = new Random(seed)
        const network = this.networkGenerator.create(rnd.uint())
        const len = network.lengths[rnd.uint() % network.lengths.length]
        const number = this.computeChecksum(new AsciiGenerator({
            characterSet: charsets.numeric,
            minLen: len,
            maxLen: len
        }).create(rnd.uint()))
        const cvv = new AsciiGenerator({
            characterSet: charsets.numeric,
            minLen: network.cvvLen,
            maxLen: network.cvvLen
        }).create(rnd.uint())

        const expirationDate = {
            year: rnd.uint() % (this.maxExpiration.year - this.minExpiration.year + 1) + this.minExpiration.year,
            month: 1
        }

        if (this.maxExpiration.year === expirationDate.year && this.minExpiration.year === expirationDate.year) {
            expirationDate.month = rnd.uint() % (this.maxExpiration.month - this.minExpiration.month + 1) + this.minExpiration.month
        }
        else if (this.maxExpiration.year === expirationDate.year) {
            expirationDate.month = rnd.uint() % (this.maxExpiration.month) + 1
        }
        else if (this.minExpiration.year === expirationDate.year) {
            expirationDate.month = rnd.uint() % (12 - this.minExpiration.month + 1) + this.minExpiration.month
        }
        else {
            expirationDate.month = rnd.uint() % 12 + 1
        }

        return {
            network: network,
            cvv: cvv,
            number: number,
            expirationDate: expirationDate
        }
    }

    private computeChecksum(cardNumber: string) {
        const zeroCharCode = '0'.charCodeAt(0)
        let sum = 0;
        let multiplier = 2;
        for (let i = cardNumber.length - 1; i > 0; --i) {
            const index = i - 1;
            sum += (cardNumber[index].charCodeAt(0) - zeroCharCode) * multiplier;
            // flip between 1 and 2
            multiplier ^= 3;
        }
        return cardNumber.slice(0, -1) + String.fromCharCode(9 - (sum + 9) % 10 + zeroCharCode)
    }
}

export class AccountNumbers extends NoShrinkGenerator<string> {
    private numericGenerator: AsciiGenerator

    public constructor(length: number = 15) {
        super();
        this.numericGenerator = new AsciiGenerator({
            characterSet:charsets.numeric,
            minLen:length,
            maxLen:length
        })
    }

    create(seed?: number): string {
        return this.numericGenerator.create(seed)
    }
}

export interface AmountOptions {
    max?: number
    min?: number
    decimalPlaces?: number
    decimalSeparator?: string
    symbol?: string
    prefixSymbol?: boolean
}

interface FinalizedAmountOptions {
    max: number,
    min: number,
    decimalPlaces: number,
    decimalSeparator: string,
    symbol: string,
    prefixSymbol: boolean
}

const defaultAmountOptions: FinalizedAmountOptions = {
    max: 1000000,
    min: 0,
    decimalPlaces: 2,
    decimalSeparator: ".",
    symbol: '',
    prefixSymbol: true
}

export class Amounts extends NoShrinkGenerator<string> {
    private options: FinalizedAmountOptions

    public constructor(options: AmountOptions = {}) {
        super();
        this.options = {...defaultAmountOptions, ...options}
    }

    public create(seed?: number): string {
        const rnd = new Random(seed)
        const value = rnd.float() * (this.options.max - this.options.min) + this.options.min
        let str = value.toFixed(this.options.decimalPlaces)
        if (this.options.decimalSeparator !== '.') {
            str = str.replace('.', this.options.decimalSeparator)
        }

        if (this.options.prefixSymbol) {
            str = this.options.symbol + str
        }
        else {
            str += this.options.symbol
        }

        return str
    }
}

export class Bics extends NoShrinkGenerator<string> {
    private includeBranchCode : boolean
    private countryGen : Countries = new Countries()
    private alphaNum : AsciiGenerator = new AsciiGenerator({
        characterSet: charsets.alphaNumericUpper,
        minLen: 2,
        maxLen: 2
    })
    private numeric: AsciiGenerator = new AsciiGenerator({
        characterSet: charsets.numeric,
        minLen: 3,
        maxLen: 3
    })
    private alpha: AsciiGenerator = new AsciiGenerator({
        characterSet: charsets.alphaUpper,
        minLen: 4,
        maxLen: 4
    })

    public constructor(includeBranchCode: boolean = false) {
        super();
        this.includeBranchCode = includeBranchCode
    }

    create(seed?: number): string {
        const rnd = new Random(seed)

        let res = this.alpha.create(rnd.uint())
        let country = this.countryGen.create(rnd.uint())
        while (country.isoAlpha2.length !== 2) {
            country = this.countryGen.create(rnd.uint())
        }
        res += country.isoAlpha2
        res += this.alphaNum.create(rnd.uint())

        if (this.includeBranchCode) {
            res += this.numeric.create(seed)
        }
        return res
    }
}

export interface MaskedNumberOptions {
    unmaskedLen?: number,
    maskedLen?: number,
    mask?: string
}

interface MaskedOptions {
    unmaskedLen: number,
    maskedLen: number,
    mask: string
}

const defaultMaskedOptions: MaskedOptions = {
    unmaskedLen: 4,
    maskedLen: 4,
    mask: '*'
}

export class MaskedNumber extends NoShrinkGenerator<string> {
    private options: MaskedOptions
    private numeric: AsciiGenerator

    public constructor(options: MaskedNumberOptions = {}) {
        super();
        this.options = {...defaultMaskedOptions, ...options}
        this.numeric = new AsciiGenerator({
            characterSet: charsets.numeric,
            minLen: this.options.unmaskedLen,
            maxLen: this.options.unmaskedLen
        })
    }

    create(seed?: number): string {
        return this.options.mask.repeat(this.options.maskedLen) + this.numeric.create(seed)
    }
}

export class Pins extends AccountNumbers {
    public constructor(length: number = 4) {
        super(length);
    }
}
