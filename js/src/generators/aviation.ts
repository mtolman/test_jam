import {IndexGenerator} from "../common";
import aircraft_types from "../data/aviation/aircraft_types";
import airports from "../data/aviation/airports";
import airlines from "../data/aviation/airlines";
import airplanes from "../data/aviation/airplanes";

export class AircraftTypes extends IndexGenerator<string, any> {
    protected elements(): any[] {
        return aircraft_types;
    }
}

export interface AviationRecord {
    name:string
    iata:string
    icao:string
}

export class Airports extends IndexGenerator<AviationRecord> {
    protected elements(): any[] {
        return airports;
    }
}

export class Airlines extends IndexGenerator<AviationRecord> {
    protected elements(): any[] {
        return airlines;
    }
}

export class Airplanes extends IndexGenerator<AviationRecord> {
    protected elements(): any[] {
        return airplanes;
    }
}
