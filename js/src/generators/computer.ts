import {IndexGenerator} from "../common";
import cmake_system_names from "../data/computer/cmake_system_names";
import cpu_architectures from "../data/computer/cpu_architectures";
import file_extensions from "../data/computer/file_extensions";
import file_mime_mapping from "../data/computer/file_mime_mapping";
import file_types from "../data/computer/file_types";
import mime_types from "../data/computer/mime_types";
import operating_systems from "../data/computer/operating_systems";

export class CMakeSystemNames extends IndexGenerator {
    protected elements(): any[] {
        return cmake_system_names;
    }
}

export class CpuArchitectures extends IndexGenerator {
    protected elements(): any[] {
        return cpu_architectures;
    }
}

export class FileExtensions extends IndexGenerator {
    protected elements(): any[] {
        return file_extensions;
    }
}

export class FileMimeMapping extends IndexGenerator<{mime: string, extension: string}> {
    protected elements(): object[] {
        return file_mime_mapping;
    }
}

export class FileTypes extends IndexGenerator {
    protected elements(): object[] {
        return file_types;
    }
}

export class MimeTypes extends IndexGenerator {
    protected elements(): object[] {
        return mime_types;
    }
}

export class OperatingYstems extends IndexGenerator {
    protected elements(): object[] {
        return operating_systems;
    }
}
