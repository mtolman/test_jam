import {range, ShrinkGenerator} from "../common";
import ascii_char_groups from "../data/text/ascii_char_groups";
import Random from "../tinymt";

export interface AsciiCharacterSet {
    characters: string
}

export const charsets = {
    any: charSetFrom(ascii_char_groups.ANY),

    alpha: charSetFrom(ascii_char_groups.ALPHA),
    alphaLower: charSetFrom(ascii_char_groups.ALPHA_LOWER),
    alphaUpper: charSetFrom(ascii_char_groups.ALPHA_UPPER),
    alphaNumeric: charSetFrom(ascii_char_groups.ALPHA_NUMERIC),
    alphaNumericLower: charSetFrom(ascii_char_groups.ALPHA_NUMERIC_LOWER),
    alphaNumericUpper: charSetFrom(ascii_char_groups.ALPHA_NUMERIC_UPPER),
    binary: charSetFrom(ascii_char_groups.BINARY),
    bracket: charSetFrom(ascii_char_groups.BRACKET),
    control: charSetFrom(ascii_char_groups.CONTROL),
    domainPiece: charSetFrom(ascii_char_groups.DOMAIN_PIECE),
    hex: charSetFrom(ascii_char_groups.HEX),
    hexLower: charSetFrom(ascii_char_groups.HEX_LOWER),
    hexUpper: charSetFrom(ascii_char_groups.HEX_UPPER),
    htmlNamed: charSetFrom(ascii_char_groups.HTML_NAMED),
    math: charSetFrom(ascii_char_groups.MATH),
    numeric: charSetFrom(ascii_char_groups.NUMERIC),
    octal: charSetFrom(ascii_char_groups.OCTAL),
    printable: charSetFrom(ascii_char_groups.PRINTABLE),
    quote: charSetFrom(ascii_char_groups.QUOTE),
    safePunctuation: charSetFrom(ascii_char_groups.SAFE_PUNCTUATION),
    sentencePunctuation: charSetFrom(ascii_char_groups.SENTENCE_PUNCTUATION),
    symbol: charSetFrom(ascii_char_groups.SYMBOL),
    text: charSetFrom(ascii_char_groups.TEXT),
    urlUnencodedChars: charSetFrom(ascii_char_groups.URL_UNENCODED_CHARS),
    urlUnencodedHashChars: charSetFrom(ascii_char_groups.URL_UNENCODED_HASH_CHARS),
    whitespace: charSetFrom(ascii_char_groups.WHITESPACE),
}

export interface AsciiOptions {
    characterSet?: AsciiCharacterSet
    minLen?: number
    maxLen?: number
}

const defaultAsciiOptions = {
    characterSet: charsets.any,
    minLen: 0,
    maxLen: 1024
}

export class AsciiGenerator extends ShrinkGenerator<string> {
    private charSet: AsciiCharacterSet
    private minLen: number
    private maxLen: number

    public constructor(options: AsciiOptions = {}) {
        super();
        const opts = {...defaultAsciiOptions, ...options}
        this.charSet = opts.characterSet
        this.minLen = opts.minLen
        this.maxLen = opts.maxLen
    }

    create(seed?: number): string {
        if (this.charSet.characters.length === 0) {
            return ''
        }
        const rnd = new Random(seed)
        const len = rnd.uint() % (this.maxLen - this.minLen + 1) + this.minLen
        let res = ''
        for (let i = 0; i < len; ++i) {
            res += this.charSet.characters[rnd.uint() % this.charSet.characters.length]
        }
        return res
    }

    *simplify(seed: number): Iterable<string> {
        let value = this.create(seed)
        yield value
        while (value.length > 0) {
            value = value.slice(-1)
            yield value
        }
    }

}

function charSetFrom(set: { group: string, description: string, charactersHexRange: { start: number, end: number }[] }): AsciiCharacterSet {
    let res = ''
    for (const hexRange of set.charactersHexRange) {
        res += range(hexRange.start, hexRange.end + 1).map(c => String.fromCharCode(c)).join('')
    }
    return {
        characters: res
    }
}
