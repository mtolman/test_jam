import {IndexGenerator} from "../common";
import countries from "../data/world/countries";
import timezones from "../data/world/timezones";

export interface Country {
    name:string
    genc:string
    isoAlpha2:string
    isoAlpha3:string
    isoNumeric:string
    stanag:string
    internet:string
}

export class Countries extends IndexGenerator<Country> {
    protected elements(): any[] {
        return countries;
    }
}

export class Timezones extends IndexGenerator {
    protected elements(): any[] {
        return timezones;
    }
}
