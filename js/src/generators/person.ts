import {complexities, NoShrinkGenerator} from "../common";
import given_names from "../data/person/given_names";
import Random from "../tinymt";
import surnames from "../data/person/surnames";
import prefixes from "../data/person/prefixes";
import suffixes from "../data/person/suffixes";

export enum Sex {
    MALE = 'MALE',
    FEMALE = 'FEMALE'
}

export interface Person {
    fullName: string,
    givenName: string,
    surname: string,
    prefix?: string,
    suffix?: string,
    sex: Sex
}

export class Persons extends NoShrinkGenerator<Person> {
    create(seed?: number): Person {
        const rnd = new Random(seed)
        const sex = rnd.uint() % 2 === 0 ? Sex.MALE : Sex.FEMALE
        const complexity = complexities[rnd.uint() % complexities.length]
        const givenName = given_names[sex][complexity][rnd.uint() % given_names[sex][complexity].length].name
        const surname = surnames[complexity][rnd.uint() % surnames[complexity].length].name
        const prefix = rnd.uint() % 3 !== 1 ? undefined : prefixes[sex][complexity][rnd.uint() % prefixes[sex][complexity].length].name
        const suffix = rnd.uint() % 3 !== 1 ? undefined : suffixes[complexity][rnd.uint() % suffixes[complexity].length].name
        const westernOrder = rnd.uint() % 3 !== 1
        const fullName = westernOrder ? `${givenName} ${surname}` : `${surname} ${givenName}`
        return {
            fullName,
            givenName,
            surname,
            sex,
            prefix,
            suffix
        };
    }
}

export class GivenNames extends NoShrinkGenerator<string> {
    create(seed?: number): string {
        const rnd = new Random(seed)
        const sex = rnd.uint() % 2 === 0 ? Sex.MALE : Sex.FEMALE
        const complexity = complexities[rnd.uint() % complexities.length]
        return given_names[sex][complexity][rnd.uint() % given_names[sex][complexity].length].name
    }
}

export class Surnames extends NoShrinkGenerator<string> {
    create(seed?: number): string {
        const rnd = new Random(seed)
        const complexity = complexities[rnd.uint() % complexities.length]
        return surnames[complexity][rnd.uint() % surnames[complexity].length].name
    }
}

export class Prefixes extends NoShrinkGenerator<string|null> {
    create(seed?: number): string|null {
        const rnd = new Random(seed)
        const sex = rnd.uint() % 2 === 0 ? Sex.MALE : Sex.FEMALE
        const complexity = complexities[rnd.uint() % complexities.length]
        return rnd.uint() % 3 !== 1 ? null : prefixes[sex][complexity][rnd.uint() % prefixes[sex][complexity].length].name
    }
}

export class Suffixes extends NoShrinkGenerator<string|null> {
    create(seed?: number): string|null {
        const rnd = new Random(seed)
        const complexity = complexities[rnd.uint() % complexities.length]
        return rnd.uint() % 3 !== 1 ? null : suffixes[complexity][rnd.uint() % suffixes[complexity].length].name
    }
}

export class Sexes extends NoShrinkGenerator<Sex> {
    create(seed?: number): Sex {
        const rnd = new Random(seed)
        return rnd.uint() % 2 === 0 ? Sex.MALE : Sex.FEMALE
    }
}
