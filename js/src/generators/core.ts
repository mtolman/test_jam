import {Generator, NoShrinkGenerator, ShrinkGenerator, zip} from "../common";
import Random from "../tinymt";

export class Recover<T> extends ShrinkGenerator<T> {
    private gen: Generator<T>
    private errProvider: (err: any) => T

    public constructor(gen: Generator<T>, errProvider: (err: any) => T) {
        super();
        this.gen = gen
        this.errProvider = errProvider
    }

    create(seed: number): T {
        try {
            return this.gen.create(seed)
        } catch (e) {
            return this.errProvider(e)
        }
    }
    *simplify(seed: number) {
        try {
            yield* this.gen.simplify(seed)
        }
        catch (e) {
            yield this.errProvider(e)
        }
    }
}

export class Map<T, R> extends ShrinkGenerator<R>{
    private inputGenerator: Generator<T>
    private mapFunc: (_: T, seed?: number) => R

    public constructor(inputGenerator: Generator<T>, mapFunc: (_: T, seed?: number) => R) {
        super();
        this.inputGenerator = inputGenerator
        this.mapFunc = mapFunc
    }

    create(seed: number): R {
        return this.mapFunc(this.inputGenerator.create(seed), seed)
    }

    *simplify(seed: number): Iterable<R> {
        for (const v of this.inputGenerator.simplify(seed)) {
            yield this.mapFunc(v)
        }
    }
}

export class Filter<T> extends ShrinkGenerator<T> {
    private inputGenerator: Generator<T>
    private filterFunc: (_: T) => boolean

    public constructor(inputGenerator: Generator<T>, filterFunc: (_: T) => boolean) {
        super();
        this.inputGenerator = inputGenerator
        this.filterFunc = filterFunc
    }

    create(seed: number): T {
        let index = seed
        let res = this.inputGenerator.create(index)
        for (;!this.filterFunc(res); ++index, res = this.inputGenerator.create(index)){}
        return res
    }

    *simplify(seed: number): Iterable<T> {
        for (const simplification of this.inputGenerator.simplify(seed)) {
            if (this.filterFunc(simplification)) {
                yield simplification
            }
        }
    }
}

function* simplifyGenerators(rootSeed: number, generators: Generator<any>[]) {
    const rnd = new Random(rootSeed)
    const seeds = [...generators.map(_ => rnd.uint())]
    let curValues = [...generators.map((g, i) => g.create(seeds[i]))]
    const iters: (Iterator<any>|null)[] = [
        ...generators.map(
            (g, i) => (
                (g.simplify(seeds[i])) as Iterable<any>
            )[Symbol.iterator]()
        )
    ]
    let foundValue = false
    do {
        foundValue = false
        for (let i = 0; i < iters.length; ++i) {
            const iter: Iterator<any>|null = iters[i]
            if (iter === null) {
                continue
            }
            const res = iter.next()
            if (!res.done) {
                curValues[i] = res.value
                yield [...curValues]
                foundValue = true
            }
        }
    } while(foundValue)
}

export class Combine extends ShrinkGenerator<{[key: string]: any}> {
    private mappings: [string, Generator<any>][]
    public constructor(gens: {[key: string]: Generator<any>}) {
        super();
        this.mappings = Object.entries(gens)
    }
    create(seed: number): { [p: string]: any } {
        const rnd = new Random(seed)
        return Object.fromEntries(this.mappings.map(([k, g]) => {
            return [k, g.create(rnd.uint())]
        }));
    }
    *simplify(seed: number) {
        const keys = this.mappings.map(([key, _]) => key)
        const gens = this.mappings.map(([_, v]) => v)
        const iter = simplifyGenerators(seed, gens)
        for (const valSet of iter) {
            const entries = [...zip(keys, valSet)]
            yield Object.fromEntries(entries)
        }
    }
}

export class Tuple extends ShrinkGenerator<any[]> {
    private gens: Generator<any>[]
    public constructor(...gens: Generator<any>[]) {
        super();
        this.gens = gens
    }

    create(seed: number): any[] {
        const rnd = new Random(seed)
        return this.gens.map(g => g.create(rnd.uint()))
    }

    *simplify(seed: number) {
        yield* simplifyGenerators(seed, this.gens)
    }
}

export class Just<T> extends NoShrinkGenerator<T> {
    private val: T
    public constructor(value: T) {
        super();
        this.val = value
    }
    create(_seed: number) { return this.val }
}

export class OneOf<T> extends ShrinkGenerator<T> {
    private gens: Generator<T>[]
    public constructor(...gens: Generator<T>[]) {
        super();
        this.gens = gens
    }
    create(seed: number) {
        return this.gens[seed % this.gens.length].create(seed / this.gens.length)
    }
    *simplify(seed: number) {
        yield* this.gens[seed % this.gens.length].simplify(seed / this.gens.length)
    }
}

export interface ArrayOptions {
    minLen?: number,
    maxLen?: number
}

const defaultArrayOpts = {
    minLen: 0,
    maxLen: 1024
}


export class Arrays<T> extends ShrinkGenerator<T[]> {
    private opts = {...defaultArrayOpts}
    private gen: Generator<T>
    public constructor(gen: Generator<T>, options: ArrayOptions = {}) {
        super();
        this.gen = gen
        this.opts = {...this.opts, ...options}
    }
    create(seed: number): T[] {
        const rnd = new Random(seed)
        return (new Array((rnd.uint() % (this.opts.maxLen - this.opts.minLen)) + this.opts.minLen))
            .fill(null)
            .map(() => this.gen.create(rnd.uint()))
    }

    *simplify(seed: number) {
        let arr = this.create(seed)
        const rnd = new Random(seed)
        while(arr.length > 0) {
            const sliceAmt = (rnd.uint() % 3)
            arr = arr.slice(0, -sliceAmt)
            yield arr
        }
    }
}
