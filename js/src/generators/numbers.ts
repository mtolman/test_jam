import {ShrinkGenerator} from "../common";
import Random from "../tinymt";

const defaultIntOptions = {
    min: -200000000,
    max:  200000000
}

export interface IntOptions {
    min?: number,
    max?: number
}

export class Ints extends ShrinkGenerator<number> {
    private min: number
    private max: number

    public constructor(options: IntOptions = {}) {
        super();
        const opts = {...defaultIntOptions, ...options}
        this.min = opts.min | 0
        this.max = opts.max | 0
    }

    create(seed?: number): number {
        if (typeof seed !== 'number') {
            seed = new Random().uint()
        }
        return (seed % (this.max - this.min) + this.min) | 0
    }

    *simplify(seed: number): Iterable<number> {
        let value = this.create(seed)
        yield value
        while (value !== 0 && value >= this.min && value <= this.max) {
            if (Math.abs(value) > 60) {
                value = (value / 2) | 0
                if (value < this.min) {
                    value = this.min
                    yield value
                    return
                }
                else if (value > this.max) {
                    value = this.max
                    yield value
                    return
                }
            }
            else if (value > 0) {
                --value
            }
            else if (value < 0) {
                ++value
            }
            if (value >= this.min && value <= this.max) {
                yield value
            }
        }
    }
}

const defaultFloatOptions = {
    min: Number.MIN_VALUE,
    max: Number.MAX_VALUE,
    allowNan: true,
    allowInfinity: true
}

export interface FloatOptions {
    min?: number
    max?: number
    allowNan?: boolean
    allowInfinity?: boolean
}

export class Floats extends ShrinkGenerator<number> {
    private min: number
    private max: number
    private allowNan: boolean
    private allowInfinity: boolean

    public constructor(options: FloatOptions = {}) {
        super();
        const opts = {...defaultFloatOptions, ...options}
        this.min = opts.min
        this.max = opts.max
        this.allowNan = opts.allowNan
        this.allowInfinity = opts.allowInfinity
    }

    create(seed?: number): number {
        const rnd = new Random()
        if (this.allowNan && rnd.uint() % 100 == 1) {
            return Number.NaN
        }
        if (this.allowInfinity) {
            if (rnd.uint() % 100 == 1) {
                return Number.POSITIVE_INFINITY
            }
            else if (rnd.uint() % 100 == 2) {
                return Number.NEGATIVE_INFINITY
            }
        }
        return rnd.float() * (this.max - this.min) + this.min
    }

    *simplify(seed: number): Iterable<number> {
        let value = this.create(seed)
        let max = this.max
        let min = this.min
        yield value
        if (!isFinite(value)) {
            return
        }
        while (value >= min + Number.EPSILON && value <= max - Number.EPSILON) {
            const next = Math.abs(max) > Math.abs(min) ? (value / 2 + min / 2) : (value / 2 + max / 2)
            if (next <= value + Number.EPSILON && next >= value - Number.EPSILON) {
                return
            }
            if (Math.abs(max) > Math.abs(min)) {
                max = value
            }
            else {
                min = value
            }
            yield next
            value = next
        }
    }
}
