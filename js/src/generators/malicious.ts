import {IndexGenerator} from "../common";
import edge_cases from "../data/malicious/edge_cases";
import format_injection from "../data/malicious/format_injection";
import sql_injection from "../data/malicious/sql_injection";
import xss from "../data/malicious/xss";

export class EdgeCases extends IndexGenerator<string, {bytes: {buffer: Buffer}}> {
    protected elements(): { bytes: { buffer: Buffer } }[] {
        return edge_cases;
    }

    protected convert(elem: { bytes: { buffer: Buffer } }): string {
        return elem.bytes.buffer.toString('ascii')
    }
}

export class FormatInjections extends IndexGenerator {
    protected elements(): any[] {
        return format_injection
    }
}

export class SqlInjections extends IndexGenerator {
    protected elements(): any[] {
        return sql_injection
    }
}

export class Xss extends IndexGenerator {
    protected elements(): any[] {
        return xss
    }
}
