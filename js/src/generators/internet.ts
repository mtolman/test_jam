import {IndexGenerator, NoShrinkGenerator, ShrinkGenerator} from "../common";
import top_level_domains from "../data/internet/top_level_domains";
import Random from "../tinymt";
import {AsciiGenerator, charsets} from "./ascii";
import url_protocols from "../data/internet/url_protocols";

export class TopLevelDomains extends IndexGenerator {
    protected elements(): any[] {
        return top_level_domains;
    }
}

export class IpV4 extends NoShrinkGenerator<string> {
    create(seed?: number): string {
        const rnd = new Random(seed)
        return `${rnd.uint() % 256}.${rnd.uint() % 256}.${rnd.uint() % 256}.${rnd.uint() % 256}`
    }
}

export interface Ipv6Options {
    allowIpV4?: boolean
    allowFolding?: boolean
    allowShrinkZeros?: boolean
}

const defaultIpv6Options = {
    allowIpV4: true,
    allowFolding: true,
    allowShrinkZeros: true,
}

export class IpV6 extends ShrinkGenerator<string> {
    private allowIpv4: boolean
    private allowFolding: boolean
    private allowShrinkZeros: boolean

    public constructor(options: Ipv6Options = {}) {
        super();
        const opts = {...defaultIpv6Options, ...options}
        this.allowIpv4 = opts.allowIpV4
        this.allowFolding = opts.allowFolding
        this.allowShrinkZeros = opts.allowShrinkZeros
    }

    create(seed?: number): string {
        const rnd = new Random(seed)

        if (this.allowIpv4 && rnd.uint() % 100 <= 10) {
            return `::ffff:${rnd.uint() % 256}.${rnd.uint() % 256}.${rnd.uint() % 256}.${rnd.uint() % 256}`
        }
        else if (this.allowFolding && rnd.uint() % 100 <= 10) {
            const num = rnd.uint() % 7 + 1;
            const offset = rnd.uint() % (8 - num);
            let res = ''

            for (let pieceIndex = 0;pieceIndex < offset; ++pieceIndex) {
                res += ipv6Value(rnd, this.allowShrinkZeros) + ':'
            }

            if (offset == 0) {
                res += ':'
            }

            for (let pieceIndex = 0; pieceIndex < offset + num; ++pieceIndex) {
                res += ":" + ipv6Value(rnd, this.allowShrinkZeros)
            }
            return res
        }
        else {
            return `${ipv6Value(rnd, this.allowShrinkZeros)}:${ipv6Value(rnd, this.allowShrinkZeros)}:${
                      ipv6Value(rnd, this.allowShrinkZeros)}:${ipv6Value(rnd, this.allowShrinkZeros)}:${
                      ipv6Value(rnd, this.allowShrinkZeros)}:${ipv6Value(rnd, this.allowShrinkZeros)}:${
                      ipv6Value(rnd, this.allowShrinkZeros)}:${ipv6Value(rnd, this.allowShrinkZeros)}`
        }
    }

    *simplify(seed: number): Iterable<string> {
        yield this.create(seed)
        if (this.allowIpv4) {
            yield* (new IpV6({allowIpV4: false, allowFolding: this.allowFolding, allowShrinkZeros: this.allowShrinkZeros})).simplify(seed)
        }
        else if (this.allowFolding) {
            yield* (new IpV6({allowIpV4: false, allowFolding: false, allowShrinkZeros: this.allowShrinkZeros})).simplify(seed)
        }
        else if (this.allowShrinkZeros) {
            yield* (new IpV6({allowIpV4: false, allowFolding: false, allowShrinkZeros: false})).simplify(seed)
        }
    }

}

function ipv6Value(rnd: Random, shrink: boolean) {
    return ipv6ToString(rnd.uint() % 65536, shrink)
}

function ipv6ToString(val: number, shrink: boolean) {
    if (shrink) {
        return val.toString(16)
    }
    return val.toString(16).padStart(4, '0')
}

export interface DomainOptions {
    minLen?: number
    maxLen?: number
}

const defaultDomainOptions = {
    minLen: 3,
    maxLen: 63
}

export class Domain extends ShrinkGenerator<string> {
    private minLen : number
    private maxLen : number
    private tld: TopLevelDomains

    public constructor(options: DomainOptions = {}) {
        super();
        const opts = {...defaultDomainOptions, ...options}
        if (opts.minLen < 3) {
            opts.minLen = 3
        }
        if (opts.maxLen > 63) {
            opts.maxLen = 63
        }
        this.minLen = opts.minLen
        this.maxLen = opts.maxLen
        this.tld = new TopLevelDomains()
    }

    create(seed?: number): string {
        const rnd = new Random(seed)
        const len = rnd.uint() % (this.maxLen - this.minLen + 1) + this.minLen
        const tld = this.tld.create(rnd.uint())
        if (tld.length + 2 >= len) {
            const res = randomLetter(rnd) + '.' + tld
            return res.slice(0, len)
        }
        const domainLen = Math.max(len - tld.length - 1, 1)

        let domain = ''
        while (domain.length < domainLen) {
            let piece = (new AsciiGenerator({
                characterSet:charsets.domainPiece,
                minLen: 1,
                maxLen: domainLen - 1
            })).create(rnd.uint()) + '.'
            piece = piece.replace(/[.\-]{2,}/g, randomLetter(rnd) + '-')
            if (piece[0] === '-') {
               piece = randomLetter(rnd) + piece.slice(1)
            }
            if (piece[piece.length - 1] === '-') {
                piece = piece.slice(0, -1) + randomLetter(rnd)
            }
            domain += piece + '.'
        }
        domain = domain.replace(/[.]{2,}/g, randomLetter(rnd) + '.')
        return domain + tld
    }

    *simplify(seed: number): Iterable<string> {
        const val = this.create(seed)
        yield val
        if (val.length > this.minLen) {
            yield* (new Domain({minLen: this.minLen, maxLen: Math.max(this.minLen, (val.length / 2) | 0)})).simplify(seed)
        }
    }
}

function randomLetter(rnd: Random) {
    return String.fromCharCode(rnd.uint() % 26 + 'a'.charCodeAt(0))
}

export class ProtocolSchemas extends IndexGenerator {
    protected elements(): any[] {
        return url_protocols;
    }

}

export interface UrlOptions {
    protocols?: string[]
    allowIpv4Host?: boolean
    allowIpv6Host?: boolean
    ipv6Options?: Ipv6Options
    allowEncodedCharacters?: boolean
    allowHash?: boolean
    allowRandomPath?: boolean
    allowUsername?: boolean
    allowPassword?: boolean
    allowQuery?: boolean
    allowPort?: boolean
    minLen?: number
    maxLen?: number
}

const defaultUrlOptions = {
    protocols: [],
    allowIpv4Host: true,
    allowIpv6Host: true,
    ipv6Options: defaultIpv6Options,
    allowEncodedCharacters: true,
    allowHash: true,
    allowRandomPath: true,
    allowUsername: true,
    allowPassword: true,
    allowQuery: true,
    allowPort: true,
    minLen: 10,
    maxLen: 256
}

export class Urls extends ShrinkGenerator<string> {
    private options: {
        protocols: string[]
        allowIpv4Host: boolean
        allowIpv6Host: boolean
        ipv6Options: {
            allowIpV4: boolean
            allowFolding: boolean
            allowShrinkZeros: boolean
        }
        allowEncodedCharacters: boolean
        allowHash: boolean
        allowRandomPath: boolean
        allowUsername: boolean
        allowPassword: boolean
        allowQuery: boolean
        allowPort: boolean
        minLen: number
        maxLen: number
    }
    private twoDigitHex : AsciiGenerator
    private ipv4: IpV4
    private ipv6: IpV6

    public constructor(options: UrlOptions = {}) {
        super();
        this.options = {
            ...defaultUrlOptions,
            ...options
        } as any
        if (!this.options?.protocols || this.options.protocols.length == 0) {
            this.options.protocols = url_protocols.map(p => p.protocol)
        }
        this.twoDigitHex = new AsciiGenerator({
            characterSet: charsets.hex,
            minLen: 2,
            maxLen: 2
        })
        this.ipv4 = new IpV4()
        this.ipv6 = new IpV6({...defaultIpv6Options, ...(options.ipv6Options || {})})
    }


    create(seed?: number): string {
        const rnd = new Random(seed)
        let res = this.options.protocols[rnd.uint() % this.options.protocols.length] + '://'
        const len = rnd.uint() % (this.options.maxLen - this.options.minLen + 1) + this.options.minLen
        const domainLen = rnd.uint() % (Math.max(3, len) - 3 + 1) + 3

        if (len - (res.length + domainLen) > 80 && this.options.allowUsername && rnd.float() <= 0.2) {
            res += this.createString(rnd.uint() % 79 + 1, rnd)
            if (len - (res.length + domainLen) > 80 && this.options.allowPassword && rnd.float() <= 0.2) {
                res += ':'
                res += this.createString(rnd.uint() % 79 + 1, rnd)
            }
            res += '@'
        }

        if (this.options.allowIpv4Host && domainLen >= 15 && rnd.float() <= 0.2) {
            res += this.ipv4.create(rnd.uint())
        }
        else if (this.options.allowIpv6Host && domainLen >= 42 && rnd.float() <= 0.2) {
            res += '[' + this.ipv6.create(rnd.uint()) + ']'
        }
        else {
            res += (new Domain({
                minLen: 3,
                maxLen: Math.max(domainLen, 3)
            })).create(rnd.uint())
        }

        if (len - res.length > 10 && this.options.allowPort && rnd.float() <= 0.6) {
            res += ':' + (rnd.uint() % 65534) + 1
        }

        let addedPath = false

        if (len - res.length > 10) {
            let numSegments = rnd.uint() % 15
            while (res.length < len && numSegments > 0) {
                addedPath = true
                res += '/';
                if (res.length < len) {
                    res += this.createStringWithPeriods(rnd.uint() % (len - res.length + 1) + 1, rnd)
                }
                --numSegments
            }
        }

        if (len - res.length > 10 && this.options.allowQuery && rnd.float() <= 0.4) {
            if (!addedPath) {
                addedPath = true
                res += '/'
            }
            let numSegments = rnd.uint() % 15
            const segments = (new Array(numSegments)).fill('').map(() => {
                const r = this.createStringWithPeriods(rnd.uint() % 20 + 1, rnd)
                if (rnd.uint() <= 0.6) {
                    return r + '=' + this.createStringWithPeriods(rnd.uint() % 20, rnd)
                }
                return r
            })
            res += ('?' + segments.join('&')).slice(Math.max(0, len - res.length))
        }

        if (len - res.length > 10 && this.options.allowQuery && rnd.float() <= 0.4) {
            if (!addedPath) {
                addedPath = true
                res += '/'
            }
            res += ('#' + this.createHashString((len - res.length) - 1, rnd))
        }

        while (res.length < len) {
            if (!addedPath) {
                addedPath = true
                res += '/'
                continue
            }

            res += 'a'
        }

        return res
    }

    *simplify(seed: number): Iterable<string> {
        const val = this.create(seed)
        yield val
        const opts = {...this.options}
        opts.maxLen = val.length
        if (this.options.allowIpv4Host) { opts.allowIpv4Host = false }
        else if (this.options.allowIpv6Host) { opts.allowIpv6Host = false }
        else if (this.options.allowEncodedCharacters) { opts.allowEncodedCharacters = false }
        else if (this.options.allowHash) { opts.allowHash = false }
        else if (this.options.allowRandomPath) { opts.allowRandomPath = false }
        else if (this.options.allowUsername) { opts.allowUsername = false }
        else if (this.options.allowPassword) { opts.allowPassword = false }
        else if (this.options.allowQuery) { opts.allowQuery = false }
        else if (this.options.allowPort) { opts.allowPort = false }
        else { return }
        yield* (new Urls(opts)).simplify(seed)
    }

    private createString(len: number, rnd: Random) {
        const chars = charsets.urlUnencodedChars.characters
        let res = ''

        if (!this.options.allowEncodedCharacters) {
            while (res.length < len) {
                res += chars[rnd.uint() % chars.length]
            }
        }
        else {
            while (res.length < len) {
                if (res.length + 2 < len && rnd.uint() % 7 === 1) {
                    res += '%' + this.twoDigitHex.create(rnd.uint())
                }
                else {
                    res += chars[rnd.uint() % chars.length]
                }
            }
        }

        return res
    }

    private createStringWithPeriods(len: number, rnd: Random) {
        const chars = charsets.urlUnencodedChars.characters + '.'
        let res = ''

        if (!this.options.allowEncodedCharacters) {
            while (res.length < len) {
                res += chars[rnd.uint() % chars.length]
            }
        }
        else {
            while (res.length < len) {
                if (res.length + 2 < len && rnd.uint() % 7 === 1) {
                    res += '%' + this.twoDigitHex.create(rnd.uint())
                }
                else {
                    res += chars[rnd.uint() % chars.length]
                }
            }
        }

        return res
    }

    private createHashString(len: number, rnd: Random) {
        const chars = charsets.urlUnencodedHashChars.characters + '.'
        let res = ''

        if (!this.options.allowEncodedCharacters) {
            while (res.length < len) {
                res += chars[rnd.uint() % chars.length]
            }
        }
        else {
            while (res.length < len) {
                if (res.length + 2 < len && rnd.uint() % 7 === 1) {
                    res += '%' + this.twoDigitHex.create(rnd.uint())
                }
                else {
                    res += chars[rnd.uint() % chars.length]
                }
            }
        }

        return res
    }
}

export interface EmailOptions {
    allowIpv4Host?: boolean
    allowIpv6Host?: boolean
    ipv6Options?: Ipv6Options
    allowPlus?: boolean
    allowQuotes?: boolean
    allowSpecials?: boolean
    minLen?: number
    maxLen?: number
}

const defaultEmailOptions = {
    allowIpv4Host: true,
    allowIpv6Host: true,
    ipv6Options: defaultIpv6Options,
    allowPlus: true,
    allowQuotes: true,
    allowSpecials: true
}

export class Emails extends ShrinkGenerator<string> {
    private options: {
        allowIpv4Host: boolean
        allowIpv6Host: boolean
        ipv6Options: Ipv6Options
        allowPlus: boolean
        allowQuotes: boolean
        allowSpecials: boolean
    }
    private ipv4: IpV4
    private ipv6: IpV6
    private domain: Domain

    public constructor(options: EmailOptions = {}) {
        super();
        this.options = {...defaultEmailOptions, ...options}
        this.ipv6 = new IpV6({...defaultEmailOptions.ipv6Options, ...(options.ipv6Options || {})})
        this.ipv4 = new IpV4()
        this.domain = new Domain()
    }

    create(seed?: number): string {
        const rnd = new Random(seed)
        const addrLen = rnd.uint() % 63 + 1;
        let res = ''
        let end = ''

        if (this.options.allowQuotes && rnd.float() <= .2) {
            res += this.createQuotedAtom(rnd)
        }
        else {
            res += this.createUnquotedAtom(rnd)
        }
        
        res += '@'

        if (this.options.allowIpv4Host && rnd.float() <= .2) {
            res += '[' + this.ipv4.create(rnd.uint()) + ']'
        }
        else if (this.options.allowIpv6Host && rnd.float() <= .2) {
            res += '[IPv6:' + this.ipv6.create(rnd.uint()) + ']'
        }
        else {
            res += this.domain.create(rnd.uint())
        }

        return res;
    }

    private createUnquotedAtom(rnd: Random) : string {
        let chars = '+_-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        if (this.options.allowSpecials && rnd.float() <= .2) {
            chars = "!#$%&*+/=?.^_`{|}~-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        }
        let res = ''
        const len = rnd.uint() % 63 + 1
        while (res.length < len) {
            let c = chars[rnd.uint() % chars.length]
            if ((c === '+' && !this.options.allowPlus) || (res.length > 1 && res[res.length - 1] === c && (c === '-' || c === '.'))) {
                c = 'a'
            }
            res += c
        }
        return res
    }

    *simplify(seed: number): Iterable<string> {
        const val = this.create(seed)
        yield val
        const opts = {...this.options}
        if (opts.allowIpv4Host) { opts.allowIpv4Host = false }
        else if (opts.allowIpv6Host) { opts.allowIpv6Host = false }
        else if (opts.allowPlus) { opts.allowPlus = false }
        else if (opts.allowSpecials) { opts.allowSpecials = false }
        else if (opts.allowQuotes) { opts.allowQuotes = false }
        else { return }
        yield* (new Emails(opts)).simplify(seed);
    }

    private createQuotedAtom(rnd: Random) {
        const chars = '!@#$%^&*()_-+=[]{}\\|;:\'\", <.>/?0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
        let res = '"'
        const len = rnd.uint() % 61 + 2
        while (res.length < len) {
            let c = chars[rnd.uint() % chars.length]
            if (c === '\\' || c === '"') {
                if (res.length + 1 < len) {
                    c = '\\"'
                }
                else {
                    c = 'a'
                }
            }
            res += c
        }
        return res + '"'
    }
}
