import {AircraftTypes, Airplanes} from "../../src/generators/aviation";
import {range} from "../../src/common";

describe('aviation', function () {
    it('aircraft types', () => {
        const gen = new AircraftTypes()
        range(100).forEach(s => expect(gen.create(s)).toMatch(/[a-z]+/))
    })
});