import {final, range} from "../../src/common";
import {Domain, Emails, IpV4, IpV6, Urls} from "../../src/generators/internet";

describe('internet', () => {
    it('ipv4', () => {
        const re = /\d{0,3}\.\d{0,3}\.\d{0,3}\.\d{0,3}/
        const gen = new IpV4()
        range(1000).forEach(s => expect(gen.create(s)).toMatch(re))
    })

    it('ipv6 create', () => {
        const re = /(::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4})/
        const gen = new IpV6()
        range(1000).forEach(s => expect(gen.create(s)).toMatch(re))
    })

    it('ipv6 simplify', () => {
        const re = /([a-fA-F0-9]{4}:)+[a-fA-F0-9]{4}/
        const gen = new IpV6()
        range(1000).forEach(s => expect(final(gen.simplify(s))).toMatch(re))
    })

    it('domain', () => {
        const re =/([a-z0-9\-]+\.)+[a-z0-9\-]+/
        const gen = new Domain()
        range(1000).forEach(s => expect(gen.create(s)).toMatch(re))
    })

    it('domain simplify', () => {
        const re = /[a-z0-9\-]\.[a-z0-9\-]/
        const gen = new Domain()
        range(1000).forEach(s => expect(final(gen.simplify(s))).toMatch(re))
    })

    it('url', () => {
        const re =/(https?|s?ftps?):\/\/((([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+(:(([a-zA-Z0-9\-\.\+]|%[a-fA-F0-9]{2})+))?)@)?((\[((::ffff:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|(([a-fA-F0-9]{0,4}::?)+[a-fA-F0-9]{0,4}))\])|(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|([a-z\-0-9A-Z]+\.)+[a-z\-0-9A-Z]+)(:\d+)?((\/([a-zA-Z0-9\-\._\+]|%[a-fA-F0-9]{2})+)+\/?|\/)?(\?([a-zA-Z0-9\-\._&=\+]|%[a-fA-F0-9]{2})+)?(\#([a-zA-Z0-9\-\._&\?=\/\+]|%[a-fA-F0-9]{2})*)?/
        const gen = new Urls()
        range(1000).forEach(s => expect(gen.create(s)).toMatch(re))
    })

    it('url simplify', () => {
        const re = /(https?|s?ftps?):\/\/(([a-z\-0-9A-Z]+\.)+[a-z\-A-Z]+)\/?/
        const gen = new Urls()
        range(1000).forEach(s => expect(final(gen.simplify(s))).toMatch(re))
    })

    it('email', () => {
        const re =/(([A-Za-z0-9!#$%&'*+-\/=?^_`{|}~]+(\.[A-Za-z0-9!#$%&'*+\-/=?^_`{|}~]+)*)|("([^\\"]|\\.)+"))@((([a-z0-9\-]+\.)+[a-z0-9\-]+)|(\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(\[IPv6:[a-fA-F0-9:.]+\]))/
        const gen = new Emails()
        range(1000).forEach(s => expect(gen.create(s)).toMatch(re))
    })

    it('email simplify', () => {
        const re = /(([A-Za-z0-9!#$%&'*+-\/=?^_`{|}~]+(\.[A-Za-z0-9!#$%&'*+\-/=?^_`{|}~]+)*)|("([^\\"]|\\.)+"))@((([a-z0-9\-]+\.)+[a-z0-9\-]+)|(\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])|(\[IPv6:[a-fA-F0-9:.]+\]))/
        const gen = new Emails()
        range(1000).forEach(s => expect(final(gen.simplify(s))).toMatch(re))
    })
})