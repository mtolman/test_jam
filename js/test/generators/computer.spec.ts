import {CMakeSystemNames, FileMimeMapping} from "../../src/generators/computer";
import {range} from "../../src/common";

describe('computer', function () {
    it('cmake', () => {
        const gen = new CMakeSystemNames()
        range(100).forEach(r => expect(gen.create(r)).toMatch(/Windows|Darwin|Linux/))
    })
})