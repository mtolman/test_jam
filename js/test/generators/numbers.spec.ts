import {Floats, Ints} from "../../src/generators/numbers";
import {final, range} from "../../src/common";

function expectInRange(val: number, min: number, max: number) {
    expect(val).toBeGreaterThanOrEqual(min)
    expect(val).toBeLessThanOrEqual(max)
}

describe('ints', function () {
    const gen1 = new Ints()
    const gen2 = new Ints({min: 0, max: 1000})
    const gen3 = new Ints({min: -1000, max: 0})
    const gen4 = new Ints({min: -1000, max: -5})
    const gen5 = new Ints({min: 5, max: 1000})
    it('can generate', () => {
        range(1000).forEach(s => {
            expect(gen1.create(s)).toBe(gen1.create(s) | 0)
            expectInRange(gen2.create(s), 0, 1000)
            expectInRange(gen3.create(s), -1000, 0)
            expectInRange(gen4.create(s), -1000, -5)
            expectInRange(gen5.create(s), 5, 1000)
        })
    })

    it('can simplify', () => {
        range(100).forEach(s => {
            expect(final(gen1.simplify(s))).toBe(0)
            expect(final(gen2.simplify(s))).toBe(0)
            expect(final(gen3.simplify(s))).toBe(0)
            expect(final(gen4.simplify(s))).toBe(-5)
            expect(final(gen5.simplify(s))).toBe(5)
        })
    })
});


describe('floats', function () {
    const gen1 = new Floats({allowInfinity: false, allowNan: false})
    const gen2 = new Floats({allowInfinity: false, allowNan: false, min: 0, max: 1000})
    const gen3 = new Floats({allowInfinity: false, allowNan: false, min: -1000, max: 0})
    const gen4 = new Floats({allowInfinity: false, allowNan: false, min: -1000, max: -5})
    const gen5 = new Floats({allowInfinity: false, allowNan: false, min: 5, max: 1000})
    it('can generate', () => {
        range(1000).forEach(s => {
            expect(typeof gen1.create(s)).toEqual('number')
            expectInRange(gen2.create(s), 0, 1000)
            expectInRange(gen3.create(s), -1000, 0)
            expectInRange(gen4.create(s), -1000, -5)
            expectInRange(gen5.create(s), 5, 1000)
        })
    })

    it('can simplify', () => {
        range(100).forEach(s => {
            expect(final(gen1.simplify(s))).toBeCloseTo(0, 5)
            expect(final(gen2.simplify(s))).toBeCloseTo(0, 5)
            expect(final(gen3.simplify(s))).toBeCloseTo(0, 5)
            expect(final(gen4.simplify(s))).toBeCloseTo(-5, 5)
            expect(final(gen5.simplify(s))).toBeCloseTo(5, 5)
        })
    })
});
