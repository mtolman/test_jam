import {charsets, AsciiGenerator} from "../../src/generators/ascii";
import {range} from "../../src/common";

const numIters = 1000

describe('ascii', () => {
    it('any', () => {
        const gen = new AsciiGenerator({characterSet: charsets.any, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[\x00-\x7f]+/))
    })

    it('alpha', () => {
        const gen = new AsciiGenerator({characterSet: charsets.alpha, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-z]+/i))
    })

    it('alpha upper', () => {
        const gen = new AsciiGenerator({characterSet: charsets.alphaUpper, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[A-Z]+/))
    })

    it('alpha lower', () => {
        const gen = new AsciiGenerator({characterSet: charsets.alphaLower, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-z]+/))
    })

    it('alpha numeric', () => {
        const gen = new AsciiGenerator({characterSet: charsets.alphaNumeric, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-z0-9]+/i))
    })

    it('alpha numeric upper', () => {
        const gen = new AsciiGenerator({characterSet: charsets.alphaNumericUpper, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[A-Z0-9]+/))
    })

    it('alpha numeric lower', () => {
        const gen = new AsciiGenerator({characterSet: charsets.alphaNumericLower, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-z0-9]+/))
    })

    it('binary', () => {
        const gen = new AsciiGenerator({characterSet: charsets.binary, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[01]+/))
    })

    it('bracket', () => {
        const gen = new AsciiGenerator({characterSet: charsets.bracket, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[(){}\[\]<>]+/))
    })

    it('control', () => {
        const gen = new AsciiGenerator({characterSet: charsets.control, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[\x00-\x1f\x7f]+/))
    })

    it('domain piece', () => {
        const gen = new AsciiGenerator({characterSet: charsets.domainPiece, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-z0-9\-]+/))
    })

    it('hex', () => {
        const gen = new AsciiGenerator({characterSet: charsets.hex, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-f0-9]+/i))
    })

    it('hex lower', () => {
        const gen = new AsciiGenerator({characterSet: charsets.hexLower, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-f0-9]+/))
    })

    it('hex upper', () => {
        const gen = new AsciiGenerator({characterSet: charsets.hexUpper, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[A-F0-9]+/))
    })

    it('html named', () => {
        const gen = new AsciiGenerator({characterSet: charsets.htmlNamed, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[!-,.-/:-@\[-`{-~]+/))
    })

    it('math', () => {
        const gen = new AsciiGenerator({characterSet: charsets.math, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[\s!%(->^A-Za-z]+/))
    })

    it('numeric', () => {
        const gen = new AsciiGenerator({characterSet: charsets.numeric, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/\d+/))
    })

    it('octal', () => {
        const gen = new AsciiGenerator({characterSet: charsets.octal, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[0-7]+/))
    })

    it('printable', () => {
        const gen = new AsciiGenerator({characterSet: charsets.printable, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[\s!-~]+/))
    })

    it('quote', () => {
        const gen = new AsciiGenerator({characterSet: charsets.quote, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/["'`]+/))
    })

    it('safe punctuation', () => {
        const gen = new AsciiGenerator({characterSet: charsets.safePunctuation, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[,.]+/))
    })

    it('sentence punctuation', () => {
        const gen = new AsciiGenerator({characterSet: charsets.sentencePunctuation, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[,-."?:;'-)]+/))
    })

    it('symbol', () => {
        const gen = new AsciiGenerator({characterSet: charsets.symbol, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[!-/:-@\[-`{-~]+/))
    })

    it('text', () => {
        const gen = new AsciiGenerator({characterSet: charsets.text, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[\s,-."?:;'-)a-zA-Z0-9]+/))
    })

    it('url unencoded characters', () => {
        const gen = new AsciiGenerator({characterSet: charsets.urlUnencodedChars, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-zA-Z0-9+\-]+/))
    })

    it('url unencoded hash characters', () => {
        const gen = new AsciiGenerator({characterSet: charsets.urlUnencodedHashChars, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/[a-zA-Z0-9+\-.?=/&]+/))
    })

    it('whitespace', () => {
        const gen = new AsciiGenerator({characterSet: charsets.whitespace, minLen: 1})
        range(numIters).forEach(s => expect(gen.create(s)).toMatch(/\s+/))
    })
})