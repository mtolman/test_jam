import {
    Arrays,
    Combine,
    Filter,
    Just,
    Map, OneOf,
    Recover, Tuple
} from "../../src/generators/core";
import {limit, range} from "../../src/common";
import {Ints} from "../../src/generators/numbers";
import {AsciiGenerator, charsets} from "../../src/generators/ascii";

describe('Combine', () => {
    it('generates', () => {
        expect(new Combine({hello: new Just('world'), a: new Just(5)}).create(1)).toEqual({hello: 'world', a: 5})

        const gen = new Combine({hello: new Ints({min: -10, max: 10}), a: new AsciiGenerator({characterSet:charsets.alpha})})
        range(100).forEach(i => {
            const v = gen.create(i)
            expect(v.hello).toBeGreaterThanOrEqual(-10)
            expect(v.hello).toBeLessThanOrEqual(10)
            expect(v.a).toMatch(/^[a-zA-Z]+$/)
        })
    })

    it ('shrinks', () => {
        const seed = 13
        const gen = new Combine({hello: new Ints({min: -10, max: 10}), a: new Ints({min: 10, max: 30})})
        const iter = limit(100, gen.simplify(seed))
        for (const val of iter) {
            if (val === null) {
                break
            }
            expect(val.hello).toBeGreaterThanOrEqual(-10)
            expect(val.hello).toBeLessThanOrEqual(10)
            expect(val.a).toBeGreaterThanOrEqual(10)
            expect(val.a).toBeLessThanOrEqual(30)
        }
    })
})

describe('recover', () => {
    const gen = new Recover(
        new Filter(
            new Ints({min: 0, max: 50}),
            (i) => {
                if (i % 13 == 0) throw 'Found unlucky factor'
                return true
            }
        ),
        (_) => -13
    )

    it('creates', () => {
        range(100).forEach(i => {
            const v = gen.create(i)
            if (v < 0) {
                expect(v).toBe(-13)
            }
            else {
                expect(v).toBeGreaterThanOrEqual(0)
                expect(v).toBeLessThanOrEqual(50)
            }
        })
    })

    it('does simplify (sort of)y', () => {
        expect([...gen.simplify(34)]).not.toEqual([])
    })
})

describe('map', () => {
    const gen = new Map(new Ints({min: 1, max: 10}), x => x * 2)

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val).toBeGreaterThanOrEqual(2)
            expect(val).toBeLessThanOrEqual(20)
            expect(val % 2).toEqual(0)
        })
    })

    it('does simplify', () => {
        range(100).forEach(i => expect([...gen.simplify(i)].length).toBeGreaterThanOrEqual(0))
    })
})

describe('filter', () => {
    const gen = new Filter(new Ints({min: 1, max: 10}), x => x % 2 == 0)

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val).toBeGreaterThanOrEqual(2)
            expect(val).toBeLessThanOrEqual(10)
            expect(val % 2).toEqual(0)
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
    })
})

describe('OneOf', () => {
    const gen = new OneOf(
        new Ints({min: 1, max: 10}),
        new Ints({min: 100, max: 110})
    )

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            if (val < 50) {
                expect(val).toBeGreaterThanOrEqual(1)
                expect(val).toBeLessThanOrEqual(10)
            }
            else {
                expect(val).toBeGreaterThanOrEqual(100)
                expect(val).toBeLessThanOrEqual(110)
            }
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
    })
})

describe('Arrays', () => {
    const gen = new Arrays(new Ints({min: 1, max: 10}), {maxLen: 10})

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val.length).toBeLessThanOrEqual(10)
            expect(val.length).toBeGreaterThanOrEqual(0)
            for (const v of val) {
                expect(v).toBeGreaterThanOrEqual(1)
                expect(v).toBeLessThanOrEqual(10)
            }
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
    })
})

describe('Tuple', () => {
    const gen = new Tuple(new Ints({min: 1, max: 100}), new Ints({min: 1, max: 100}), new Ints({min: 1, max: 100}))

    it('creates', () => {
        range(100).forEach(i => {
            const val = gen.create(i)
            expect(val.length).toBe(3)
            for (const v of val) {
                expect(v).toBeGreaterThanOrEqual(1)
                expect(v).toBeLessThanOrEqual(100)
            }
        })
    })

    it('does simplify', () => {
        expect([...gen.simplify(7)]).not.toEqual([])
        const vals = [...gen.simplify(7)]

        // We should be starting with different values in all three spots
        expect(vals[0]).not.toEqual((new Array(3)).fill(vals[0][0]))

        // We should still have different values after a few iterations
        expect(vals.slice(10)[0]).not.toEqual((new Array(3)).fill(vals.slice(10)[0][0]))

        // We should end with matching simplified values
        expect(vals.slice(-1)[0]).toEqual((new Array(3)).fill(vals.slice(-1)[0][0]))
    })
})
