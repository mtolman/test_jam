import {Rgb, Rgba, Hsl, Hsla} from "../../src/generators/color";
import {range} from "../../src/common";

describe('color', function () {
    it('rgb', () => {
        const gen = new Rgb()
        range(100).forEach((s) => {
            const v = gen.create(s)
            expect(v.red).toBeGreaterThanOrEqual(0)
            expect(v.red).toBeLessThanOrEqual(255)
            expect(v.green).toBeGreaterThanOrEqual(0)
            expect(v.green).toBeLessThanOrEqual(255)
            expect(v.blue).toBeGreaterThanOrEqual(0)
            expect(v.blue).toBeLessThanOrEqual(255)
        })
    })

    it('rgba', () => {
        const gen = new Rgba()
        range(100).forEach((s) => {
            const v = gen.create(s)
            expect(v.red).toBeGreaterThanOrEqual(0)
            expect(v.red).toBeLessThanOrEqual(255)
            expect(v.green).toBeGreaterThanOrEqual(0)
            expect(v.green).toBeLessThanOrEqual(255)
            expect(v.blue).toBeGreaterThanOrEqual(0)
            expect(v.blue).toBeLessThanOrEqual(255)
            expect(v.alpha).toBeGreaterThanOrEqual(0)
            expect(v.alpha).toBeLessThanOrEqual(255)
        })
    })

    it('hsl', () => {
        const gen = new Hsl()
        range(100).forEach((s) => {
            const v = gen.create(s)
            expect(v.hue).toBeGreaterThanOrEqual(0)
            expect(v.hue).toBeLessThanOrEqual(360)
            expect(v.saturation).toBeGreaterThanOrEqual(0)
            expect(v.saturation).toBeLessThanOrEqual(1)
            expect(v.luminosity).toBeGreaterThanOrEqual(0)
            expect(v.luminosity).toBeLessThanOrEqual(1)
        })
    })

    it('hsla', () => {
        const gen = new Hsla()
        range(100).forEach((s) => {
            const v = gen.create(s)
            expect(v.hue).toBeGreaterThanOrEqual(0)
            expect(v.hue).toBeLessThanOrEqual(360)
            expect(v.saturation).toBeGreaterThanOrEqual(0)
            expect(v.saturation).toBeLessThanOrEqual(1)
            expect(v.luminosity).toBeGreaterThanOrEqual(0)
            expect(v.luminosity).toBeLessThanOrEqual(1)
            expect(v.alpha).toBeGreaterThanOrEqual(0)
            expect(v.alpha).toBeLessThanOrEqual(1)
        })
    })
});