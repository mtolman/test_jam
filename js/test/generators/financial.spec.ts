import {AccountNames, Amounts, Bics, CreditCards, MaskedNumber} from "../../src/generators/financial";
import {range} from "../../src/common";

describe('financial', () => {
    it('account name', () => {
        const gen = new AccountNames()
        range(1000).forEach(s => expect(typeof gen.create(s)).toEqual('string'))
    })

    it('credit card', () => {
        const gen = new CreditCards()
        range(1000).forEach(s => {
            const card = gen.create(s)
            expect(card.cvv.length).toBeGreaterThanOrEqual(3)
            expect(card.number.length).toBeGreaterThanOrEqual(13)
            expect(card.expirationDate.year).toBeGreaterThanOrEqual(2000)
            expect(card.expirationDate.month).toBeGreaterThanOrEqual(1)
            expect(card.expirationDate.month).toBeLessThanOrEqual(12)
            expect(card.network.name.length).toBeGreaterThan(1)
            expect(card.cvv.length).toEqual(card.network.cvvLen)
            expect(card.network.lengths).toContain(card.number.length)
            luhnValidation(card.number)
        })
    })

    it('amount', () => {
        const gen = new Amounts({
            symbol: '$',
            decimalSeparator: ','
        })
        range(1000).forEach(s => {
            const v = gen.create(s)
            expect(v).toMatch(/\$\d+\,\d{2}/)
        })
    })

    it('bic', () => {
        const gen1 = new Bics(false)
        const gen2 = new Bics(true)
        range(1000).forEach(s => {
            expect(gen1.create(s)).toMatch(/[A-Z]{4}[A-Z0-9]{4}/)
            expect(gen2.create(s)).toMatch(/[A-Z]{4}[A-Z0-9]{4}\d{3}/)
        })
    })

    it('masked number', () => {
        const gen = new MaskedNumber()
        range(1000).forEach(s => {
            expect(gen.create(s)).toMatch(/\*{4}\d{4}/)
        })
    })
})

function luhnValidation(number: string) {
    let sum = 0;
    let multiplier = 2;
    for (let chIndex = number.length - 1; chIndex > 0; --chIndex) {
        const index = chIndex - 1;
        sum += (number.charCodeAt(index) - '0'.charCodeAt(0)) * multiplier;
        multiplier = multiplier == 1 ? 2 : 1;
    }
    const check = (9 - ((sum + 9) % 10));
    expect(number.charCodeAt(number.length - 1) - '0'.charCodeAt(0)).toEqual(check)
}
