import {property} from "../src/property";
import {Ints} from "../src/generators/numbers";

describe('property check', () => {
    it('handles success cases', () => {
        property('cumulative')
            .withGens(
                new Ints({min: 1, max: 500}),
                new Ints({min: 1, max: 500})
            )
            .withSeed(9994938283)
            .test((a: number, b: number) => {
                expect(a + b).toEqual(b + a)
            })
    })

    it('handles error cases', () => {
        try {
            property('cumulative')
                .withGens(
                    new Ints({min: 1, max: 500}),
                    new Ints({min: 1, max: 500})
                )
                .withSeed(9994938283)
                .test((a: number, b: number) => {
                    expect(a + b).toEqual(b - a)
                })
            fail('Should throw, above test should fail')
        }
        catch (e: any) {
            expect(e.message).toMatch(/Seed: \d+/gm)
            expect(e.message).toMatch(/Original Input: /gm)
            expect(e.message).toMatch(/Simplified Input: /gm)
            expect(e.message).toMatch(/Property Failed: cumulative/gm)
        }
    })
})