import csv
import os
import json
import re
from re import sub

def is_active(targetVersion, version, removedVersion):
    if (int(targetVersion) >= int(version)):
        return removedVersion == "-" or int(targetVersion) < int(removedVersion)
    return False


def process_cols(rows, processCols=[]):
    for row in rows:
        row = process_row(row, processCols)
        for key in row.keys():
            if row[key] == '-':
                row[key] = ''
    return rows


def process_cell(cell=''):
    return cell.split('|')


def process_row(row={}, cols=[]):
    for col in cols:
        if col in row:
            row[col] = process_cell(row[col])
    return row


def organize_by_col(rows, col, simplifyArray):
    resMap = {}
    for row in rows:
        key = row[col]
        if not key in resMap:
            resMap[key] = []
        resMap[key].append(process_row(row))
    if simplifyArray:
        for key in resMap.keys():
            if len(resMap[key]) == 1:
                resMap[key] = resMap[key][0]
    return resMap


def organize_by_cols_impl(possibleDict, col, simplifyArray):
    if type(possibleDict) is not dict:
        return organize_by_col(possibleDict, col, simplifyArray)
    for k, v in possibleDict.items():
        possibleDict[k] = organize_by_cols_impl(v, col, simplifyArray)
    return possibleDict


def organize_by_cols(rows, cols=[], simplifyArray=False, processCols=[]):
    processedRows = process_cols(rows, processCols=processCols)
    if not bool(cols):
        return processedRows

    for col in cols:
        processedRows = organize_by_cols_impl(processedRows, col, simplifyArray)
    return processedRows


def camel_case(s):
    s = sub(r"(_|-)+", " ", s).title().replace(" ", "")
    return ''.join([s[0].lower(), s[1:]])


def camel_case_keys(dict={}):
    res = {}
    for key in dict.keys():
        res[camel_case(key)] = dict[key]

    return res


def hex_buffer(seq):
    return {"buffer": [int(x, 16) for x in seq.strip().split(' ') if len(x) > 0]}


def hex_seq(seq):
    return int(''.join([x for x in seq.strip().split(' ') if len(x) > 0]), 16)


def hexify(row={}, hexRangeCols=[], hexSeqCols=[]):
    for colKey in hexRangeCols:
        rowVal = []
        sections = [section for section in row[colKey].split('|')]
        for section in sections:
            if '..' in section:
                split = section.split('..')
                rowVal.append({"start": hex_seq(split[0]), "end": hex_seq(split[1])})
            else:
                s = hex_seq(section)
                rowVal.append({"start": s, "end": s})
        row[colKey] = rowVal

    for colKey in hexSeqCols:
        row[colKey] = hex_buffer(row[colKey].strip())
    return row


def read_file(dataDir, fileName, hexRangeCols=[], hexSeqCols=[]):
    res = []
    with(open(os.path.join('..', 'data', dataDir, fileName))) as file:
        reader = csv.DictReader(file)
        for row in reader:
            row = camel_case_keys(row)
            res.append(hexify(row, hexRangeCols, hexSeqCols))
    return res


def read_organized_file(dataDir, fileName, otherCols=[], hexRangeCols=[], hexSeqCols=[], simplifyArray=False,
                        processCols=[]):
    return organize_by_cols(read_file(dataDir, fileName, hexRangeCols, hexSeqCols), otherCols,
                            simplifyArray=simplifyArray, processCols=processCols)


output = {
    'aviation': {
        'aircraftTypes': read_organized_file('aviation', 'aircraft_types.csv'),
        'airlines': read_organized_file('aviation', 'airlines.csv'),
        'airplanes': read_organized_file('aviation', 'airplanes.csv'),
        'airports': read_organized_file('aviation', 'airports.csv'),
    },
    'color': {
        'named': read_organized_file('color', 'named.csv')
    },
    'computer': {
        'cmakeSystemNames': read_organized_file('computer', 'cmake_system_names.csv'),
        'cpuArchitectures': read_organized_file('computer', 'cpu_architectures.csv'),
        'fileExtensions': read_organized_file('computer', 'file_extensions.csv'),
        'fileMimeMapping': read_organized_file('computer', 'file_mime_mapping.csv'),
        'fileTypes': read_organized_file('computer', 'file_types.csv'),
        'mimeTypes': read_organized_file('computer', 'mime_types.csv'),
        'operating_systems': read_organized_file('computer', 'operating_systems.csv'),
    },
    'financial': {
        'accountNames': read_organized_file('financial', 'account_names.csv', ['complexity']),
        'creditCardNetworks': read_organized_file('financial', 'cc_networks.csv', [],
                                                  processCols=['starts', 'lengths']),
        'currencies': read_organized_file('financial', 'currencies.csv', []),
        'transactionTypes': read_organized_file('financial', 'transaction_types.csv', []),
    },
    'internet': {
        "topLevelDomains": read_organized_file('internet', 'top_level_domains.csv', []),
        "urlProtocols": read_organized_file('internet', 'url_protocols.csv', []),
    },
    'malicious': {
        "edgeCases": read_organized_file('malicious', 'edge_cases.csv', [], hexSeqCols=['bytes']),
        "formatInjection": read_organized_file('malicious', 'format_injection.csv', []),
        "sqlInjection": read_organized_file('malicious', 'sql_injection.csv', []),
        "xss": read_organized_file('malicious', 'xss.csv', []),
    },
    'person': {
        "givenNames": read_organized_file('person', 'given_names.csv', ['sex', 'complexity']),
        "prefixes": read_organized_file('person', 'prefixes.csv', ['sex', 'complexity']),
        "suffixes": read_organized_file('person', 'suffixes.csv', ['complexity']),
        "surnames": read_organized_file('person', 'surnames.csv', ['complexity']),
    },
    'text': {
        'asciiCharGroups': read_organized_file('text', 'ascii_char_groups.csv', otherCols=['group'],
                                               hexRangeCols=['charactersHexRange'], simplifyArray=True),
        'unicodeCharCategories': read_organized_file('text', 'unicode_char_categories.csv', otherCols=['category'],
                                                     hexRangeCols=['hexRanges'], simplifyArray=True),
    },
    'world': {
        "countries": read_organized_file('world', 'countries.csv'),
        "timezones": read_organized_file('world', 'timezones.csv')
    },
}

jsonEncoder = json.JSONEncoder(indent=2)

bufferRegex = re.compile(r"\"buffer\": \[([^\]]*)\]")


def build_type_for_impl(val):
    if type(val) is dict:
        elems = []
        for key in val.keys():
            elems.append('{key}: {type}'.format(key=key, type=build_type_for_impl(val[key])))
        return '{' + ';'.join(elems) + '}'
    elif type(val) is list:
        if len(val) > 0:
            return build_type_for_impl(val[0]) + '[]'
        return 'any[]'
    elif type(val) is str:
        return 'string'
    else:
        return 'number'


def find_types_impl(val = {}):
    keyType = '[key: string]'
    firstKey = next(iter(val.keys()))
    firstElem = val[firstKey]
    if not type(firstElem) is dict or firstKey in firstElem.values():
        return '{{{key}: {val}}}'.format(key=keyType, val=build_type_for_impl(firstElem))
    else:
        return '{{{key}: {val}}}'.format(key=keyType, val=find_types_impl(firstElem))
    


def build_types_for(val):
    versionElem = next(iter(val))
    if type(versionElem) is dict:
        elemType = find_types_impl(versionElem)
    else:
        elemType = build_type_for_impl(versionElem)
    return elemType + '[]'


def js_out(val, types=None):
    return ("""/**
 * AUTOGENERATED FILE! DO NOT EDIT DIRECTLY!
 **/
const data : {type} = {data};\nexport default data""".format(
        data=bufferRegex.sub('"buffer": Buffer.from(new Uint8Array([\\1]))',
                              jsonEncoder.encode(val)),
        type=(types or build_types_for(val))))


def write_file(targetPath, data, types=None):
    if not os.path.exists(os.path.dirname(targetPath)):
        os.makedirs(os.path.dirname(targetPath))
    with(open(targetPath, 'w')) as outFile:
        outFile.write(js_out(data, types=types))


def write_index_file(dirPath):
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)
    requires = [file for file in os.listdir(dirPath) if file.endswith('.ts') and file != 'index.ts']
    export = 'module.exports = {'
    imports = ''
    for require in requires:
        varName = require.replace('.ts', '')
        imports += 'const {varName} = require("./{varName}");\n'.format(varName=varName)
        export += varName + ','
    export += "};"
    with(open(os.path.join(dirPath, 'index.ts'), 'w')) as outFile:
        outFile.write(imports)
        outFile.write(export)


def write_data_index_file(dirPath):
    if not os.path.exists(dirPath):
        os.mkdir(dirPath)
    requires = [file for file in os.listdir(dirPath) if
                os.path.isdir(os.path.join(dirPath, file)) and os.path.exists(os.path.join(dirPath, file, 'index.ts'))]
    export = 'module.exports = {'
    imports = ''
    for require in requires:
        varName = require.replace('.ts', '')
        imports += 'const {varName} = require("./{varName}");\n'.format(varName=varName)
        export += varName + ','
    export += "};"
    with(open(os.path.join(dirPath, 'index.ts'), 'w')) as outFile:
        outFile.write(imports)
        outFile.write(export)

write_file(os.path.join('src', 'data', 'aviation', 'aircraft_types.ts'),
           output['aviation']['aircraftTypes'], types="{type:string}[]")
write_file(os.path.join('src', 'data', 'aviation', 'airlines.ts'), output['aviation']['airlines'],
           types="{name:string,iata:string,icao:string}[]")
write_file(os.path.join('src', 'data', 'aviation', 'airplanes.ts'), output['aviation']['airplanes'],
           types="{name:string,iata:string,icao:string}[]")
write_file(os.path.join('src', 'data', 'aviation', 'airports.ts'), output['aviation']['airports'],
           types="{name:string,iata:string,icao:string}[]")

write_file(os.path.join('src', 'data', 'color', 'named.ts'), output['color']['named'],
           types="{name:string,hex:string,red:string,green:string,blue:string}[]")

write_file(os.path.join('src', 'data', 'computer', 'cmake_system_names.ts'),
           output['computer']['cmakeSystemNames'],
           types="{name:string}[]")
write_file(os.path.join('src', 'data', 'computer', 'cpu_architectures.ts'),
           output['computer']['cpuArchitectures'],
           types="{arch:string}[]")
write_file(os.path.join('src', 'data', 'computer', 'file_extensions.ts'),
           output['computer']['fileExtensions'],
           types="{ext:string}[]")
write_file(os.path.join('src', 'data', 'computer', 'file_mime_mapping.ts'),
           output['computer']['fileMimeMapping'],
           types="{mime:string,extension:string}[]")
write_file(os.path.join('src', 'data', 'computer', 'file_types.ts'), output['computer']['fileTypes'],
           types="{type:string}[]")
write_file(os.path.join('src', 'data', 'computer', 'mime_types.ts'), output['computer']['mimeTypes'],
           types="{type:string}[]")
write_file(os.path.join('src', 'data', 'computer', 'operating_systems.ts'), output['computer']['operating_systems'],
           types="{os:string}[]")

write_file(os.path.join('src', 'data', 'financial', 'account_names.ts'),
           output['financial']['accountNames'],
           types="""{
   RUDIMENTARY:{name:string,complexity:'RUDIMENTARY'}[],
   BASIC:{name:string,complexity:'BASIC'}[],
   INTERMEDIATE:{name:string,complexity:'INTERMEDIATE'}[],
   ADVANCED:{name:string,complexity:'ADVANCED'}[],
   COMPLEX:{name:string,complexity:'COMPLEX'}[],
   EDGE_CASE:{name:string,complexity:'EDGE_CASE'}[],
}""")
write_file(os.path.join('src', 'data', 'financial', 'credit_card_networks.ts'),
           output['financial']['creditCardNetworks'],
           types="""{
   id: string,
   name: string,
   starts: string[],
   lengths: string[],
   cvvlen: string
}[]""")
write_file(os.path.join('src', 'data', 'financial', 'currencies.ts'), output['financial']['currencies'],
           types="{isoCode:string,name:string,symbol:string}[]")
write_file(os.path.join('src', 'data', 'financial', 'transaction_types.ts'),
           output['financial']['transactionTypes'], types="{type:string}[]")

write_file(os.path.join('src', 'data', 'internet', 'top_level_domains.ts'),
           output['internet']['topLevelDomains'], types="{domain:string}[]")
write_file(os.path.join('src', 'data', 'internet', 'url_protocols.ts'), output['internet']['urlProtocols'],
           types="{protocol:string}[]")

write_file(os.path.join('src', 'data', 'malicious', 'edge_cases.ts'), output['malicious']['edgeCases'],
           types='{name: string;bytes: {buffer: Buffer}}[]')
write_file(os.path.join('src', 'data', 'malicious', 'format_injection.ts'),
           output['malicious']['formatInjection'], types='{attack:string}[]')
write_file(os.path.join('src', 'data', 'malicious', 'sql_injection.ts'),
           output['malicious']['sqlInjection'], types='{attack:string}[]')
write_file(os.path.join('src', 'data', 'malicious', 'xss.ts'), output['malicious']['xss'], types='{attack:string}[]')

write_file(os.path.join('src', 'data', 'person', 'given_names.ts'), output['person']['givenNames'], types="""{
    MALE: {[complexity: string]: {name:string,sex:string,complexity:string}[]},
    FEMALE: {[complexity: string]: {name:string,sex:string,complexity:string}[]}
}""")
write_file(os.path.join('src', 'data', 'person', 'prefixes.ts'), output['person']['prefixes'], types="""{
    MALE: {[complexity: string]: {name: string,sex:string,complexity:string}[]},
    FEMALE: {[complexity: string]: {name: string,sex:string,complexity:string}[]}
}""")
write_file(os.path.join('src', 'data', 'person', 'suffixes.ts'), output['person']['suffixes'], types="{[complexity: string]: {name:string,complexity: string}[]}")
write_file(os.path.join('src', 'data', 'person', 'surnames.ts'), output['person']['surnames'], types="{[complexity: string]: {name:string,complexity: string}[]}")

write_file(os.path.join('src', 'data', 'text', 'ascii_char_groups.ts'), output['text']['asciiCharGroups'],
           types="{[group:string]:{group:string,description:string,charactersHexRange:{start:number,end:number}[]}}")
write_file(os.path.join('src', 'data', 'text', 'unicode_char_categories.ts'),
           output['text']['unicodeCharCategories'],
           types="{[group:string]:{category:string,hexRanges:{start:number,end:number}[]}}")

write_file(os.path.join('src', 'data', 'world', 'countries.ts'), output['world']['countries'],
           types="{name:string,genc:string,isoAlpha2:string,isoAlpha3:string,isoNumeric:string,stanag:string,internet:string}[]")
write_file(os.path.join('src', 'data', 'world', 'timezones.ts'), output['world']['timezones'],types="{name:string}[]")
