#ifndef TEST_JAM_GENERATORS_H
#define TEST_JAM_GENERATORS_H

#include "generators/ascii.h"
#include "generators/aviation.h"
#include "generators/color.h"
#include "generators/computer.h"
#include "generators/financial.h"
#include "generators/internet.h"
#include "generators/numbers.h"
#include "generators/malicious.h"
#include "generators/person.h"
#include "generators/world.h"

#include "simplifiers.h"

#endif //TEST_JAM_GENERATORS_H
