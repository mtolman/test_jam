#ifndef TEST_JAM_DATA_COLOR_H
#define TEST_JAM_DATA_COLOR_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamColorNamed* test_jam_data_color_named(size_t* outLen);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_COLOR_H
