#ifndef TEST_JAM_DATA_AVIATION_H
#define TEST_JAM_DATA_AVIATION_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamConstString* test_jam_data_aviation_aircraft_type(size_t* outLen);
const TestJamAviationRecord* test_jam_data_aviation_airline(size_t* outLen);
const TestJamAviationRecord* test_jam_data_aviation_airplane(size_t* outLen);
const TestJamAviationRecord* test_jam_data_aviation_airport(size_t* outLen);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_AVIATION_H
