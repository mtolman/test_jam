#ifndef TEST_JAM_DATA_INTERNET_H
#define TEST_JAM_DATA_INTERNET_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamConstString* test_jam_data_internet_top_level_domains(size_t* outLen);

const TestJamConstString* test_jam_data_internet_url_schemas(size_t* outLen);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_INTERNET_H
