#ifndef TEST_JAM_DATA_MALICIOUS_H
#define TEST_JAM_DATA_MALICIOUS_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamConstString* test_jam_data_malicious_sql_injection(size_t* outLen);
const TestJamConstString* test_jam_data_malicious_xss(size_t* outLen);
const TestJamConstString* test_jam_data_malicious_format_injection(size_t* outLen);
const TestJamConstString* test_jam_data_malicious_edge_cases(size_t* outLen);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_MALICIOUS_H
