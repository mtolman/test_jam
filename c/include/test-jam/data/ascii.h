#ifndef TEST_JAM_DATA_ASCII_H
#define TEST_JAM_DATA_ASCII_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamAsciiCharSet* test_jam_data_ascii_any();
const TestJamAsciiCharSet* test_jam_data_ascii_alpha();
const TestJamAsciiCharSet* test_jam_data_ascii_alpha_lower();
const TestJamAsciiCharSet* test_jam_data_ascii_alpha_upper();
const TestJamAsciiCharSet* test_jam_data_ascii_alpha_numeric();
const TestJamAsciiCharSet* test_jam_data_ascii_alpha_numeric_lower();
const TestJamAsciiCharSet* test_jam_data_ascii_alpha_numeric_upper();
const TestJamAsciiCharSet* test_jam_data_ascii_binary();
const TestJamAsciiCharSet* test_jam_data_ascii_bracket();
const TestJamAsciiCharSet* test_jam_data_ascii_control();
const TestJamAsciiCharSet* test_jam_data_ascii_domain_piece();
const TestJamAsciiCharSet* test_jam_data_ascii_hex();
const TestJamAsciiCharSet* test_jam_data_ascii_hex_lower();
const TestJamAsciiCharSet* test_jam_data_ascii_hex_upper();
const TestJamAsciiCharSet* test_jam_data_ascii_html_named();
const TestJamAsciiCharSet* test_jam_data_ascii_math();
const TestJamAsciiCharSet* test_jam_data_ascii_numeric();
const TestJamAsciiCharSet* test_jam_data_ascii_octal();
const TestJamAsciiCharSet* test_jam_data_ascii_printable();
const TestJamAsciiCharSet* test_jam_data_ascii_quote();
const TestJamAsciiCharSet* test_jam_data_ascii_safe_punctuation();
const TestJamAsciiCharSet* test_jam_data_ascii_sentence_punctuation();
const TestJamAsciiCharSet* test_jam_data_ascii_symbol();
const TestJamAsciiCharSet* test_jam_data_ascii_text();
const TestJamAsciiCharSet* test_jam_data_ascii_url_unencoded_chars();
const TestJamAsciiCharSet* test_jam_data_ascii_url_unencoded_hash_chars();
const TestJamAsciiCharSet* test_jam_data_ascii_whitespace();

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_ASCII_H
