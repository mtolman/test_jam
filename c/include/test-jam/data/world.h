#ifndef TEST_JAM_DATA_WORLD_H
#define TEST_JAM_DATA_WORLD_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamCountry* test_jam_data_world_countries(size_t* outLen);
const TestJamConstString * test_jam_data_world_timezones(size_t* outLen);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_WORLD_H
