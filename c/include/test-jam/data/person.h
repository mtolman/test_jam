#ifndef TEST_JAM_DATA_PERSON_H
#define TEST_JAM_DATA_PERSON_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamConstString* test_jam_data_person_given_name(size_t* outNumEntries, TestJamPersonSex sex, TestJamComplexity complexity);
const TestJamConstString* test_jam_data_person_surname(size_t* outNumEntries, TestJamComplexity complexity);
const TestJamConstString* test_jam_data_person_prefix(size_t* outNumEntries, TestJamPersonSex sex, TestJamComplexity complexity);
const TestJamConstString* test_jam_data_person_suffix(size_t* outNumEntries, TestJamComplexity complexity);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_PERSON_H
