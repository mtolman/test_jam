#ifndef TEST_JAM_DATA_FINANCIAL_H
#define TEST_JAM_DATA_FINANCIAL_H

#ifdef __cplusplus
extern "C" {
#endif

const TestJamConstString* test_jam_data_financial_account_name(size_t* outLen, TestJamComplexity complexity);
const TestJamCcNetwork* test_jam_data_financial_cc_network(size_t* outLen);
const TestJamCurrency* test_jam_data_financial_currency(size_t* outLen);
const TestJamConstString* test_jam_data_financial_transaction_type(size_t* outLen);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_FINANCIAL_H
