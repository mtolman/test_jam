#ifndef TEST_JAM_DATA_COMPUTER_H
#define TEST_JAM_DATA_COMPUTER_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

const TestJamConstString* test_jam_data_computer_cmake_system_name(size_t* outNumEntries);
const TestJamConstString* test_jam_data_computer_cpu_architecture(size_t* outNumEntries);
const TestJamConstString* test_jam_data_computer_file_extension(size_t* outNumEntries);
const TestJamMimeMapping* test_jam_data_computer_file_mime_mapping(size_t* outNumEntries);
const TestJamConstString* test_jam_data_computer_file_type(size_t* outNumEntries);
const TestJamConstString* test_jam_data_computer_mime_type(size_t* outNumEntries);
const TestJamConstString* test_jam_data_computer_operating_system(size_t* outNumEntries);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_DATA_COMPUTER_H
