#ifndef TEST_JAM_MEM_H
#define TEST_JAM_MEM_H

#include <stddef.h>
#include <stdint.h>

#include "objects.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct TestJamAllocator {
    void* (*alloc)(size_t size);
    void (*free)(void* ptr);
} TestJamAllocator;

void test_jam_free_string_contents(TestJamString* ptr, const TestJamAllocator* allocator);

TestJamAllocator test_jam_mem_malloc_allocator();

TestJamAllocator test_jam_mem_calloc_allocator();

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_MEM_H
