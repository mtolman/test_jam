#ifndef TEST_JAM_GENS_PERSON_H
#define TEST_JAM_GENS_PERSON_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamPerson test_jam_person(uint64_t seed);
TestJamConstString test_jam_person_given_name(uint64_t seed);
TestJamConstString test_jam_person_surname(uint64_t seed);
TestJamConstStringOptional test_jam_person_prefix(uint64_t seed);
TestJamConstStringOptional test_jam_person_suffix(uint64_t seed);
TestJamPersonSex test_jam_person_sex(uint64_t seed);

TestJamPerson test_jam_person_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity);
TestJamConstString test_jam_person_given_name_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity);
TestJamConstString test_jam_person_surname_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity);
TestJamConstStringOptional test_jam_person_prefix_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity);
TestJamConstStringOptional test_jam_person_suffix_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GENS_PERSON_H
