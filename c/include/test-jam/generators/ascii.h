#ifndef TEST_JAM_GENS_ASCII_H
#define TEST_JAM_GENS_ASCII_H

#include "../objects.h"
#include "../mem.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * Creates a random ASCII string from a particular character set
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param charSet Character set to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_from_charset(TestJamString* outPtr, uint64_t seed, const TestJamAsciiCharSet* charSet, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string from a particular character set and places it in a buffer
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param charSet Character set to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_from_charset_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, const TestJamAsciiCharSet* charSet, size_t minLen, size_t maxLen);



/**
 * Creates a random ASCII string with any 7-bit ASCII characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_any(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with any 7-bit ASCII characters
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_any_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for alphabetic strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_alpha(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for alphabetic strings
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_alpha_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for lowercase alphabetic strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_alpha_lower(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for lowercase alphabetic strings
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_alpha_lower_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for uppercase alphabetic strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_alpha_upper(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for uppercase alphabetic strings
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_alpha_upper_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for alpha-numeric strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_alpha_numeric(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for alpha-numeric strings
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_alpha_numeric_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for lowercase alpha-numeric strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_alpha_numeric_lower(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for lowercase alpha-numeric strings
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_alpha_numeric_lower_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for lowercase alpha-numeric strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_alpha_numeric_upper(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for uppercase alpha-numeric strings
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_alpha_numeric_upper_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for binary strings (0, 1)
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_binary(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string for binary strings (0, 1)
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_binary_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with bracket characters ([](){})
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_bracket(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with bracket chars ([](){})
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_bracket_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with control characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_control(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with control chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_control_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string for domains segments strings
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_domain_piece(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with domains segment chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_domain_piece_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with hex characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_hex(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with hex chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_hex_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with lowercase hex characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_hex_lower(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with lowercase hex chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_hex_lower_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with uppercase hex characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_hex_upper(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with uppercase hex chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_hex_upper_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with typical HTML named_colors characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_html_named(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with HTML named_colors chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_html_named_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with typical math text characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_math(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with math chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_math_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with numeric characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_numeric(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with numeric chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_numeric_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with octal characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_octal(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with octal chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_octal_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with printable characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_printable(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with printable chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_printable_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with quote characters ('"`)
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_quote(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with quote chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_quote_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with typical safe punctuation characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_safe_punctuation(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with safe punctuation chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_safe_punctuation_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with typical sentence punctuation characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_sentence_punctuation(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with sentence punctuation chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_sentence_punctuation_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with typical symbol characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_symbol(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with symbol chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_symbol_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with typical English text characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_text(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with text chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_text_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with URL unencoded characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_url_unencoded_chars(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with URL unencoded chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_url_unencoded_chars_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with URL unencoded hash characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_url_unencoded_hash_chars(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with URL unencoded hash chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_url_unencoded_hash_chars_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);

/**
 * Creates a random ASCII string with whitespace characters
 * @param outPtr Output pointer. Will allocate the data pointer. Does NOT free the data pointer. If previously used, call test_jam_free_string_contents prior to passing into this method
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @param allocator Allocator to use for allocating the data pointer
 * @return Length of generated string
 */
size_t test_jam_ascii_whitespace(TestJamString* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

/**
 * Creates a random ASCII string with whitespace chars
 * @param bufferPtr Pointer to output buffer
 * @param bufferSize Size of output buffer. Size should include room for terminating null character.
 * @param seed PRNG seed to use
 * @param minLen Minimum output string length
 * @param maxLen Maximum output string length
 * @return Length of generated string, excludes terminating null character
 */
size_t test_jam_ascii_whitespace_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen);


#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GENS_ASCII_H
