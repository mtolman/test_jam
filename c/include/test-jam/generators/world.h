#ifndef TEST_JAM_GENS_WORLD_H
#define TEST_JAM_GENS_WORLD_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamCountry test_jam_world_country(size_t seed);
TestJamConstString test_jam_world_timezone(size_t seed);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GENS_WORLD_H
