#ifndef TEST_JAM_GENS_NUMBERS_H
#define TEST_JAM_GENS_NUMBERS_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

uint64_t test_jam_numbers_uint64(uint64_t seed, uint64_t min, uint64_t max);
uint32_t test_jam_numbers_uint32(uint64_t seed, uint32_t min, uint32_t max);
uint16_t test_jam_numbers_uint16(uint64_t seed, uint16_t min, uint16_t max);
uint8_t  test_jam_numbers_uint8(uint64_t seed, uint8_t min, uint8_t max);

int64_t test_jam_numbers_int64(uint64_t seed, int64_t min, int64_t max);
int32_t test_jam_numbers_int32(uint64_t seed, int32_t min, int32_t max);
int16_t test_jam_numbers_int16(uint64_t seed, int16_t min, int16_t max);
int8_t test_jam_numbers_int8(uint64_t seed, int8_t min, int8_t max);

float test_jam_numbers_float(uint64_t seed, float min, float max, bool allowNan, bool allowInf);
double test_jam_numbers_double(uint64_t seed, double min, double max, bool allowNan, bool allowInf);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GENS_NUMBERS_H
