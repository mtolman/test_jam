#ifndef TEST_JAM_GEN_COLORS_H
#define TEST_JAM_GEN_COLORS_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamColorNamed test_jam_color_named(uint64_t seed);
TestJamColorRgb test_jam_color_rgb(uint64_t seed);
TestJamColorRgba test_jam_color_rgba(uint64_t seed);
TestJamColorHsl test_jam_color_hsl(uint64_t seed);
TestJamColorHsla test_jam_color_hsla(uint64_t seed);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GEN_COLORS_H
