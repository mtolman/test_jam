#ifndef TEST_JAM_GEN_FINANCIAL_H
#define TEST_JAM_GEN_FINANCIAL_H

#include "../objects.h"
#include "../mem.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamConstString test_jam_financial_account_name(uint64_t seed);
TestJamConstString test_jam_financial_account_name_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity);
TestJamCcNetwork test_jam_financial_cc_network(uint64_t seed);
TestJamCurrency test_jam_financial_currency(uint64_t seed);
TestJamConstString test_jam_financial_transaction_type(uint64_t seed);

typedef struct TestJamFinancialCcDate {
    uint32_t year;
    uint16_t month;
} TestJamFinancialCcDate;

typedef struct TestJamFinancialCreditCard {
    TestJamCcNetwork network;
    TestJamString number;
    TestJamString cvv;
    TestJamFinancialCcDate expirationDate;
} TestJamFinancialCreditCard;

void test_jam_financial_credit_card(TestJamFinancialCreditCard* outPtr, uint64_t seed, const TestJamFinancialCcDate* minDate, const TestJamFinancialCcDate* maxDate, const TestJamAllocator* allocator);
void test_jam_free_financial_credit_card_contents(TestJamFinancialCreditCard* ptr, const TestJamAllocator* allocator);

void test_jam_financial_account_number(TestJamString* outPtr, uint64_t seed, size_t length, const TestJamAllocator* allocator);
size_t test_jam_financial_account_number_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t length);

typedef struct TestJamFinancialAmountOptions {
    double max;
    double min;
    unsigned decimalPlaces;
    const char* decimalSeparator;
    const char* symbol;
    bool prefixSymbol;
} TestJamFinancialAmountOptions;

TestJamFinancialAmountOptions test_jam_financial_default_amount_options();

void test_jam_financial_amount(TestJamString* outPtr, size_t seed, const TestJamFinancialAmountOptions* options, const TestJamAllocator* allocator);
void test_jam_financial_bic(TestJamString* outPtr, size_t seed, bool includeBranchCode, const TestJamAllocator* allocator);

size_t test_jam_financial_amount_buff(char* bufferPtr, size_t bufferSize, size_t seed, const TestJamFinancialAmountOptions* options);
size_t test_jam_financial_bic_buff(char* bufferPtr, size_t bufferSize, size_t seed, bool includeBranchCode);

typedef struct TestJamFinancialMaskedNumOptions {
    size_t unMaskedLen;
    size_t maskedLen;
    char maskedChar;
} TestJamFinancialMaskedNumOptions;

TestJamFinancialMaskedNumOptions test_jam_financial_default_masked_num_options();

void test_jam_financial_masked_num(TestJamString* outPtr, uint64_t seed, const TestJamFinancialMaskedNumOptions* options, const TestJamAllocator* allocator);
size_t test_jam_financial_masked_num_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, const TestJamFinancialMaskedNumOptions* options);

void test_jam_financial_pin(TestJamString* outPtr, uint64_t seed, size_t length, const TestJamAllocator* allocator);
size_t test_jam_financial_pin_buff(char* bufferPtr, size_t bufferSize, uint64_t seed, size_t length);


#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GEN_FINANCIAL_H
