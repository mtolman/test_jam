#ifndef TEST_JAM_GENS_MALICIOUS_H
#define TEST_JAM_GENS_MALICIOUS_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamConstString test_jam_malicious_sql_injection(size_t seed);
TestJamConstString test_jam_malicious_xss(size_t seed);
TestJamConstString test_jam_malicious_format_injection(size_t seed);
TestJamConstString test_jam_malicious_edge_cases(size_t seed);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GENS_MALICIOUS_H
