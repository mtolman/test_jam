#ifndef TEST_JAM_GEN_AVIATION_H
#define TEST_JAM_GEN_AVIATION_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamConstString test_jam_aviation_aircraft_type(uint64_t seed);
TestJamAviationRecord test_jam_aviation_airline(uint64_t seed);
TestJamAviationRecord test_jam_aviation_airplane(uint64_t seed);
TestJamAviationRecord test_jam_aviation_airport(uint64_t seed);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GEN_AVIATION_H
