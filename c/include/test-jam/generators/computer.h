#ifndef TEST_JAM_GEN_COMPUTER_H
#define TEST_JAM_GEN_COMPUTER_H

#include "../objects.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamConstString test_jam_computer_cmake_system_name(uint64_t seed);
TestJamConstString test_jam_computer_cpu_architecture(uint64_t seed);
TestJamConstString test_jam_computer_file_extension(uint64_t seed);
TestJamMimeMapping test_jam_computer_file_mime_mapping(uint64_t seed);
TestJamConstString test_jam_computer_file_type(uint64_t seed);
TestJamConstString test_jam_computer_mime_type(uint64_t seed);
TestJamConstString test_jam_computer_operating_system(uint64_t seed);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GEN_COMPUTER_H
