#ifndef TEST_JAM_GENS_INTERNET_H
#define TEST_JAM_GENS_INTERNET_H

#include "../objects.h"
#include "../mem.h"

#ifdef __cplusplus
extern "C" {
#endif

TestJamConstString test_jam_internet_top_level_domain(uint64_t seed);

TestJamConstString test_jam_internet_url_schema(uint64_t seed);

void test_jam_internet_ipv4(TestJamString* outPtr, uint64_t seed, const TestJamAllocator* alloc);
size_t test_jam_internet_ipv4_buff(char* buffer, size_t bufferSize, uint64_t seed);

typedef enum TestJamIpv6Flags {
    TEST_JAM_IPV6_NONE = 0x0,
    TEST_JAM_IPV6_ALLOW_IPV4 = 0x1 << 0,
    TEST_JAM_IPV6_ALLOW_FOLDING = 0x1 << 1,
    TEST_JAM_IPV6_SHRINK_ZEROS = 0x1 << 2,
} TestJamIpv6Flags;

typedef struct TestJamIpv6Options {
    unsigned flags;
} TestJamIpv6Options;

TestJamIpv6Options test_jam_default_ipv6_options();

void test_jam_internet_ipv6(TestJamString* outPtr, uint64_t seed, const TestJamIpv6Options* options, const TestJamAllocator* alloc);
size_t test_jam_internet_ipv6_buff(char* buffer, size_t bufferSize, uint64_t seed, const TestJamIpv6Options* options);

typedef struct TestJamDomainOptions {
    size_t minLen;
    size_t maxLen;
} TestJamDomainOptions;

TestJamDomainOptions test_jam_default_domain_options();

void test_jam_internet_domain(TestJamString* outPtr, uint64_t seed, const TestJamDomainOptions* options, const TestJamAllocator* alloc);
size_t test_jam_internet_domain_buff(char* buffer, size_t bufferSize, uint64_t seed, const TestJamDomainOptions* options);

typedef enum TestJamUrlFlags {
    TEST_JAM_URL_NONE = 0x0,
    // IPv6 options come first so that we can just mask these values and send them to our ip6 generator
    TEST_JAM_URL_ALLOW_IPV6_IPV4 = 0x1 << 0,
    TEST_JAM_URL_ALLOW_IPV6_FOLDING = 0x1 << 1,
    TEST_JAM_URL_IPV6_SHRINK_ZEROS = 0x1 << 2,

    // Options specific to URL generation
    TEST_JAM_URL_ALLOW_IPV6_HOST = 0x1 << 3,
    TEST_JAM_URL_ALLOW_IPV4_HOST = 0x1 << 4,
    TEST_JAM_URL_ALLOW_ENCODED_CHARS = 0x1 << 5,
    TEST_JAM_URL_ALLOW_HASH = 0x1 << 6,
    TEST_JAM_URL_ALLOW_RANDOM_PATH = 0x1 << 7,
    TEST_JAM_URL_ALLOW_USERNAME = 0x1 << 8,
    TEST_JAM_URL_ALLOW_PASSWORD = 0x1 << 9,
    TEST_JAM_URL_ALLOW_QUERY = 0x1 << 10,
    TEST_JAM_URL_ALLOW_PORT = 0x1 << 11,
} TestJamUrlFlags;

typedef struct TestJamUrlOptions {
    /**
     * Flags used to determine what is allowed to be part of the generated URLs
     * Defaults to "everything is allowed"
     */
    unsigned flags;

    /**
     * A set of URL schemas to choose from. Defaults to "any schema from the built-in data set"
     */
    const TestJamConstStringArray* possibleSchemas;

    size_t minLen;
    size_t maxLen;
    const TestJamDomainOptions* domainOptions;
} TestJamUrlOptions;

TestJamUrlOptions test_jam_default_url_options();

void test_jam_internet_url(TestJamString* outPtr, uint64_t seed, const TestJamUrlOptions* options, const TestJamAllocator* alloc);
size_t test_jam_internet_url_buff(char* buffer, size_t bufferSize, uint64_t seed, const TestJamUrlOptions* options);


typedef enum TestJamEmailFlags {
    TEST_JAM_EMAIL_NONE = 0x0,
    // IPv6 options come first so that we can just mask these values and send them to our ip6 generator
    TEST_JAM_EMAIL_ALLOW_IPV6_IPV4 = 0x1 << 0,
    TEST_JAM_EMAIL_ALLOW_IPV6_FOLDING = 0x1 << 1,
    TEST_JAM_EMAIL_IPV6_SHRINK_ZEROS = 0x1 << 2,

    // Options specific to URL generation
    TEST_JAM_EMAIL_ALLOW_IPV6_HOST = 0x1 << 3,
    TEST_JAM_EMAIL_ALLOW_IPV4_HOST = 0x1 << 4,
    TEST_JAM_EMAIL_ALLOW_PLUS = 0x1 << 5,
    TEST_JAM_EMAIL_ALLOW_QUOTES = 0x1 << 6,
    TEST_JAM_EMAIL_ALLOW_SPECIALS = 0x1 << 7,
} TestJamEmailFlags;

typedef struct TestJamEmailOptions {
    unsigned flags;
    size_t minLen;
    size_t maxLen;
    const TestJamDomainOptions* domainOptions;
} TestJamEmailOptions;

TestJamEmailOptions test_jam_default_email_options();

size_t test_jam_internet_email_atom_buff(char* buffer, size_t bufferSize, uint64_t seed, const TestJamEmailOptions* options);
size_t test_jam_internet_email_quoted_buff(char* buffer, size_t bufferSize, uint64_t seed, const TestJamEmailOptions* options);

void test_jam_internet_email(TestJamString* outPtr, uint64_t seed, const TestJamEmailOptions* options, const TestJamAllocator* allocator);
size_t test_jam_internet_email_buff(char* buffer, size_t bufferSize, uint64_t seed, const TestJamEmailOptions* options);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_GENS_INTERNET_H
