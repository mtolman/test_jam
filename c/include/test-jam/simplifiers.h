#ifndef TEST_JAM_SIMPLIFIERS_H
#define TEST_JAM_SIMPLIFIERS_H

#ifdef __cplusplus
extern "C" {
#endif

#include "simplifiers/objects.h"
#include "simplifiers/ascii.h"
#include "simplifiers/internet.h"
#include "simplifiers/numbers.h"

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_SIMPLIFIERS_H
