#ifndef TEST_JAM_SIMPLE_NUMBERS_H
#define TEST_JAM_SIMPLE_NUMBERS_H

#include "objects.h"

#ifdef __cplusplus
extern "C" {
#endif

void test_jam_numbers_simplifier_uint64(TestJamUint64Simplifier * outPtr, uint64_t seed, uint64_t min, uint64_t max);
void test_jam_numbers_simplifier_uint32(TestJamUint32Simplifier * outPtr, uint64_t seed, uint32_t min, uint32_t max);
void test_jam_numbers_simplifier_uint16(TestJamUint16Simplifier * outPtr, uint64_t seed, uint16_t min, uint16_t max);
void test_jam_numbers_simplifier_uint8(TestJamUint8Simplifier * outPtr, uint64_t seed, uint8_t min, uint8_t max);

void test_jam_numbers_simplifier_int64(TestJamInt64Simplifier * outPtr, uint64_t seed, int64_t min, int64_t max);
void test_jam_numbers_simplifier_int32(TestJamInt32Simplifier * outPtr, uint64_t seed, int32_t min, int32_t max);
void test_jam_numbers_simplifier_int16(TestJamInt16Simplifier * outPtr, uint64_t seed, int16_t min, int16_t max);
void test_jam_numbers_simplifier_int8(TestJamInt8Simplifier * outPtr, uint64_t seed, int8_t min, int8_t max);

void test_jam_numbers_simplifier_double(TestJamDoubleSimplifier * outPtr, uint64_t seed, double min, double max, bool allowNan, bool allowInfinity);
void test_jam_numbers_simplifier_float(TestJamFloatSimplifier * outPtr, uint64_t seed, float min, float max, bool allowNan, bool allowInfinity);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_SIMPLE_NUMBERS_H
