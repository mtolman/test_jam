#ifndef TEST_JAM_SIMPLIFY_OBJECTS_H
#define TEST_JAM_SIMPLIFY_OBJECTS_H

#include <stdbool.h>
#include "../mem.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct TestJamStringSimplifier {
    bool hasValue;
    TestJamString value;
    void* state;
    void (*nextState)(struct TestJamStringSimplifier*);
    TestJamAllocator allocator;
} TestJamStringSimplifier;

const TestJamString* test_jam_string_simplifier_next(TestJamStringSimplifier*);
void test_jam_string_simplifier_free(TestJamStringSimplifier*);

typedef struct TestJamUint64Simplifier {
    bool hasValue;
    uint64_t value;
    uint64_t min;
    void (*nextState)(struct TestJamUint64Simplifier*);
} TestJamUint64Simplifier;


const uint64_t * test_jam_uint64_simplifier_next(TestJamUint64Simplifier*);

typedef struct TestJamUint32Simplifier {
    bool hasValue;
    uint32_t value;
    uint32_t min;
    void (*nextState)(struct TestJamUint32Simplifier*);
} TestJamUint32Simplifier;

const uint32_t * test_jam_uint32_simplifier_next(TestJamUint32Simplifier*);

typedef struct TestJamUint16Simplifier {
    bool hasValue;
    uint16_t value;
    uint16_t min;
    void (*nextState)(struct TestJamUint16Simplifier*);
} TestJamUint16Simplifier;

const uint16_t * test_jam_uint16_simplifier_next(TestJamUint16Simplifier*);

typedef struct TestJamUint8Simplifier {
    bool hasValue;
    uint8_t value;
    uint8_t min;
    void (*nextState)(struct TestJamUint8Simplifier*);
} TestJamUint8Simplifier;

const uint8_t * test_jam_uint8_simplifier_next(TestJamUint8Simplifier*);

typedef struct TestJamInt64Simplifier {
    bool hasValue;
    int64_t value;
    int64_t min;
    int64_t max;
    void (*nextState)(struct TestJamInt64Simplifier*);
} TestJamInt64Simplifier;

const int64_t * test_jam_int64_simplifier_next(TestJamInt64Simplifier*);

typedef struct TestJamInt32Simplifier {
    bool hasValue;
    int32_t value;
    int32_t min;
    int32_t max;
    void (*nextState)(struct TestJamInt32Simplifier*);
} TestJamInt32Simplifier;

const int32_t * test_jam_int32_simplifier_next(TestJamInt32Simplifier*);

typedef struct TestJamInt16Simplifier {
    bool hasValue;
    int16_t value;
    int16_t min;
    int16_t max;
    void (*nextState)(struct TestJamInt16Simplifier*);
} TestJamInt16Simplifier;

const int16_t * test_jam_int16_simplifier_next(TestJamInt16Simplifier*);

typedef struct TestJamInt8Simplifier {
    bool hasValue;
    int8_t value;
    int8_t min;
    int8_t max;
    void (*nextState)(struct TestJamInt8Simplifier*);
} TestJamInt8Simplifier;

const int8_t * test_jam_int8_simplifier_next(TestJamInt8Simplifier*);

typedef struct TestJamDoubleSimplifier {
    bool hasValue;
    double value;
    double min;
    double max;
    void (*nextState)(struct TestJamDoubleSimplifier*);
} TestJamDoubleSimplifier;

const double * test_jam_double_simplifier_next(TestJamDoubleSimplifier*);

typedef struct TestJamFloatSimplifier {
    bool hasValue;
    float value;
    float min;
    float max;
    void (*nextState)(struct TestJamFloatSimplifier*);
} TestJamFloatSimplifier;


const float * test_jam_float_simplifier_next(TestJamFloatSimplifier*);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_SIMPLIFY_OBJECTS_H
