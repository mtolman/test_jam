#ifndef TEST_JAM_SIMPLE_INTERNET_H
#define TEST_JAM_SIMPLE_INTERNET_H

#include "objects.h"
#include "../generators/internet.h"

#ifdef __cplusplus
extern "C" {
#endif

void test_jam_internet_simplifier_ipv6(TestJamStringSimplifier* outPtr, uint64_t seed, const TestJamIpv6Options* options, const TestJamAllocator* allocator);
void test_jam_internet_simplifier_domain(TestJamStringSimplifier* outPtr, uint64_t seed, const TestJamDomainOptions* options, const TestJamAllocator* allocator);
void test_jam_internet_simplifier_url(TestJamStringSimplifier* outPtr, uint64_t seed, const TestJamUrlOptions* options, const TestJamAllocator* allocator);
void test_jam_internet_simplifier_email(TestJamStringSimplifier* outPtr, uint64_t seed, const TestJamEmailOptions* options, const TestJamAllocator* allocator);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_SIMPLE_INTERNET_H
