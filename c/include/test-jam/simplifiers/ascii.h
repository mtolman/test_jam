#ifndef TEST_JAM_SIMPLIFY_ASCII_H
#define TEST_JAM_SIMPLIFY_ASCII_H

#include "objects.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct TestJamAsciiSimplifyState {
    size_t minLen;
} TestJamAsciiSimplifyState;

void test_jam_simplifier_ascii(TestJamStringSimplifier* outPtr, uint64_t seed, const TestJamAsciiCharSet* charSet, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_any(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_alpha(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_alpha_lower(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_alpha_upper(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_alpha_numeric(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_alpha_numeric_lower(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_alpha_numeric_upper(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_binary(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_bracket(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_control(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_domain_piece(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_hex(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_hex_lower(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_hex_upper(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_html_named(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_math(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_numeric(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_octal(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_printable(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_quote(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_safe_punctuation(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_sentence_punctuation(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_symbol(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_text(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_url_unencoded_chars(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_url_unencoded_hash_chars(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);
void test_jam_simplifier_ascii_whitespace(TestJamStringSimplifier* outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator* allocator);

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_SIMPLIFY_ASCII_H
