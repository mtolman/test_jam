#ifndef TEST_JAM_OBJECTS_H
#define TEST_JAM_OBJECTS_H

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef enum {
    TEST_JAM_COMPLEXITY_RUDIMENTARY,
    TEST_JAM_COMPLEXITY_BASIC,
    TEST_JAM_COMPLEXITY_INTERMEDIATE,
    TEST_JAM_COMPLEXITY_ADVANCED,
    TEST_JAM_COMPLEXITY_COMPLEX,
    TEST_JAM_COMPLEXITY_EDGE_CASE,
} TestJamComplexity;

typedef enum TestJamPersonSex {
    TEST_JAM_PERSON_SEX_MALE,
    TEST_JAM_PERSON_SEX_FEMALE,
} TestJamPersonSex;

typedef struct TestJamConstString {
    size_t length;
    const char* data;
} TestJamConstString;

typedef struct TestJamString {
    size_t length;
    char* data;
} TestJamString;

typedef struct TestJamColorRgb {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} TestJamColorRgb;

typedef struct TestJamColorRgba {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
    uint8_t alpha;
} TestJamColorRgba;

typedef struct TestJamColorHsl {
    double hue;
    double saturation;
    double luminosity;
} TestJamColorHsl;

typedef struct TestJamColorHsla {
    double hue;
    double saturation;
    double luminosity;
    double alpha;
} TestJamColorHsla;

typedef struct TestJamColorNamed {
    TestJamConstString name;
    TestJamConstString hex;
    TestJamColorRgb rgb;
} TestJamColorNamed;

typedef struct TestJamMimeMapping {
    TestJamConstString mime;
    TestJamConstString extension;
} TestJamMimeMapping;

typedef struct TestJamConstStringArray {
    size_t size;
    const TestJamConstString* values;
} TestJamConstStringArray;

typedef struct TestJamI32Array {
    size_t size;
    const int32_t * values;
} TestJamI32Array;

typedef struct TestJamCcNetwork {
    TestJamConstString id;
    TestJamConstString name;
    TestJamConstStringArray starts;
    TestJamI32Array lengths;
    unsigned cvvLen;
} TestJamCcNetwork;

typedef struct TestJamCurrency {
    TestJamConstString isoCode;
    TestJamConstString name;
    TestJamConstString symbol;
} TestJamCurrency;

typedef struct TestJamMaliciousEdgeCase {
    TestJamConstString name;
    TestJamConstString data;
} TestJamMaliciousEdgeCase;

typedef struct TestJamConstStringOptional {
    bool present;
    TestJamConstString str;
} TestJamConstStringOptional;

typedef struct TestJamPerson {
    TestJamPersonSex sex;
    TestJamConstString givenName;
    TestJamConstString surname;
    TestJamConstStringOptional prefix;
    TestJamConstStringOptional suffix;
    bool westernNameOrder;
} TestJamPerson;

typedef struct TestJamAsciiCharSet {
    size_t length;
    const char* chars;
} TestJamAsciiCharSet;

typedef struct TestJamUnicodeCharRange {
    uint32_t start;
    uint32_t end;
} TestJamUnicodeCharRange;

typedef struct TestJamUnicodeCharSet {
    size_t numRanges;
    const TestJamUnicodeCharRange* ranges;
} TestJamUnicodeCharSet;

typedef struct TestJamCountry {
    TestJamConstString name;
    TestJamConstStringOptional genc;
    TestJamConstStringOptional iso2;
    TestJamConstStringOptional iso3;
    TestJamConstStringOptional isoNum;
    TestJamConstStringOptional stanag;
    TestJamConstStringOptional tld;
} TestJamCountry;

typedef struct TestJamAviationRecord {
    TestJamConstString name;
    TestJamConstString iata;
    TestJamConstString icao;
} TestJamAviationRecord;

#ifdef __cplusplus
}
#endif

#endif //TEST_JAM_OBJECTS_H
