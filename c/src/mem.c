#include <test-jam/mem.h>
#include <stdlib.h>

#ifndef NDEBUG
static uint64_t allocated = 0;
#endif

static void* malloc_impl(size_t numBytes) {
#ifdef NDEBUG
    return malloc(numBytes);
#else
    void* ptr = malloc(numBytes + sizeof(uint64_t));
    *((uint64_t*)ptr) = numBytes;
    allocated += numBytes;
    return (void*)(((char*)ptr) + sizeof(uint64_t));
#endif
}

static void free_impl(void* ptr) {
#ifdef NDEBUG
    return free(ptr);
#else
    ptr = (void*)(((char*)ptr) - sizeof(uint64_t));
    allocated -= *((uint64_t*)ptr);
    return free(ptr);
#endif
}

#ifndef NDEBUG
size_t test_jam_mem_amt_allocated() {
    return allocated;
}
#else
size_t test_jam_mem_amt_allocated() {
    return 0;
}
#endif

TestJamAllocator test_jam_mem_malloc_allocator() {
    TestJamAllocator res;
    res.alloc = malloc_impl;
    res.free = free_impl;
    return res;
}

static void* calloc_impl(size_t numBytes) {
#ifdef NDEBUG
    return calloc(numBytes, 1);
#else
    void* ptr = calloc(numBytes + sizeof(uint64_t), 1);
    *((uint64_t*)ptr) = numBytes;
    allocated += numBytes;
    return (void*)(((char*)ptr) + sizeof(uint64_t));
#endif
}

TestJamAllocator test_jam_mem_calloc_allocator() {
    TestJamAllocator res;
    res.alloc = calloc_impl;
    res.free = free_impl;
    return res;
}

void test_jam_free_string_contents(TestJamString *ptr, const TestJamAllocator *allocator) {
    if (allocator == NULL) {
        TestJamAllocator alloc= test_jam_mem_malloc_allocator();
        return test_jam_free_string_contents(ptr, &alloc);
    }
    ptr->length = 0;
    if (ptr->data != NULL) {
        allocator->free((void *) ptr->data);
        ptr->data = NULL;
    }
}
