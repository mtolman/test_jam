#include <test-jam/generators/world.h>
#include <test-jam/data/world.h>
#include "common.h"

TestJamCountry test_jam_world_country(size_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_world_countries);
}

TestJamConstString test_jam_world_timezone(size_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_world_timezones);
}
