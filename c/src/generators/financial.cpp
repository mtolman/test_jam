#include <test-jam/generators/financial.h>
#include <test-jam/generators/ascii.h>
#include <test-jam/generators/numbers.h>
#include <test-jam/generators/world.h>
#include <test-jam/data/financial.h>
#include "common.h"
#include <algorithm>
#include <cfloat>
#include <string>
#include <sstream>
#include <cstring>

TestJamConstString test_jam_financial_account_name(uint64_t seed) {
    return test_jam_financial_account_name_complexity(seed, TestJamComplexity::TEST_JAM_COMPLEXITY_RUDIMENTARY, TestJamComplexity::TEST_JAM_COMPLEXITY_EDGE_CASE);
}

TestJamConstString test_jam_financial_account_name_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity) {
    return test_jam::impl::complexity_generator(seed, minComplexity, maxComplexity, test_jam_data_financial_account_name);
}

TestJamCcNetwork test_jam_financial_cc_network(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_financial_cc_network);
}

TestJamCurrency test_jam_financial_currency(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_financial_currency);
}

TestJamConstString test_jam_financial_transaction_type(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_financial_transaction_type);
}

void test_jam_financial_account_number(TestJamString *outPtr, uint64_t seed, size_t length, const TestJamAllocator *allocator) {
    test_jam_ascii_numeric(outPtr, seed, length, length, allocator);
}

static void compute_checksum(char* data, size_t len) {
    size_t sum = 0;
    int multiplier = 2;
    for (auto i = len - 1; i > 0; --i) {
        const auto index = i - 1;
        sum += (data[index] - '0') * multiplier;
        // flip between 1 and 2
        multiplier ^= 3;
    }
    data[len - 1] = static_cast<char>(9 - (sum + 9) % 10 + '0');
}

void
test_jam_financial_credit_card(TestJamFinancialCreditCard* outPtr, uint64_t seed, const TestJamFinancialCcDate* minDate, const TestJamFinancialCcDate* maxDate, const TestJamAllocator* allocator) {
    if (outPtr == nullptr) {
        return;
    }
    if (allocator == nullptr) {
        const auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_financial_credit_card(outPtr, seed, minDate, maxDate, &alloc);
    }
    if (minDate == nullptr) {
        const auto min = TestJamFinancialCcDate {.year=2000, .month=1};
        return test_jam_financial_credit_card(outPtr, seed, &min, maxDate, allocator);
    }
    if (maxDate == nullptr) {
        const auto max = TestJamFinancialCcDate {.year=2200, .month=12};
        return test_jam_financial_credit_card(outPtr, seed, minDate, &max, allocator);
    }
    if (maxDate->year < minDate->year || (maxDate->year == minDate->year && maxDate->month < minDate->month)) {
        return test_jam_financial_credit_card(outPtr, seed, maxDate, minDate, allocator);
    }
    auto rnd = test_jam::impl::TinyRand(seed);
    outPtr->network = test_jam_financial_cc_network(rnd());
    test_jam_ascii_numeric(&outPtr->cvv, rnd(), outPtr->network.cvvLen, outPtr->network.cvvLen, allocator);
    outPtr->expirationDate.year = rnd() % (maxDate->year - minDate->year + 1) + minDate->year;
    if (outPtr->expirationDate.year == maxDate->year && outPtr->expirationDate.year == minDate->year) {
        outPtr->expirationDate.month = rnd() % (maxDate->month - minDate->month + 1) + minDate->month;
    }
    else if (outPtr->expirationDate.year == maxDate->year) {
        outPtr->expirationDate.month = rnd() % (maxDate->month) + 1;
    }
    else if (outPtr->expirationDate.year == minDate->year) {
        outPtr->expirationDate.month = rnd() % (12 - minDate->month + 1) + minDate->month;
    }
    else {
        outPtr->expirationDate.month = rnd() % 12 + 1;
    }
    auto numLen = rnd.index_of(outPtr->network.lengths.values, outPtr->network.lengths.size).value_or(15);
    test_jam_ascii_numeric(&outPtr->number, rnd(), numLen, numLen, allocator);

    auto start = rnd.index_of(outPtr->network.starts.values, outPtr->network.starts.size);
    memcpy(outPtr->number.data, start->data, std::min(outPtr->number.length, start->length));
    compute_checksum(outPtr->number.data, outPtr->number.length);
}

void test_jam_free_financial_credit_card_contents(TestJamFinancialCreditCard *ptr, const TestJamAllocator *allocator) {
    if (ptr == nullptr) {
        return;
    }
    if (!allocator) {
        const auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_free_financial_credit_card_contents(ptr, &alloc);
    }
    test_jam_free_string_contents(&ptr->cvv, allocator);
    test_jam_free_string_contents(&ptr->number, allocator);
}

TestJamFinancialAmountOptions test_jam_financial_default_amount_options() {
    return TestJamFinancialAmountOptions{
        .max = DBL_MAX,
        .min = 0,
        .decimalPlaces = 2,
        .decimalSeparator = ".",
        .symbol = "",
        .prefixSymbol = true
    };
}

static auto replace(std::string &str, const std::string &from, const std::string &to) -> void {
    size_t start_pos = str.find(from);
    if(start_pos == std::string::npos) {
        return;
    }
    str.replace(start_pos, from.length(), to);
}

#ifdef WIN32
#include <cstdarg>
int vscprintf(const char *format, va_list ap)
{
  va_list ap_copy;
  va_copy(ap_copy, ap);
  int retval = vsnprintf(NULL, 0, format, ap_copy);
  va_end(ap_copy);
  return retval;
}

int vasprintf(char **strp, const char *format, va_list ap) {
  int len = vscprintf(format, ap);
  if (len == -1)
    return -1;
  char *str = (char*)malloc((size_t) len + 1);
  if (!str)
    return -1;
  int retval = vsnprintf(str, len + 1, format, ap);
  if (retval == -1) {
    free(str);
    return -1;
  }
  *strp = str;
  return retval;
}

int asprintf(char **strp, const char *format, ...) {
  va_list ap;
  va_start(ap, format);
  int retval = vasprintf(strp, format, ap);
  va_end(ap);
  return retval;
}
#endif

static auto amount_to_str(const TestJamFinancialAmountOptions *options, double value) {
    const auto fmtStr = "%." + std::to_string(options->decimalPlaces) + "f";
    char *buffer = nullptr;
    asprintf(&buffer, fmtStr.c_str(), value);
    auto res = std::string{buffer};
    free(buffer);

    if (strcmp(options->decimalSeparator, ".") != 0) {
        replace(res, ".", options->decimalSeparator);
    }

    if (strlen(options->symbol)) {
        if (options->prefixSymbol) {
            res = options->symbol + res;
        } else {
            res += options->symbol;
        }
    }

    return res;
}

void test_jam_financial_amount(TestJamString *outPtr, size_t seed, const TestJamFinancialAmountOptions *options,
                               const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return;
    }
    if (allocator == nullptr) {
        const auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_financial_amount(outPtr, seed, options, &alloc);
    }

    if (options == nullptr) {
        TestJamFinancialAmountOptions opts = test_jam_financial_default_amount_options();
        return test_jam_financial_amount(outPtr, seed, &opts, allocator);
    }

    auto rnd = test_jam::impl::TinyRand(seed);
    const auto doubleVal = test_jam_numbers_double(rnd(), options->min, options->max, false, false);
    auto str = amount_to_str(options, doubleVal);
    outPtr->length = str.size();
    outPtr->data = (char*)allocator->alloc(str.size() + 1);
    memcpy(outPtr->data, str.data(), str.size());
    outPtr->data[outPtr->length] = '\0';
}

size_t test_jam_financial_amount_buff(char *bufferPtr, size_t bufferSize, size_t seed,
                                    const TestJamFinancialAmountOptions *options) {
    if (bufferPtr == nullptr || bufferSize < 2) {
        return 0;
    }

    if (options == nullptr) {
        TestJamFinancialAmountOptions opts = test_jam_financial_default_amount_options();
        return test_jam_financial_amount_buff(bufferPtr, bufferSize, seed, &opts);
    }

    auto rnd = test_jam::impl::TinyRand(seed);
    const auto doubleVal = test_jam_numbers_double(rnd(), options->min, options->max, false, false);
    auto str = amount_to_str(options, doubleVal);
    const auto len = std::min(bufferSize - 1, str.size());
    memcpy(bufferPtr, str.data(), len);
    bufferPtr[len] = '\0';
    return len;
}

size_t test_jam_financial_bic_buff(char *bufferPtr, size_t bufferSize, size_t seed, bool includeBranchCode) {
    if (bufferPtr == nullptr || bufferSize < 2) {
        return 0;
    }

    size_t len = std::min(static_cast<size_t>(includeBranchCode ? 11 : 8), bufferSize - 1);
    auto rnd = test_jam::impl::TinyRand(seed);
    size_t index = 0;

    for (; index < 4 && index < len; ++index) {
        bufferPtr[index] = static_cast<char>((rnd() % 26) + 'A');
    }

    if (len > 4) {
        auto v = rnd();
        TestJamCountry country;
        do {
            country = test_jam_world_country(v++);
        } while (!country.iso2.present && country.iso2.str.length != 2);
        memcpy(bufferPtr + 4, country.iso2.str.data, std::min(country.iso2.str.length, bufferSize - 4));
    }

    // Location code
    for (index = 6; index < 8 && index < len; ++index) {
        auto v = rnd();
        const bool useNum = v % 2;
        v /= 2;

        if (useNum) {
            bufferPtr[index] = static_cast<char>((rnd() % 10) + '0');
        }
        else {
            bufferPtr[index] = static_cast<char>((rnd() % 26) + 'A');
        }
    }

    if (includeBranchCode) {
        for (index = 8; index < 11 && index < len; ++index) {
            bufferPtr[index] = static_cast<char>((rnd() % 10) + '0');
        }
    }

    bufferPtr[len] = '\0';
    return len;
}

void
test_jam_financial_bic(TestJamString *outPtr, size_t seed, bool includeBranchCode, const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return;
    }
    if (allocator == nullptr) {
        const auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_financial_bic(outPtr, seed, includeBranchCode, &alloc);
    }

    size_t len = includeBranchCode ? 11 : 8;
    outPtr->length = len;
    outPtr->data = (char*)allocator->alloc(len + 1);
    outPtr->data[len] = '\0';

    test_jam_financial_bic_buff(outPtr->data, len+1, seed, includeBranchCode);
}

void test_jam_financial_pin(TestJamString* outPtr, uint64_t seed, size_t length, const TestJamAllocator* allocator) {
    test_jam_financial_account_number(outPtr, seed, length, allocator);
}

TestJamFinancialMaskedNumOptions test_jam_financial_default_masked_num_options() {
    return TestJamFinancialMaskedNumOptions{
        .unMaskedLen = 4,
        .maskedLen = 4,
        .maskedChar = '*',
    };
}

void test_jam_financial_masked_num(TestJamString* outPtr, uint64_t seed, const TestJamFinancialMaskedNumOptions *options,
                                             const TestJamAllocator *allocator) {
    if (!allocator) {
        TestJamAllocator alloc = test_jam_mem_malloc_allocator();
        return test_jam_financial_masked_num(outPtr, seed, options, &alloc);
    }

    if (!options) {
        TestJamFinancialMaskedNumOptions opts = test_jam_financial_default_masked_num_options();
        return test_jam_financial_masked_num(outPtr, seed, &opts, allocator);
    }

    size_t len = options->unMaskedLen + options->maskedLen;
    outPtr->data = (char*)allocator->alloc(len + 1);
    outPtr->length = test_jam_financial_masked_num_buff(outPtr->data, len+1, seed, options);
}

size_t test_jam_financial_masked_num_buff(char *bufferPtr, size_t bufferSize, uint64_t seed,
                                          const TestJamFinancialMaskedNumOptions *options) {
    if (bufferPtr == nullptr || bufferSize < 2) {
        return 0;
    }

    if (!options) {
        TestJamFinancialMaskedNumOptions opts = test_jam_financial_default_masked_num_options();
        return test_jam_financial_masked_num_buff(bufferPtr, bufferSize, seed, &opts);
    }
    size_t len = std::min(options->unMaskedLen + options->maskedLen, bufferSize - 1);
    memset(bufferPtr, options->maskedChar, std::min(len, options->maskedLen));
    auto rnd = test_jam::impl::TinyRand(seed);
    for (size_t index = options->maskedLen; index < len; ++index) {
        bufferPtr[index] = static_cast<char>((rnd() % 10) + '0');
    }
    bufferPtr[len] = '\0';
    return len;
}

size_t test_jam_financial_account_number_buff(char *bufferPtr, size_t bufferSize, uint64_t seed, size_t length) {
    return test_jam_ascii_numeric_buff(bufferPtr, bufferSize, seed, length, length);
}

size_t test_jam_financial_pin_buff(char *bufferPtr, size_t bufferSize, uint64_t seed, size_t length) {
    return test_jam_ascii_numeric_buff(bufferPtr, bufferSize, seed, length, length);
}


