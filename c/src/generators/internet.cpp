#include <test-jam/generators/internet.h>
#include <test-jam/generators/ascii.h>
#include <test-jam/data/internet.h>
#include <test-jam/data/ascii.h>
#include <test-jam/generators/numbers.h>
#include "common.h"
#include "test-jam/simplifiers/internet.h"
#include "../../../cxx/include/test-jam-cxx/gens/internet.h"
#include <cstring>
#include <string_view>

TestJamConstString test_jam_internet_top_level_domain(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_internet_top_level_domains);
}

TestJamConstString test_jam_internet_url_schema(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_internet_url_schemas);
}

TestJamUrlOptions test_jam_default_url_options() {
    return TestJamUrlOptions{
        .flags=~0u,
        .possibleSchemas=nullptr,
        .minLen=20,
        .maxLen=255,
        .domainOptions=NULL
    };
}

size_t test_jam_internet_ipv4_buff(char *buffer, size_t bufferSize, uint64_t seed) {
    if (buffer == nullptr || bufferSize == 0) {
        return 0;
    }

    auto rnd = test_jam::impl::TinyRand(seed);
    int first = rnd() % 256;
    int second = rnd() % 256;
    int third = rnd() % 256;
    int fourth = rnd() % 256;

    size_t len = std::min<size_t>(bufferSize - 1, snprintf(buffer, bufferSize, "%d.%d.%d.%d", first, second, third, fourth));
    buffer[len] = '\0';
    return len;
}

void test_jam_internet_ipv4(TestJamString* outPtr, uint64_t seed, const TestJamAllocator* alloc) {
    if (outPtr == nullptr) {
        return;
    }
    if (alloc == nullptr) {
        const auto allocator = test_jam_mem_malloc_allocator();
        return test_jam_internet_ipv4(outPtr, seed, &allocator);
    }
    outPtr->data = (char*)alloc->alloc(16);
    outPtr->length = test_jam_internet_ipv4_buff(outPtr->data, 16, seed);
}

TestJamIpv6Options test_jam_default_ipv6_options() {
    return TestJamIpv6Options{
        .flags = ~0u
    };
}

size_t test_jam_internet_ipv6_buff(char *buffer, size_t bufferSize, uint64_t seed, const TestJamIpv6Options *options) {
    if (buffer == nullptr || bufferSize < 2) {
        return 0;
    }

    if (!options) {
        TestJamIpv6Options opts = test_jam_default_ipv6_options();
        return test_jam_internet_ipv6_buff(buffer, bufferSize, seed, &opts);
    }

    auto flagsVal = seed;
    auto rnd = test_jam::impl::TinyRand(seed);

    bool ip_v4 = (options->flags & TEST_JAM_IPV6_ALLOW_IPV4) && (flagsVal % 100 <= 10);
    bool collapsed = !ip_v4 && (options->flags & TEST_JAM_IPV6_ALLOW_FOLDING) && (flagsVal % 100 <= 20);

    if (ip_v4) {
        int first = rnd() % 256;
        int second = rnd() % 256;
        int third = rnd() % 256;
        int fourth = rnd() % 256;

        return std::min<size_t>(bufferSize - 1, snprintf(buffer, bufferSize, "::ffff:%d.%d.%d.%d", first, second, third, fourth));
    }

#define TEST_JAM_GEN_VAL static_cast<int>(rnd() % 65536)
    const bool shrinkZeros = options->flags & TEST_JAM_IPV6_SHRINK_ZEROS;

    if (collapsed) {
        auto num = rnd() % 7 + 1;
        auto offset = rnd() % (8 - num);

        size_t pieceIndex = 0;
        size_t strIndex = 0;

        for (; pieceIndex < offset && strIndex < bufferSize - 1; ++pieceIndex) {
            if (shrinkZeros) {
                strIndex += std::min<size_t>(bufferSize - strIndex - 1, snprintf(buffer + strIndex, std::min<size_t>(bufferSize - strIndex, 6), "%X:", std::max(TEST_JAM_GEN_VAL, 1)));
            }
            else {
                strIndex += std::min<size_t>(bufferSize - strIndex - 1, snprintf(buffer + strIndex, std::min<size_t>(bufferSize - strIndex, 6), "%04X:", std::max(TEST_JAM_GEN_VAL, 1)));
            }
        }

        if (offset == 0 && strIndex + 1 < bufferSize) {
            buffer[strIndex++] = ':';
        }

        for (; pieceIndex < offset + num && strIndex < bufferSize - 1; ++pieceIndex) {
            if (shrinkZeros) {
                strIndex += std::min<size_t>(bufferSize - strIndex - 1, snprintf(buffer + strIndex, std::min<size_t>(bufferSize - strIndex, 6), ":%X", std::max(TEST_JAM_GEN_VAL, 1)));
            }
            else {
                strIndex += std::min<size_t>(bufferSize - strIndex - 1, snprintf(buffer + strIndex, std::min<size_t>(bufferSize - strIndex, 6), ":%04X", std::max(TEST_JAM_GEN_VAL, 1)));
            }
        }

        return strIndex;
    }

    if (!shrinkZeros) {
        return std::min<size_t>(bufferSize- 1, snprintf(
                buffer,
                bufferSize,
                "%04X:%04X:%04X:%04X:%04X:%04X:%04X:%04X",
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL
        ));
    }
    else {
        return std::min<size_t>(bufferSize- 1, snprintf(
                buffer,
                bufferSize,
                "%X:%X:%X:%X:%X:%X:%X:%X",
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL,
                TEST_JAM_GEN_VAL
        ));
    }

#undef TEST_JAM_GEN_VAL
}


void test_jam_internet_ipv6(TestJamString *outPtr, uint64_t seed, const TestJamIpv6Options *options,
                            const TestJamAllocator *alloc) {
    if (outPtr == nullptr) {
        return;
    }
    if (alloc == nullptr) {
        const auto allocator = test_jam_mem_malloc_allocator();
        return test_jam_internet_ipv6(outPtr, seed, options, &allocator);
    }

    // Maximum size of buffer needed
    const auto memLen = 5 * 8;
    outPtr->data = (char*)alloc->alloc(memLen);
    outPtr->length = test_jam_internet_ipv6_buff(outPtr->data, memLen, seed, options);
}

size_t test_jam_internet_domain_buff(char *buffer, size_t bufferSize, uint64_t seed, const TestJamDomainOptions* options) {
    if (buffer == nullptr || bufferSize < 3) {
        return 0;
    }
    if (!options) {
        TestJamDomainOptions opts = test_jam_default_domain_options();
        return test_jam_internet_domain_buff(buffer, bufferSize, seed, &opts);
    }

    const size_t minLen = (options->minLen < 3) ? 3 : options->minLen;
    auto rnd = test_jam::impl::TinyRand(seed);
    const auto len = std::min<size_t>(bufferSize - 1, (rnd() % (options->maxLen - minLen + 1)) + minLen);
    const auto tld = test_jam_internet_top_level_domain(rnd());
    if (tld.length + 2 >= len) {
        buffer[0] = static_cast<char>((rnd() % 26) + 'a');
        buffer[1] = '.';
        snprintf(buffer + 2, len - 1, "%s", tld.data);
        return len;
    }
    const auto dLen = std::max<size_t>(len - tld.length - 1, 1);

    size_t i = 0;
    while (i + 1 < dLen) {
        const auto start = i;
        i += test_jam_ascii_domain_piece_buff(buffer + i, dLen - i, rnd(), 1, dLen - 1);
        if (buffer[start] == '-') {
            buffer[start] = static_cast<char>((rnd() % 26) + 'a');
        }

        if (buffer[i - 1] == '-') {
            buffer[i - 1] = static_cast<char>((rnd() % 26) + 'a');
        }
        buffer[i++] = '.';
    }
    while (i < dLen) {
        buffer[i++] = static_cast<char>((rnd() % 26) + 'a');
    }
    snprintf(buffer + dLen, len - dLen + 1, ".%s", tld.data);
    for (i = 0; i + 1 < len; ++i) {
        if ((buffer[i] == '-' || buffer[i] == '.') && (buffer[i + 1] == '-' || buffer[i + 1] == '.')) {
            buffer[i] = static_cast<char>((rnd() % 26) + 'a');
        }
    }
    return len;
}

void test_jam_internet_domain(TestJamString *outPtr, uint64_t seed, const TestJamDomainOptions* options, const TestJamAllocator *alloc) {
    if (alloc == nullptr) {
        const auto allocator = test_jam_mem_malloc_allocator();
        return test_jam_internet_domain(outPtr, seed, options, &allocator);
    }
    if (!options) {
        TestJamDomainOptions opts = test_jam_default_domain_options();
        return test_jam_internet_domain(outPtr, seed, &opts, alloc);
    }
    outPtr->data = (char*)alloc->alloc(64);
    outPtr->length = test_jam_internet_domain_buff(outPtr->data, 64, seed, options);
}

size_t test_jam_internet_url_buff(char *buffer, size_t bufferSize, uint64_t seed, const TestJamUrlOptions *options) {
    // URLs do so much more character generation that it's just much faster to hard code these rather than read them from a CSV
    static constexpr std::string_view hexChars = "abcdefABCDEF0123456789";

    // Setup
    if (!options) {
        const auto opts = test_jam_default_url_options();
        return test_jam_internet_url_buff(buffer, bufferSize, seed, &opts);
    }
    auto rnd = test_jam::impl::TinyRand(seed);
    const auto minLen = std::min<size_t>(bufferSize, options->minLen);
    const size_t maxLen = std::min<size_t>(rnd() % (options->maxLen - options->minLen + 1) + options->minLen, bufferSize - 1);
    size_t curLen = 0;

    // Convenience
    const auto percChance = [&rnd](size_t perc) {
        const auto m = rnd() % 100;
        return m < perc;
    };
    const auto remaining = [&maxLen, &curLen](){ return maxLen - curLen; };
    const auto printToBuff = [&remaining, &maxLen, &buffer, &curLen](const char* fmt, auto ...args) {
        if (curLen + 1 < maxLen) {
            const auto r = remaining();
            curLen += std::min<size_t>(r - 1, snprintf(buffer + curLen, r, fmt, args...));
        }
    };
    const auto addEncoded = [options, &rnd, seed](char* buffer, size_t bufferSize) {
        if (!(options->flags & TEST_JAM_URL_ALLOW_ENCODED_CHARS)) {
            return;
        }

        for (size_t i = 0; i + 3 < bufferSize && buffer[i] && buffer[i + 1] && buffer[i + 2]; ++i) {
            // Not using RND here to save on compute costs
            if ((i ^ seed) % 17 <= 2) {
                // Only doing 1 rnd call per encoded character since rnd calls do have some overhead
                const auto raw = rnd();
                const auto firstIndex = raw % hexChars.size();
                const auto secondIndex = (raw / hexChars.size()) % hexChars.size();
                buffer[i] = '%';
                buffer[i + 1] = hexChars[firstIndex];
                buffer[i + 2] = hexChars[secondIndex];
                i += 2;
            }
        }
    };

    // Schema
    if (options->possibleSchemas && options->possibleSchemas->size) {
        const auto& schema = options->possibleSchemas->values[rnd() % options->possibleSchemas->size];
        printToBuff("%s", schema.data);
    }
    else {
        const auto schema = test_jam_internet_url_schema(rnd());
        printToBuff("%s", schema.data);
    }
    printToBuff("%s", "://");

    // User
    if (remaining() > 90 && options->flags & TEST_JAM_URL_ALLOW_USERNAME && percChance(60)) {
        char cred[80];
        size_t len = test_jam::impl::from_charset_buff_rnd(cred, 80, rnd, test_jam_data_ascii_url_unencoded_chars(), 1, 79);
        addEncoded(cred, std::min(len, remaining() - 3));
        printToBuff("%s", cred);

        if (remaining() > 90 && options->flags & TEST_JAM_URL_ALLOW_PASSWORD && percChance(60)) {
            size_t len = test_jam::impl::from_charset_buff_rnd(cred, 80, rnd, test_jam_data_ascii_url_unencoded_chars(), 1, 79);
            addEncoded(cred, std::min(len, remaining() - 3));
            printToBuff(":%s", cred);
        }
        printToBuff("%s", "@");
    }

    // Host
    if (remaining() >= 15 && options->flags & TEST_JAM_URL_ALLOW_IPV4_HOST && percChance(10)) {
        char ip4[16];
        test_jam_internet_ipv4_buff(ip4, 16, rnd());
        printToBuff("%s", ip4);
    }
    else if (remaining() >= 42 && options->flags & TEST_JAM_URL_ALLOW_IPV6_HOST && percChance(10)) {
        char ip6[40];
        auto ip6Opts = test_jam_default_ipv6_options();
        ip6Opts.flags = options->flags & 0b111;
        test_jam_internet_ipv6_buff(ip6, 40, rnd(), &ip6Opts);
        printToBuff("[%s]", ip6);
    }
    else {
        char domain[64];
        test_jam_internet_domain_buff(domain, std::min<size_t>(64, remaining()), rnd(), options->domainOptions);
        printToBuff("%s", domain);
    }

    // Port
    if (remaining() > 10 && options->flags & TEST_JAM_URL_ALLOW_PORT && percChance(60)) {
        const auto port = test_jam_numbers_uint16(rnd(), 1, UINT16_MAX);
        printToBuff(":%u", port);
    }

    bool addedPath = false;

    if (remaining() > 10 && options->flags & TEST_JAM_URL_ALLOW_RANDOM_PATH) {
        size_t numPathSegments = rnd() % 15;
        char pathSegment[40];
        for (size_t i = 0; i < numPathSegments; ++i) {
            addedPath = true;
            const size_t len = test_jam::impl::from_charset_buff_rnd(pathSegment, 40, rnd, test_jam_data_ascii_url_unencoded_chars(), 1, 39);
            for (size_t j = 0; j < len; ++j) {
                if ((j ^ seed) % 20 == 1) {
                    pathSegment[j] = '.';
                    j += 2;
                }
            }
            addEncoded(pathSegment, std::min(len, remaining() - 3));
            printToBuff("/%s", pathSegment);
        }
    }

    if (remaining() > 10 && options->flags & TEST_JAM_URL_ALLOW_QUERY && percChance(50)) {
        if (!addedPath) {
            addedPath = true;
            printToBuff("%s", "/");
        }

        char querySegment[40];
        size_t numQuerySegments = rnd() % 20;
        for (size_t i = 0; i < numQuerySegments; ++i) {
            size_t len = test_jam::impl::from_charset_buff_rnd(querySegment, 40, rnd, test_jam_data_ascii_url_unencoded_chars(), 1, 39);
            addEncoded(querySegment, std::min(len, remaining() - 3));
            if (i == 0) {
                printToBuff("?%s", querySegment);
            }
            else if (i % 2 == 1) {
                printToBuff("=%s", querySegment);
            }
            else {
                printToBuff("&%s", querySegment);
            }
        }
    }

    if (remaining() > 10 && options->flags & TEST_JAM_URL_ALLOW_HASH && percChance(50)) {
        if (!addedPath) {
            addedPath = true;
            printToBuff("%s", "/");
        }

        char hashSegment[90];
        size_t len = test_jam::impl::from_charset_buff_rnd(hashSegment, 90, rnd, test_jam_data_ascii_url_unencoded_chars(), 0, 89);
        addEncoded(hashSegment, std::min(len, remaining() - 3));
        printToBuff("#%s", hashSegment);
    }

    while (curLen < minLen) {
        if (!addedPath) {
            addedPath = true;
            printToBuff("%s", "/");
            continue;
        }
        // Doing up to 4 characters at once to make this faster
        auto val = rnd();
        const char fc = static_cast<char>(val % 26 + 'a');
        val /= 26;
        const char sc = static_cast<char>(val % 26 + 'a');
        val /= 26;
        const char tc = static_cast<char>(val % 26 + 'a');
        val /= 26;
        const char fthc = static_cast<char>(val % 26 + 'a');
        curLen += std::min<size_t>(minLen - curLen, snprintf(buffer + curLen, minLen - curLen + 1, "%c%c%c%c", fc, sc, tc, fthc));
    }

    return curLen;
}

void
test_jam_internet_url(TestJamString *outPtr, uint64_t seed, const TestJamUrlOptions *options, const TestJamAllocator *alloc) {
    if (outPtr == nullptr) {
        return;
    }
    if (alloc == nullptr) {
        auto allocator = test_jam_mem_malloc_allocator();
        return test_jam_internet_url(outPtr, seed, options, &allocator);
    }
    if (options == nullptr) {
        auto opts = test_jam_default_url_options();
        return test_jam_internet_url(outPtr, seed, &opts, alloc);
    }
    outPtr->data = (char*)alloc->alloc(options->maxLen + 1);
    outPtr->length = test_jam_internet_url_buff(outPtr->data, options->maxLen + 1, seed, options);
}

TestJamDomainOptions test_jam_default_domain_options() {
    return TestJamDomainOptions{
        .minLen = 3,
        .maxLen = 63
    };
}

TestJamEmailOptions test_jam_default_email_options() {
    return TestJamEmailOptions{
        .flags = ~0u,
        .minLen=10,
        .maxLen=90,
        .domainOptions=nullptr
    };
}

size_t
test_jam_internet_email_buff(char *buffer, size_t bSize, uint64_t seed, const TestJamEmailOptions *options) {
    if (!options) {
        auto opts = test_jam_default_email_options();
        return test_jam_internet_email_buff(buffer, bSize, seed, &opts);
    }

    auto rnd = test_jam::impl::TinyRand(seed);
    const auto finalLen = std::min<size_t>(rnd() % (options->maxLen - options->minLen + 1) + options->minLen, bSize - 1);
    size_t writtenLen = 0;

    // Convenience
    const auto percChance = [&rnd](size_t perc) {
        const auto m = rnd() % 100;
        return m < perc;
    };
    const auto remaining = [&finalLen, writtenLen](){ return finalLen - writtenLen + 1; };
    const auto printToBuff = [&remaining, &finalLen, &buffer, &writtenLen](const char* fmt, auto ...args) -> size_t {
        if (writtenLen < finalLen) {
            const auto oldWrittenLen = writtenLen;
            const auto r = remaining();
            const auto len = std::min<size_t>(r - 1, snprintf(buffer + writtenLen, r, fmt, args...));
            writtenLen += len;
            assert(writtenLen == strlen(buffer));
            return len;
        }
        return 0;
    };

    constexpr size_t minAddrLen = 1;
    constexpr size_t minAtLen = 1;
    size_t maxDomainLen = 63;
    size_t minDomainLen = 20;
    if (options->domainOptions) {
        maxDomainLen = options->domainOptions->maxLen;
        minDomainLen = options->domainOptions->minLen;
        minDomainLen = std::max<size_t>(minDomainLen, 3);
        maxDomainLen = std::max<size_t>(maxDomainLen, 3);
        if (minDomainLen > maxDomainLen) {
            std::swap(maxDomainLen, minDomainLen);
        }
    }
    size_t domainLen = std::min<size_t>(rnd() % (maxDomainLen - minDomainLen + 1) + minDomainLen, finalLen - 3);
    if (domainLen >= finalLen - 3) {
        if (finalLen > 6) {
            writtenLen += test_jam_ascii_alpha_numeric_lower_buff(buffer, finalLen - 5, rnd(), finalLen - 6,
                                                                  finalLen - 6);
            printToBuff("@%c.com", static_cast<char>(rnd() % 26 + 'a'));
            assert(writtenLen == finalLen);
            assert(writtenLen == strlen(buffer));
            return writtenLen;
        }
        else {
            printToBuff("%c@%c.com", static_cast<char>(rnd() % 26 + 'a'), static_cast<char>(rnd() % 26 + 'a'));
            assert(writtenLen == strlen(buffer));
            return writtenLen;
        }
    }
    char domain[64];
    bool usedDomain = false;

    if (domainLen >= 47 && options->flags & TEST_JAM_EMAIL_ALLOW_IPV6_HOST && percChance(45)) {
        char ip6[41];
        auto ip6Opts = test_jam_default_ipv6_options();
        ip6Opts.flags = options->flags & 0b111;
        test_jam_internet_ipv6_buff(ip6, 41, rnd(), &ip6Opts);
        domainLen = snprintf(domain, 64, "[IPv6:%s]", ip6);
    }
    else if (domainLen >= 16 && options->flags & TEST_JAM_EMAIL_ALLOW_IPV4_HOST && percChance(45)) {
        char ip4[16];
        test_jam_internet_ipv4_buff(ip4, 16, rnd());
        domainLen = snprintf(domain, 64, "[%s]", ip4);
    }
    else {
        usedDomain = true;
        auto opts = options->domainOptions ? *options->domainOptions : test_jam_default_domain_options();
        opts.minLen = domainLen;
        opts.maxLen = domainLen;
        domainLen = test_jam_internet_domain_buff(domain, 64, rnd(), &opts);
    }

    size_t addressLen = finalLen  - domainLen - minAtLen;
    const auto origLen = writtenLen;
    if (addressLen <= 10) {
        char address[11];
        test_jam_internet_email_atom_buff(address, 11, rnd(), options);
        printToBuff("%s@%s", address, domain);
        assert(writtenLen == finalLen);
    }
    else if (options->flags & TEST_JAM_EMAIL_ALLOW_QUOTES && percChance(30)) {
        writtenLen += test_jam_internet_email_quoted_buff(buffer, addressLen + 1, rnd(), options);
        printToBuff("@%s", domain);
        assert(writtenLen == finalLen);
    }
    else {
        writtenLen += test_jam_internet_email_atom_buff(buffer, addressLen + 1, rnd(), options);
        printToBuff("@%s", domain);
        assert(writtenLen == finalLen);
    }

    bool foundPeriod = !usedDomain;
    for (size_t i = origLen; i < writtenLen - 1; ++i) {
        if (buffer[i] == '.') {
            foundPeriod = true;
            break;
        }
    }
    if (!foundPeriod) {
        buffer[writtenLen - 2] = '.';
    }

    if (buffer[finalLen - 1] == '.') {
        buffer[finalLen - 1] = 'a';
    }

    assert(writtenLen == finalLen);
    assert(finalLen == strlen(buffer));
    return finalLen;
}

size_t test_jam_internet_email_atom_buff(char *buffer, size_t bufferSize, uint64_t seed, const TestJamEmailOptions* options) {
    if (bufferSize < 1) {
        return 0;
    }

    static constexpr std::string_view alphaNum = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static constexpr std::string_view localNonQuoted = "!#$%&*+/=?^_`{|}~-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    static constexpr std::string_view localNonQuotedNoSpecials = "+_-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    auto rnd = test_jam::impl::TinyRand(seed);

    buffer[bufferSize - 1] = '\0';
    buffer[0] = alphaNum[rnd() % alphaNum.size()];

    const auto& chars = (options->flags & TEST_JAM_EMAIL_ALLOW_SPECIALS) != 0 && rnd() % 100 <= 25
            ? localNonQuotedNoSpecials
            : localNonQuoted;

    for (size_t i = 1; i < bufferSize - 1; ++i) {
        auto index = rnd() % chars.size();
        char c = chars[index];
        if ((i == 0 || i + 1 >= bufferSize - 1) && (c == '-') || (options->flags & TEST_JAM_EMAIL_ALLOW_PLUS) == 0 && c == '+') {
            c = static_cast<char>(rnd() % 26 + 'a');
        }
        buffer[i] = c;
    }

    assert(buffer[bufferSize - 1] == '\0');
    return bufferSize - 1;
}

size_t test_jam_internet_email_quoted_buff(char *buffer, size_t bufferSize, uint64_t seed,
                                           const TestJamEmailOptions *options) {
    static constexpr std::string_view localQuoted = "!@#$%^&*()_-+=[]{}\\|;:'\", <.>/?0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    auto rnd = test_jam::impl::TinyRand(seed);
    buffer[bufferSize - 1] = '\0';
    size_t size = bufferSize - 1;
    buffer[0] = '"';
    for (size_t i = 1; i < size; ++i) {
        auto index = rnd() % localQuoted.size();
        char c = localQuoted[index];
        if (c == '\\' || c == '"') {
            if (i + 2 < size) {
                buffer[i] = '\\';
                buffer[i + 1] = c;
            }
            else {
                buffer[i] = static_cast<char>(rnd() % 26 + 'a');
            }
        }
        else {
            buffer[i] = c;
        }
    }
    buffer[size - 1] = '"';
    assert(buffer[bufferSize - 1] == '\0');
    return bufferSize - 1;
}

void test_jam_internet_email(TestJamString *outPtr, uint64_t seed, const TestJamEmailOptions *options,
                             const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return;
    }
    if (options == nullptr) {
        auto opts = test_jam_default_email_options();
        return test_jam_internet_email(outPtr, seed, &opts, allocator);
    }
    if (allocator == nullptr) {
        auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_internet_email(outPtr, seed, options, &alloc);
    }
    outPtr->data = (char*)allocator->alloc(options->maxLen + 1);
    outPtr->length = test_jam_internet_email_buff(outPtr->data, options->maxLen + 1, seed, options);
}

template<typename T>
struct TestJamInternetOpts {
    uint64_t seed;
    T opts;
};

void test_jam_internet_simplifier_ipv6(TestJamStringSimplifier *outPtr, uint64_t seed, const TestJamIpv6Options *options,
                                       const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return;
    }

    if (options == nullptr) {
        auto opts = test_jam_default_ipv6_options();
        return test_jam_internet_simplifier_ipv6(outPtr, seed, &opts, allocator);
    }

    if (allocator == nullptr) {
        auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_internet_simplifier_ipv6(outPtr, seed, options, &alloc);
    }

    outPtr->allocator = *allocator;
    test_jam_internet_ipv6(&outPtr->value, seed, options, allocator);
    outPtr->nextState = +[](TestJamStringSimplifier* simplifier){
        auto* state = (TestJamInternetOpts<TestJamIpv6Options>*)simplifier->state;
        if (state->opts.flags) {
            state->opts.flags = ((state->opts.flags & 0b111) >> 1) & (state->opts.flags & 0b111);
            test_jam_free_string_contents(&simplifier->value, &simplifier->allocator);
            test_jam_internet_ipv6(&simplifier->value, state->seed, &state->opts, &simplifier->allocator);
        }
        else {
            simplifier->hasValue = false;
            simplifier->nextState = nullptr;
        }
    };
    outPtr->hasValue = true;
    outPtr->state = allocator->alloc(sizeof(TestJamInternetOpts<TestJamIpv6Options>));
    ((TestJamInternetOpts<TestJamIpv6Options>*)outPtr->state)->opts = *options;
    ((TestJamInternetOpts<TestJamIpv6Options>*)outPtr->state)->seed = seed;
}

void
test_jam_internet_simplifier_domain(TestJamStringSimplifier *outPtr, uint64_t seed, const TestJamDomainOptions *options,
                                    const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return;
    }

    if (options == nullptr) {
        TestJamDomainOptions opts = test_jam_default_domain_options();
        return test_jam_internet_simplifier_domain(outPtr, seed, &opts, allocator);
    }

    if (allocator == nullptr) {
        auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_internet_simplifier_domain(outPtr, seed, options, &alloc);
    }

    outPtr->allocator = *allocator;
    test_jam_internet_domain(&outPtr->value, seed, options, allocator);
    outPtr->nextState = +[](TestJamStringSimplifier* simplifier){
        auto* state = (TestJamInternetOpts<TestJamDomainOptions>*)simplifier->state;
        if (simplifier->hasValue && simplifier->value.length > state->opts.minLen) {
            state->opts.maxLen = simplifier->value.length - 1;
            test_jam_free_string_contents(&simplifier->value, &simplifier->allocator);
            auto opts = state->opts;
            opts.minLen = opts.maxLen;
            test_jam_internet_domain(&simplifier->value, state->seed, &opts, &simplifier->allocator);
            if (!simplifier->value.length) {
                simplifier->hasValue = false;
                simplifier->nextState = nullptr;
            }
        }
        else {
            simplifier->hasValue = false;
            simplifier->nextState = nullptr;
        }
    };
    outPtr->hasValue = true;
    outPtr->state = allocator->alloc(sizeof(TestJamInternetOpts<TestJamDomainOptions>));
    ((TestJamInternetOpts<TestJamDomainOptions>*)outPtr->state)->opts = *options;
    ((TestJamInternetOpts<TestJamDomainOptions>*)outPtr->state)->seed = seed;
}

void
test_jam_internet_simplifier_url(TestJamStringSimplifier *outPtr, uint64_t seed, const TestJamUrlOptions *options,
                                 const TestJamAllocator *allocator) {

    if (outPtr == nullptr) {
        return;
    }

    if (options == nullptr) {
        TestJamUrlOptions opts = test_jam_default_url_options();
        return test_jam_internet_simplifier_url(outPtr, seed, &opts, allocator);
    }

    if (allocator == nullptr) {
        auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_internet_simplifier_url(outPtr, seed, options, &alloc);
    }

    outPtr->allocator = *allocator;
    test_jam_internet_url(&outPtr->value, seed, options, allocator);
    outPtr->nextState = +[](TestJamStringSimplifier* simplifier){
        auto* state = (TestJamInternetOpts<TestJamUrlOptions>*)simplifier->state;
        if (simplifier->hasValue && simplifier->value.length > state->opts.minLen) {
            state->opts.maxLen = std::max(simplifier->value.length - 10, state->opts.minLen);
            test_jam_free_string_contents(&simplifier->value, &simplifier->allocator);
            auto opts = state->opts;
            opts.minLen = opts.maxLen;
            test_jam_internet_url(&simplifier->value, state->seed, &opts, &simplifier->allocator);
            if (!simplifier->value.length) {
                simplifier->hasValue = false;
                simplifier->nextState = nullptr;
            }
        }
        else {
            simplifier->hasValue = false;
            simplifier->nextState = nullptr;
        }
    };
    outPtr->hasValue = true;
    outPtr->state = allocator->alloc(sizeof(TestJamInternetOpts<TestJamUrlOptions>));
    ((TestJamInternetOpts<TestJamUrlOptions>*)outPtr->state)->opts = *options;
    ((TestJamInternetOpts<TestJamUrlOptions>*)outPtr->state)->seed = seed;
}

void
test_jam_internet_simplifier_email(TestJamStringSimplifier *outPtr, uint64_t seed, const TestJamEmailOptions *options,
                                 const TestJamAllocator *allocator) {

    if (outPtr == nullptr) {
        return;
    }

    if (options == nullptr) {
        TestJamEmailOptions opts = test_jam_default_email_options();
        return test_jam_internet_simplifier_email(outPtr, seed, &opts, allocator);
    }

    if (allocator == nullptr) {
        auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_internet_simplifier_email(outPtr, seed, options, &alloc);
    }

    outPtr->allocator = *allocator;
    test_jam_internet_email(&outPtr->value, seed, options, allocator);
    outPtr->nextState = +[](TestJamStringSimplifier* simplifier){
        auto* state = (TestJamInternetOpts<TestJamEmailOptions>*)simplifier->state;
        if (simplifier->hasValue && simplifier->value.length > state->opts.minLen) {
            state->opts.maxLen = std::max(simplifier->value.length - 10, state->opts.minLen);
            test_jam_free_string_contents(&simplifier->value, &simplifier->allocator);
            auto opts = state->opts;
            opts.minLen = opts.maxLen;
            test_jam_internet_email(&simplifier->value, state->seed, &opts, &simplifier->allocator);
            if (!simplifier->value.length) {
                simplifier->hasValue = false;
                simplifier->nextState = nullptr;
            }
        }
        else {
            simplifier->hasValue = false;
            simplifier->nextState = nullptr;
        }
    };
    outPtr->hasValue = true;
    outPtr->state = allocator->alloc(sizeof(TestJamInternetOpts<TestJamEmailOptions>));
    ((TestJamInternetOpts<TestJamEmailOptions>*)outPtr->state)->opts = *options;
    ((TestJamInternetOpts<TestJamEmailOptions>*)outPtr->state)->seed = seed;
}
