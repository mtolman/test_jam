#include "common.h"

const TestJamConstStringOptional test_jam::impl::nullOpt = {
        .present = false,
        .str = {
                .length = 0,
                .data = ""
        }
};
