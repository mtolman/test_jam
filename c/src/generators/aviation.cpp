#include <test-jam/generators/aviation.h>
#include <test-jam/data/aviation.h>
#include "common.h"

TestJamConstString test_jam_aviation_aircraft_type(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_aviation_aircraft_type);
}

TestJamAviationRecord test_jam_aviation_airline(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_aviation_airline);
}

TestJamAviationRecord test_jam_aviation_airplane(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_aviation_airplane);
}

TestJamAviationRecord test_jam_aviation_airport(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_aviation_airport);
}
