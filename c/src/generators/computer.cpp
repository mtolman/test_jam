#include <test-jam/generators/computer.h>
#include <test-jam/data/computer.h>
#include "common.h"

TestJamConstString test_jam_computer_cmake_system_name(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_cmake_system_name);
}

TestJamConstString test_jam_computer_cpu_architecture(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_cpu_architecture);
}

TestJamConstString test_jam_computer_file_extension(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_file_extension);
}

TestJamMimeMapping test_jam_computer_file_mime_mapping(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_file_mime_mapping);
}

TestJamConstString test_jam_computer_file_type(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_file_type);
}

TestJamConstString test_jam_computer_mime_type(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_mime_type);
}

TestJamConstString test_jam_computer_operating_system(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_computer_operating_system);
}
