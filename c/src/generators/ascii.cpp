#include <test-jam/generators/ascii.h>
#include <test-jam/data/ascii.h>
#include "common.h"
#include "test-jam/simplifiers/ascii.h"


size_t
test_jam_ascii_from_charset_buff(char *bufferPtr, size_t bufferSize, uint64_t seed, const TestJamAsciiCharSet *charSet,
                                 size_t minLen, size_t maxLen) {
    auto rnd = test_jam::impl::TinyRand(seed);
    return test_jam::impl::from_charset_buff_rnd(bufferPtr, bufferSize, rnd, charSet, minLen, maxLen);
}

size_t
test_jam_ascii_from_charset(TestJamString *outPtr, uint64_t seed, const TestJamAsciiCharSet *charSet, size_t minLen,
                            size_t maxLen,
                            const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return 0;
    }
    if (allocator == nullptr) {
        const auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_ascii_from_charset(outPtr, seed, charSet, minLen, maxLen, &alloc);
    }

    auto rnd = test_jam::impl::TinyRand(seed);
    size_t len = maxLen >= minLen ? rnd() % (maxLen - minLen + 1) + minLen : minLen;
    outPtr->data = (char *) allocator->alloc(sizeof(char) * (len + 1));
    outPtr->data[len] = '\0';
    outPtr->length = len;
    return test_jam_ascii_from_charset_buff(outPtr->data, len + 1, seed, charSet, minLen, maxLen);
}

#define TEST_JAM_ASCII_FN_DEF(SUFFIX) \
size_t test_jam_ascii_ ## SUFFIX(TestJamString *outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator *allocator) { \
    return test_jam_ascii_from_charset(outPtr, seed, test_jam_data_ascii_ ## SUFFIX(), minLen, maxLen, allocator); \
} \
size_t test_jam_ascii_ ## SUFFIX ## _buff(char *bufferPtr, size_t bufferSize, uint64_t seed, size_t minLen, size_t maxLen) { \
    return test_jam_ascii_from_charset_buff(bufferPtr, bufferSize, seed, test_jam_data_ascii_ ## SUFFIX(), minLen, maxLen); \
}

TEST_JAM_ASCII_FN_DEF(any)
TEST_JAM_ASCII_FN_DEF(alpha)
TEST_JAM_ASCII_FN_DEF(alpha_lower)
TEST_JAM_ASCII_FN_DEF(alpha_upper)
TEST_JAM_ASCII_FN_DEF(alpha_numeric)
TEST_JAM_ASCII_FN_DEF(alpha_numeric_lower)
TEST_JAM_ASCII_FN_DEF(alpha_numeric_upper)
TEST_JAM_ASCII_FN_DEF(binary)
TEST_JAM_ASCII_FN_DEF(bracket)
TEST_JAM_ASCII_FN_DEF(control)
TEST_JAM_ASCII_FN_DEF(domain_piece)
TEST_JAM_ASCII_FN_DEF(hex)
TEST_JAM_ASCII_FN_DEF(hex_lower)
TEST_JAM_ASCII_FN_DEF(hex_upper)
TEST_JAM_ASCII_FN_DEF(html_named)
TEST_JAM_ASCII_FN_DEF(math)
TEST_JAM_ASCII_FN_DEF(numeric)
TEST_JAM_ASCII_FN_DEF(octal)
TEST_JAM_ASCII_FN_DEF(printable)
TEST_JAM_ASCII_FN_DEF(quote)
TEST_JAM_ASCII_FN_DEF(safe_punctuation)
TEST_JAM_ASCII_FN_DEF(sentence_punctuation)
TEST_JAM_ASCII_FN_DEF(symbol)
TEST_JAM_ASCII_FN_DEF(text)
TEST_JAM_ASCII_FN_DEF(url_unencoded_chars)
TEST_JAM_ASCII_FN_DEF(url_unencoded_hash_chars)
TEST_JAM_ASCII_FN_DEF(whitespace)

#undef TEST_JAM_ASCII_FN_DEF

static void ascii_next_simple_value(struct TestJamStringSimplifier* simplifier) {
    auto* state = (TestJamAsciiSimplifyState*)simplifier->state;
    if (simplifier->value.length <= state->minLen) {
        simplifier->hasValue = false;
        simplifier->nextState = nullptr;
        return;
    }
    simplifier->value.length--;
    simplifier->value.data[simplifier->value.length] = '\0';
}

void test_jam_simplifier_ascii(TestJamStringSimplifier *outPtr, uint64_t seed, const TestJamAsciiCharSet *charSet,
                               size_t minLen, size_t maxLen, const TestJamAllocator *allocator) {
    if (outPtr == nullptr) {
        return;
    }

    if (allocator == nullptr) {
        auto alloc = test_jam_mem_malloc_allocator();
        return test_jam_simplifier_ascii(outPtr, seed, charSet, minLen, maxLen, &alloc);
    }

    outPtr->allocator = *allocator;
    if (charSet == nullptr || minLen == maxLen) {
        outPtr->nextState = nullptr;
        outPtr->hasValue = false;
        outPtr->state = nullptr;
        return;
    }

    test_jam_ascii_from_charset(&outPtr->value, seed, charSet, minLen, maxLen, allocator);
    if (minLen >= outPtr->value.length) {
        test_jam_free_string_contents(&outPtr->value, allocator);
        outPtr->nextState = nullptr;
        outPtr->hasValue = false;
        outPtr->state = nullptr;
        return;
    }
    outPtr->nextState = ascii_next_simple_value;
    outPtr->hasValue = true;

    auto* state = (TestJamAsciiSimplifyState*)allocator->alloc(sizeof(TestJamAsciiSimplifyState));
    state->minLen = minLen;
    outPtr->state = (void*)state;
}

#define TEST_JAM_ASCII_SIMPL_FN_DEF(SUFFIX) \
void test_jam_simplifier_ascii_ ## SUFFIX(TestJamStringSimplifier *outPtr, uint64_t seed, size_t minLen, size_t maxLen, const TestJamAllocator *allocator) { \
    test_jam_simplifier_ascii(outPtr, seed, test_jam_data_ascii_ ## SUFFIX(), minLen, maxLen, allocator); \
}

TEST_JAM_ASCII_SIMPL_FN_DEF(any)
TEST_JAM_ASCII_SIMPL_FN_DEF(alpha)
TEST_JAM_ASCII_SIMPL_FN_DEF(alpha_lower)
TEST_JAM_ASCII_SIMPL_FN_DEF(alpha_upper)
TEST_JAM_ASCII_SIMPL_FN_DEF(alpha_numeric)
TEST_JAM_ASCII_SIMPL_FN_DEF(alpha_numeric_lower)
TEST_JAM_ASCII_SIMPL_FN_DEF(alpha_numeric_upper)
TEST_JAM_ASCII_SIMPL_FN_DEF(binary)
TEST_JAM_ASCII_SIMPL_FN_DEF(bracket)
TEST_JAM_ASCII_SIMPL_FN_DEF(control)
TEST_JAM_ASCII_SIMPL_FN_DEF(domain_piece)
TEST_JAM_ASCII_SIMPL_FN_DEF(hex)
TEST_JAM_ASCII_SIMPL_FN_DEF(hex_lower)
TEST_JAM_ASCII_SIMPL_FN_DEF(hex_upper)
TEST_JAM_ASCII_SIMPL_FN_DEF(html_named)
TEST_JAM_ASCII_SIMPL_FN_DEF(math)
TEST_JAM_ASCII_SIMPL_FN_DEF(numeric)
TEST_JAM_ASCII_SIMPL_FN_DEF(octal)
TEST_JAM_ASCII_SIMPL_FN_DEF(printable)
TEST_JAM_ASCII_SIMPL_FN_DEF(quote)
TEST_JAM_ASCII_SIMPL_FN_DEF(safe_punctuation)
TEST_JAM_ASCII_SIMPL_FN_DEF(sentence_punctuation)
TEST_JAM_ASCII_SIMPL_FN_DEF(symbol)
TEST_JAM_ASCII_SIMPL_FN_DEF(text)
TEST_JAM_ASCII_SIMPL_FN_DEF(url_unencoded_chars)
TEST_JAM_ASCII_SIMPL_FN_DEF(url_unencoded_hash_chars)
TEST_JAM_ASCII_SIMPL_FN_DEF(whitespace)

#undef TEST_JAM_ASCII_SIMPL_FN_DEF
