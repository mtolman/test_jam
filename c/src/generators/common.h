#pragma once

#include <test-jam/objects.h>
#include <tinymt/tinymt64.h>
#include <optional>
#include <cassert>

namespace test_jam::impl {
    struct TinyRand {
        tinymt64_t state = {
                // Initialize our memory
                .mat1=0xfa051f40,
                .mat2=0xffd0fff4,
                .tmat=0x58d02ffeffbfffbc
        };

        TinyRand(size_t seed) {
            // Initialize our seed
            tinymt64_init(&state, seed);
        }

        auto uint64() -> uint64_t { return tinymt64_generate_uint64(&state); }

        auto fp64() -> double { return tinymt64_generate_double(&state); }

        auto operator()() { return uint64(); }

        template<class T>
        auto index_of(const T* values, size_t len) -> std::optional<T> {
            if (len == 0 || values == nullptr) {
                return std::nullopt;
            }
            return values[uint64() % len];
        }
    };

    template<typename F>
    auto index_generator(size_t seed, const F& func) {
        size_t len;
        auto* arr = func(&len);
        assert(len > 0);
        return arr[seed % len];
    }

    extern const TestJamConstStringOptional nullOpt;

    template<typename F>
    auto complexity_generator(size_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity, const F& func) {
        auto rnd = test_jam::impl::TinyRand(seed);
        const auto numComplexities = (static_cast<uint64_t>(maxComplexity) - static_cast<uint64_t>(minComplexity) + 1);
        auto complexity = static_cast<TestJamComplexity>(
                (rnd.uint64() % numComplexities) + static_cast<uint64_t>(minComplexity)
        );

        size_t len;
        auto* arr = func(&len, complexity);
        assert(len > 0);
        return arr[static_cast<size_t>(rnd.uint64()) % len];
    }

    template<typename CharSet>
    size_t
    from_charset_buff_rnd(char *bufferPtr, size_t bufferSize, TinyRand& rnd, const CharSet *charSet, size_t minLen, size_t maxLen) {
        if (bufferPtr == nullptr || bufferSize < 2) {
            return 0;
        }
        if (charSet == nullptr || charSet->length == 0 || charSet->chars == nullptr) {
            bufferPtr[0] = '\0';
            return 0;
        }
        size_t len = maxLen >= minLen ? rnd() % (maxLen - minLen + 1) + minLen : minLen;
        len = len > bufferSize ? bufferSize  - 1: len;
        for (size_t i = 0; i < len; i += 2) {
            const auto r = rnd();
            const auto first = r % charSet->length;
            const auto second = (r / charSet->length) % charSet->length;
            bufferPtr[i] = charSet->chars[first];
            bufferPtr[i + 1] = charSet->chars[second];
        }
        bufferPtr[len] = '\0';
        return len;
    }
}
