#include <test-jam/generators/color.h>
#include <test-jam/data/color.h>
#include "common.h"

TestJamColorNamed test_jam_color_named(uint64_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_color_named);
}

TestJamColorRgb test_jam_color_rgb(uint64_t seed) {
    auto rnd = test_jam::impl::TinyRand(seed);
    return {
            .red=static_cast<uint8_t>(rnd()),
            .green=static_cast<uint8_t>(rnd()),
            .blue=static_cast<uint8_t>(rnd()),
    };
}

TestJamColorRgba test_jam_color_rgba(uint64_t seed) {
    auto rnd = test_jam::impl::TinyRand(seed);
    return {
            .red=static_cast<uint8_t>(rnd()),
            .green=static_cast<uint8_t>(rnd()),
            .blue=static_cast<uint8_t>(rnd()),
            .alpha=static_cast<uint8_t>(rnd()),
    };
}

TestJamColorHsl test_jam_color_hsl(uint64_t seed) {
    auto rnd = test_jam::impl::TinyRand(seed);
    return {
            .hue=rnd.fp64() * 360.0,
            .saturation=rnd.fp64(),
            .luminosity=rnd.fp64()
    };
}

TestJamColorHsla test_jam_color_hsla(uint64_t seed) {
    auto rnd = test_jam::impl::TinyRand(seed);
    return {
            .hue=rnd.fp64() * 360.0,
            .saturation=rnd.fp64(),
            .luminosity=rnd.fp64(),
            .alpha=rnd.fp64()
    };
}
