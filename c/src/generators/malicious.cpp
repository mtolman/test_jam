#include <test-jam/generators/malicious.h>
#include <test-jam/data/malicious.h>
#include "common.h"

TestJamConstString test_jam_malicious_sql_injection(size_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_malicious_sql_injection);
}

TestJamConstString test_jam_malicious_format_injection(size_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_malicious_format_injection);
}

TestJamConstString test_jam_malicious_xss(size_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_malicious_xss);
}

TestJamConstString test_jam_malicious_edge_cases(size_t seed) {
    return test_jam::impl::index_generator(seed, test_jam_data_malicious_edge_cases);
}
