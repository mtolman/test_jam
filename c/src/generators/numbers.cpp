#include <test-jam/generators/numbers.h>
#include <test-jam/simplifiers/numbers.h>
#include <algorithm>
#include "common.h"
#include "../../../cxx/include/test-jam-cxx/gens/numbers.h"

#include <cmath>

uint64_t test_jam_numbers_uint64(uint64_t seed, uint64_t min, uint64_t max) {
    if (max == std::numeric_limits<decltype(max)>::max() && min == 0) {
        return seed;
    }
    return seed % (max - min + 1) + min;
}

int64_t test_jam_numbers_int64(uint64_t seed, int64_t min, int64_t max) {
    if (max >= std::numeric_limits<decltype(max)>::max()/2 && min <= std::numeric_limits<decltype(min)>::min()/2) {
        auto val = *(int64_t*)(void*)(&seed);
        return std::max(std::min(val, max), min);
    }
    uint64_t range = static_cast<uint64_t>(max - min) + 1;
    return static_cast<int64_t>(seed % range) + min;
}

uint32_t test_jam_numbers_uint32(uint64_t seed, uint32_t min, uint32_t max) {
    return static_cast<uint32_t>(test_jam_numbers_uint64(seed, min, max));
}

uint16_t test_jam_numbers_uint16(uint64_t seed, uint16_t min, uint16_t max) {
    return static_cast<uint16_t>(test_jam_numbers_uint64(seed, min, max));
}

uint8_t test_jam_numbers_uint8(uint64_t seed, uint8_t min, uint8_t max) {
    return static_cast<uint8_t>(test_jam_numbers_uint64(seed, min, max));
}

int32_t test_jam_numbers_int32(uint64_t seed, int32_t min, int32_t max) {
    return static_cast<int32_t>(test_jam_numbers_uint64(seed, min, max));
}

int16_t test_jam_numbers_int16(uint64_t seed, int16_t min, int16_t max) {
    return static_cast<int16_t>(test_jam_numbers_uint64(seed, min, max));
}

int8_t test_jam_numbers_int8(uint64_t seed, int8_t min, int8_t max) {
    return static_cast<int8_t>(test_jam_numbers_uint64(seed, min, max));
}

double test_jam_numbers_double(uint64_t seed, double min, double max, bool allowNan, bool allowInf) {
    if (allowNan && seed % 100 == 1) {
        return std::numeric_limits<double>::quiet_NaN();
    }
    else if (allowInf && seed % 100 == 2) {
        return std::numeric_limits<double>::infinity();
    }
    else if (allowInf && seed % 100 == 3) {
        return -std::numeric_limits<double>::infinity();
    }
    auto rnd = test_jam::impl::TinyRand(seed);
    const auto diff = (max / 2 - min / 2) * 2;
    if (diff <= 0) {
        return min;
    }
    auto v = rnd.fp64();
    return v * diff + min;
}

float test_jam_numbers_float(uint64_t seed, float min, float max, bool allowNan, bool allowInf) {
    if (allowNan && seed % 100 == 1) {
        return std::numeric_limits<float>::quiet_NaN();
    }
    else if (allowInf && seed % 100 == 2) {
        return std::numeric_limits<float>::infinity();
    }
    else if (allowInf && seed % 100 == 3) {
        return -std::numeric_limits<float>::infinity();
    }
    auto rnd = test_jam::impl::TinyRand(seed);
    const auto diff = (max / 2 - min / 2) * 2;
    if (diff <= 0) {
        return min;
    }
    auto v = static_cast<float>(rnd.fp64());
    return v * diff + min;
}

template<typename S, typename T>
static void simplify_unsigned(S *outPtr, uint64_t seed, T min, T max, T (*fn)(uint64_t, T, T)) {
    if (outPtr == nullptr) {
        return;
    }

    outPtr->value = fn(seed, min, max);
    outPtr->hasValue = true;
    outPtr->min = min;
    outPtr->nextState = +[](S* ptr) {
        if (ptr->value > ptr->min) {
            if (ptr->value - ptr->min > 50) {
                ptr->value = (ptr->value - ptr->min) / 2 + ptr->min;
            }
            else {
                ptr->value--;
            }
        }
        else {
            ptr->hasValue = false;
            ptr->nextState = nullptr;
        }
    };
}

void test_jam_numbers_simplifier_uint64(TestJamUint64Simplifier *outPtr, uint64_t seed, uint64_t min, uint64_t max) {
    simplify_unsigned(outPtr, seed, min, max, test_jam_numbers_uint64);
}

void test_jam_numbers_simplifier_uint32(TestJamUint32Simplifier *outPtr, uint64_t seed, uint32_t min, uint32_t max) {
    simplify_unsigned(outPtr, seed, min, max, test_jam_numbers_uint32);
}

void test_jam_numbers_simplifier_uint16(TestJamUint16Simplifier *outPtr, uint64_t seed, uint16_t min, uint16_t max) {
    simplify_unsigned(outPtr, seed, min, max, test_jam_numbers_uint16);
}

void test_jam_numbers_simplifier_uint8(TestJamUint8Simplifier *outPtr, uint64_t seed, uint8_t min, uint8_t max) {
    simplify_unsigned(outPtr, seed, min, max, test_jam_numbers_uint8);
}

template<typename S, typename T>
static void simplify_signed(S *outPtr, uint64_t seed, T min, T max, T (*fn)(uint64_t, T, T)) {
    if (outPtr == nullptr) {
        return;
    }

    outPtr->value = fn(seed, min, max);
    outPtr->hasValue = true;
    outPtr->max = max;
    outPtr->min = min;
    outPtr->nextState = +[](S* ptr) {
        if (ptr->value && ptr->value >= ptr->min && ptr->value <= ptr->max) {
            if (ptr->value == 0) {
                ptr->hasValue = false;
                ptr->nextState = nullptr;
                return;
            }

            auto absVal = std::abs(ptr->value);

            if (absVal > 60 && ptr->value != ptr->min && ptr->value != ptr->max) {
                ptr->value /= 2;
                if (ptr->value < ptr->min) {
                    ptr->value = ptr->min;
                }
                else if (ptr->value > ptr->max) {
                    ptr->value = ptr->max;
                }
            }
            else if (ptr->value > 0) {
                ptr->value--;
            }
            else {
                ptr->value++;
            }

            if (ptr->value < ptr->min) {
                ptr->value = ptr->min;
                ptr->hasValue = false;
                ptr->nextState = nullptr;
            }
            else if (ptr->value > ptr->max) {
                ptr->value = ptr->max;
                ptr->hasValue = false;
                ptr->nextState = nullptr;
            }
        }
        else {
            ptr->hasValue = false;
            ptr->nextState = nullptr;
        }
    };
}

void test_jam_numbers_simplifier_int64(TestJamInt64Simplifier *outPtr, uint64_t seed, int64_t min, int64_t max) {
    simplify_signed(outPtr, seed, min, max, test_jam_numbers_int64);
}

void test_jam_numbers_simplifier_int32(TestJamInt32Simplifier *outPtr, uint64_t seed, int32_t min, int32_t max) {
    simplify_signed(outPtr, seed, min, max, test_jam_numbers_int32);
}

void test_jam_numbers_simplifier_int16(TestJamInt16Simplifier *outPtr, uint64_t seed, int16_t min, int16_t max) {
    simplify_signed(outPtr, seed, min, max, test_jam_numbers_int16);
}

void test_jam_numbers_simplifier_int8(TestJamInt8Simplifier *outPtr, uint64_t seed, int8_t min, int8_t max) {
    simplify_signed(outPtr, seed, min, max, test_jam_numbers_int8);
}

template<typename T, typename S>
static void simplify_fp(S* outPtr, int64_t seed, T min, T max, bool allowNan, bool allowInfinity, T (*fn)(uint64_t, T, T, bool, bool)) {
    if (outPtr == nullptr) {
        return;
    }

    outPtr->value = fn(seed, min, max, allowNan, allowInfinity);
    if (std::isnan(outPtr->value) || std::isinf(outPtr->value)) {
        outPtr->hasValue = false;
        outPtr->nextState = nullptr;
        outPtr->max = outPtr->value;
        outPtr->min = outPtr->value;
        return;
    }
    outPtr->hasValue = true;
    outPtr->max = max;
    outPtr->min = min;
    outPtr->nextState = +[](S* ptr) {
        if (ptr->value >= ptr->min + std::numeric_limits<T>::epsilon() && ptr->value <= ptr->max - std::numeric_limits<T>::epsilon()) {
            auto next = fabs(ptr->max) > fabs(ptr->min) ? (ptr->value / 2 + ptr->min / 2) : (ptr->value / 2 + ptr->max / 2);
            if (next <= ptr->value + std::numeric_limits<T>::epsilon() && next >= ptr->value - std::numeric_limits<T>::epsilon()) {
                ptr->hasValue = false;
                ptr->nextState = nullptr;
                return;
            }
            if (std::abs(ptr->max) > std::abs(ptr->min)) {
                ptr->max = ptr->value;
            }
            else {
                ptr->min = ptr->value;
            }
            ptr->value = next;
        }
        else {
            ptr->hasValue = false;
            ptr->nextState = nullptr;
        }
    };
}

void test_jam_numbers_simplifier_double(TestJamDoubleSimplifier * outPtr, uint64_t seed, double min, double max, bool allowNan, bool allowInfinity) {
    simplify_fp(outPtr, seed, min, max, allowNan, allowInfinity, test_jam_numbers_double);
}

void test_jam_numbers_simplifier_float(TestJamFloatSimplifier * outPtr, uint64_t seed, float min, float max, bool allowNan, bool allowInfinity) {
    simplify_fp(outPtr, seed, min, max, allowNan, allowInfinity, test_jam_numbers_float);
}
