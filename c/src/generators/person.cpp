#include <test-jam/generators/person.h>
#include <test-jam/data/person.h>
#include "common.h"

TestJamPerson test_jam_person_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity) {
    auto rnd = test_jam::impl::TinyRand(seed);
    const auto numComplexities = (static_cast<uint64_t>(maxComplexity) - static_cast<uint64_t>(minComplexity) + 1);
    auto complexity = static_cast<TestJamComplexity>(
            (rnd.uint64() % numComplexities) + static_cast<uint64_t>(minComplexity)
    );
    const auto sex = static_cast<TestJamPersonSex>(rnd() % 2);

    size_t numGivenNames;
    const auto* givenNames = test_jam_data_person_given_name(&numGivenNames, sex, complexity);
    assert(numGivenNames > 0);

    size_t numSurnames;
    const auto* surnames = test_jam_data_person_surname(&numSurnames, complexity);
    assert(numSurnames > 0);

    size_t numPrefixes;
    const auto* prefixes = test_jam_data_person_prefix(&numPrefixes, sex, complexity);
    assert(numPrefixes > 0);

    size_t numSuffixes;
    const auto* suffixes = test_jam_data_person_suffix(&numSuffixes, complexity);
    assert(numSuffixes > 0);

    bool hasSuffix = rnd() % 3 == 1;
    bool hasPrefix = rnd() % 3 == 1;

    return TestJamPerson{
        .sex=sex,
        .givenName=givenNames[rnd() % numGivenNames],
        .surname = surnames[rnd() % numSurnames],
        .prefix={.present=hasPrefix, .str=prefixes[rnd() % numPrefixes]},
        .suffix={.present=hasSuffix, .str=suffixes[rnd() % numSuffixes]},
        .westernNameOrder=rnd() % 3 != 1
    };
}

TestJamConstString
test_jam_person_given_name_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity) {
    auto rnd = test_jam::impl::TinyRand(seed);
    const auto numComplexities = (static_cast<uint64_t>(maxComplexity) - static_cast<uint64_t>(minComplexity) + 1);
    auto complexity = static_cast<TestJamComplexity>(
            (rnd.uint64() % numComplexities) + static_cast<uint64_t>(minComplexity)
    );
    const auto sex = static_cast<TestJamPersonSex>(rnd() % 2);

    size_t numGivenNames;
    const auto* givenNames = test_jam_data_person_given_name(&numGivenNames, sex, complexity);
    assert(numGivenNames > 0);

    return givenNames[rnd() % numGivenNames];
}

TestJamConstString
test_jam_person_surname_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity) {
    return test_jam::impl::complexity_generator(seed, minComplexity, maxComplexity, test_jam_data_person_surname);
}

TestJamConstStringOptional
test_jam_person_prefix_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity) {
    auto rnd = test_jam::impl::TinyRand(seed);
    if (rnd() % 3 != 1) {
        return {.present = false, .str={.length=0,.data=nullptr}};
    }

    const auto numComplexities = (static_cast<uint64_t>(maxComplexity) - static_cast<uint64_t>(minComplexity) + 1);
    auto complexity = static_cast<TestJamComplexity>(
            (rnd.uint64() % numComplexities) + static_cast<uint64_t>(minComplexity)
    );
    const auto sex = static_cast<TestJamPersonSex>(rnd() % 2);

    size_t numPrefixes;
    const auto* prefixes = test_jam_data_person_prefix(&numPrefixes, sex, complexity);
    assert(numPrefixes > 0);

    return {.present=true,.str=prefixes[rnd() % numPrefixes]};
}

TestJamConstStringOptional
test_jam_person_suffix_complexity(uint64_t seed, TestJamComplexity minComplexity, TestJamComplexity maxComplexity) {
    auto rnd = test_jam::impl::TinyRand(seed);
    if (rnd() % 3 != 1) {
        return {.present = false, .str={.length=0,.data=nullptr}};
    }

    const auto numComplexities = (static_cast<uint64_t>(maxComplexity) - static_cast<uint64_t>(minComplexity) + 1);
    auto complexity = static_cast<TestJamComplexity>(
            (rnd.uint64() % numComplexities) + static_cast<uint64_t>(minComplexity)
    );

    size_t numSuffixes;
    const auto* suffixes = test_jam_data_person_suffix(&numSuffixes, complexity);
    assert(numSuffixes > 0);

    return {.present=true,.str=suffixes[rnd() % numSuffixes]};
}

TestJamPersonSex test_jam_person_sex(uint64_t seed) {
    auto rnd = test_jam::impl::TinyRand(seed);
    return static_cast<TestJamPersonSex>(rnd() % 2);
}

TestJamPerson test_jam_person(uint64_t seed) {
    return test_jam_person_complexity(seed, TEST_JAM_COMPLEXITY_RUDIMENTARY, TEST_JAM_COMPLEXITY_EDGE_CASE);
}

TestJamConstString test_jam_person_given_name(uint64_t seed) {
    return test_jam_person_given_name_complexity(seed, TEST_JAM_COMPLEXITY_RUDIMENTARY, TEST_JAM_COMPLEXITY_EDGE_CASE);
}

TestJamConstString test_jam_person_surname(uint64_t seed) {
    return test_jam_person_surname_complexity(seed, TEST_JAM_COMPLEXITY_RUDIMENTARY, TEST_JAM_COMPLEXITY_EDGE_CASE);
}

TestJamConstStringOptional test_jam_person_prefix(uint64_t seed) {
    return test_jam_person_prefix_complexity(seed, TEST_JAM_COMPLEXITY_RUDIMENTARY, TEST_JAM_COMPLEXITY_EDGE_CASE);
}

TestJamConstStringOptional test_jam_person_suffix(uint64_t seed) {
    return test_jam_person_suffix_complexity(seed, TEST_JAM_COMPLEXITY_RUDIMENTARY, TEST_JAM_COMPLEXITY_EDGE_CASE);
}
