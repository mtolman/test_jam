#include <test-jam/simplifiers/objects.h>

template<typename R, typename T>
static const R* simplifier_next(T* simplifier) {
    if (simplifier->nextState && simplifier->hasValue) {
        simplifier->nextState(simplifier);
    }
    else {
        simplifier->hasValue = false;
    }

    if (simplifier->hasValue) {
        return &simplifier->value;
    }
    return nullptr;
}

const TestJamString *test_jam_string_simplifier_next(TestJamStringSimplifier * simplifier) {
    return simplifier_next<TestJamString>(simplifier);
}

const uint64_t *test_jam_uint64_simplifier_next(TestJamUint64Simplifier * simplifier) {
    return simplifier_next<uint64_t>(simplifier);
}

const uint32_t *test_jam_uint32_simplifier_next(TestJamUint32Simplifier * simplifier) {
    return simplifier_next<uint32_t>(simplifier);
}

const uint16_t *test_jam_uint16_simplifier_next(TestJamUint16Simplifier * simplifier) {
    return simplifier_next<uint16_t>(simplifier);
}

const uint8_t *test_jam_uint8_simplifier_next(TestJamUint8Simplifier * simplifier) {
    return simplifier_next<uint8_t>(simplifier);
}

const int64_t *test_jam_int64_simplifier_next(TestJamInt64Simplifier * simplifier) {
    return simplifier_next<int64_t>(simplifier);
}

const int32_t *test_jam_int32_simplifier_next(TestJamInt32Simplifier * simplifier) {
    return simplifier_next<int32_t>(simplifier);
}

const int16_t *test_jam_int16_simplifier_next(TestJamInt16Simplifier * simplifier) {
    return simplifier_next<int16_t>(simplifier);
}

const int8_t *test_jam_int8_simplifier_next(TestJamInt8Simplifier * simplifier) {
    return simplifier_next<int8_t>(simplifier);
}

const double *test_jam_double_simplifier_next(TestJamDoubleSimplifier * simplifier) {
    return simplifier_next<double>(simplifier);
}

const float *test_jam_float_simplifier_next(TestJamFloatSimplifier * simplifier) {
    return simplifier_next<float>(simplifier);
}

void test_jam_string_simplifier_free(TestJamStringSimplifier * simplifier) {
    if (!simplifier) {
        return;
    }
    if (simplifier->state) {
        simplifier->allocator.free(simplifier->state);
    }
    test_jam_free_string_contents(&simplifier->value, &simplifier->allocator);
    simplifier->hasValue = false;
    simplifier->nextState = nullptr;
    simplifier->state = nullptr;
}
