import csv
import sys
import os
import string
import functools


def c_escape(str=bytes()):
    mp = ""
    for c in str:
        if c == ord('\\'):
            mp += "\\\\"
        elif c == ord('?'):
            mp += "\\?"
        elif c == ord('\''):
            mp += "\\'"
        elif c == ord('"'):
            mp += "\\\""
        elif c == ord('\a'):
            mp += "\\a"
        elif c == ord('\b'):
            mp += "\\b"
        elif c == ord('\f'):
            mp += "\\f"
        elif c == ord('\n'):
            mp += "\\n"
        elif c == ord('\r'):
            mp += "\\r"
        elif c == ord('\t'):
            mp += "\\t"
        elif c == ord('\v'):
            mp += "\\v"
        elif chr(c) in string.printable:
            mp += chr(c)
        else:
            mp += "\\x" + format(c, '02x') + "\" \""
    return mp


def c_str(value=''):
    if value is None:
        return "\"\""
    elif type(value) is bytes:
        return '"' + c_escape(value) + '"'
    else:
        return '"' + c_escape(value.encode('utf8')) + '"'


def c_complexity(value=''):
    return "TEST_JAM_COMPLEXITY_" + value.strip().upper()


def c_sex(value=''):
    return "TEST_JAM_PERSON_SEX_" + value.strip().upper()


def c_len(value=''):
    if value is None:
        return 0
    elif type(value) is bytes:
        return len(value)
    return len(value.encode('utf8'))


def c_bool(value=True):
    if value:
        return 'true'
    return 'false'


def field_range(field=''):
    if '..' in field:
        parts = field.split('..')
        return {"start": parts[0].strip(), "end": parts[1].strip()}
    elif ' ' in field:
        bytes([int(hex, 16) for hex in field.strip().split(' ')])
    else:
        return field


def process_field(field):
    return [field_range(x.strip()) for x in field.split('|')]


def process_bytes(field):
    return bytes([int(h, 16) for h in field.strip().split(' ') if len(h) > 0])


def write_string_entries(csvfile, outfile, methodSuffix, valueColumn, process=False):
    c = 0
    with open(csvfile, 'r', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            c += 1
    outfile.write("\nstatic constexpr auto {method} = std::array<TestJamConstString, {count}>{{".format(method=methodSuffix, count=c))
    with open(csvfile, 'r', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            data = row[valueColumn]
            if process:
                data = process_bytes(data)
            outfile.write(
                """
TestJamConstString{{
    .length={len},
    .data={text}
}},""".format(
                    text=c_str(data),
                    len=c_len(data)
                )
            )
    outfile.write("""
}};

const TestJamConstString *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_aviation_entries(csvfile, outfile, methodSuffix):
    c = 0
    with open(csvfile, 'r', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            c += 1
    outfile.write("\nstatic constexpr auto {method} = std::array<TestJamAviationRecord, {count}>{{".format(method=methodSuffix, count=c))
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            name = row["Name"]
            iata = row["IATA"]
            icao = row["ICAO"]
            outfile.write(
                """
    TestJamAviationRecord{{
        .name=TestJamConstString{{
            .length={nameLen},
            .data={name}
        }},
        .iata=TestJamConstString{{
            .length={iataLen},
            .data={iata}
        }},
        .icao=TestJamConstString{{
            .length={icaoLen},
            .data={icao}
        }},
    }},""".format(
                    nameLen=c_len(name), name=c_str(name),
                    iataLen=c_len(iata), iata=c_str(iata),
                    icaoLen=c_len(icao), icao=c_str(icao)
                )
            )
    outfile.write("""
}};

const TestJamAviationRecord *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_mime_mapping(csvfile, outfile, methodSuffix):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            mime = row["MIME"]
            ext = row["EXTENSION"]
            outfile.write(
                """
    TestJamMimeMapping{{
        .mime=TestJamConstString{{
            .length={mimeLen},
            .data={mime}
        }},
        .extension=TestJamConstString{{
            .length={extLen},
            .data={ext}
        }},
    }},""".format(
                    mimeLen=c_len(mime), mime=c_str(mime),
                    extLen=c_len(ext), ext=c_str(ext)
                )
            )
    outfile.write("""
}};

const TestJamMimeMapping *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_color_entries(csvfile, outfile, methodSuffix):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            name = row["name"]
            hex = row["hex"]
            red = row["red"]
            green = row["green"]
            blue = row["blue"]
            outfile.write(
                """
    TestJamColorNamed{{
        .name=TestJamConstString{{
            .length={nameLen},
            .data={name}
        }},
        .hex=TestJamConstString{{
            .length={hexLen},
            .data={hex}
        }},
        .rgb=TestJamColorRgb{{
            .red={red},
            .green={green},
            .blue={blue}
        }},
    }},""".format(
                    nameLen=c_len(name), name=c_str(name),
                    hexLen=c_len(hex), hex=c_str(hex),
                    red=red, green=green, blue=blue
                )
            )
    outfile.write("""
}};

const TestJamColorNamed *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_string_complexity_entries(csvfile, outfile, methodSuffix, valueColumn):
    complexities = {
        'rudimentary': [],
        'basic': [],
        'intermediate': [],
        'advanced': [],
        'complex': [],
        'edge_case': [],
    }
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            complexity = row['COMPLEXITY'].lower()
            complexities[complexity].append(row[valueColumn])
    for key in complexities:
        if complexities[key]:
            outfile.write("\nstatic constexpr auto {method}_{complexity} = std::array{{".format(
                method=methodSuffix,
                complexity=key
            ))
            for str in complexities[key]:
                outfile.write("""
    TestJamConstString{{
        .length={len},
        .data={str}
    }},""".format(len=c_len(str), str=c_str(str)))
        else:
            outfile.write("\nstatic constexpr auto {method}_{complexity} = std::array<TestJamConstString, 0>{{".format(
                method=methodSuffix,
                complexity=key
            ))
        outfile.write("};")
    outfile.write("""
const TestJamConstString *test_jam_data_{method}(size_t *outNumEntries, TestJamComplexity complexity) {{
    static constexpr auto invalid = std::array<TestJamConstString, 0>{{}};
    switch (complexity) {{
        case TEST_JAM_COMPLEXITY_RUDIMENTARY: {{
            if (outNumEntries != 0) {{
                *outNumEntries = {method}_rudimentary.size();
            }}
            return {method}_rudimentary.data();
        }}
        case TEST_JAM_COMPLEXITY_BASIC: {{
            if (outNumEntries != 0) {{
                *outNumEntries = {method}_basic.size();
            }}
            return {method}_basic.data();
        }}
        case TEST_JAM_COMPLEXITY_INTERMEDIATE: {{
            if (outNumEntries != 0) {{
                *outNumEntries = {method}_intermediate.size();
            }}
            return {method}_intermediate.data();
        }}
        case TEST_JAM_COMPLEXITY_ADVANCED: {{
            if (outNumEntries != 0) {{
                *outNumEntries = {method}_advanced.size();
            }}
            return {method}_advanced.data();
        }}
        case TEST_JAM_COMPLEXITY_COMPLEX: {{
            if (outNumEntries != 0) {{
                *outNumEntries = {method}_complex.size();
            }}
            return {method}_complex.data();
        }}
        case TEST_JAM_COMPLEXITY_EDGE_CASE: {{
            if (outNumEntries != 0) {{
                *outNumEntries = {method}_edge_case.size();
            }}
            return {method}_edge_case.data();
        }}
    }}

    if (outNumEntries != 0) {{
        *outNumEntries = invalid.size();
    }}
    return invalid.data();
}}
""".format(method=methodSuffix))




def write_string_sex_complexity_entries(csvfile, outfile, methodSuffix):
    complexities = {
        'rudimentary': {'male':[],'female':[]},
        'basic': {'male':[],'female':[]},
        'intermediate': {'male':[],'female':[]},
        'advanced': {'male':[],'female':[]},
        'complex': {'male':[],'female':[]},
        'edge_case': {'male':[],'female':[]},
    }
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            complexity = row['COMPLEXITY'].lower()
            sex = row['SEX'].lower()
            complexities[complexity][sex].append(row['NAME'])
    for key in complexities:
        for sex in complexities[key]:
            if complexities[key][sex]:
                outfile.write("\nstatic constexpr auto {method}_{complexity}_{sex} = std::array{{".format(
                    method=methodSuffix,
                    complexity=key,
                    sex=sex
                ))
                for str in complexities[key][sex]:
                    outfile.write("""
        TestJamConstString{{
            .length={len},
            .data={str}
        }},""".format(len=c_len(str), str=c_str(str)))
            else:
                outfile.write("\nstatic constexpr auto {method}_{complexity}_{sex} = std::array<TestJamConstString, 0>{{".format(
                    method=methodSuffix,
                    complexity=key,
                    sex=sex
                ))
            outfile.write("};")

    outfile.write("""
const TestJamConstString *test_jam_data_{method}(size_t *outNumEntries, TestJamPersonSex sex, TestJamComplexity complexity) {{
    static constexpr auto invalid = std::array<TestJamConstString, 0>{{}};
    switch (complexity) {{
        case TEST_JAM_COMPLEXITY_RUDIMENTARY: {{
            if (outNumEntries != 0) {{
                *outNumEntries = sex == TEST_JAM_PERSON_SEX_MALE ? {method}_rudimentary_male.size() : {method}_rudimentary_female.size();
            }}
            return sex == TEST_JAM_PERSON_SEX_MALE ? {method}_rudimentary_male.data() : {method}_rudimentary_female.data();
        }}
        case TEST_JAM_COMPLEXITY_BASIC: {{
            if (outNumEntries != 0) {{
                *outNumEntries = sex == TEST_JAM_PERSON_SEX_MALE ? {method}_basic_male.size() : {method}_basic_female.size();
            }}
            return sex == TEST_JAM_PERSON_SEX_MALE ? {method}_basic_male.data() : {method}_basic_female.data();
        }}
        case TEST_JAM_COMPLEXITY_INTERMEDIATE: {{
            if (outNumEntries != 0) {{
                *outNumEntries = sex == TEST_JAM_PERSON_SEX_MALE ? {method}_intermediate_male.size() : {method}_intermediate_female.size();
            }}
            return sex == TEST_JAM_PERSON_SEX_MALE ? {method}_intermediate_male.data() : {method}_intermediate_female.data();
        }}
        case TEST_JAM_COMPLEXITY_ADVANCED: {{
            if (outNumEntries != 0) {{
                *outNumEntries = sex == TEST_JAM_PERSON_SEX_MALE ? {method}_advanced_male.size() : {method}_advanced_female.size();
            }}
            return sex == TEST_JAM_PERSON_SEX_MALE ? {method}_advanced_male.data() : {method}_advanced_female.data();
        }}
        case TEST_JAM_COMPLEXITY_COMPLEX: {{
            if (outNumEntries != 0) {{
                *outNumEntries = sex == TEST_JAM_PERSON_SEX_MALE ? {method}_complex_male.size() : {method}_complex_female.size();
            }}
            return sex == TEST_JAM_PERSON_SEX_MALE ? {method}_complex_male.data() : {method}_complex_female.data();
        }}
        case TEST_JAM_COMPLEXITY_EDGE_CASE: {{
            if (outNumEntries != 0) {{
                *outNumEntries = sex == TEST_JAM_PERSON_SEX_MALE ? {method}_edge_case_male.size() : {method}_edge_case_female.size();
            }}
            return sex == TEST_JAM_PERSON_SEX_MALE ? {method}_edge_case_male.data() : {method}_edge_case_female.data();
        }}
    }}

    if (outNumEntries != 0) {{
        *outNumEntries = invalid.size();
    }}
    return invalid.data();
}}
""".format(method=methodSuffix))


def write_cc_networks(csvfile, outfile, methodSuffix):
    networks = []
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            id = row["id"]
            name = row["name"]
            starts = process_field(row["starts"])
            lengths = process_field(row["lengths"])
            cvvLen = row["cvvLen"]
            networks.append({
                "id": id,
                "name": name,
                "cvvLen": cvvLen
            })

            outfile.write("\nstatic constexpr auto {method}_starts_{id} = std::array{{".format(
                method=methodSuffix, id=id.lower()
            ))
            for start in starts:
                outfile.write("""
    TestJamConstString{{
        .length={len},
        .data={str}
    }},
""".format(len=c_len(start), str=c_str(start)))
            outfile.write("};")

            outfile.write("\nstatic constexpr auto {method}_lengths_{id} = std::array{{".format(
                method=methodSuffix, id=id.lower()
            ))
            for length in lengths:
                outfile.write("""
    static_cast<int32_t>({l}),
""".format(l=length))
            outfile.write("};")

    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))

    for network in networks:
        id = network["id"]
        name = network["name"]
        cvvLen = network["cvvLen"]
        outfile.write(
            """
TestJamCcNetwork{{
    .id=TestJamConstString{{
        .length={idLen},
        .data={id}
    }},
    .name=TestJamConstString{{
        .length={nameLen},
        .data={name}
    }},
    .starts=TestJamConstStringArray{{
        .size={method}_starts_{idLower}.size(),
        .values={method}_starts_{idLower}.data()
    }},
    .lengths=TestJamI32Array{{
        .size={method}_lengths_{idLower}.size(),
        .values={method}_lengths_{idLower}.data()
    }},
    .cvvLen={cvvLen}
}},""".format(
                method=methodSuffix,
                idLen=c_len(id), id=c_str(id),
                nameLen=c_len(name), name=c_str(name),
                idLower=id.lower(),
                cvvLen=cvvLen
            )
        )

    outfile.write("""
}};

const TestJamCcNetwork *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_currencies(csvfile, outfile, methodSuffix):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            iso = row["ISO Code"]
            name = row["Name"]
            symbol = row["Symbol"]
            outfile.write(
                """
    TestJamCurrency{{
        .isoCode=TestJamConstString{{
            .length={isoLen},
            .data={iso}
        }},
        .name=TestJamConstString{{
            .length={nameLen},
            .data={name}
        }},
        .symbol=TestJamConstString{{
            .length={symbolLen},
            .data={symbol}
        }},
    }},""".format(
                    isoLen=c_len(iso), iso=c_str(iso),
                    nameLen=c_len(name), name=c_str(name),
                    symbolLen=c_len(symbol), symbol=c_str(symbol)
                )
            )
    outfile.write("""
}};

const TestJamCurrency *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def range_start(r):
    if type(r) == dict:
        return int(r['start'], 16)
    return int(r, 16)


def range_end(r):
    if type(r) == dict:
        return int(r['end'], 16)
    return int(r, 16)


def range_str(r):
    start = range_start(r)
    end = range_end(r)
    return c_escape(bytes([x for x in range(start, end + 1)]))


def range_len(r):
    start = range_start(r)
    end = range_end(r)
    return end - start + 1


def write_ascii_charset_entries(csvfile, outfile):
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            group = row['GROUP']
            ranges = process_field(row['CHARACTERS_HEX_RANGE'])
            groupCodeName = group.lower()

            rangeStr = '"' + ''.join([range_str(r) for r in ranges]) + '"'
            len = functools.reduce(lambda x,y: x + y, [range_len(r) for r in ranges])

            outfile.write(f"""
const TestJamAsciiCharSet* test_jam_data_ascii_{groupCodeName}() {{
    static constexpr auto data = TestJamAsciiCharSet{{
        .length={len},
        .chars={rangeStr}
    }};
    return &data;
}}
""")


def write_country_entries(csvfile, outfile):
    methodSuffix = 'world_countries'
    c = 0
    with open(csvfile, 'r', encoding='utf-8') as f:
        reader = csv.DictReader(f)
        for row in reader:
            c += 1
    outfile.write("\nstatic constexpr auto {method} = std::array<TestJamCountry, {count}>{{".format(method=methodSuffix, count=c))
    with open(csvfile, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            name = row['Name']
            genc = row['GENC']
            iso2 = row['ISO Alpha 2']
            iso3 = row['ISO Alpha 3']
            isoNum = row['ISO Numeric']
            stanag = row['Stanag']
            tld = row['Internet']
            outfile.write(
                """
    TestJamCountry{{
        .name=TestJamConstString{{
            .length={nameLen},
            .data={name}
        }},
        .genc=TestJamConstStringOptional{{
            .present={hasGenc},
            .str=TestJamConstString{{
                .length={gencLen},
                .data={genc}
            }}
        }},
        .iso2=TestJamConstStringOptional{{
            .present={hasIso2},
            .str=TestJamConstString{{
                .length={iso2Len},
                .data={iso2}
            }}
        }},
        .iso3=TestJamConstStringOptional{{
            .present={hasIso3},
            .str=TestJamConstString{{
                .length={iso3Len},
                .data={iso3}
            }}
        }},
        .isoNum=TestJamConstStringOptional{{
            .present={hasIsoNum},
            .str=TestJamConstString{{
                .length={isoNumLen},
                .data={isoNum}
            }}
        }},
        .stanag=TestJamConstStringOptional{{
            .present={hasStanag},
            .str=TestJamConstString{{
                .length={stanagLen},
                .data={stanag}
            }}
        }},
        .tld=TestJamConstStringOptional{{
            .present={hasTld},
            .str=TestJamConstString{{
                .length={tldLen},
                .data={tld}
            }}
        }},
    }},""".format(
                    nameLen=c_len(name), name=c_str(name),
                    hasGenc=c_bool(genc is not None and len(genc) > 0), gencLen=c_len(genc), genc=c_str(genc),
                    hasTld=c_bool(tld is not None and len(tld) > 0), tldLen=c_len(tld), tld=c_str(tld),
                    hasStanag=c_bool(stanag is not None and len(stanag) > 0), stanagLen=c_len(stanag), stanag=c_str(stanag),
                    hasIsoNum=c_bool(isoNum is not None and len(isoNum) > 0), isoNumLen=c_len(isoNum), isoNum=c_str(isoNum),
                    hasIso3=c_bool(iso3 is not None and len(iso3) > 0), iso3Len=c_len(iso3), iso3=c_str(iso3),
                    hasIso2=c_bool(iso2 is not None and len(iso2) > 0), iso2Len=c_len(iso2), iso2=c_str(iso2),
                )
            )
    outfile.write("""
}};

const TestJamCountry *test_jam_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


with(open(sys.argv[1], 'w')) as outfile:
    outfile.write("""/*************************************************
    AUTOGENERATED FILE! DO NOT EDIT MANUALLY!
*************************************************/

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <test-jam/data/ascii.h>
#include <test-jam/data/aviation.h>
#include <test-jam/data/color.h>
#include <test-jam/data/computer.h>
#include <test-jam/data/financial.h>
#include <test-jam/data/internet.h>
#include <test-jam/data/malicious.h>
#include <test-jam/data/person.h>
#include <test-jam/data/world.h>
#include <array>

""")
    write_string_entries(os.path.join("..", "data","aviation", "aircraft_types.csv"), outfile, "aviation_aircraft_type", "TYPE")
    write_aviation_entries(os.path.join("..", "data","aviation", "airlines.csv"), outfile, "aviation_airline")
    write_aviation_entries(os.path.join("..", "data","aviation", "airplanes.csv"), outfile, "aviation_airplane")
    write_aviation_entries(os.path.join("..", "data","aviation", "airports.csv"), outfile, "aviation_airport")
    write_color_entries(os.path.join("..", "data","color", "named.csv"), outfile, "color_named")
    write_string_entries(os.path.join("..", "data","computer", "cmake_system_names.csv"), outfile, "computer_cmake_system_name",
                         "NAME")
    write_string_entries(os.path.join("..", "data","computer", "cpu_architectures.csv"), outfile, "computer_cpu_architecture",
                         "ARCH")
    write_string_entries(os.path.join("..", "data","computer", "file_extensions.csv"), outfile, "computer_file_extension", "EXT")
    write_mime_mapping(os.path.join("..", "data","computer", "file_mime_mapping.csv"), outfile, "computer_file_mime_mapping")
    write_string_entries(os.path.join("..", "data","computer", "file_types.csv"), outfile, "computer_file_type", "TYPE")
    write_string_entries(os.path.join("..", "data","computer", "mime_types.csv"), outfile, "computer_mime_type", "TYPE")
    write_string_entries(os.path.join("..", "data","computer", "operating_systems.csv"), outfile, "computer_operating_system", "OS")
    write_string_complexity_entries(os.path.join("..", "data","financial", "account_names.csv"), outfile, "financial_account_name",
                                    "NAME")
    write_cc_networks(os.path.join("..", "data","financial", "cc_networks.csv"), outfile, "financial_cc_network")
    write_currencies(os.path.join("..", "data","financial", "currencies.csv"), outfile, "financial_currency")
    write_string_entries(os.path.join("..", "data","financial", "transaction_types.csv"), outfile, "financial_transaction_type",
                         "TYPE")
    write_ascii_charset_entries(os.path.join("..", "data","text", "ascii_char_groups.csv"), outfile)
    write_country_entries(os.path.join("..", "data","world", "countries.csv"), outfile)
    write_string_entries(os.path.join("..", "data","world", "timezones.csv"), outfile, "world_timezones", "NAME")
    write_string_entries(os.path.join("..", "data","internet", "top_level_domains.csv"), outfile, "internet_top_level_domains", "DOMAIN")
    write_string_entries(os.path.join("..", "data","internet", "url_protocols.csv"), outfile, "internet_url_schemas", "PROTOCOL")
    write_string_entries(os.path.join("..", "data","malicious", "format_injection.csv"), outfile, "malicious_format_injection", "ATTACK")
    write_string_entries(os.path.join("..", "data","malicious", "sql_injection.csv"), outfile, "malicious_sql_injection", "ATTACK")
    write_string_entries(os.path.join("..", "data","malicious", "xss.csv"), outfile, "malicious_xss", "ATTACK")
    write_string_entries(os.path.join("..", "data","malicious", "edge_cases.csv"), outfile, "malicious_edge_cases", "bytes", process=True)
    write_string_sex_complexity_entries(os.path.join("..", "data","person", "given_names.csv"), outfile, "person_given_name")
    write_string_complexity_entries(os.path.join("..", "data","person", "surnames.csv"), outfile, "person_surname", "NAME")
    write_string_sex_complexity_entries(os.path.join("..", "data","person", "prefixes.csv"), outfile, "person_prefix")
    write_string_complexity_entries(os.path.join("..", "data","person", "suffixes.csv"), outfile, "person_suffix", "NAME")
