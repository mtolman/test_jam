#include <stddef.h>
#include <tau/tau.h>
#include <test-jam/generators.h>
#include <float.h>
#include <math.h>
#include <inttypes.h>
extern size_t test_jam_mem_amt_allocated();

TAU_NO_MAIN()

int main(const int argc, const char* const * const argv) {
    int res = tau_main(argc, argv);
#ifndef NDEBUG
    uint64_t allocatedBytes = test_jam_mem_amt_allocated();
    printf("Bytes still allocated: %" PRIu64 "\n", allocatedBytes);
    if (allocatedBytes > 0) {
        printf("Memory Leak Detected! Failing Test!\n");
        return -1;
    }
#endif
    return res;
}

#define TEST_JAM_STR_TEST(S, T, FN) \
TEST(S, T) {                        \
    int i;                          \
    for (i = 0; i < 1000; ++i) { \
        TestJamConstString res = FN(i); \
        CHECK_GT(res.length, 0); \
        CHECK_EQ(strlen(res.data), res.length); \
    }\
}

#define TEST_JAM_STR_OPT_TEST(S, T, FN) \
TEST(S, T) {                        \
    int i;                          \
    for (i = 0; i < 1000; ++i) {        \
        TestJamConstStringOptional res = FN(i); \
        if (res.present) {      \
            CHECK_GT(res.str.length, 0); \
            CHECK_EQ(strlen(res.str.data), res.str.length); \
        }                                \
    }\
}

#define TEST_JAM_STR_CMPLX_TEST(S, T, FN) \
TEST(S, T) {                        \
    int i, c;                          \
    for (i = 0; i < 1000; ++i) {    \
        for (c = TEST_JAM_COMPLEXITY_RUDIMENTARY; c <= TEST_JAM_COMPLEXITY_EDGE_CASE; ++c) { \
            TestJamConstString res = FN(i, c, c);                                            \
            CHECK_GT(res.length, 0); \
            CHECK_EQ(strlen(res.data), res.length); \
        }                            \
    } \
}

#define TEST_JAM_AVIATION_RECORD_TEST(S, T, FN) \
TEST(S, T) { \
    TestJamAviationRecord res = FN(234); \
    CHECK_GT(res.name.length, 0); \
    CHECK_EQ(strlen(res.name.data), res.name.length); \
    CHECK_GT(res.iata.length, 0); \
    CHECK_EQ(strlen(res.iata.data), res.iata.length); \
    CHECK_GT(res.icao.length, 0); \
    CHECK_EQ(strlen(res.icao.data), res.icao.length); \
}

TEST_JAM_STR_TEST(Aviation, AircraftTypes, test_jam_aviation_aircraft_type)

TEST_JAM_AVIATION_RECORD_TEST(Aviation, Airlines, test_jam_aviation_airline)

TEST_JAM_AVIATION_RECORD_TEST(Aviation, Airplanes, test_jam_aviation_airplane)

TEST_JAM_AVIATION_RECORD_TEST(Aviation, Airports, test_jam_aviation_airport)

TEST(Color, Named) {
    TestJamColorNamed res = test_jam_color_named(234);
    CHECK_GT(res.name.length, 0);
    CHECK_EQ(strlen(res.name.data), res.name.length);

    CHECK_GT(res.hex.length, 0);
    CHECK_EQ(strlen(res.hex.data), res.hex.length);
}

TEST(Color, Rgb) {
    const TestJamColorRgb res1 = test_jam_color_rgb(234);
    const TestJamColorRgb res2 = test_jam_color_rgb(235);
    CHECK_NE(res1.red, res2.red);
    CHECK_NE(res1.green, res2.green);
    CHECK_NE(res1.blue, res2.blue);
}

TEST(Color, Rgba) {
    const TestJamColorRgba res1 = test_jam_color_rgba(234);
    const TestJamColorRgba res2 = test_jam_color_rgba(235);
    CHECK_NE(res1.red, res2.red);
    CHECK_NE(res1.green, res2.green);
    CHECK_NE(res1.blue, res2.blue);
    CHECK_NE(res1.alpha, res2.alpha);

    CHECK_EQ(res1.red, 216);
}

TEST(Color, Hsl) {
    const TestJamColorHsl res1 = test_jam_color_hsl(234);
    const TestJamColorHsl res2 = test_jam_color_hsl(235);
    CHECK_NE(res1.hue, res2.hue);
    CHECK_GE(res1.hue, 0);
    CHECK_LE(res1.hue, 360);

    CHECK_NE(res1.luminosity, res2.luminosity);
    CHECK_GE(res1.luminosity, 0);
    CHECK_LE(res1.luminosity, 1);

    CHECK_NE(res1.saturation, res2.saturation);
    CHECK_GE(res1.saturation, 0);
    CHECK_LE(res1.saturation, 1);
}

TEST(Color, Hsla) {
    const TestJamColorHsla res1 = test_jam_color_hsla(234);
    const TestJamColorHsla res2 = test_jam_color_hsla(235);
    CHECK_NE(res1.hue, res2.hue);
    CHECK_GE(res1.hue, 0);
    CHECK_LE(res1.hue, 360);

    CHECK_NE(res1.luminosity, res2.luminosity);
    CHECK_GE(res1.luminosity, 0);
    CHECK_LE(res1.luminosity, 1);

    CHECK_NE(res1.saturation, res2.saturation);
    CHECK_GE(res1.saturation, 0);
    CHECK_LE(res1.saturation, 1);

    CHECK_NE(res1.alpha, res2.alpha);
    CHECK_GE(res1.alpha, 0);
    CHECK_LE(res1.alpha, 1);
}

TEST_JAM_STR_TEST(Computer, CMakeSystemName, test_jam_computer_cmake_system_name)

TEST_JAM_STR_TEST(Computer, CpuArchitecture, test_jam_computer_cpu_architecture)

TEST_JAM_STR_TEST(Computer, FileExtension, test_jam_computer_file_extension)

TEST(Computer, FileMimeMapping) {
    TestJamMimeMapping res = test_jam_computer_file_mime_mapping(234);
    CHECK_GT(res.mime.length, 0);
    CHECK_EQ(strlen(res.mime.data), res.mime.length);
    CHECK_GT(res.extension.length, 0);
    CHECK_EQ(strlen(res.extension.data), res.extension.length);
}

TEST_JAM_STR_TEST(Computer, FileType, test_jam_computer_file_type)

TEST_JAM_STR_TEST(Computer, MimeType, test_jam_computer_mime_type)

TEST_JAM_STR_TEST(Computer, OperatingSystem, test_jam_computer_operating_system)

TEST_JAM_STR_CMPLX_TEST(Financial, AccountNameComplexity, test_jam_financial_account_name_complexity)
TEST_JAM_STR_TEST(Financial, AccountName, test_jam_financial_account_name)

TEST(Financial, CcNetwork) {
    int i, s;
    for (i = 0; i < 1000; ++i) {
        TestJamCcNetwork res = test_jam_financial_cc_network(i);
        CHECK_GT(res.name.length, 0);
        CHECK_EQ(res.name.length, strlen(res.name.data));

        CHECK_GT(res.id.length, 0);
        CHECK_EQ(res.id.length, strlen(res.id.data));

        CHECK_GT(res.starts.size, 0);
        for (s = 0; s < res.starts.size; ++s) {
            CHECK_GT(res.starts.values[s].length, 0);
            CHECK_EQ(res.starts.values[s].length, strlen(res.starts.values[s].data));
        }

        CHECK_GT(res.lengths.size, 0);
        for (s = 0; s < res.lengths.size; ++s) {
            CHECK_GT(res.lengths.values[s], 0);
        }
    }
}

TEST(Financial, Currency) {
    int i, s;
    for (i = 0; i < 1000; ++i) {
        TestJamCurrency res = test_jam_financial_currency(i);
        CHECK_GT(res.name.length, 0);
        CHECK_EQ(res.name.length, strlen(res.name.data));

        CHECK_GT(res.isoCode.length, 0);
        CHECK_EQ(res.isoCode.length, strlen(res.isoCode.data));

        CHECK_EQ(res.symbol.length, strlen(res.symbol.data));
    }
}

TEST_JAM_STR_TEST(Financial, TxType, test_jam_financial_transaction_type)

TEST(Financial, AccountNumber) {
    int i, s;
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();
    TestJamString str;
    char buffer[100];
    char buff5[5];
    for (i = 0; i < 1000; ++i) {
        test_jam_financial_account_number(&str, i, 15, &alloc);
        CHECK_EQ(strlen(str.data), 15);
        CHECK_EQ(str.length, 15);
        CHECK_EQ(test_jam_financial_account_number_buff(buffer, 100, i, 15), 15);
        CHECK_STREQ(buffer, str.data);
        CHECK_EQ(test_jam_financial_account_number_buff(buff5, 5, i, 15), 4);
        buffer[4] = '\0';
        CHECK_STREQ(buff5, buffer);
        test_jam_free_string_contents(&str, &alloc);
    }
}

TEST(Financial, Pin) {
    int i, s;
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();
    TestJamString str;
    char buffer[100];
    char buff3[3];
    for (i = 0; i < 1000; ++i) {
        test_jam_financial_pin(&str, i, 4, &alloc);
        CHECK_EQ(strlen(str.data), 4);
        CHECK_EQ(str.length, 4);
        CHECK_EQ(test_jam_financial_pin_buff(buffer, 100, i, 4), 4);
        CHECK_STREQ(buffer, str.data);
        CHECK_EQ(test_jam_financial_pin_buff(buff3, 3, i, 4), 2);
        buffer[2] = '\0';
        CHECK_STREQ(buff3, buffer);
        test_jam_free_string_contents(&str, &alloc);
    }
}

TEST(Financial, Bic) {
    int i, s;
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();
    TestJamString str;
    char buffer[12];
    char buff5[5];
    for (i = 0; i < 1000; ++i) {
        test_jam_financial_bic(&str, i, false, &alloc);
        CHECK_EQ(strlen(str.data), 8);
        CHECK_EQ(str.length, 8);

        CHECK_EQ(test_jam_financial_bic_buff(buffer, 12, i, false), str.length);
        CHECK_EQ(strlen(buffer), str.length);
        CHECK_STREQ(buffer, str.data);
        CHECK_EQ(test_jam_financial_bic_buff(buff5, 5, i, false), 4);
        buffer[4] = '\0';
        CHECK_STREQ(buff5, buffer);
        test_jam_free_string_contents(&str, &alloc);

        test_jam_financial_bic(&str, i, true, &alloc);
        CHECK_EQ(strlen(str.data), 11);
        CHECK_EQ(str.length, 11);

        CHECK_EQ(test_jam_financial_bic_buff(buffer, 12, i, true), str.length);
        CHECK_EQ(strlen(buffer), str.length);
        CHECK_STREQ(buffer, str.data);
        CHECK_EQ(test_jam_financial_bic_buff(buff5, 5, i, true), 4);
        buffer[4] = '\0';
        CHECK_STREQ(buff5, buffer);
        test_jam_free_string_contents(&str, &alloc);
    }
}

TEST(Financial, MaskedNumber) {
    int i, s;
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();
    TestJamString str;
    char buffer[100];
    for (i = 0; i < 1000; ++i) {
        test_jam_financial_masked_num(&str, i, NULL, &alloc);
        CHECK_EQ(strlen(str.data), 8);
        CHECK_EQ(str.length, strlen(str.data));
        CHECK_EQ(str.length, test_jam_financial_masked_num_buff(buffer, 100, i, NULL));
        CHECK_STREQ(buffer, str.data);
        for (size_t j = 0; j < 4; ++j) {
            CHECK_EQ(str.data[j], '*');
            CHECK_GE(str.data[j + 4], '0');
            CHECK_LE(str.data[j + 4], '9');
        }
        test_jam_free_string_contents(&str, &alloc);
    }
}

TEST(Financial, Amount) {
    int i, s;
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();

    TestJamString str;
    TestJamFinancialAmountOptions opts;
    char buffer[10000];
    opts = test_jam_financial_default_amount_options();
    opts.min = 0;
    opts.max = 1200;

    test_jam_financial_amount(&str, 120, &opts, &alloc);
    CHECK_EQ(test_jam_financial_amount_buff(buffer, 10000, 120, &opts), str.length);
    CHECK_STREQ(buffer, str.data);
    test_jam_free_string_contents(&str, &alloc);

    opts.symbol = "$";
    test_jam_financial_amount(&str, 120, &opts, &alloc);
    CHECK_EQ(test_jam_financial_amount_buff(buffer, 10000, 120, &opts), str.length);
    CHECK_STREQ(buffer, str.data);
    CHECK_EQ(str.data[0], '$');
    test_jam_free_string_contents(&str, &alloc);

    opts.prefixSymbol = false;
    test_jam_financial_amount(&str, 120, &opts, &alloc);
    CHECK_EQ(test_jam_financial_amount_buff(buffer, 10000, 120, &opts), str.length);
    CHECK_STREQ(buffer, str.data);
    CHECK_EQ(str.data[strlen(str.data) - 1], '$');
    test_jam_free_string_contents(&str, &alloc);

    opts.decimalSeparator = ", ";
    test_jam_financial_amount(&str, 120, &opts, &alloc);
    CHECK_EQ(test_jam_financial_amount_buff(buffer, 10000, 120, &opts), str.length);
    CHECK_STREQ(buffer, str.data);
    CHECK_EQ(str.data[strlen(str.data) - 1], '$');
    test_jam_free_string_contents(&str, &alloc);

    opts.decimalPlaces = 1;
    test_jam_financial_amount(&str, 120, &opts, &alloc);
    CHECK_EQ(test_jam_financial_amount_buff(buffer, 10000, 120, &opts), str.length);
    CHECK_STREQ(buffer, str.data);
    CHECK_EQ(str.data[strlen(str.data) - 1], '$');
    test_jam_free_string_contents(&str, &alloc);

    opts.decimalPlaces = 13;
    test_jam_financial_amount(&str, 120, &opts, &alloc);
    CHECK_EQ(test_jam_financial_amount_buff(buffer, 10000, 120, &opts), str.length);
    CHECK_STREQ(buffer, str.data);
    CHECK_EQ(str.data[strlen(str.data) - 1], '$');
    test_jam_free_string_contents(&str, &alloc);

    for (i = 0; i < 1000; ++i) {
        test_jam_financial_amount(&str, i, NULL, &alloc);
        CHECK_GT(strlen(str.data), 0);
        CHECK_EQ(str.length, strlen(str.data));
        test_jam_free_string_contents(&str, &alloc);
    }
}

// Used to verify that our credit card numbers are plausibly corrects
#define LUHN_VALIDATION(card) \
    { \
        size_t sum = 0; \
        int multiplier = 2; \
        for (size_t chIndex = card.number.length - 1; chIndex > 0; --chIndex) { \
            size_t index = chIndex - 1; \
            sum += ((card).number.data[index] - '0') * multiplier; \
            multiplier = multiplier == 1 ? 2 : 1; \
        } \
        const size_t check = (9 - ((sum + 9) % 10)); \
        CHECK_EQ(check, (size_t)((card).number.data[(card).number.length - 1] - '0')); \
    }

TEST(Financial, CreditCard) {
    int i, s;
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();
    TestJamFinancialCcDate start;
    TestJamFinancialCcDate end;
    for (i = 0; i < 1000; ++i) {
        TestJamFinancialCreditCard cc;
        test_jam_financial_credit_card(&cc, i, NULL, NULL, &alloc);
        CHECK_GE(strlen(cc.number.data), 13);
        CHECK_LE(strlen(cc.number.data), 18);
        CHECK_EQ(cc.number.length, strlen(cc.number.data));

        CHECK_GE(strlen(cc.cvv.data), 3);
        CHECK_LE(strlen(cc.cvv.data), 6);
        CHECK_EQ(cc.cvv.length, strlen(cc.cvv.data));

        CHECK_GE(cc.expirationDate.year, 2000);
        CHECK_GE(cc.expirationDate.month, 1);
        CHECK_LE(cc.expirationDate.month, 12);

        LUHN_VALIDATION(cc);
        test_jam_free_financial_credit_card_contents(&cc, &alloc);

        // Check date constraints

        // Check same year
        start.year = 2022;
        start.month = 4;
        end.year = 2022;
        end.month = 6;
        test_jam_financial_credit_card(&cc, i, &start, &end, &alloc);
        CHECK_EQ(cc.expirationDate.year, 2022);
        CHECK_GE(cc.expirationDate.month, 4);
        CHECK_LE(cc.expirationDate.month, 6);
        test_jam_free_financial_credit_card_contents(&cc, &alloc);

        // Check same start end date
        start.year = 2022;
        start.month = 6;
        end.year = 2022;
        end.month = 6;
        test_jam_financial_credit_card(&cc, i, &start, &end, &alloc);
        CHECK_EQ(cc.expirationDate.year, 2022);
        CHECK_EQ(cc.expirationDate.month, 6);
        test_jam_free_financial_credit_card_contents(&cc, &alloc);

        // Check different years
        start.year = 2020;
        start.month = 7;
        end.year = 2024;
        end.month = 4;
        test_jam_financial_credit_card(&cc, i, &start, &end, &alloc);
        CHECK_GE(cc.expirationDate.year, 2020);
        CHECK_LE(cc.expirationDate.year, 2024);

        if (cc.expirationDate.year == 2020) {
            CHECK_GE(cc.expirationDate.month, 7);
        }
        else if (cc.expirationDate.year == 2024) {
            CHECK_LE(cc.expirationDate.month, 4);
        }
        else {
            CHECK_GE(cc.expirationDate.month, 1);
            CHECK_LE(cc.expirationDate.month, 12);
        }
        test_jam_free_financial_credit_card_contents(&cc, &alloc);
    }
}

#define TEST_JAM_NO_NULL_STR_GEN(S, T, F) TEST(S, T) { \
    int i, s;                                          \
    TestJamAllocator alloc = test_jam_mem_malloc_allocator();       \
    TestJamString str;                                 \
    char buff[101];                                    \
    char buff5[5];                                     \
    for (i = 0; i < 1000; ++i) {                       \
        size_t len = F(&str, i, 10, 100, &alloc);      \
        CHECK_GE(len, 10);                             \
        CHECK_LE(len, 100);                            \
        CHECK_EQ(strlen(str.data), len);               \
        size_t blen = F ## _buff(buff, 101, i, 10, 100);              \
        CHECK_STREQ(str.data, buff);                   \
        CHECK_EQ(len, blen);                           \
        size_t b5len = F ## _buff(buff5, 5, i, 10, 100);              \
        CHECK_EQ(b5len, 4);                                               \
        buff[4] = '\0';                                               \
        CHECK_STREQ(buff5, buff);                   \
        test_jam_free_string_contents(&str, &alloc);   \
    }                                                  \
}

#define TEST_JAM_NULL_STR_GEN(S, T, F) TEST(S, T) { \
    int i, s;                                       \
    TestJamAllocator alloc = test_jam_mem_calloc_allocator();    \
    char buff[101];                                 \
    char buff5[5];                                  \
    for (i = 0; i < 1000; ++i) {                    \
        TestJamString str;                          \
        size_t len = F(&str, i, 10, 100, &alloc);   \
        CHECK_GE(len, 10);                          \
        CHECK_LE(len, 100);                         \
        size_t blen = F ## _buff(buff, 101, i, 10, 100);              \
        CHECK_STREQ(str.data, buff);                   \
        CHECK_EQ(len, blen);                           \
        size_t b5len = F ## _buff(buff5, 5, i, 10, 100);              \
        CHECK_EQ(b5len, 4);                                            \
        buff[4] = '\0';                                               \
        CHECK_STREQ(buff5, buff);                   \
        CHECK_EQ(F ## _buff(buff5, 0, i, 10, 100), 0);                \
        CHECK_STREQ(buff5, buff); \
        test_jam_free_string_contents(&str, &alloc);               \
    } \
}

TEST_JAM_NULL_STR_GEN(Ascii, Any, test_jam_ascii_any)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Alpha, test_jam_ascii_alpha)
TEST_JAM_NO_NULL_STR_GEN(Ascii, AlphaLower, test_jam_ascii_alpha_lower)
TEST_JAM_NO_NULL_STR_GEN(Ascii, AlphaUpper, test_jam_ascii_alpha_upper)
TEST_JAM_NO_NULL_STR_GEN(Ascii, AlphaNumeric, test_jam_ascii_alpha_numeric)
TEST_JAM_NO_NULL_STR_GEN(Ascii, AlphaNumericLower, test_jam_ascii_alpha_numeric_lower)
TEST_JAM_NO_NULL_STR_GEN(Ascii, AlphaNumericUpper, test_jam_ascii_alpha_numeric_upper)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Binary, test_jam_ascii_binary)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Bracket, test_jam_ascii_bracket)
TEST_JAM_NULL_STR_GEN(Ascii, Control, test_jam_ascii_control)
TEST_JAM_NO_NULL_STR_GEN(Ascii, DomainPiece, test_jam_ascii_domain_piece)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Hex, test_jam_ascii_hex)
TEST_JAM_NO_NULL_STR_GEN(Ascii, HexLower, test_jam_ascii_hex_lower)
TEST_JAM_NO_NULL_STR_GEN(Ascii, HexUpper, test_jam_ascii_hex_upper)
TEST_JAM_NO_NULL_STR_GEN(Ascii, HtmlNamed, test_jam_ascii_html_named)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Math, test_jam_ascii_math)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Numeric, test_jam_ascii_numeric)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Octal, test_jam_ascii_octal)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Printable, test_jam_ascii_printable)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Quote, test_jam_ascii_quote)
TEST_JAM_NO_NULL_STR_GEN(Ascii, SafePunctuation, test_jam_ascii_safe_punctuation)
TEST_JAM_NO_NULL_STR_GEN(Ascii, SentencePunctuation, test_jam_ascii_sentence_punctuation)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Symbol, test_jam_ascii_symbol)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Text, test_jam_ascii_text)
TEST_JAM_NO_NULL_STR_GEN(Ascii, UrlUnencodedChars, test_jam_ascii_url_unencoded_chars)
TEST_JAM_NO_NULL_STR_GEN(Ascii, UrlUnencodedHashChars, test_jam_ascii_url_unencoded_hash_chars)
TEST_JAM_NO_NULL_STR_GEN(Ascii, Whitespace, test_jam_ascii_whitespace)

#define TEST_JAM_UNSIGNED_INT(TYPE, MAX_TYPE)                                                       \
TEST(Numbers, TYPE) {                                                                               \
    size_t i;                                                                                       \
    srand(1999);                                                                                    \
    for (i = 0; i < 20000; ++i) {                                                                  \
        TYPE ## _t v = rand();                                                                      \
        CHECK_EQ(v, test_jam_numbers_ ## TYPE(v, 0, MAX_TYPE ##_MAX));                              \
    }                                                                                               \
    CHECK_EQ(test_jam_numbers_ ## TYPE(0, 0, MAX_TYPE ## _MAX), 0);                                 \
    CHECK_EQ(test_jam_numbers_ ## TYPE(MAX_TYPE ## _MAX, 0, MAX_TYPE ## _MAX), MAX_TYPE ## _MAX);   \
    CHECK_EQ(test_jam_numbers_ ## TYPE(0, 0, MAX_TYPE ## _MAX - 1), 0);                             \
    CHECK_EQ(test_jam_numbers_ ## TYPE(MAX_TYPE ## _MAX - 1, 1, MAX_TYPE ## _MAX), MAX_TYPE ## _MAX); \
}

#define TEST_JAM_SIGNED_INT(TYPE, MAX_TYPE) \
TEST(Numbers, TYPE) {                                                                                               \
    size_t i;                                                                                                       \
    srand(1999);                                                                                                    \
    for (i = 0; i < 20000; ++i) {                                                                                  \
        TYPE ## _t v = rand();                                                                                      \
        CHECK_GE(test_jam_numbers_ ## TYPE(v, 0, MAX_TYPE ## _MAX), 0);                              \
        CHECK_LE(test_jam_numbers_ ## TYPE(v, MAX_TYPE ## _MIN, 0), 0);                              \
    }                                                                                                               \
}


TEST_JAM_UNSIGNED_INT(uint64, UINT64)
TEST_JAM_SIGNED_INT(int64, INT64)
TEST_JAM_UNSIGNED_INT(uint32, UINT32)
TEST_JAM_SIGNED_INT(int32, INT32)
TEST_JAM_UNSIGNED_INT(uint16, UINT16)
TEST_JAM_SIGNED_INT(int16, INT16)
TEST_JAM_UNSIGNED_INT(uint8, UINT8)
TEST_JAM_SIGNED_INT(int8, INT8)


TEST(Numbers, double) {
    size_t i;
    srand(2999);
    for (i = 0; i < 20000; ++i) {
        double v = test_jam_numbers_double(i, DBL_MIN, DBL_MAX, false, false);
        CHECK_FALSE(isnan(v));
        CHECK_FALSE(isinf(v));

        CHECK_GE(test_jam_numbers_double(i, 0, DBL_MAX, false, false), 0.0 - DBL_EPSILON);
        CHECK_LE(test_jam_numbers_double(i, DBL_MIN, 0, false, false), 0.0 + DBL_EPSILON);
    }

    bool foundPosInfinite = false;
    bool foundNegInfinite = false;
    bool foundNan = false;
    for (i = 0; i < 20000; ++i) {
        double v = test_jam_numbers_double(i, DBL_MIN, DBL_MAX, true, true);
        if (isnan(v)) {
            foundNan = true;
        }
        else if (isinf(v)) {
            if (v > 0) {
                foundPosInfinite = true;
            }
            else {
                foundNegInfinite = true;
            }
        }
    }
    CHECK(foundNan);
    CHECK(foundNegInfinite);
    CHECK(foundPosInfinite);
}

TEST(Numbers, float) {
    size_t i;
    srand(2999);
    for (i = 0; i < 20000; ++i) {
        float v = test_jam_numbers_float(i, FLT_MIN, FLT_MAX, false, false);
        CHECK_FALSE(isnan(v));
        CHECK_FALSE(isinf(v));

        CHECK_GE(test_jam_numbers_float(i, 0, FLT_MAX, false, false), 0.0 - DBL_EPSILON);
        CHECK_LE(test_jam_numbers_float(i, FLT_MIN, 0, false, false), 0.0 + DBL_EPSILON);
    }

    bool foundPosInfinite = false;
    bool foundNegInfinite = false;
    bool foundNan = false;
    for (i = 0; i < 20000; ++i) {
        float v = test_jam_numbers_float(i, FLT_MIN, FLT_MAX, true, true);
        if (isnan(v)) {
            foundNan = true;
        }
        else if (isinf(v)) {
            if (v > 0) {
                foundPosInfinite = true;
            }
            else {
                foundNegInfinite = true;
            }
        }
    }
    CHECK(foundNan);
    CHECK(foundNegInfinite);
    CHECK(foundPosInfinite);
}

TEST(World, Countries) {
    for (size_t i = 0; i < 20000; ++i) {
        TestJamCountry country = test_jam_world_country(i);
        CHECK_GT(strlen(country.name.data), 0);
        CHECK_EQ(country.name.length, strlen(country.name.data));

        if (country.iso2.present) {
            CHECK_GT(strlen(country.iso2.str.data), 0);
            CHECK_EQ(country.iso2.str.length, strlen(country.iso2.str.data));
        }

        if (country.iso3.present) {
            CHECK_GT(strlen(country.iso3.str.data), 0);
            CHECK_EQ(country.iso3.str.length, strlen(country.iso3.str.data));
        }

        if (country.isoNum.present) {
            CHECK_GT(strlen(country.isoNum.str.data), 0);
            CHECK_EQ(country.isoNum.str.length, strlen(country.isoNum.str.data));
        }

        if (country.stanag.present) {
            CHECK_GT(strlen(country.stanag.str.data), 0);
            CHECK_EQ(country.stanag.str.length, strlen(country.stanag.str.data));
        }

        if (country.genc.present) {
            CHECK_GT(strlen(country.genc.str.data), 0);
            CHECK_EQ(country.genc.str.length, strlen(country.genc.str.data));
        }

        if (country.tld.present) {
            CHECK_GT(strlen(country.tld.str.data), 0);
            CHECK_EQ(country.tld.str.length, strlen(country.tld.str.data));
        }
    }
}

TEST_JAM_STR_TEST(Internet, TopLevelDomain, test_jam_internet_top_level_domain)
TEST_JAM_STR_TEST(Internet, UrlSchema, test_jam_internet_url_schema)
TEST(Internet, Ipv4Str) {
    TestJamString str;
    TestJamAllocator alloc = test_jam_mem_malloc_allocator();

    char buffer[100];
    char buff5[5];

    test_jam_internet_ipv4(&str, 19793498019, &alloc);
    test_jam_internet_ipv4_buff(buffer, 100, 19793498019);
    CHECK_STREQ(str.data, buffer);
    test_jam_internet_ipv4_buff(buff5, 5, 19793498019);
    buffer[4] = '\0';
    CHECK_STREQ(buff5, buffer);
    CHECK_EQ(test_jam_internet_ipv4_buff(buff5, 0, 50), 0);
    CHECK_STREQ(buff5, buffer);
    test_jam_free_string_contents(&str, &alloc);

    for (size_t i = 0; i < 20000; ++i) {
        test_jam_internet_ipv4(&str, i, &alloc);

        CHECK_EQ(strlen(str.data), str.length);
        CHECK_GE(str.length, 7);

        test_jam_free_string_contents(&str, &alloc);
    }
}

TEST(Internet, Ipv6Str) {
    TestJamString shrinkStr;
    TestJamString noShrinkStr;
    TestJamAllocator alloc = test_jam_mem_malloc_allocator();
    TestJamIpv6Options noZeroShrink;
    noZeroShrink = test_jam_default_ipv6_options();
    noZeroShrink.flags &= (~TEST_JAM_IPV6_SHRINK_ZEROS);

    char buffer[100];
    char buff5[5];

#define TEST_JAM_IPV6_CASE(SEED, SHRINK, NO_SHRINK) \
    test_jam_internet_ipv6(&shrinkStr, SEED, NULL, &alloc); \
    CHECK_STREQ(shrinkStr.data, SHRINK); \
    CHECK_EQ(strlen(shrinkStr.data), shrinkStr.length); \
    test_jam_internet_ipv6_buff(buffer, 100, SEED, NULL); \
    CHECK_STREQ(buffer, shrinkStr.data); \
    buffer[4] = '\0'; \
    CHECK_EQ(test_jam_internet_ipv6_buff(buff5, 5, SEED, NULL), 4); \
    CHECK_STREQ(buff5, buffer); \
    test_jam_free_string_contents(&shrinkStr, &alloc); \
 \
    test_jam_internet_ipv6(&noShrinkStr, SEED, &noZeroShrink, NULL); \
    CHECK_STREQ(noShrinkStr.data, NO_SHRINK); \
    CHECK_EQ(strlen(noShrinkStr.data), noShrinkStr.length); \
    test_jam_internet_ipv6_buff(buffer, 100, SEED, &noZeroShrink); \
    CHECK_STREQ(buffer, noShrinkStr.data); \
    buffer[4] = '\0'; \
    CHECK_EQ(test_jam_internet_ipv6_buff(buff5, 5, SEED, &noZeroShrink), 4); \
    CHECK_STREQ(buff5, buffer); \
    CHECK_EQ(test_jam_internet_ipv6_buff(buff5, 0, SEED, NULL), 0); \
    CHECK_STREQ(buff5, buffer); \
    test_jam_free_string_contents(&noShrinkStr, NULL);

#define TEST_JAM_IPV6_CASE_SIMPLE(SEED, EXPECTED) TEST_JAM_IPV6_CASE(SEED, EXPECTED, EXPECTED);

    TEST_JAM_IPV6_CASE_SIMPLE(1, "::ffff:87.110.17.126");
    TEST_JAM_IPV6_CASE_SIMPLE(2, "::ffff:140.79.99.109");
    TEST_JAM_IPV6_CASE_SIMPLE(3, "::ffff:49.38.32.76");
    TEST_JAM_IPV6_CASE_SIMPLE(4, "::ffff:148.209.82.79");
    TEST_JAM_IPV6_CASE_SIMPLE(5, "::ffff:245.9.99.162");
    TEST_JAM_IPV6_CASE_SIMPLE(6, "::ffff:149.58.96.136");
    TEST_JAM_IPV6_CASE_SIMPLE(7, "::ffff:215.166.133.228");
    TEST_JAM_IPV6_CASE_SIMPLE(8, "::ffff:157.6.243.39");
    TEST_JAM_IPV6_CASE_SIMPLE(9, "::ffff:46.85.197.22");
    TEST_JAM_IPV6_CASE_SIMPLE(10, "::ffff:194.177.121.199");
    TEST_JAM_IPV6_CASE(11, "::F1AE:A8B5:44D1:A2D0:D4F9:6A38", "::F1AE:A8B5:44D1:A2D0:D4F9:6A38")
    TEST_JAM_IPV6_CASE(12, "C1D1::1C44:907A:10C:8B53:D4E8:55AD", "C1D1::1C44:907A:010C:8B53:D4E8:55AD")
    TEST_JAM_IPV6_CASE(13, "::31FC:FA72:569D:4780:322:B3CB:8D52", "::31FC:FA72:569D:4780:0322:B3CB:8D52");
    TEST_JAM_IPV6_CASE(14, "4FB7:B7DC:4EDE:AFC7:87E8::E04D:DE11", "4FB7:B7DC:4EDE:AFC7:87E8::E04D:DE11");
    TEST_JAM_IPV6_CASE(15, "A728::A004:E925:9FAC:C998:91A4:EB52", "A728::A004:E925:9FAC:C998:91A4:EB52");
    TEST_JAM_IPV6_CASE(16, "BDE6::7482:3D87:4F48", "BDE6::7482:3D87:4F48");
    TEST_JAM_IPV6_CASE(17, "9C7F:2215::E0A2:D189", "9C7F:2215::E0A2:D189");
    TEST_JAM_IPV6_CASE(18, "::D520:186F:C3F8", "::D520:186F:C3F8");
    TEST_JAM_IPV6_CASE(19, "::9485:23:2379:4073:5643:78B9", "::9485:0023:2379:4073:5643:78B9");
    TEST_JAM_IPV6_CASE(20, "1066:DDC6:7A0B:F1A3::AAC5:9CDC", "1066:DDC6:7A0B:F1A3::AAC5:9CDC");

#undef TEST_JAM_IPV6_CASE_SIMPLE
#undef TEST_JAM_IPV6_CASE
}

TEST(Internet, Domain) {
    TestJamString str;
    char buffer[100];
    char buff5[5];

    for (size_t i = 0; i < 20000; ++i) {
        const size_t len = test_jam_internet_domain_buff(buffer, 100, i, NULL);
        const size_t cLen = strlen(buffer);
        CHECK_LE(len, 63);
        CHECK_EQ(len, cLen);
        CHECK_LE(strlen(buffer), 63);
        bool foundPeriod = false;
        for (char* ch = buffer; *ch; ++ch) {
            if (*ch == '.') {
                foundPeriod = true;
                break;
            }
        }
        CHECK(foundPeriod);

        test_jam_internet_domain(&str, i, NULL, NULL);
        const size_t cALen = strlen(str.data);
        CHECK_LE(str.length, 63);
        CHECK_EQ(str.length, cALen);
        CHECK_STREQ(str.data, buffer);
        test_jam_free_string_contents(&str, NULL);

        const size_t b5Len = test_jam_internet_domain_buff(buff5, 5, i, NULL);
        const size_t cB5Len = strlen(buff5);
        CHECK_LE(b5Len, 4);
        CHECK_LE(cB5Len, 4);
        CHECK_EQ(cB5Len, b5Len);
        foundPeriod = false;
        for (char* ch = buffer; *ch; ++ch) {
            if (*ch == '.') {
                foundPeriod = true;
                break;
            }
        }
        CHECK(foundPeriod);
    }
}

TEST(Internet, URL) {
    char buffer[256];
    TestJamString str;
    for (size_t i = 0; i < 20000; ++i) {
        test_jam_internet_url(&str, i, NULL, NULL);
        CHECK_GE(str.length, 20);
        CHECK_EQ(strlen(str.data), str.length);

        const size_t len = test_jam_internet_url_buff(buffer, 256, i, NULL);
        CHECK_GE(len, 20);
        CHECK_EQ(strlen(buffer), len);

        CHECK_STREQ(buffer, str.data);
        test_jam_free_string_contents(&str, NULL);
    }
}

TEST(Internet, Email) {
    TestJamString str;
    char buffer[256];
    char b5[5];
    for (size_t i = 0; i < 20000; ++i) {
        test_jam_internet_email(&str, i, NULL, NULL);
        CHECK_GE(str.length, 10);
        CHECK_EQ(strlen(str.data), str.length);

        const size_t len = test_jam_internet_email_buff(buffer, 256, i, NULL);
        const int cmp = strcmp(buffer, str.data);
        CHECK_STREQ(buffer, str.data);
        test_jam_free_string_contents(&str, NULL);

        const size_t len5 = test_jam_internet_email_buff(b5, 5, i, NULL);
        CHECK_EQ(len5, 4);
        CHECK_EQ(strlen(b5), len5);
    }
}

TEST_JAM_STR_TEST(Malicious, SqlInjection, test_jam_malicious_sql_injection)
TEST_JAM_STR_TEST(Malicious, FormatInjection, test_jam_malicious_format_injection)
TEST_JAM_STR_TEST(Malicious, Xss, test_jam_malicious_xss)
TEST(Malicious, EdgeCases) {
    int i;
    for (i = 0; i < 1000; ++i) {
        // These edge cases are designed to break C, so can't reliably check them past linking
        TestJamConstString res = test_jam_malicious_edge_cases(i);
    }
}

TEST(Person, Person) {
    int i;
    for (i = 0; i < 1000; ++i) {
        TestJamPerson person = test_jam_person(i);
        CHECK_GE(person.sex, TEST_JAM_PERSON_SEX_MALE);
        CHECK_LE(person.sex, TEST_JAM_PERSON_SEX_FEMALE);
        CHECK_EQ(strlen(person.givenName.data), person.givenName.length);
        CHECK_GT(person.givenName.length,0);
        CHECK_EQ(strlen(person.surname.data), person.surname.length);
        CHECK_GT(person.surname.length,0);

        if (person.prefix.present) {
            CHECK_EQ(strlen(person.prefix.str.data), person.prefix.str.length);
            CHECK_GT(person.prefix.str.length, 0);
        }

        if (person.suffix.present) {
            CHECK_EQ(strlen(person.suffix.str.data), person.suffix.str.length);
            CHECK_GT(person.suffix.str.length, 0);
        }
    }
}

TEST_JAM_STR_CMPLX_TEST(Person, GivenNameComplexity, test_jam_person_given_name_complexity)
TEST_JAM_STR_TEST(Person, GivenName, test_jam_person_given_name)

TEST_JAM_STR_CMPLX_TEST(Person, SurnameComplexity, test_jam_person_surname_complexity)
TEST_JAM_STR_TEST(Person, Surname, test_jam_person_surname)
