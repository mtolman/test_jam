#include <stddef.h>
#include <tau/tau.h>
#include <test-jam/simplifiers.h>
#include <test-jam/generators.h>
#include <inttypes.h>
#include <math.h>
extern size_t test_jam_mem_amt_allocated();

TAU_NO_MAIN()

int main(const int argc, const char* const * const argv) {
    int res = tau_main(argc, argv);
#ifndef NDEBUG
    uint64_t allocatedBytes = test_jam_mem_amt_allocated();
    printf("Bytes still allocated: %" PRIu64 "\n", allocatedBytes);
    if (allocatedBytes > 0) {
        printf("Memory Leak Detected! Failing Test!\n");
        return -1;
    }
#endif
    return res;
}

#define TEST_ASCII(SUFFIX) \
TEST(Ascii, SUFFIX) {      \
    TestJamString str;     \
    const uint64_t seed = 1294; \
    const size_t min = 5;  \
    const size_t max = 120;\
    test_jam_ascii_ ## SUFFIX(&str, seed, min, max, NULL); \
                           \
    TestJamStringSimplifier simplifier;                    \
    test_jam_simplifier_ascii_ ## SUFFIX(&simplifier, seed, min, max, NULL); \
    CHECK_STREQ(str.data, simplifier.value.data);          \
    test_jam_free_string_contents(&str, NULL);             \
                           \
    size_t lastLen = simplifier.value.length;              \
    while (simplifier.hasValue) {                          \
        const TestJamString * ptr = test_jam_string_simplifier_next(&simplifier); \
        if (ptr) {         \
            lastLen = ptr->length;                         \
        }                  \
    }                      \
    CHECK_EQ(lastLen, 5);  \
    test_jam_string_simplifier_free(&simplifier);          \
                           \
    for (uint64_t i = 0; i < 10000; ++i) {                 \
        test_jam_ascii_ ## SUFFIX(&str, i, min, max, NULL);\
        test_jam_simplifier_ascii_ ## SUFFIX(&simplifier, i, min, max, NULL);\
        if (simplifier.hasValue) {                         \
            CHECK_STREQ(str.data, simplifier.value.data);  \
            test_jam_free_string_contents(&str, NULL);     \
            lastLen = simplifier.value.length;             \
            while (simplifier.hasValue) {                  \
                const TestJamString * ptr = test_jam_string_simplifier_next(&simplifier); \
                if (ptr) { \
                    lastLen = ptr->length;                 \
                }          \
            }              \
            CHECK_EQ(lastLen, 5);                          \
        }                  \
        else {             \
            test_jam_free_string_contents(&str, NULL);     \
        }                  \
        test_jam_string_simplifier_free(&simplifier);      \
    }                      \
}

TEST_ASCII(any)
TEST_ASCII(alpha)
TEST_ASCII(alpha_lower)
TEST_ASCII(alpha_upper)
TEST_ASCII(alpha_numeric)
TEST_ASCII(alpha_numeric_lower)
TEST_ASCII(alpha_numeric_upper)
TEST_ASCII(binary)
TEST_ASCII(bracket)
TEST_ASCII(control)
TEST_ASCII(domain_piece)
TEST_ASCII(hex)
TEST_ASCII(hex_lower)
TEST_ASCII(hex_upper)
TEST_ASCII(html_named)
TEST_ASCII(math)
TEST_ASCII(numeric)
TEST_ASCII(octal)
TEST_ASCII(printable)
TEST_ASCII(quote)
TEST_ASCII(safe_punctuation)
TEST_ASCII(sentence_punctuation)
TEST_ASCII(symbol)
TEST_ASCII(text)
TEST_ASCII(url_unencoded_chars)
TEST_ASCII(url_unencoded_hash_chars)
TEST_ASCII(whitespace)

TEST(Internet, Ipv6) {
    TestJamString str;
    TestJamStringSimplifier simp;
    for (uint64_t i = 0; i < 20000; ++i) {
        test_jam_internet_ipv6(&str, i, NULL, NULL);
        test_jam_internet_simplifier_ipv6(&simp, i, NULL, NULL);
        CHECK(simp.hasValue);
        CHECK_STREQ(str.data, simp.value.data);
        test_jam_free_string_contents(&str, NULL);

        while (simp.hasValue) {
            const TestJamString * ptr = test_jam_string_simplifier_next(&simp);
            if (ptr) {
                CHECK_GT(ptr->length, 0);
            }
            else {
                CHECK_FALSE(simp.hasValue);
            }
        }
        test_jam_string_simplifier_free(&simp);
    }
}

TEST(Internet, Domain) {
    TestJamString str;
    TestJamStringSimplifier simp;
    for (uint64_t i = 0; i < 10000; ++i) {
        test_jam_internet_domain(&str, i, NULL, NULL);
        test_jam_internet_simplifier_domain(&simp, i, NULL, NULL);
        CHECK_GE(str.length, 3);
        CHECK(simp.hasValue);
        CHECK_STREQ(str.data, simp.value.data);

        size_t lastLen = str.length;

        while (simp.hasValue) {
            const TestJamString * ptr = test_jam_string_simplifier_next(&simp);
            if (ptr) {
                CHECK_GT(ptr->length, 0);
                lastLen = ptr->length;
            }
            else {
                CHECK_FALSE(simp.hasValue);
            }
        }
        if (str.length > 3) {
            CHECK_LT(lastLen, str.length);
        }
        test_jam_free_string_contents(&str, NULL);
        test_jam_string_simplifier_free(&simp);
    }
}

TEST(Internet, Url) {
    TestJamString str;
    TestJamStringSimplifier simp;
    // TODO: Fix performance issues and change max iterations to 10000
    for (uint64_t i = 0; i < 100; ++i) {
        test_jam_internet_url(&str, i, NULL, NULL);
        test_jam_internet_simplifier_url(&simp, i, NULL, NULL);
        CHECK_GE(str.length, 3);
        CHECK(simp.hasValue);
        CHECK_STREQ(str.data, simp.value.data);

        size_t lastLen = str.length;

        while (simp.hasValue) {
            const TestJamString * ptr = test_jam_string_simplifier_next(&simp);
            if (ptr) {
                CHECK_GT(ptr->length, 0);
                lastLen = ptr->length;
            }
            else {
                CHECK_FALSE(simp.hasValue);
            }
        }
        if (str.length > 20) {
            CHECK_LT(lastLen, str.length);
        }
        test_jam_free_string_contents(&str, NULL);
        test_jam_string_simplifier_free(&simp);
    }
}

TEST(Internet, Email) {
    TestJamString str;
    TestJamStringSimplifier simp;
    for (uint64_t i = 0; i < 10000; ++i) {
        test_jam_internet_email(&str, i, NULL, NULL);
        test_jam_internet_simplifier_email(&simp, i, NULL, NULL);
        CHECK_GE(str.length, 3);
        CHECK(simp.hasValue);
        CHECK_STREQ(str.data, simp.value.data);

        size_t lastLen = str.length;

        while (simp.hasValue) {
            const TestJamString * ptr = test_jam_string_simplifier_next(&simp);
            if (ptr) {
                CHECK_GT(ptr->length, 0);
                lastLen = ptr->length;
            }
            else {
                CHECK_FALSE(simp.hasValue);
            }
        }
        if (str.length > 10) {
            CHECK_LT(lastLen, str.length);
        }
        test_jam_free_string_contents(&str, NULL);
        test_jam_string_simplifier_free(&simp);
    }
}

TEST(Numbers, Unsigned) {
    const size_t min = 0;
    const size_t max = 255;

    TestJamUint64Simplifier u64s;
    TestJamUint32Simplifier u32s;
    TestJamUint16Simplifier u16s;
    TestJamUint8Simplifier u8s;
    for (uint64_t i = 0; i < 1000; ++i) {
        uint64_t u64 = test_jam_numbers_uint64(i, min, max);
        uint32_t u32 = test_jam_numbers_uint32(i, min, max);
        uint16_t u16 = test_jam_numbers_uint16(i, min, max);
        uint8_t u8 = test_jam_numbers_uint8(i, min, max);

        test_jam_numbers_simplifier_uint64(&u64s, i, min, max);
        test_jam_numbers_simplifier_uint32(&u32s, i, min, max);
        test_jam_numbers_simplifier_uint16(&u16s, i, min, max);
        test_jam_numbers_simplifier_uint8(&u8s, i, min, max);

        CHECK_EQ(u64s.value, u64);
        CHECK_EQ(u32s.value, u32);
        CHECK_EQ(u16s.value, u16);
        CHECK_EQ(u8s.value, u8);

        while (u64s.hasValue) {
            const uint64_t * ptr = test_jam_uint64_simplifier_next(&u64s);
            if (ptr) {
                u64 = *ptr;
            }
        }
        CHECK_EQ(u64, min);

        while (u32s.hasValue) {
            const uint32_t * ptr = test_jam_uint32_simplifier_next(&u32s);
            if (ptr) {
                u32 = *ptr;
            }
        }
        CHECK_EQ(u32, min);

        while (u16s.hasValue) {
            const uint16_t * ptr = test_jam_uint16_simplifier_next(&u16s);
            if (ptr) {
                u16 = *ptr;
            }
        }
        CHECK_EQ(u16, min);

        while (u8s.hasValue) {
            const uint8_t * ptr = test_jam_uint8_simplifier_next(&u8s);
            if (ptr) {
                u8 = *ptr;
            }
        }
        CHECK_EQ(u8, min);
    }
}

TEST(Numbers, Signed) {
    const int64_t mins[] = {0, -128, -128};
    const int64_t maxes[] = {127, 127, -55};
    const int64_t expecteds[] = {0, 0, -55};

    TestJamInt64Simplifier i64s;
    TestJamInt32Simplifier i32s;
    TestJamInt16Simplifier i16s;
    TestJamInt8Simplifier i8s;

    for (size_t j = 0; j < 3; ++j) {
        const int64_t min = mins[j];
        const int64_t max = maxes[j];
        const int64_t expected = expecteds[j];

        for (uint64_t i = 0; i < 1000; ++i) {
            int64_t i64 = test_jam_numbers_int64(i, min, max);
            int32_t i32 = test_jam_numbers_int32(i, min, max);
            int16_t i16 = test_jam_numbers_int16(i, min, max);
            int8_t i8 = test_jam_numbers_int8(i, min, max);

            test_jam_numbers_simplifier_int64(&i64s, i, min, max);
            test_jam_numbers_simplifier_int32(&i32s, i, min, max);
            test_jam_numbers_simplifier_int16(&i16s, i, min, max);
            test_jam_numbers_simplifier_int8(&i8s, i, min, max);

            CHECK_EQ(i64s.value, i64);
            CHECK_EQ(i32s.value, i32);
            CHECK_EQ(i16s.value, i16);
            CHECK_EQ(i8s.value, i8);

            const int64_t s = i64;

            while (i64s.hasValue) {
                const int64_t *ptr = test_jam_int64_simplifier_next(&i64s);
                if (ptr) {
                    i64 = *ptr;
                }
            }

            CHECK_EQ(i64, expected);

            while (i32s.hasValue) {
                const int32_t *ptr = test_jam_int32_simplifier_next(&i32s);
                if (ptr) {
                    i32 = *ptr;
                }
            }
            CHECK_EQ(i32, expected);

            while (i16s.hasValue) {
                const int16_t *ptr = test_jam_int16_simplifier_next(&i16s);
                if (ptr) {
                    i16 = *ptr;
                }
            }
            CHECK_EQ(i16, expected);

            while (i8s.hasValue) {
                const int8_t *ptr = test_jam_int8_simplifier_next(&i8s);
                if (ptr) {
                    i8 = *ptr;
                }
            }
            CHECK_EQ(i8, expected);
        }
    }
}

TEST(Numbers, Fp) {
    const double mins[] = {0, -128, -128};
    const double maxes[] = {127, 127, -55};

    TestJamDoubleSimplifier ds;
    TestJamFloatSimplifier fs;

    for (size_t j = 0; j < 3; ++j) {
        const double min = mins[j];
        const double max = maxes[j];

        for (uint64_t i = 0; i < 1000; ++i) {
            double d = test_jam_numbers_double(i, min, max, false, false);
            float f = test_jam_numbers_float(i, min, max, false, false);

            double sd = d;
            float sf = f;

            test_jam_numbers_simplifier_double(&ds, i, min, max, false, false);
            test_jam_numbers_simplifier_float(&fs, i, min, max, false, false);

            CHECK_EQ(ds.value, d);
            CHECK_EQ(fs.value, f);

            while (ds.hasValue) {
                const double *ptr = test_jam_double_simplifier_next(&ds);
                if (ptr) {
                    d = *ptr;
                }
            }

            CHECK_LE(fabs(d), fabs(sd) + 0.001);

            while (fs.hasValue) {
                const float *ptr = test_jam_float_simplifier_next(&fs);
                if (ptr) {
                    f = *ptr;
                }
            }
            CHECK_LE(fabsf(f), fabsf(sf) + 0.001);
        }
    }
}
