const wasm = require('test-jam-wasm')
const ts = require('test-jam-ts')
const b = require('benny')

const doEncode = false
const do1000 = false

const enc = doEncode ? new TextEncoder() : {encode: s => s}

// b.suite(
//     'Number',
//
//     b.add('TypeScript', () => {
//         (new ts.number.Floats()).create(Math.floor(Math.random() * 1000))
//     }),
//
//     b.add('WASM', () => {
//         wasm.numbers.number().create(Math.floor(Math.random() * 1000))
//     }),
//
//     b.cycle(),
//     b.complete(),
//     b.save({ file: 'num', version: '1.0.0' }),
//     b.save({ file: 'num', format: 'chart.html' }),
// )

if (do1000) {
    b.suite(
        'Number x1000',

        b.add('TypeScript', () => {
            const gen = (new ts.number.Floats())
            for (let i = 0; i < 1000; ++i) {
                gen.create(Math.floor(Math.random() * 1000))
            }
        }),

        b.add('WASM', () => {
            const gen = wasm.numbers.number()
            for (let i = 0; i < 1000; ++i) {
                gen.create(Math.floor(Math.random() * 1000))
            }
        }),

        b.cycle(),
        b.complete(),
        b.save({file: 'num1000', version: '1.0.0'}),
        b.save({file: 'num1000', format: 'chart.html'}),
    )
}

// b.suite(
//     'URL',
//
//     b.add('TypeScript', () => {
//         enc.encode((new ts.internet.Urls()).create(Math.floor(Math.random() * 1000)))
//     }),
//
//     b.add('WASM', () => {
//         enc.encode(wasm.internet.url().create(Math.floor(Math.random() * 1000)))
//     }),
//
//     b.cycle(),
//     b.complete(),
//     b.save({ file: 'url', version: '1.0.0' }),
//     b.save({ file: 'url', format: 'chart.html' }),
// )

if (do1000) {
    b.suite(
        'URL x1000',

        b.add('TypeScript', () => {
            const g = new ts.internet.Urls()
            for (let i = 0; i < 1000; ++i) {
                enc.encode(g.create(i))
            }
        }),

        b.add('WASM', () => {
            const g = wasm.internet.url()
            for (let i = 0; i < 1000; ++i) {
                enc.encode(g.create(i))
            }
        }),

        b.cycle(),
        b.complete(),
        b.save({file: 'url1000', version: '1.0.0'}),
        b.save({file: 'url1000', format: 'chart.html'}),
    )
}

b.suite(
    'ASCII ANY',

    b.add('TypeScript', () => {
        enc.encode((new ts.ascii.AsciiGenerator({characterSet: ts.ascii.charsets.any})).create(Math.floor(Math.random() * 1000)))
    }),

    b.add('WASM', () => {
        enc.encode(wasm.ascii.str({charset: wasm.ascii.charsets.any}).create(Math.floor(Math.random() * 1000)))
    }),

    b.cycle(),
    b.complete(),
    b.save({ file: 'ascii_any', version: '1.0.0' }),
    b.save({ file: 'ascii_any', format: 'chart.html' }),
)

if (do1000) {
    b.suite(
        'ASCII ANY x1000',

        b.add('TypeScript', () => {
            const g = new ts.ascii.AsciiGenerator({characterSet: ts.ascii.charsets.any})
            for (let i = 0; i < 1000; ++i) {
                enc.encode(g.create(i))
            }
        }),

        b.add('WASM', () => {
            const g = wasm.ascii.str({charset: wasm.ascii.charsets.any})
            for (let i = 0; i < 1000; ++i) {
                enc.encode(g.create(i))
            }
        }),

        b.cycle(),
        b.complete(),
        b.save({file: 'ascii_any1000', version: '1.0.0'}),
        b.save({file: 'ascii_any1000', format: 'chart.html'}),
    )
}
