cmake_minimum_required(VERSION 3.25)
project(test_jam_js_bench)

find_program(NPM npm REQUIRED)

find_package(Python3 COMPONENTS Interpreter REQUIRED)

execute_process(COMMAND ${NPM} install
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

add_custom_target(
        ${PROJECT_NAME} ALL
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        COMMAND ${NPM} run build
        DEPENDS test_jam_js test_jam_embind_js
)

if (test_jam_BUILD_TESTS)
    add_test(NAME ${PROJECT_NAME}-unit
            COMMAND ${NPM} run --prefix ${CMAKE_CURRENT_SOURCE_DIR} test
            )
    set_tests_properties(${PROJECT_NAME}-unit PROPERTIES FIXTURES_SETUP ${PROJECT_NAME}_js)
endif()